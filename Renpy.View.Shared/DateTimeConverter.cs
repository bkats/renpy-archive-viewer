﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Renpy.View.Shared
{
    public class DateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime dt)
            {
                if (parameter is string format)
                {
                    return dt.ToString(format);
                }
                return dt.ToString("g");
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
