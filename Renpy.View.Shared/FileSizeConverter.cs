﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Renpy.View.Shared
{
    public class FileSizeConverter : IValueConverter
    {
        private static readonly string[] Units = { "B", "kB", "MB", "GB", "TB" };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double v = (long)value;
            int unit = 0;

            if (v < 1)
            {
                return "";
            }

            while (v > 1024)
            {
                v /= 1024;
                unit++;
            }

            if (unit > 0)
            {
                if (v < 10)
                {
                    return $"{v:F2}{Units[unit]}";
                }
                if (v < 100)
                {
                    return $"{v:F1}{Units[unit]}";
                }
                if (v < 1000)
                {
                    return $"{v:F0}{Units[unit]}";
                }
            }
            return $"{v:F0}{Units[unit]}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
