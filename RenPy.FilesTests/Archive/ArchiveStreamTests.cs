﻿using Moq;
using Xunit;

namespace RenPy.Files.Archive.Tests
{
    public class ArchiveStreamTests
    {
        [Fact()]
        public void ArchiveStreamTest()
        {
            // Setup
            var entry = new Mock<IArchiveEntry>();
            var archive = new Mock<IArchiveReader>();

            entry.SetupGet(x => x.Size).Returns(123);

            // Act
            var rd = new ArchiveStream(archive.Object, entry.Object);

            // Assert
            Assert.Equal(123, rd.Length);
            Assert.Equal(0, rd.Position);
            Assert.True(rd.CanSeek);
            Assert.True(rd.CanRead);
            Assert.False(rd.CanWrite);
        }

        [Fact()]
        public void FlushTest()
        {
            // Note: Flush doesn't do anything

            // Setup
            var entry = new Mock<IArchiveEntry>();
            var archive = new Mock<IArchiveReader>();

            var rd = new ArchiveStream(archive.Object, entry.Object);

            // Act/Assert
            rd.Flush();
        }

        [Fact()]
        public void ReadTest()
        {
            // Setup
            var entry = new Mock<IArchiveEntry>();
            var archive = new Mock<IArchiveReader>();

            entry.SetupGet(x => x.Size).Returns(65);
            entry.SetupGet(x => x.Offset).Returns(13);
            entry.SetupGet(x => x.Prefix).Returns(Array.Empty<byte>());

            var data = new byte[5];
            archive.Setup(x => x.ReadInternal(13, data, 0, 5))
                .Callback<long, byte[], int, int>((r, d, o, s) => d[o] = 0xAA)
                .Returns(5).Verifiable();

            var rd = new ArchiveStream(archive.Object, entry.Object);

            var read = new byte[5];

            // Act
            int r = rd.Read(read, 0, 5);

            // Assert
            archive.Verify();
            Assert.Equal(5, rd.Position);
            Assert.Equal(5, r);
            Assert.Equal(0xAA, read[0]);
        }

        [Fact()]
        public void ReadPrefixTest()
        {
            // Setup
            var entry = new Mock<IArchiveEntry>();
            var archive = new Mock<IArchiveReader>();

            entry.SetupGet(x => x.Size).Returns(65);
            entry.SetupGet(x => x.Offset).Returns(23);
            entry.SetupGet(x => x.Prefix).Returns(new byte[] { 0x55 });

            var data = new byte[] { 0x55, 0, 0, 0, 0 };
            archive.Setup(x => x.ReadInternal(23, data, 1, 4))
                .Callback<long, byte[], int, int>((r, d, o, s) => d[o] = 0xAA)
                .Returns(4).Verifiable();

            var rd = new ArchiveStream(archive.Object, entry.Object);

            var read = new byte[5];

            // Act
            int r = rd.Read(read, 0, 5);

            // Assert
            archive.Verify();
            Assert.Equal(5, rd.Position);
            Assert.Equal(5, r);
            Assert.Equal(0x55, read[0]);
            Assert.Equal(0xAA, read[1]);
        }

        [Theory()]
        [InlineData(45, 55, 45)]
        [InlineData(55, 35, 35)]
        [InlineData(5, 0, 0)]
        [InlineData(1, 1, 1)]
        [InlineData(0, 1, 0)]
        public void SeekTest(int size, int newPos, int expect)
        {
            // Setup
            var entry = new Mock<IArchiveEntry>();
            var archive = new Mock<IArchiveReader>();

            entry.SetupGet(x => x.Size).Returns(size);

            var rd = new ArchiveStream(archive.Object, entry.Object);

            // Act
            rd.Seek(newPos, SeekOrigin.Current);

            // Assert
            Assert.Equal(expect, rd.Position);
        }

        [Fact()]
        public void SetLengthTest()
        {
            // Note: SetLength doesn't do anything so length shouldn't change

            // Setup
            var entry = new Mock<IArchiveEntry>();
            var archive = new Mock<IArchiveReader>();

            entry.SetupGet(x => x.Size).Returns(123);
            var rd = new ArchiveStream(archive.Object, entry.Object);

            // Act
            rd.SetLength(5);

            // Assert
            Assert.Equal(123, rd.Length);
        }

        [Fact()]
        public void WriteTest()
        {
            // Setup
            var entry = new Mock<IArchiveEntry>();
            var archive = new Mock<IArchiveReader>();

            var rd = new ArchiveStream(archive.Object, entry.Object);

            // Act/Assert
            Assert.Throws<IOException>(() => rd.Write(new byte[1]));
        }
    }
}