﻿using RenPy.Files.Shared.Attributes;
using System.Collections;
using Xunit;

#pragma warning disable CS8602 // Dereference of a possibly null reference. Assert.NotNull doesn't tell that further access are null

namespace RenPy.Files.Shared.Tests
{
    public class PythonPickleBaseClassTests
    {
        internal class SetStateTest : IPythonSetState
        {
            internal string? Test { get; private set; }
            public void SetState(object state)
            {
                Test = state.ToString() ?? "None";
            }
        }

        internal class TestClass : PythonPickleBaseClass
        {
            internal class TwoField
            {
                internal string F1 { get; }
                internal string F2 { get; }

                public TwoField(string f1, string f2)
                {
                    F1 = f1;
                    F2 = f2;
                }
            }

            [RenPyMultiField("1", "2")]
            public TwoField? MultiField { get; set; }

            [RenPyTagsProperty('_')]
            public string? Name { get; private set; }

            public SetStateTest? State { get; private set; }

            [RenPyFieldName("bool")]
            public bool Boolean { get; protected set; }

            public string Code { get; protected set; } = string.Empty;
        }

        [Fact()]
        public void MultiFieldPropertyTest()
        {
            var state = new Hashtable() { { "1", "A" }, { "2", "b" } };

            var test = new TestClass();
            test.SetState(state);

            Assert.NotNull(test.MultiField);
            Assert.Null(test.Name);
            Assert.Equal("A", test.MultiField.F1);
            Assert.Equal("b", test.MultiField.F2);
        }

        [Fact()]
        public void TagsPropertyMultipleTest()
        {
            var state = new Hashtable() { { "name", new object[] { "q", "5", 13 } } };

            var test = new TestClass();
            test.SetState(state);

            Assert.NotNull(test.Name);
            Assert.Equal("q_5_13", test.Name);
        }

        [Fact()]
        public void TagsPropertySingleTest()
        {
            var state = new Hashtable() { { "name", "one" } };

            var test = new TestClass();
            test.SetState(state);

            Assert.NotNull(test.Name);
            Assert.Null(test.MultiField);
            Assert.Equal("one", test.Name);
        }

        [Fact()]
        public void SetStateSubClassTest()
        {
            var state = new Hashtable() { { "state", "two and a half" } };

            var test = new TestClass();
            test.SetState(state);

            Assert.NotNull(test.State);
            Assert.NotNull(test.State.Test);
            Assert.Equal("two and a half", test.State.Test);
        }

        [Fact()]
        public void FieldNameTest()
        {
            var state = new Hashtable() { { "bool", true } };

            var test = new TestClass();
            test.SetState(state);

            Assert.True(test.Boolean, "Field name message failed");
        }

        //[Fact()]
        //public void FieldCodePyExprTest()
        //{
        //    var state = new Hashtable() { { "code", new PyExpr("my code", "pickle.bas", 123) } };

        //    var test = new TestClass();
        //    test.SetState(state);

        //    Assert.Equal("my code", test.Code);
        //}
    }
}