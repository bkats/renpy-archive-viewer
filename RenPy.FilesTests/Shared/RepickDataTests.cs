﻿using System.Collections;
using Xunit;

namespace RenPy.Files.Shared.Tests
{
    public class RepickDataTests
    {
        [Fact()]
        public void TransformValueStringTest()
        {
            object input = "Testje";
            RepickData.TransformValue(input, typeof(string), out object? test);

            Assert.NotNull(test);
            Assert.IsType<string>(test);
            Assert.Equal(input, test);
        }

        [Fact()]
        public void TransformValueListTest()
        {
            var al = new ArrayList() { "test", "value" };
            RepickData.TransformValue(al, typeof(IReadOnlyList<string>), out object? test);

            Assert.NotNull(test);
            Assert.IsType<List<string>>(test);
            if (test is List<string> l)
            {
                Assert.Equal(2, l.Count);
                Assert.Contains(l, x => x == "test");
                Assert.Contains(l, x => x == "value");
            }
        }

        [Fact()]
        public void TransformValueListNullTest()
        {
            var al = new ArrayList() { "notnull", null };
            RepickData.TransformValue(al, typeof(IReadOnlyList<string>), out object? test, true);

            Assert.NotNull(test);
            Assert.IsType<List<string?>>(test);
            if (test is List<string?> l)
            {
                Assert.Equal(2, l.Count);
                Assert.Contains(l, x => x == "notnull");
                Assert.Contains(l, x => x == null);
            }
        }

        [Fact()]
        public void TransformValueDictionaryFromListTest()
        {
            var al = new ArrayList() { new object[] { "test", "value" } };
            RepickData.TransformValue(al, typeof(IReadOnlyDictionary<string, object>), out object? test);

            Assert.NotNull(test);
            Assert.IsType<Dictionary<string, object>>(test);
            if (test is Dictionary<string, object> d)
            {
                Assert.Contains(d.Keys, x => x == "test");
            }
        }

        [Fact()]
        public void TransformValueDictionaryFromIDictionaryTest()
        {
            var al = new Hashtable() { { "test", "value" } };
            RepickData.TransformValue(al, typeof(IReadOnlyDictionary<string, object>), out object? test);

            Assert.NotNull(test);
            Assert.IsType<Dictionary<string, object>>(test);
            if (test is Dictionary<string, object> d)
            {
                Assert.Contains(d.Keys, x => x == "test");
            }
        }

        //[Fact()]
        //public void TransformValuePyExprFloatTest()
        //{
        //    var expr = new PyExpr("1.0", "test.cs", 13);
        //    RepickData.TransformValue(expr, typeof(float), out object? test);

        //    Assert.NotNull(test);
        //    Assert.IsType<float>(test);
        //    if (test is float f)
        //    {
        //        Assert.Equal(1.0f, f);
        //    }
        //}

        //[Fact()]
        //public void TransformValuePyExprStringTest()
        //{
        //    var expr = new PyExpr("\"Az\"", "test1.cs", 21);
        //    RepickData.TransformValue(expr, typeof(string), out object? test);

        //    Assert.NotNull(test);
        //    Assert.IsType<string>(test);
        //    if (test is string s)
        //    {
        //        Assert.Equal("\"Az\"", s);
        //    }
        //}
    }
}