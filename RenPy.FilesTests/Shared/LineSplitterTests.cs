﻿using Xunit;

namespace RenPy.Files.Shared.Tests
{
    public class LineSplitterTests
    {
        [InlineData("Single Line", new[] { "Single Line" })]
        [InlineData("Still Single Line\n", new[] { "Still Single Line" })]
        [InlineData("First Line\nSecond Line", new[] { "First Line", "Second Line" })]
        [InlineData("First Line\r\nSecond Line", new[] { "First Line", "Second Line" })]
        [InlineData("First Line\n\rSecond Line", new[] { "First Line", "Second Line" })]
        [InlineData("First     \n  Second Line", new[] { "First", "  Second Line" })]
        [InlineData("First  \n \n \n  Third Line", new[] { "First", "", "  Third Line" })]
        [InlineData("\nNext Line", new[] { "Next Line" })]
        [Theory()]
        public void GetLinesTest(string input, string[] expected)
        {
            var lines = LineSplitter.GetLines(input);
            Assert.Equal(lines, expected);
        }
    }
}
