﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class TestNodeDummy : Node
    {
        private readonly string test;
        public TestNodeDummy(string t)
        {
            test = t;
        }
        public override void BuildTextView(TextBuilder output)
        {
            output.AppendLine(test);
        }
    }
}