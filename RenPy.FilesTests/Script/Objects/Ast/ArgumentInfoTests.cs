﻿using Moq;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class ArgumentInfoTests
    {
        [Fact()]
        public void BuildText2ArgsAndExtraTest()
        {
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { null, "25" },
                    new object?[] { "state", "rare" }
                } },
                { "extrakw", "kw" },
                { "extrapos", "pos" },
            };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("(25, state = rare, *pos, **kw)\r\n", txt);
        }

        [Fact()]
        public void BuildText2ArgsTest()
        {
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { null, "25" },
                    new object?[] { "state", "rare" }
                } },
            };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("(25, state = rare)\r\n", txt);
        }

        [Fact()]
        public void BuildTextStarredArgsTest()
        {
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { null, "13" },
                    new object?[] { null, "single" },
                    new object?[] { null, "double" },
                    new object?[] { null, "12" },
                } },
                { "starred_indexes", new ArrayList() { 1 } },
                { "doublestarred_indexes", new ArrayList() { 2 } },
            };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("(13, *single, **double, 12)\r\n", txt);
        }

        [Fact()]
        public void BuildTextNoArgsAndExtraTest()
        {
            var state = new Hashtable() {
                { "extrakw", "kw" },
                { "extrapos", "pos" },
            };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("(*pos, **kw)\r\n", txt);
        }

        [Fact()]
        public void BuildTextNoArgs()
        {
            var state = new Hashtable();
            var arg = new ArgumentInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("()\r\n", txt);
        }

        [Fact()]
        public void GetCodeTest()
        {
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { "name", "123" }
                } },
                { "extrakw", "text" },
                { "extrapos", "dummy" },
            };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            var txt = arg.GetCode();
            Assert.Equal("(name=123, *dummy, **text)", txt);
        }

        [Fact()]
        public void GetCodeStarredTest()
        {
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { null, "1" },
                    new object?[] { null, "double" },
                    new object?[] { null, "single" },
                    new object?[] { "name", "123" },
                } },
                { "starred_indexes", new ArrayList() { 2 } },
                { "doublestarred_indexes", new ArrayList() { 1 } },
            };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            var txt = arg.GetCode();
            Assert.Equal("(1, **double, *single, name=123)", txt);
        }

        [Fact()]
        public void BuildTextTranslateValueTest()
        {
            // Setup
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { null, "translate1" },
                    new object?[] { "state", "translate2" }
                } },
            };
            var arg = new ArgumentInfo();
            arg.SetState(state);

            var translate = new Mock<ITranslateInfo>();
            translate.Setup(x => x.TranslateFunctions("translate1")).Returns("first").Verifiable();
            translate.Setup(x => x.TranslateFunctions("translate2")).Returns("second").Verifiable();

            // Act
            var tb = new TextBuilder(new[] { translate.Object });
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();

            // Assert
            translate.Verify();
            Assert.Equal("(first, state = second)\r\n", txt);
        }
    }
}