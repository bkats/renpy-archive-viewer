﻿using Moq;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class ParameterInfoTests
    {
        [Fact()]
        public void BuildText2ParaAndExtraTest()
        {
            var state = new Hashtable() {
                { "parameters", new ArrayList() {
                    new object?[] { "input", null },
                    new object?[] { "leaving", "true" }
                } },
                { "extrakw", "this" },
                { "extrapos", "that" },
            };
            var arg = new ParameterInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("(input, leaving = true, *that, **this)\r\n", txt);
        }

        [Fact()]
        public void BuildText2ParaTest()
        {
            var state = new Hashtable() {
                { "parameters", new ArrayList() {
                    new object?[] { "input", null },
                    new object?[] { "leaving", "true" }
                } },
            };
            var arg = new ParameterInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("(input, leaving = true)\r\n", txt);
        }

        [Fact()]
        public void BuildTextNoParaAndExtraTest()
        {
            var state = new Hashtable() {
                { "extrakw", "this" },
                { "extrapos", "that" },
            };
            var arg = new ParameterInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("(*that, **this)\r\n", txt);
        }

        [Fact()]
        public void BuildTextNoParaTest()
        {
            var state = new Hashtable();
            var arg = new ParameterInfo();
            arg.SetState(state);
            var tb = new TextBuilder();
            arg.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();
            Assert.Equal("()\r\n", txt);
        }

        [Fact()]
        public void BuildTextTranslateValueTest()
        {
            // Setup
            var state = new Hashtable() {
                { "parameters", new ArrayList() {
                    new object?[] { "p1", null },
                    new object?[] { "state", "translate" }
                } },
            };
            var para = new ParameterInfo();
            para.SetState(state);

            var translate = new Mock<ITranslateInfo>();
            translate.Setup(x => x.TranslateFunctions("translate")).Returns("some").Verifiable();

            // Act
            var tb = new TextBuilder(new[] { translate.Object });
            para.BuildText(tb);
            tb.AppendLine();
            var txt = tb.ToString();

            // Assert
            translate.Verify();
            Assert.Equal("(p1, state = some)\r\n", txt);
        }

    }
}