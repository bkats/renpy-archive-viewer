﻿using Moq;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeMenuTests
    {
        private static object?[] CreateMenuItem(string label, string condition, string[]? nodes)
        {
            ArrayList? ns = null;
            if (nodes != null)
            {
                ns = [];
                foreach (var node in nodes)
                {
                    ns.Add(new TestNodeDummy(node));
                }
            }
            return [label, condition, ns];
        }

        [Fact()]
        public void BuildTextViewTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "items", new ArrayList() {
                    CreateMenuItem("choice", "True", ["line1"] )
                } },
            };
            var menu = new NodeMenu();
            menu.SetState(state);

            // Act
            var tb = new TextBuilder();
            menu.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("menu:\r\n" +
                         "    \"choice\":\r\n" +
                         "        line1\r\n"
                         , txt);
        }

        [Fact()]
        public void BuildTextViewLineBreakTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "items", new ArrayList() {
                    CreateMenuItem("choice\nsplit", "True", ["line one"] )
                } },
            };
            var menu = new NodeMenu();
            menu.SetState(state);

            // Act
            var tb = new TextBuilder();
            menu.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("menu:\r\n" +
                         "    \"choice\\nsplit\":\r\n" +
                         "        line one\r\n"
                         , txt);
        }

        [Fact()]
        public void BuildTextViewAllTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "items", new ArrayList() {
                    CreateMenuItem("narration", "True", null ),
                    CreateMenuItem("choice", "True", ["lines"] )
                } },
                { "set", new PyExpr("mySet") },
                { "with_", new PyExpr("fade") },
                { "arguments", new ArgumentInfo() },
                { "item_arguments", new ArrayList() {
                    null,
                    new ArgumentInfo(),
                } },
            };
            var menu = new NodeMenu();
            menu.SetState(state);

            // Act
            var tb = new TextBuilder();
            menu.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("menu():\r\n" +
                         "    set mySet\r\n" +
                         "    with fade\r\n" +
                         "    \"narration\"\r\n" +
                         "    \"choice\"():\r\n" +
                         "        lines\r\n"
                         , txt);
        }

        [Fact()]
        public void BuildTextViewConditionTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "items", new ArrayList() {
                    CreateMenuItem("always", "True", ["all1"] ),
                    CreateMenuItem("optional", "condition", ["opt1", "opt2"] ),
                } },
            };
            var menu = new NodeMenu();
            menu.SetState(state);

            // Act
            var tb = new TextBuilder();
            menu.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("menu:\r\n" +
                         "    \"always\":\r\n" +
                         "        all1\r\n" +
                         "    \"optional\" if condition:\r\n" +
                         "        opt1\r\n" +
                         "        opt2\r\n"
                         , txt);
        }

        [Fact()]
        public void BuildTextViewTranslateTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "items", new ArrayList() {
                    CreateMenuItem("input", "True", ["line"] )
                } },
            };
            var menu = new NodeMenu();
            menu.SetState(state);
            var translate = new Mock<ITranslateInfo>();
            // Note: Following works because both strings ("input") are reference equal....
            var inputLiteral = "input";
            translate.Setup(x => x.TranslateLiteral(ref inputLiteral))
                .Returns((ref string output) => { output = "Output"; return true; }).Verifiable();

            // Act
            var tb = new TextBuilder([translate.Object]);
            menu.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            translate.Verify();
            Assert.Equal("menu:\r\n" +
                         "    \"Output\":\r\n" +
                         "        line\r\n"
                         , txt);
        }

        [Fact]
        public void BuildTextViewLabelSayTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "items", new ArrayList() {
                    CreateMenuItem("item", "True", ["line"] )
                } },
            };
            var menu = new NodeMenu();
            menu.SetState(state);

            var labelState = new Hashtable()
            {
                { "name", "label" },
            };
            var label = new NodeLabel();
            label.SetState(labelState);

            var sayState = new Hashtable()
            {
                { "who", "gek" },
                { "what", "ik zei de gek" },
            };
            var say = new NodeSay();
            say.SetState(sayState);

            var group = new StatementGroup(label);
            group.AddNode(label);
            group.AddNode(say);

            // act
            var tb = new TextBuilder([group]);
            menu.BuildTextView(tb);
            var txt = tb.ToString();

            // assert
            Assert.Equal("menu label:\r\n" +
                         "    gek \"ik zei de gek\" nointeract\r\n" +
                         "    \"item\":\r\n" +
                         "        line\r\n",
                txt);
        }
    }
}