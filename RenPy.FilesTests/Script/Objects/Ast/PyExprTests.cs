﻿using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class PyExprTests
    {
        [Fact()]
        public void PyExprTest()
        {
            var expr = new PyExpr();
            Assert.Equal("", expr.Expression);
            Assert.Equal(LocationInfo.Unknown, expr.Location);
        }

        [Fact()]
        public void PyExprTest1()
        {
            var expr = new PyExpr("content1\nx");
            Assert.Equal("content1\nx", expr.Expression);
            Assert.Equal(LocationInfo.Unknown, expr.Location);
        }

        [Fact()]
        public void PyExprTest2()
        {
            var expr = new PyExpr("content2", new LocationInfo("test2.cs", 13));
            Assert.Equal("content2", expr.Expression);
            Assert.Equal("test2.cs", expr.Location.Source);
            Assert.Equal(13, expr.Location.LineNumber);
        }

        [Fact()]
        public void PyExprTest3()
        {
            var expr = new PyExpr("content3", "test3.cs", 133);
            Assert.Equal("content3", expr.Expression);
            Assert.Equal("test3.cs", expr.Location.Source);
            Assert.Equal(133, expr.Location.LineNumber);
        }

        [Fact()]
        public void ToStringTest()
        {
            var expr = new PyExpr("ToString4\nSecond", "test4.cs", 333);
            Assert.Equal("ToString4\nSecond", expr.ToString());
        }
    }
}
