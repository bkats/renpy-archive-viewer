﻿using Moq;
using RenPy.Files.Shared;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class PyCodeTests
    {
        [Fact()]
        public void SetStateTest()
        {
            var state = new object[]
            {
                1,
                "This is just some code",
                new object[] { "line", 13 },
                "eval"
            };
            var code = new PyCode();
            code.SetState(state);

            Assert.Equal("This is just some code", code.SourceCode);
            Assert.Equal("line", code.Location.Source);
            Assert.Equal(13, code.Location.LineNumber);
            Assert.Equal("eval", code.Mode);
        }

        [Fact()]
        public void BuildTextViewTest()
        {
            var given = "This text is given.\nAnd should also be returned.\n  Without any changes";
            var expect = "This text is given.\r\nAnd should also be returned.\r\n  Without any changes\r\n";
            var state = new object[]
            {
                1,
                given,
                new object[] { "test", 813 },
                "exec"
            };
            var code = new PyCode();
            code.SetState(state);
            var tb = new TextBuilder();
            code.BuildTextView(tb);
            var txt = tb.ToString();
            Assert.Equal(expect, txt);
        }

        [Fact()]
        public void BuildTextViewTranslateTest()
        {
            // Setup
            var given = "This text is given.\nBut should be translated.\n";
            var expect = "This is the replacement\r\nAs fake translation\r\n";
            var state = new object[]
            {
                1,
                given,
                new object[] { "test", 813 },
                "exec"
            };
            var code = new PyCode();
            code.SetState(state);
            var translate = new Mock<ITranslateInfo>();
            translate.Setup(x => x.TranslateFunctions("This text is given.\nBut should be translated.\n"))
                .Returns("This is the replacement\nAs fake translation").Verifiable();

            // Act
            var tb = new TextBuilder([translate.Object]);
            code.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            translate.Verify();
            Assert.Equal(expect, txt);
        }

        [Fact()]
        public void BuildTextViewTranslateStringTest()
        {
            var input = "This has \"original\" text";
            // Setup
            var state = new object[]
            {
                1,
                input,
                new object[] { "TestStr", 813 },
                "exec"
            };
            var code = new PyCode();
            code.SetState(state);
            var translate = new Mock<ITranslateInfo>();
            translate.Setup(x => x.TranslateFunctions(input)).Returns(input);
            // Next part is maybe not perfect, but it works....
            var inputLiteral = "original";
            translate.Setup(x => x.TranslateLiteral(ref It.Ref<string>.IsAny))
                .Returns((ref string output) => { var r = output == inputLiteral; output = "translated"; return r; }).Verifiable();

            // Act
            var tb = new TextBuilder([translate.Object]);
            code.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            translate.Verify();
            Assert.Equal("This has \"original\" text # Translation: \"translated\"\r\n", txt);
        }
    }
}