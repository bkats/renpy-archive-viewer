﻿using RenPy.Files.Shared;
using System.Collections;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodePythonTests
    {
        private PyCode CreatePyCode(string source)
        {
            var state = new object[] { "1", source, new object[] { "test.cs", 12 }, "exec" };
            var code = new PyCode();
            code.SetState(state);
            return code;
        }

        [Fact()]
        public void BuildTextViewTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "hide", true },
                { "code", CreatePyCode("source line") },
                { "store", "store.test" }
            };
            var p = new NodePython();
            p.SetState(state);

            // Act
            var tb = new TextBuilder();
            p.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("python hide in test:\r\n    source line\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewSingleLineTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "hide", false },
                { "code", CreatePyCode("single line") },
                { "store", "store" }
            };
            var p = new NodePython();
            p.SetState(state);

            // Act
            var tb = new TextBuilder();
            p.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("$ single line\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewLargeTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "hide", false },
                { "code", CreatePyCode("source line:\n    Second line\n    Third line\n") },
                { "store", "store" }
            };
            var p = new NodePython();
            p.SetState(state);

            // Act
            var tb = new TextBuilder();
            p.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal(
                "python:\r\n" +
                "    source line:\r\n" +
                "        Second line\r\n" +
                "        Third line\r\n"
                , txt);
        }
    }
}