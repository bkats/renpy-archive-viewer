﻿using RenPy.Files.Shared;
using System.Collections;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public partial class NodeInitTests
    {
        [Fact()]
        public void BuildTextViewNoBlockTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "priority", 0 },
            };
            var init = new NodeInit();
            init.SetState(state);

            // act
            var tb = new TextBuilder();
            init.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("\r\ninit:\r\n", txt);
        }

        public static IEnumerable<object[]> TestData =>
            new List<object[]>() {
                new object[] { 0, new NodeDefine(), "define " },
                new object[] { 5, new NodeDefine(), "init 5 define " },
                new object[] { 0, new NodeDefault(), "default " },
                new object[] { 9, new NodeDefault(), "init 9 default " },
                new object[] { 0, new NodeTransform(), "transform " },
                new object[] { 6, new NodeTransform(), "init 6 transform " },
                new object[] { 0, new NodeStyle(), "style " },
                new object[] { 3, new NodeStyle(), "init 3 style " },
                new object[] { -500, new NodeScreen(), "screen" },
                new object[] { -501, new NodeScreen(), "init -501 screen" },
                new object[] { 500, new NodeImage(), "image " },
                new object[] { 501, new NodeImage(), "init 501 image " },
                new object[] { 0, new NodePython(), "init python:" },
                new object[] { 1, new NodePython(), "init 1 python:" },
            };

        [Theory()]
        [MemberData(nameof(TestData))]
        public void BuildTextViewSingleBlockTest(int priority, Node node, string expectStartWith)
        {
            // Setup
            var state = new Hashtable()
            {
                { "priority", priority },
                { "block", new ArrayList() { node } },
            };
            var init = new NodeInit();
            init.SetState(state);

            // act
            var tb = new TextBuilder();
            tb.AppendLine();
            init.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.StartsWith("\r\n" + expectStartWith, txt);
        }

        [Fact()]
        public void BuildTextViewMultiBlockTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "priority", 0 },
                { "block", new ArrayList() {
                    new TestNodeDummy("node1"),
                    new TestNodeDummy("node2"),
                } },
            };
            var init = new NodeInit();
            init.SetState(state);

            // act
            var tb = new TextBuilder();
            init.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("\r\ninit:\r\n    node1\r\n    node2\r\n\r\n", txt);
        }
    }
}