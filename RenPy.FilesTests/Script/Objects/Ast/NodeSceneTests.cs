﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeSceneTests
    {
        [Fact()]
        public void BuildTextViewTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "layer", "master" },
                { "atl", new RawBlock() },
            };
            var scene = new NodeScene();
            scene.SetState(state);

            // Act
            var tb = new TextBuilder();
            scene.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("scene onlayer master:\r\n\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewWithTest()
        {
            // Setup
            var image = new ImageSpecifier();
            ((IPythonSetState)image).SetState(new[] { "image", null, null });
            var state = new Hashtable()
            {
                { "imspec", image },
            };
            var scene = new NodeScene();
            scene.SetState(state);
            var with = new WithPaired(new NodeWith());
            with.CurrentWith.SetState(new Hashtable() { { "paired", "fade to gray" } });

            // Act
            var tb = new TextBuilder();
            tb.AddInfo(with);
            scene.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("scene image with fade to gray\r\n", txt);
        }
    }
}