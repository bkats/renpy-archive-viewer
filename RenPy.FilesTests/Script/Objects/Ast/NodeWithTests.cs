﻿using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeWithTests
    {
        [Fact()]
        public void BuildTextViewTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "expr", "dissolve" },
            };
            var with = new NodeWith();
            with.SetState(state);

            // Act
            var tb = new TextBuilder();
            with.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("with dissolve\r\n", txt);
        }

        [Fact()]
        public void BuildPairedTest()
        {
            // Setup
            var statePaired = new Hashtable()
            {
                { "expr", "None" },
                { "paired", "fade" },
            };
            var stateExpr = new Hashtable()
            {
                { "expr", "isnotprinted" },
            };
            var withPaired = new NodeWith();
            withPaired.SetState(statePaired);
            var show = new NodeShow();
            var withExpr = new NodeWith();
            withExpr.SetState(stateExpr);

            // Act
            var tb = new TextBuilder();
            withPaired.BuildTextView(tb);
            Assert.NotNull(tb.GetInfo<WithPaired>());

            show.BuildTextView(tb);
            withExpr.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.EndsWith(" with fade\r\n", txt);
            Assert.Null(tb.GetInfo<WithPaired>());
        }
    }
}