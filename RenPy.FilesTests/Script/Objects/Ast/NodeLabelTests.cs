﻿using Moq;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeLabelTests
    {
        [Fact()]
        public void BuildTextViewMinTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "name", "minimum" },
            };
            var lbl = new NodeLabel();
            lbl.SetState(state);

            // Act
            var tb = new TextBuilder();
            lbl.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("\r\nlabel minimum:\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewAllTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "name", "start" },
                { "parameters", new ParameterInfo() },
                { "hide", true },
            };
            var lbl = new NodeLabel();
            lbl.SetState(state);
            var lblinfo = new Mock<ILabelInfo>();
            lblinfo.SetupProperty(x => x.CurrentLabel);

            // Act
            var tb = new TextBuilder(new[] { lblinfo.Object });
            lbl.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal(lbl, lblinfo.Object.CurrentLabel);
            Assert.Equal("\r\nlabel start() hide:\r\n", txt);
        }
    }
}