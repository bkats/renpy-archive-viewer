﻿using Moq;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeTranslateTests
    {
        [Fact()]
        public void BuildTextViewLanguageTest()
        {
            // setup
            var state = new Hashtable()
            {
                { "language", "french" },
                { "identifier", "id_1" },
            };
            var n = new NodeTranslate();
            n.SetState(state);

            // act
            var tb = new TextBuilder();
            n.BuildTextView(tb);

            // assert
            Assert.Equal("translate french id_1:\r\n\r\n", tb.ToString());
        }

        [Fact()]
        public void BuildTextViewOriginalTest()
        {
            // setup
            var state = new Hashtable()
            {
                { "block", new ArrayList() { new TestNodeDummy("test") } },
                { "identifier", "id_1" },
            };
            var n = new NodeTranslate();
            n.SetState(state);

            // act
            var tb = new TextBuilder();
            n.BuildTextView(tb);

            // assert
            Assert.Equal("test\r\n", tb.ToString());
        }

        [Fact()]
        public void BuildTextViewTranslateTest()
        {
            // setup
            var stateSay = new Hashtable()
            {
                { "who", "org" },
                { "what", "not translated" },
                { "attributes", new [] { "attr" } },
                { "interact", true },
            };
            var say = new NodeSay();
            say.SetState(stateSay);

            var stateTranslate = new Hashtable()
            {
                { "identifier", "id1" },
                { "block", new ArrayList()
                    {
                        new TestNodeDummy("not displayed"),
                        say
                    }
                },
            };
            var translate = new NodeTranslate();
            translate.SetState(stateTranslate);

            var stateT = new Hashtable()
            {
                { "who", "tr" },
                { "what", "translated" },
                { "interact", true },
            };
            var translateSay = new NodeSay();
            translateSay.SetState(stateT);
            var result = new Node[] { new TestNodeDummy("Before"), translateSay };

            var translateInfo = new Mock<ITranslateInfo>();
            translateInfo.Setup(x => x.GetTranslatedNodes("id1", out result)).Returns(true).Verifiable();

            // act
            var tb = new TextBuilder(new[] { translateInfo.Object });
            translate.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            translateInfo.Verify();
            // expect the nodes from translation to be used and original say make-up (attribute in this test)
            Assert.Equal("Before\r\ntr attr \"translated\"\r\n", txt);
        }
    }
}
