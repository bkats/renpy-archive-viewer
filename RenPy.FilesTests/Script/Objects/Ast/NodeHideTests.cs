﻿using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeHideTests
    {
        [Fact()]
        public void BuildTextViewTest()
        {
            // Setup
            var image = new ImageSpecifier();
            ((IPythonSetState)image).SetState(new[] { "image", null, null });
            var state = new Hashtable()
            {
                { "imspec", image },
            };
            var hide = new NodeHide();
            hide.SetState(state);

            // Act
            var tb = new TextBuilder();
            hide.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("hide image\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewWithTest()
        {
            // Setup
            var image = new ImageSpecifier();
            ((IPythonSetState)image).SetState(new[] { "image", null, null });
            var state = new Hashtable()
            {
                { "imspec", image },
            };
            var hide = new NodeHide();
            hide.SetState(state);
            var with = new WithPaired(new NodeWith());
            with.CurrentWith.SetState(new Hashtable() { { "paired", "black" } });

            // Act
            var tb = new TextBuilder();
            tb.AddInfo(with);
            hide.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("hide image with black\r\n", txt);
        }
    }
}
