﻿using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeCallTests
    {
        [Fact()]
        public void BuildTextViewWithoutParametersTest()
        {
            var state = new Hashtable() {
                { "label", "calllabel" },
            };
            var call = new NodeCall();
            call.SetState(state);
            var tb = new TextBuilder();
            call.BuildTextView(tb);
            var txt = tb.ToString();
            Assert.Equal("call calllabel\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewExpressionTest()
        {
            var state = new Hashtable() {
                { "label", new PyExpr("x + \"test\"") },
                { "expression", true },
            };
            var call = new NodeCall();
            call.SetState(state);
            var tb = new TextBuilder();
            call.BuildTextView(tb);
            var txt = tb.ToString();
            Assert.Equal("call expression x + \"test\"\r\n", txt);
        }

        private static ArgumentInfo GetArguments()
        {
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { null, "25" },
                }
                } };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            return arg;
        }

        [Fact()]
        public void BuildTextViewWithParametersTest()
        {
            var state = new Hashtable() {
                    { "label", "labelpara" },
                    { "arguments", GetArguments() },
                };
            var call = new NodeCall();
            call.SetState(state);
            var tb = new TextBuilder();
            call.BuildTextView(tb);
            var txt = tb.ToString();
            Assert.Equal("call labelpara(25)\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewExpressionWithParametersTest()
        {
            var state = new Hashtable() {
                    { "label", "express(c=1)" },
                    { "expression", true },
                    { "arguments", GetArguments() },
                };
            var call = new NodeCall();
            call.SetState(state);
            var tb = new TextBuilder();
            call.BuildTextView(tb);
            var txt = tb.ToString();
            Assert.Equal("call expression express(c=1) pass (25)\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewReturnLabelTest()
        {
            // Setup
            var state = new Hashtable() {
                { "label", "goto" },
            };
            var call = new NodeCall();
            call.SetState(state);

            var stateLbl = new Hashtable()
            {
                { "name", "return" },
            };
            var lbl = new NodeLabel();
            lbl.SetState(stateLbl);

            // Act
            var tb = new TextBuilder();
            call.BuildTextView(tb, lbl);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("call goto from return\r\n", txt);
        }

    }
}