﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeShowTests
    {
        [Fact()]
        public void BuildTextViewTest()
        {
            // Setup
            var image = new ImageSpecifier();
            ((IPythonSetState)image).SetState(new[] { "image", null, null });
            var state = new Hashtable()
            {
                { "imspec", image },
                { "atl", new RawBlock() },
            };
            var show = new NodeShow();
            show.SetState(state);

            // Act
            var tb = new TextBuilder();
            show.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("show image:\r\n\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewWithTest()
        {
            // Setup
            var image = new ImageSpecifier();
            ((IPythonSetState)image).SetState(new[] { "image", null, null });
            var state = new Hashtable()
            {
                { "imspec", image },
            };
            var show = new NodeShow();
            show.SetState(state);
            var with = new WithPaired(new NodeWith());
            with.CurrentWith.SetState(new Hashtable() { { "paired", "fade" } });

            // Act
            var tb = new TextBuilder();
            tb.AddInfo(with);
            show.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("show image with fade\r\n", txt);
        }
    }
}
