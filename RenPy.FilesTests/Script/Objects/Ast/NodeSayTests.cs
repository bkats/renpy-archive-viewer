﻿using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeSayTests
    {
        [Fact()]
        public void BuildTextViewTest()
        {
            // setup
            var state = new Hashtable()
            {
                { "who", "me" },
                { "attributes", new [] { "a1", "a2" } },
                { "temporary_attributes", new [] { "ta" } },
                { "what", "something" },
                { "interact", false },
                { "arguments", new ArgumentInfo() },
            };
            var say = new NodeSay();
            say.SetState(state);

            // Act
            var tb = new TextBuilder();
            say.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("me a1 a2 @ ta \"something\"() nointeract\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewTranslateTest()
        {
            // setup
            var state = new Hashtable()
            {
                { "who", "" },
                { "what", "" },
                { "interact", true },
            };
            var say = new NodeSay();
            say.SetState(state);

            var stateT = new Hashtable()
            {
                { "who", "tr" },
                { "what", "translated" },
                { "interact", true },
            };
            var translateSay = new NodeSay();
            translateSay.SetState(stateT);

            // Act
            var tb = new TextBuilder();
            say.BuildTextView(tb, translateSay);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("tr \"translated\"\r\n", txt);
        }


        private static ArgumentInfo GetArguments()
        {
            var state = new Hashtable() {
                { "arguments", new ArrayList() {
                    new object?[] { "multi", "2" },
                }
                } };
            var arg = new ArgumentInfo();
            arg.SetState(state);
            return arg;
        }

        [Fact()]
        public void CalculateIdentifierTest()
        {
            // setup
            var state = new Hashtable()
            {
                { "who", "you" },
                { "attributes", new [] { "a1", "a2" } },
                { "temporary_attributes", new [] { "ta" } },
                { "what", "This is something" },
                { "interact", false },
                { "identifier", "EW_12" },
                { "with_", new PyExpr("dissolve") },
                { "arguments", GetArguments() },
            };
            var say = new NodeSay();
            say.SetState(state);

            // Act
            var id = say.CalculateIdentifier();

            // Assert
            Assert.Equal("7988495c", id);
        }

        [Fact()]
        public void ToStringTest()
        {
            // setup
            var state = new Hashtable()
            {
                { "who", "ik" },
                { "what", "zei de gek" },
                { "interact", true },
            };
            var say = new NodeSay();
            say.SetState(state);

            // Act
            var tb = new TextBuilder();
            say.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("ik \"zei de gek\"\r\n", txt);
        }
    }
}