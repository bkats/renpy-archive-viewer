﻿using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{

#pragma warning disable CS8604 // Dereference of a possibly null reference. Assert.NotNull doesn't tell that further access are null

    public class ImageSpecifierTests
    {
        [Fact()]
        public void SetStateTuple3Test()
        {
            var imspec = new ImageSpecifier();
            ((IPythonSetState)imspec).SetState(new object[] {
                "MyImage1",
                new ArrayList() { new PyExpr("at1") },
                "layer1"
            });
            Assert.Equal("MyImage1", imspec.Name);
            Assert.Equal("layer1", imspec.Layer);
            Assert.Contains(new PyExpr("at1"), imspec.AtList);
        }

        [Fact()]
        public void SetStateTuple6Test()
        {
            var imspec = new ImageSpecifier();
            ((IPythonSetState)imspec).SetState(new object[] {
                "MyImage2",
                new PyExpr("Expr2"),
                "Tag2",
                new ArrayList() { new PyExpr("at11"), new PyExpr("at12") },
                "layer2",
                new PyExpr("5"),
            });
            Assert.Equal("MyImage2", imspec.Name);
            Assert.Equal("layer2", imspec.Layer);
            Assert.Contains(new PyExpr("at11"), imspec.AtList);
            Assert.Contains(new PyExpr("at12"), imspec.AtList);
            Assert.Equal("Expr2", imspec.Expression);
            Assert.Equal("Tag2", imspec.Tag);
            Assert.Equal("5", imspec.Zorder);
        }

        [Fact()]
        public void SetStateTuple7Test()
        {
            var imspec = new ImageSpecifier();
            ((IPythonSetState)imspec).SetState(new object[] {
                "MyImage3",
                new PyExpr("Expr3"),
                "Tag3",
                new ArrayList() { new PyExpr("at21") },
                "layer3",
                new PyExpr("25"),
                new ArrayList() { new PyExpr("behind me") }
            });
            Assert.Equal("MyImage3", imspec.Name);
            Assert.Equal("layer3", imspec.Layer);
            Assert.Contains(new PyExpr("at21"), imspec.AtList);
            Assert.Equal("Expr3", imspec.Expression);
            Assert.Equal("Tag3", imspec.Tag);
            Assert.Equal("25", imspec.Zorder);
            Assert.Contains(new PyExpr("behind me"), imspec.Behind);
        }

        [Fact()]
        public void BuildOutputNameTest()
        {
            var imspec = new ImageSpecifier();
            ((IPythonSetState)imspec).SetState(new object?[] {
                new object[] { "split", "-name" },
                null,
                "T4",
                new ArrayList() { new PyExpr("el") },
                "L4",
                new PyExpr("1"),
                new ArrayList() { new PyExpr("me") }
            });
            var tb = new TextBuilder();
            tb.Append("show");
            imspec.BuildOutput(tb, ReferrerType.Show, LocationInfo.Unknown);
            tb.AppendLine();
            var txt = tb.ToString();

            Assert.Equal("show split -name onlayer L4 at el as T4 zorder 1 behind me\r\n", txt);
        }

        [Fact()]
        public void BuildOutputExpressionTest()
        {
            var imspec = new ImageSpecifier();
            ((IPythonSetState)imspec).SetState(new object?[] {
                "",
                new PyExpr("E5"),
                null,
                new ArrayList() { new PyExpr("a1"), new PyExpr("a2") },
                null,
                null,
                null,
            });
            var tb = new TextBuilder();
            tb.Append("show");
            imspec.BuildOutput(tb, ReferrerType.Show, LocationInfo.Unknown);
            tb.AppendLine();
            var txt = tb.ToString();

            Assert.Equal("show expression E5 at a1 a2\r\n", txt);
        }
    }
}