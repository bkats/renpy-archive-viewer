﻿using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Ast.Tests
{
    public class NodeJumpTests
    {
        [Fact()]
        public void BuildTextViewTest()
        {
            var state = new object[] { "1",
                new Hashtable() {
                    { "target", "mylabel" }
                }};
            var jump = new NodeJump();
            jump.SetState(state);
            var tb = new TextBuilder();
            jump.BuildTextView(tb);
            var txt = tb.ToString();
            Assert.Equal("jump mylabel\r\n", txt);
        }
    }
}