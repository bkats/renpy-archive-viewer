﻿using Moq;
using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Sl2.Tests
{
    public class SlDefaultTests
    {
        [Fact()]
        public void BuildTextViewTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "variable", "x" },
                { "expression", new PyExpr("25") },
            };
            var def = new SlDefault();
            def.SetState(state);

            // Act
            var tb = new TextBuilder();
            def.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("x", def.Variable);
            Assert.Equal("25", def.Expression);
            Assert.Equal("default x = 25\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewTranslateTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "variable", "xyz" },
                { "expression", new PyExpr("input") },
            };
            var def = new SlDefault();
            def.SetState(state);

            var translate = new Mock<ITranslateInfo>();
            translate.Setup(x => x.TranslateFunctions("input")).Returns("output").Verifiable();

            // Act
            var tb = new TextBuilder(new[] { translate.Object });
            def.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("default xyz = output\r\n", txt);
        }
    }
}