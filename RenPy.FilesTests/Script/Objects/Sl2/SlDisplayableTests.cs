﻿using Moq;
using Pickle;
using Pickle.Objects;
using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using System.Collections;
using Xunit;

namespace RenPy.Files.Script.Objects.Sl2.Tests
{
    /*
        displayable
        style
        positional
        keyword
        variable
        children
    */

    public class SlDisplayableTests
    {
        [Fact()]
        public void BuildTextViewWithAllTest()
        {
            // Setup
            var pt = new Mock<IPythonType>();
            pt.Setup(pt => pt.Module).Returns("renpy");
            pt.Setup(pt => pt.Name).Returns("sl2add");
            var state = new Hashtable()
            {
                { "displayable", pt.Object },
                { "style", null },
                { "positional", new ArrayList() { new PyExpr("image") } },
                { "keyword", new ArrayList()
                    {
                        new object[] { "xpos", 60 },
                    } },
                { "variable", "var" },
            };

            var disp = new SlDisplayable();
            disp.SetState(state);

            // Act
            var tb = new TextBuilder();
            disp.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("add image:\r\n    variable var\r\n    xpos 60\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewOnlyPositionalTest()
        {
            // Setup
            var pt = new Mock<IPythonType>();
            pt.Setup(pt => pt.Module).Returns("renpy");
            pt.Setup(pt => pt.Name).Returns("sl2add");
            var state = new Hashtable()
            {
                { "displayable", pt.Object },
                { "style", null },
                { "positional", new ArrayList() { new PyExpr("this") } },
                { "keyword", new ArrayList() },
                { "children", new ArrayList() },
                { "variable", "" },
            };

            var disp = new SlDisplayable();
            disp.SetState(state);

            // Act
            var tb = new TextBuilder();
            disp.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            Assert.Equal("add this\r\n", txt);
        }

        [Fact()]
        public void BuildTextViewTranslateTest()
        {
            // Setup
            var state = new Hashtable()
            {
                { "displayable", new ClassDictConstructor("renpy", "Text") },
                { "style", "text" },
                { "positional", new ArrayList() { new PyExpr("English") } },
                { "keyword", new ArrayList()
                    {
                        new object[] { "action", new PyExpr("before") },
                    } },
            };

            var disp = new SlDisplayable();
            disp.SetState(state);
            var translate = new Mock<ITranslateInfo>();
            translate.Setup(x => x.TranslateFunctions("English")).Returns("French").Verifiable();
            translate.Setup(x => x.TranslateFunctions("before")).Returns("after").Verifiable();

            // Act
            var tb = new TextBuilder(new[] { translate.Object });
            disp.BuildTextView(tb);
            var txt = tb.ToString();

            // Assert
            translate.Verify();
            Assert.Equal("text French:\r\n    action after\r\n", txt);
        }
    }
}