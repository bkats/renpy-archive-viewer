﻿using Moq;
using RenPy.Files.Script.Objects.Ast;
using Xunit;

namespace RenPy.Files.Script.Tests
{
    public class TranslateInfoTests
    {
        [InlineData("_p(\"\"\"Test\"\"\")", new[] { "Test", }, new[] { "Success", }, "_p(\"\"\"Success\"\"\")")]
        [InlineData("_p(\"\"\"Test\nSplit\"\"\")", new[] { "Test\nSplit", }, new[] { "NoSplit", }, "_p(\"\"\"NoSplit\"\"\")")]
        [InlineData("_p(\"\"\"no split\"\"\")", new[] { "no split", }, new[] { "split\nhere", }, "_p(\"\"\"split\nhere\"\"\")")]

        [InlineData("Within _p(\"\"\"t1\"\"\") some text \n _(\"t2\")", new[] { "t1", "t2" }, new[] { "r1", null },
            "Within _p(\"\"\"r1\"\"\") some text \n _(\"t2\")")]

        [InlineData("__(\"Test1\")", new[] { "Test1", }, new[] { "Success1", }, "__(\"Success1\")")]
        [InlineData("_(\"Test2\")", new[] { "Test2", }, new[] { "Success2", }, "_(\"Success2\")")]
        [InlineData("_(\"Test3\n\")", new string[0], new string[0], "_(\"Test3\n\")")]
        [Theory()]
        public void TranslateFunctionsTest(string input, string[] from, string?[] to, string expected)
        {
            // Setup
            var trs = new Mock<IReadOnlyDictionary<string, string>>();
            for (int i = 0; i < from.Length; i++)
            {
                trs.SetupGet(x => x.Count).Returns(1);
                var o = to[i];
                var f = from[i];
                var r = to[i] != null;
                trs.Setup(x => x.TryGetValue(f, out o)).Returns(r).Verifiable();
            }

            var translateInfo = new TranslateInfo(new Dictionary<string, Node[]>(), trs.Object);

            // Act
            var output = translateInfo.TranslateFunctions(input);

            // Assert
            trs.Verify();
            Assert.Equal(expected, output);
        }
    }

}
