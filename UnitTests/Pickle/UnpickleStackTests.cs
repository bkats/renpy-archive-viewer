/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using Pickle;
using System.Collections;
using Xunit;
// ReSharper disable CheckNamespace

namespace PickleTests
{

    /// <summary>
    /// Unit tests for the unpickler stack object. 
    /// </summary>
    public class UnpickleStackTest
    {

        [Fact]
        public void TestPopSinceMarker()
        {
            UnpickleStack s = new UnpickleStack();
            s.Add("a");
            s.Add("b");
            s.AddMark();
            s.Add("c");
            s.Add("d");
            s.AddMark();
            s.Add("e");
            s.Add("f");
            ArrayList top = s.PopAllSinceMarker();
            ArrayList expected = new ArrayList { "e", "f" };
            Assert.Equal(expected, top);
            Assert.Equal("d", s.Pop());
            Assert.Equal("c", s.Pop());
        }

        [Fact]
        public void TestAddPop()
        {
            UnpickleStack s = new UnpickleStack();
            Assert.Equal(0, s.Size());
            s.Add("x");
            Assert.Equal(1, s.Size());
            s.Add("y");
            Assert.Equal(2, s.Size());
            Assert.Equal("y", s.Peek());
            Assert.Equal("y", s.Pop());
            Assert.Equal("x", s.Peek());
            Assert.Equal("x", s.Pop());
            Assert.Equal(0, s.Size());
        }

        [Fact]
        public void TestClear()
        {
            UnpickleStack s = new UnpickleStack();
            s.Add("x");
            s.Add("y");
            Assert.Equal(2, s.Size());
            s.Clear();
            Assert.Equal(0, s.Size());
        }

        [Fact]
        public void TestTrim()
        {
            UnpickleStack s = new UnpickleStack();
            s.Add("a");
            s.Add("b");
            s.Add("c");
            s.Add("d");
            s.Add("e");
            Assert.Equal(5, s.Size());
            s.Trim();
            Assert.Equal(5, s.Size());
        }
    }

}
