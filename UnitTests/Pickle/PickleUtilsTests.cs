/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using Pickle;
using System;
using System.IO;
using System.Text;
using Xunit;
// ReSharper disable CheckNamespace
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace PickleTests
{

    /// <summary>
    /// Unit tests for the pickler utils. 
    /// </summary>
    public class PickleUtilsTest
    {

        private readonly byte[] _filedata;

        public PickleUtilsTest()
        {
            _filedata = Encoding.UTF8.GetBytes("str1\nstr2  \n  str3  \nend");
        }

        [Fact]
        public void TestReadline()
        {
            Stream bis = new MemoryStream(_filedata);
            Assert.Equal("str1", PickleUtils.Readline(bis));
            Assert.Equal("str2  ", PickleUtils.Readline(bis));
            Assert.Equal("  str3  ", PickleUtils.Readline(bis));
            Assert.Equal("end", PickleUtils.Readline(bis));
            try
            {
                PickleUtils.Readline(bis);
                Assert.Fail("expected IOException");
            }
            catch (IOException) { }
        }

        [Fact]
        public void TestReadlineWithLf()
        {
            Stream bis = new MemoryStream(_filedata);
            Assert.Equal("str1\n", PickleUtils.Readline(bis, true));
            Assert.Equal("str2  \n", PickleUtils.Readline(bis, true));
            Assert.Equal("  str3  \n", PickleUtils.Readline(bis, true));
            Assert.Equal("end", PickleUtils.Readline(bis, true));
            try
            {
                PickleUtils.Readline(bis, true);
                Assert.Fail("expected IOException");
            }
            catch (IOException) { }
        }

        [Fact]
        public void TestReadbytes()
        {
            Stream bis = new MemoryStream(_filedata);

            Assert.Equal(115, PickleUtils.ReadByte(bis));
            Assert.Equal(Array.Empty<byte>(), PickleUtils.ReadBytes(bis, 0));
            Assert.Equal(new byte[] { 116 }, PickleUtils.ReadBytes(bis, 1));
            Assert.Equal(new byte[] { 114, 49, 10, 115, 116 }, PickleUtils.ReadBytes(bis, 5));
            try
            {
                PickleUtils.ReadBytes(bis, 999);
                Assert.Fail("expected IOException");
            }
            catch (IOException) { }
        }

        [Fact]
        public void TestReadbytes_into()
        {
            Stream bis = new MemoryStream(_filedata);
            byte[] bytes = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            PickleUtils.ReadBytesInto(bis, bytes, 1, 4);
            Assert.Equal(new byte[] { 0, 115, 116, 114, 49, 0, 0, 0, 0, 0 }, bytes);
            PickleUtils.ReadBytesInto(bis, bytes, 8, 1);
            Assert.Equal(new byte[] { 0, 115, 116, 114, 49, 0, 0, 0, 10, 0 }, bytes);
        }

        [Fact]
        public void TestBytes_to_integer()
        {
            try
            {
                PickleUtils.BytesToInteger(Array.Empty<byte>());
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            try
            {
                PickleUtils.BytesToInteger(new byte[] { 0 });
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            Assert.Equal(0x00000000, PickleUtils.BytesToInteger(new byte[] { 0x00, 0x00 }));
            Assert.Equal(0x00003412, PickleUtils.BytesToInteger(new byte[] { 0x12, 0x34 }));
            Assert.Equal(0x0000ffff, PickleUtils.BytesToInteger(new byte[] { 0xff, 0xff }));
            Assert.Equal(0x00000000, PickleUtils.BytesToInteger(new byte[] { 0, 0, 0, 0 }));
            Assert.Equal(0x12345678, PickleUtils.BytesToInteger(new byte[] { 0x78, 0x56, 0x34, 0x12 }));
            Assert.Equal(-8380352, PickleUtils.BytesToInteger(new byte[] { 0x40, 0x20, 0x80, 0xff }));
            Assert.Equal(0x01cc02ee, PickleUtils.BytesToInteger(new byte[] { 0xee, 0x02, 0xcc, 0x01 }));
            Assert.Equal(-872288766, PickleUtils.BytesToInteger(new byte[] { 0x02, 0xee, 0x01, 0xcc }));
            Assert.Equal(-285212674, PickleUtils.BytesToInteger(new byte[] { 0xfe, 0xff, 0xff, 0xee }));
            try
            {
                PickleUtils.BytesToInteger(new byte[] { 200, 50, 25, 100, 1, 2, 3, 4 });
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
        }

        [Fact]
        public void TestBytes_to_uint()
        {
            try
            {
                PickleUtils.BytesToUint(Array.Empty<byte>(), 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            try
            {
                PickleUtils.BytesToUint(new byte[] { 0 }, 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            Assert.Equal(0x000000000L, PickleUtils.BytesToUint(new byte[] { 0, 0, 0, 0 }, 0));
            Assert.Equal(0x012345678L, PickleUtils.BytesToUint(new byte[] { 0x78, 0x56, 0x34, 0x12 }, 0));
            Assert.Equal(0x0ff802040L, PickleUtils.BytesToUint(new byte[] { 0x40, 0x20, 0x80, 0xff }, 0));
            Assert.Equal(0x0eefffffeL, PickleUtils.BytesToUint(new byte[] { 0xfe, 0xff, 0xff, 0xee }, 0));
        }

        [Fact]
        public void TestBytes_to_long()
        {
            try
            {
                PickleUtils.BytesToLong(Array.Empty<byte>(), 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            try
            {
                PickleUtils.BytesToLong(new byte[] { 0 }, 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }

            Assert.Equal(0x00000000L, PickleUtils.BytesToLong(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 0));
            Assert.Equal(0x00003412L, PickleUtils.BytesToLong(new byte[] { 0x12, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 0));
            Assert.Equal(-0xffffffffffff01L, PickleUtils.BytesToLong(new byte[] { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff }, 0));
            Assert.Equal(0L, PickleUtils.BytesToLong(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, 0));
            Assert.Equal(-0x778899aabbccddefL, PickleUtils.BytesToLong(new byte[] { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88 }, 0));
            Assert.Equal(0x1122334455667788L, PickleUtils.BytesToLong(new byte[] { 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11 }, 0));
            Assert.Equal(-1L, PickleUtils.BytesToLong(new byte[] { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }, 0));
            Assert.Equal(-2L, PickleUtils.BytesToLong(new byte[] { 0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }, 0));
        }

        [Fact]
        public void TestInteger_to_bytes()
        {
            Assert.Equal(new byte[] { 0, 0, 0, 0 }, PickleUtils.IntegerToBytes(0));
            Assert.Equal(new byte[] { 0x78, 0x56, 0x34, 0x12 }, PickleUtils.IntegerToBytes(0x12345678));
            Assert.Equal(new byte[] { 0x40, 0x20, 0x80, 0xff }, PickleUtils.IntegerToBytes(-8380352));
            Assert.Equal(new byte[] { 0xfe, 0xff, 0xff, 0xee }, PickleUtils.IntegerToBytes(-285212674));
            Assert.Equal(new byte[] { 0xff, 0xff, 0xff, 0xff }, PickleUtils.IntegerToBytes(-1));
            Assert.Equal(new byte[] { 0xee, 0x02, 0xcc, 0x01 }, PickleUtils.IntegerToBytes(0x01cc02ee));
            Assert.Equal(new byte[] { 0x02, 0xee, 0x01, 0xcc }, PickleUtils.IntegerToBytes(-872288766));
        }

        [Fact]
        public void TestBytes_to_double()
        {
            try
            {
                PickleUtils.BytesBigendianToDouble(Array.Empty<byte>(), 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            try
            {
                PickleUtils.BytesBigendianToDouble(new byte[] { 0 }, 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            Assert.Equal(0.0d, PickleUtils.BytesBigendianToDouble(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, 0));
            Assert.Equal(1.0d, PickleUtils.BytesBigendianToDouble(new byte[] { 0x3f, 0xf0, 0, 0, 0, 0, 0, 0 }, 0));
            Assert.Equal(1.1d, PickleUtils.BytesBigendianToDouble(new byte[] { 0x3f, 0xf1, 0x99, 0x99, 0x99, 0x99, 0x99, 0x9a }, 0));
            Assert.Equal(1234.5678d, PickleUtils.BytesBigendianToDouble(new byte[] { 0x40, 0x93, 0x4a, 0x45, 0x6d, 0x5c, 0xfa, 0xad }, 0));
            Assert.Equal(2.17e123d, PickleUtils.BytesBigendianToDouble(new byte[] { 0x59, 0x8a, 0x42, 0xd1, 0xce, 0xf5, 0x3f, 0x46 }, 0));
            Assert.Equal(1.23456789e300d, PickleUtils.BytesBigendianToDouble(new byte[] { 0x7e, 0x3d, 0x7e, 0xe8, 0xbc, 0xaf, 0x28, 0x3a }, 0));
            Assert.Equal(double.PositiveInfinity, PickleUtils.BytesBigendianToDouble(new byte[] { 0x7f, 0xf0, 0, 0, 0, 0, 0, 0 }, 0));
            Assert.Equal(double.NegativeInfinity, PickleUtils.BytesBigendianToDouble(new byte[] { 0xff, 0xf0, 0, 0, 0, 0, 0, 0 }, 0));
            try
            {
                PickleUtils.BytesBigendianToDouble(new byte[] { 200, 50, 25, 100 }, 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }

            // test offset
            Assert.Equal(1.23456789e300d, PickleUtils.BytesBigendianToDouble(new byte[] { 0, 0, 0, 0x7e, 0x3d, 0x7e, 0xe8, 0xbc, 0xaf, 0x28, 0x3a }, 3));
            Assert.Equal(1.23456789e300d, PickleUtils.BytesBigendianToDouble(new byte[] { 0x7e, 0x3d, 0x7e, 0xe8, 0xbc, 0xaf, 0x28, 0x3a, 0, 0, 0 }, 0));
        }

        [Fact]
        public void TestBytes_to_float()
        {
            try
            {
                PickleUtils.BytesBigendianToFloat(Array.Empty<byte>(), 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            try
            {
                PickleUtils.BytesBigendianToFloat(new byte[] { 0 }, 0);
                Assert.Fail("expected PickleException");
            }
            catch (PickleException) { }
            Assert.Equal(0.0f, PickleUtils.BytesBigendianToFloat(new byte[] { 0, 0, 0, 0 }, 0));
            Assert.Equal(1.0f, PickleUtils.BytesBigendianToFloat(new byte[] { 0x3f, 0x80, 0, 0 }, 0));
            Assert.Equal(1.1f, PickleUtils.BytesBigendianToFloat(new byte[] { 0x3f, 0x8c, 0xcc, 0xcd }, 0));
            Assert.Equal(1234.5678f, PickleUtils.BytesBigendianToFloat(new byte[] { 0x44, 0x9a, 0x52, 0x2b }, 0));
            Assert.True(float.PositiveInfinity == PickleUtils.BytesBigendianToFloat(new byte[] { 0x7f, 0x80, 0, 0 }, 0));
            Assert.True(float.NegativeInfinity == PickleUtils.BytesBigendianToFloat(new byte[] { 0xff, 0x80, 0, 0 }, 0));

            // test offset
            Assert.Equal(1234.5678f, PickleUtils.BytesBigendianToFloat(new byte[] { 0, 0, 0, 0x44, 0x9a, 0x52, 0x2b }, 3));
            Assert.Equal(1234.5678f, PickleUtils.BytesBigendianToFloat(new byte[] { 0x44, 0x9a, 0x52, 0x2b, 0, 0, 0 }, 0));
        }

        [Fact]
        public void TestDouble_to_bytes()
        {
            Assert.Equal(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, PickleUtils.DoubleToBytesBigendian(0.0d));
            Assert.Equal(new byte[] { 0x3f, 0xf0, 0, 0, 0, 0, 0, 0 }, PickleUtils.DoubleToBytesBigendian(1.0d));
            Assert.Equal(new byte[] { 0x3f, 0xf1, 0x99, 0x99, 0x99, 0x99, 0x99, 0x9a }, PickleUtils.DoubleToBytesBigendian(1.1d));
            Assert.Equal(new byte[] { 0x40, 0x93, 0x4a, 0x45, 0x6d, 0x5c, 0xfa, 0xad }, PickleUtils.DoubleToBytesBigendian(1234.5678d));
            Assert.Equal(new byte[] { 0x59, 0x8a, 0x42, 0xd1, 0xce, 0xf5, 0x3f, 0x46 }, PickleUtils.DoubleToBytesBigendian(2.17e123d));
            Assert.Equal(new byte[] { 0x7e, 0x3d, 0x7e, 0xe8, 0xbc, 0xaf, 0x28, 0x3a }, PickleUtils.DoubleToBytesBigendian(1.23456789e300d));
            // cannot test NaN because it's not always the same byte representation...
            // Assert.Equal(new byte[]{0xff,0xf8,0,0,0,0,0,0}, p.double_to_bytes(Double.NaN));
            Assert.Equal(new byte[] { 0x7f, 0xf0, 0, 0, 0, 0, 0, 0 }, PickleUtils.DoubleToBytesBigendian(double.PositiveInfinity));
            Assert.Equal(new byte[] { 0xff, 0xf0, 0, 0, 0, 0, 0, 0 }, PickleUtils.DoubleToBytesBigendian(double.NegativeInfinity));
        }

        [Fact]
        public void TestDecode_long()
        {
            Assert.Equal(0L, PickleUtils.DecodeLong(Array.Empty<byte>()));
            Assert.Equal(0L, PickleUtils.DecodeLong(new byte[] { 0 }));
            Assert.Equal(1L, PickleUtils.DecodeLong(new byte[] { 1 }));
            Assert.Equal(10L, PickleUtils.DecodeLong(new byte[] { 10 }));
            Assert.Equal(255L, PickleUtils.DecodeLong(new byte[] { 0xff, 0x00 }));
            Assert.Equal(32767L, PickleUtils.DecodeLong(new byte[] { 0xff, 0x7f }));
            Assert.Equal(-256L, PickleUtils.DecodeLong(new byte[] { 0x00, 0xff }));
            Assert.Equal(-32768L, PickleUtils.DecodeLong(new byte[] { 0x00, 0x80 }));
            Assert.Equal(-128L, PickleUtils.DecodeLong(new byte[] { 0x80 }));
            Assert.Equal(127L, PickleUtils.DecodeLong(new byte[] { 0x7f }));
            Assert.Equal(128L, PickleUtils.DecodeLong(new byte[] { 0x80, 0x00 }));

            Assert.Equal(128L, PickleUtils.DecodeLong(new byte[] { 0x80, 0x00 }));
            Assert.Equal(0x78563412L, PickleUtils.DecodeLong(new byte[] { 0x12, 0x34, 0x56, 0x78 }));
            Assert.Equal(0x785634f2L, PickleUtils.DecodeLong(new byte[] { 0xf2, 0x34, 0x56, 0x78 }));
            Assert.Equal(0x12345678L, PickleUtils.DecodeLong(new byte[] { 0x78, 0x56, 0x34, 0x12 }));

            Assert.Equal(-231451016L, PickleUtils.DecodeLong(new byte[] { 0x78, 0x56, 0x34, 0xf2 }));
            Assert.Equal(0xf2345678L, PickleUtils.DecodeLong(new byte[] { 0x78, 0x56, 0x34, 0xf2, 0x00 }));
        }

        [Fact]
        public void TestDecode_escaped()
        {
            Assert.Equal("abc", PickleUtils.DecodeEscaped("abc"));
            Assert.Equal("a\\c", PickleUtils.DecodeEscaped(@"a\\c"));
            Assert.Equal("a\u0042c", PickleUtils.DecodeEscaped("a\\x42c"));
            Assert.Equal("a\nc", PickleUtils.DecodeEscaped("a\\nc"));
            Assert.Equal("a\tc", PickleUtils.DecodeEscaped("a\\tc"));
            Assert.Equal("a\rc", PickleUtils.DecodeEscaped("a\\rc"));
            Assert.Equal("a'c", PickleUtils.DecodeEscaped("a\\'c"));
        }

        [Fact]
        public void TestDecode_unicode_escaped()
        {
            Assert.Equal("abc", PickleUtils.DecodeUnicodeEscaped("abc"));
            Assert.Equal("a\\c", PickleUtils.DecodeUnicodeEscaped(@"a\\c"));
            Assert.Equal("a\u0042c", PickleUtils.DecodeUnicodeEscaped("a\\u0042c"));
            Assert.Equal("a\nc", PickleUtils.DecodeUnicodeEscaped("a\\nc"));
            Assert.Equal("a\tc", PickleUtils.DecodeUnicodeEscaped("a\\tc"));
            Assert.Equal("a\rc", PickleUtils.DecodeUnicodeEscaped("a\\rc"));
        }
    }


}
