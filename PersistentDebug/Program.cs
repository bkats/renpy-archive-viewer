﻿using RenpyGameViewer.Logic;

var line = "This is a longer text, with text";
var srce = "This is a text, with text";

var words = new string[] { "test", "tex", "text" };
foreach (var word in words)
{
    var rw = BestTextMatch.MatchWord("text", word);
    Console.WriteLine($" text ~ {word} => {rw}");
}
Console.WriteLine("----");

var rws = BestTextMatch.MatchWords("text", words.ToList()).OrderBy(w => w.coeff).LastOrDefault();
Console.WriteLine($"Text => {rws.index} : {rws.coeff * 100}%");

Console.WriteLine("----");

var rl = BestTextMatch.MatchLine(srce, line, true);
Console.WriteLine($"source: \"{srce}\"");
Console.WriteLine($"line  : \"{line}\"");
Console.WriteLine($"Matches: {rl * 100}%");

Console.WriteLine("----");
Console.WriteLine();

string[] lines = ["Longer text, with text", "This a line", "This is another line", "This is a text", "What is this a text", "Longer text, with text"];
rl = BestTextMatch.MatchLine(srce, lines[5], true);
Console.WriteLine($"source: \"{srce}\"");
Console.WriteLine($"line  : \"{lines[5]}\"");
Console.WriteLine($"Matches: {rl * 100}%");

Console.WriteLine("----");
Console.WriteLine();

foreach (var l in lines)
{
    rl = BestTextMatch.MatchLine(srce, l);
    Console.WriteLine($"source: \"{srce}\"");
    Console.WriteLine($"line  : \"{l}\"");
    Console.WriteLine($"Match : {rl * 100}%");
    Console.WriteLine("-");
}
Console.WriteLine();
Console.WriteLine("----");
Console.WriteLine();

int lineNo = 23;
var finalResult = BestTextMatch.MatchLines(lineNo, srce, lines, true);
Console.WriteLine($"Best match for: \"{srce}\" at line {lineNo}");
Console.WriteLine($"And it found: {finalResult.lineNo}) \"{finalResult.line}\"");

Console.WriteLine();
Console.WriteLine("----");
Console.WriteLine();

lineNo = 3;
finalResult = BestTextMatch.MatchLines(lineNo, srce, lines, true);
Console.WriteLine($"Best match for: \"{srce}\" at line {lineNo}");
Console.WriteLine($"And it found: {finalResult.lineNo}) \"{finalResult.line}\"");

Console.WriteLine();
Console.WriteLine("----");
Console.WriteLine();

for (lineNo = 1; lineNo < 7; lineNo++)
{
    finalResult = BestTextMatch.MatchLines(lineNo, srce, lines, false);
    Console.WriteLine($"Best match at line {lineNo} is {finalResult.lineNo}");
}
