# Ren'Py Archive Tools

Simple tools to look at Ren'Py games

For most recent version of Ren'Py Game viewer go to https://gitlab.com/bkats/renpygameviewer

## Description

Consists of two simple tools for viewing game files and extracting RPA files of RenPy games.

The first tool is a simple Ren'Py archive tool which allows to view RPA files and modify and store them. This means (individual) files can be extracted, removed or added. There is also a file viewer that shows the content of RPY, RPYC, images and font files. (Audio & Video are work in progress)

The second tool is a game script viewer that loads all script files (rpy and rpyc files) and shows these in handy way. The script can be searched through with a find tool. In case of multiple translations the RPYC files can be viewed in one of the translations. This can be handy when the original language isn't one you can read. Also jumps can be performed to definition of labels, images, screen, etc.

Note: The view for rpyc files isn't perfect and is not intended to replace unrpyc (https://github.com/CensoredUsername/unrpyc)

## Todo list:

**No longer maintained**

## Installation

There is no installer provided, just download the source and build it with VS2022 or dotnet command line.
Or download the binaries from [Itch](https://spamboxje1601.itch.io/renpy-viewers)

If you like the tools or code and want to donate something, you can [buy me a coffee](https://buymeacoffee.com/bkats).

## Authors and acknowledgment

This project is depending three libraries [Flyleaf](https://github.com/SuRGeoNix/Flyleaf), [Razorvine Pickle](https://github.com/irmen/pickle) and [Avalon Editor](http://avalonedit.net/). Thanks for those libraries it made it a lot simpler to read rpa/rpyc-files and displaying the scripts and videos.

## Licenses

All code and two libraries Razorvine Pickle and Avalon Edit are MIT licensed, third library Flyleaf is LGPL-3.0.

## Project status

Finished

## Additional license info

Adler32 calculation is from Zlib library coming with following license clause:

Copyright (c) 2006, ComponentAce
http://www.componentace.com
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
Neither the name of ComponentAce nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Copyright (c) 2000,2001,2002,2003 ymnk, JCraft,Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright 
notice, this list of conditions and the following disclaimer in 
the documentation and/or other materials provided with the distribution.

3. The names of the authors may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL JCRAFT,
INC. OR ANY CONTRIBUTORS TO THIS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
