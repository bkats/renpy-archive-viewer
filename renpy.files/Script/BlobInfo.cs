﻿namespace RenPy.Files.Script
{
    internal class BlobInfo
    {
        public BlobInfo(int index, int offset, int length)
        {
            Index = index;
            Offset = offset;
            Length = length;
        }

        public int Index { get; }
        public int Offset { get; }
        public int Length { get; }
    }
}