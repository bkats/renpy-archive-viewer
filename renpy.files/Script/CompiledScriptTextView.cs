﻿namespace RenPy.Files.Script
{
    public class CompiledScriptTextView
    {
        public string Text { get; }
        public IReadOnlyList<DefinitionLocation> Definitions { get; }
        public IReadOnlyList<ReferrerLocation> Referrers { get; }

        internal CompiledScriptTextView(string text, RefDefCollector collector)
        {
            Text = text;
            Definitions = collector.Definitions;
            Referrers = collector.Referrers;
        }
    }
}
