﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Internal;
using System.Collections;
using System.Text;

namespace RenPy.Files.Script
{
    public static class CompiledScriptLoader
    {
        public static CompiledScript GetScript(string fileName, Stream rpycFile, Languages? languages = null)
        {
            var root = LoadTree(rpycFile);
            if (languages != null)
            {
                root.SetupLanguage(languages);
            }
            return new CompiledScript(fileName, root);
        }

        private static ScriptRoot LoadTree(Stream rpycFile)
        {
            var blobs = GetBlobInfo(rpycFile);
            var b2 = blobs.First(b => b.Index == 2);
            byte[] rawData;
            if (b2 != null)
            {
                rawData = LoadBlob(b2, rpycFile);
            }
            else
            {
                rawData = LoadBlob(blobs[0], rpycFile);
            }
            return GetScriptTree(rawData);
        }

        private static List<BlobInfo> GetBlobInfo(Stream rpycFile)
        {
            var buf = new byte[10];
            rpycFile.Seek(0, SeekOrigin.Begin);
            rpycFile.Read(buf);
            // Check header is correct 
            string header = Encoding.Default.GetString(buf);
            if (header != "RENPY RPC2")
            {
                throw new RenPyScriptException($"Invalid header: Expected 'RENPY RPC2', got {header}");
            }

            var list = new List<BlobInfo>();
            var info = GetBlob(rpycFile);
            while (info.Index > 0 && info.Index <= 3)
            {
                list.Add(info);
                info = GetBlob(rpycFile);
            }
            return list;
        }

        private static BlobInfo GetBlob(Stream rpycFile)
        {
            try
            {
                byte[] buf = new byte[12];
                rpycFile.Read(buf);
                var index = BitConverter.ToInt32(buf, 0);
                var offset = BitConverter.ToInt32(buf, 4);
                var length = BitConverter.ToInt32(buf, 8);
                return new BlobInfo(index, offset, length);
            }
            catch (Exception ex)
            {
                throw new RenPyScriptException("Failed to load script blob info", ex);
            }
        }

        private static byte[] LoadBlob(BlobInfo info, Stream rpycFile)
        {
            try
            {
                var buf = new byte[info.Length];
                rpycFile.Seek(info.Offset, SeekOrigin.Begin);
                rpycFile.Read(buf);
                return DeflateZlib.DecompressZlib(buf);
            }
            catch (Exception ex)
            {
                throw new RenPyScriptException("Failed to decompress script blob", ex);
            }
        }

        private static ScriptRoot GetScriptTree(byte[] blobRawData)
        {
            try
            {
                var script = Pickler.Unpickle<object[]>(blobRawData);
                if ((script.Length == 2)
                    && (script[0] is Hashtable info)
                    && (script[1] is ArrayList tree))
                {
                    return new ScriptRoot(info, Node.Cast(tree).ToList());
                }
                return new ScriptRoot([], []);
            }
            catch (Exception ex)
            {
                throw new RenPyScriptException("Failed to decode script blob", ex);
            }
        }

    }
}
