﻿using RenPy.Files.Script.Objects;
using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using System.Collections;

namespace RenPy.Files.Script
{
    public class ScriptRoot
    {
        public IReadOnlyDictionary<string, object> Info { get; private set; }
        public IReadOnlyList<Node> Nodes { get; private set; }
        public string Language { get; private set; } = Languages.None;

        public ScriptRoot(Hashtable info, List<Node> nodes)
        {
            var di = new Dictionary<string, object>();
            foreach (var i in info.Keys)
            {
                var value = info[i];
                if (i is string key && value != null)
                {
                    di.Add(key, value);
                }
            }
            Info = di;

            Nodes = nodes;
            if (nodes.Count > 0)
            {
                // Detect if there is a consitented offset in inits
                DetectInitOffset(nodes);

                // Remove added return statement from the end
                if (nodes[^1] is NodeReturn)
                {
                    nodes.RemoveAt(nodes.Count - 1);
                }
            }
        }

        public CompiledScriptTextView BuildTextView(ITranslateInfo? translation = null)
        {
            var collector = new RefDefCollector();
            TextBuilder tb;
            if (translation != null)
            {
                tb = new TextBuilder([collector, translation]);
            }
            else
            {
                tb = new TextBuilder([collector]);
            }
            if (translation == null)
            {
                tb.AppendLine("# This a decoded view of a RPYC file");
            }
            else
            {
                tb.AppendLine("# This a *translated* decoded view of a RPYC file");
                tb.AppendLine("# Translations of strings/text might be missing (Still original language)");
                tb.AppendLine("# Translating of RPYC has only limited support for string replacements");
            }
            tb.AppendLine("# The view tries to be as complete and accurate as possible");
            tb.AppendLine("# But some statements or parts of statement might be missing");
            tb.AppendLine();

            if (Nodes.Count > 0)
            {
                StatementBlockBuilder.BuildBlock(Nodes, tb);
            }
            else
            {
                tb.AppendLine("# Move along now, nothing to see here");
                tb.AppendLine();
                tb.AppendLine("# Seriously, there is nothing to see here. Apparently the Author made an empty file");
            }

            return new CompiledScriptTextView(tb.ToString(), collector);
        }

        public void RunOnAll(Action<Node> action)
        {
            foreach (var node in Nodes)
            {
                node.RunOnAll(action);
            }
        }

        private static void DetectInitOffset(List<Node> Nodes)
        {
            var groups = new List<(Node first, int offset, int count)>();
            var offset = 0;
            Node first = Nodes[0];
            int count = 0;
            foreach (var node in Nodes)
            {
                var (o, c) = DetectLevel(node, offset);
                if (o == offset || first == node)
                {
                    offset = o;
                    count += c;
                }
                else
                {
                    groups.Add((first, offset, count));
                    first = node;
                    offset = o;
                    count = c;
                }
            }
            groups.Add((first, offset, count));

            var currentOffset = 0;
            foreach (var (n, o, c) in groups)
            {
                if (o != currentOffset && c > 2)
                {
                    currentOffset = o;
                    Nodes.Insert(Nodes.IndexOf(n), new NodeInitOffset(o));
                }
            }
        }

        private static (int offset, int count) DetectLevel(Node node, int offset)
        {
            var levels = new Dictionary<int, int>();

            node.RunOnAll(n => DetectLevel(n, levels));

            var usage = levels.Select(i => (offset: i.Key, count: i.Value)).OrderByDescending(p => p.count).ToArray();
            if (usage.Length == 1)
            {
                offset = usage[0].offset;
            }
            if (usage.Length > 1)
            {
                if (usage[0].count > usage[1].count * 2)
                {
                    offset = usage[0].offset;
                }
            }

            return (offset, usage.Sum(x => x.count));
        }

        private static void DetectLevel(Node node, Dictionary<int, int> levels)
        {
            if (node is NodeInit ni)
            {
                var stm = ni.Block?[0];
                if (stm != null)
                {
                    var offset = ni.Priority - InitLevel.GetDefaultPriority(stm);
                    if (levels.TryGetValue(offset, out int value))
                    {
                        levels[offset] = ++value;
                    }
                    else
                    {
                        levels.Add(offset, 1);
                    }
                }
            }
        }

        internal void SetupLanguage(Languages languages)
        {
            var trans = Nodes.FirstOrDefault(n => n is ITranslate) as ITranslate;
            if (trans == null)
            {
                var init = Nodes.FirstOrDefault(n => n is NodeInit) as NodeInit;
                trans = init?.Block?.FirstOrDefault(n => n is ITranslate) as ITranslate;
            }
            Language = languages.TryAddLanguage(trans?.Language);

            RunOnAll(n => { if (n is ITranslate t) languages.TryAddTranslation(t.Language, n); });
        }
    }
}
