﻿using RenPy.Files.Script.Objects;
using RenPy.Files.Shared;

namespace RenPy.Files.Script
{
    public class CodeLocation : ICodeLocation
    {
        public int LineNumberStatement { get; protected set; }
        public int LineNumber { get; protected set; }
        public int ColumnNumber { get; protected set; }
        public string Name { get; private set; }
        public string Text { get; private set; }
        public string? MatchHint { get; set; }
        public LocationInfo OriginalSource { get; private set; }

        public CodeLocation(string text, LocationInfo info)
        {
            Name = text;
            Text = text;
            OriginalSource = info;
        }

        public CodeLocation(string name, string text, LocationInfo info)
        {
            Name = name;
            Text = text;
            OriginalSource = info;
        }

        void ICodeLocation.SetLineColumn(int lineStatement, int line, int column)
        {
            LineNumberStatement = lineStatement;
            LineNumber = line;
            ColumnNumber = column;
        }
    }
}
