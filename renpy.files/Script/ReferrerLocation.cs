﻿using RenPy.Files.Script.Objects;

namespace RenPy.Files.Script
{
    public enum ReferrerType
    {
        Unknown,
        Scene,
        Show,
        Hide,
        Call,
        Jump,
        Action,
        Assignment,
    }

    public class ReferrerLocation : CodeLocation
    {
        public ReferrerType Type { get; private set; }
        public DefinitionType DefinitionType { get; }

        public ReferrerLocation(ReferrerType type, string text, LocationInfo originalSource, DefinitionType defType)
            : base(text, originalSource)
        {
            Type = type;
            DefinitionType = defType;
        }

        public ReferrerLocation(ReferrerType type, string name, string text, LocationInfo originalSource, DefinitionType defType)
            : base(name, text, originalSource)
        {
            Type = type;
            DefinitionType = defType;
        }

        public override string ToString()
        {
            return $"{Type} {Name} ({LineNumber})";
        }
    }
}
