﻿using RenPy.Files.Script.Objects.Ast;
using System.Diagnostics.CodeAnalysis;

namespace RenPy.Files.Script
{
    public interface ITranslateInfo
    {
        bool GetTranslatedNodes(string id, [NotNullWhen(true)] out Node[]? result);
        string TranslateFunctions(string text);
        bool TranslateLiteral(ref string text);
    }
}