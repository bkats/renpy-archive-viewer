﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script
{
    internal class RefDefCollector : ICodeCollector
    {
        public List<DefinitionLocation> Definitions { get; } = new();
        public List<ReferrerLocation> Referrers { get; } = new();

        public void AddCodeBlock(ICodeLocation code)
        {
            switch (code)
            {
                case DefinitionLocation d:
                    Definitions.Add(d);
                    break;
                case ReferrerLocation r:
                    Referrers.Add(r);
                    break;
                default:
                    break;
            }
        }
    }
}
