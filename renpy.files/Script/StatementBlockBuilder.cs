﻿using RenPy.Files.Script.Objects;
using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using System.Diagnostics.CodeAnalysis;

namespace RenPy.Files.Script
{
    internal static class StatementBlockBuilder
    {
        private class EnumPeek
        {
            private readonly IEnumerator<Node> enumerator;

            public Node? Current { get; private set; }
            public Node? Next { get; private set; }

            internal EnumPeek(IEnumerable<Node> block)
            {
                enumerator = block.GetEnumerator();
                if (enumerator.MoveNext())
                {
                    Next = enumerator.Current;
                }
            }

            [MemberNotNullWhen(true, nameof(Current))]
            public bool MoveNext()
            {
                Current = Next;
                if (Next != null)
                {
                    if (enumerator.MoveNext())
                    {
                        Next = enumerator.Current;
                    }
                    else
                    {
                        Next = null;
                    }
                }
                return Current != null;
            }
        }


        internal static void BuildBlock(IEnumerable<Node> block, TextBuilder tb)
        {
            var i = new EnumPeek(block);
            while (i.MoveNext())
            {
                switch (i.Current)
                {
                    case NodeCall call:
                        BuildCallStatement(tb, i, call);
                        break;
                    case NodeTranslateString translateString:
                        BuildTranslateString(tb, i, translateString);
                        break;
                    default:
                        // Check for statement group
                        var statementStart = GetStatementStart(i.Current);
                        if (statementStart != null)
                        {
                            BuildStatementGroup(tb, i, statementStart);
                        }
                        else
                        {
                            i.Current.BuildTextView(tb);
                        }
                        break;
                }
            }
        }

        private static void BuildCallStatement(TextBuilder tb, EnumPeek i, NodeCall call)
        {
            if ((i.Next is NodeLabel label)
                && label.IsReturnCandidate)
            {
                call.BuildTextView(tb, label);
                // Additional move next to skip the label
                i.MoveNext();
            }
            else
            {
                call.BuildTextView(tb);
            }
            if (i.Next is NodePass)
            {
                // Skip the added pass statement (is added after call statement by the parser)
                i.MoveNext();
            }
        }

        private static void BuildTranslateString(TextBuilder tb, EnumPeek i, NodeTranslateString translateString)
        {
            // Build first normal (with header) 
            translateString.BuildTextView(tb);
            // Check if next is also translate string with same language
            // Print next without header
            // Repeat until next isn't translate string or different language (not probably)
            while (i.Next is NodeTranslateString next && next.Language == translateString.Language)
            {
                next.BuildTextView(tb, false);
                i.MoveNext();
            }
            // Current is now last printed statement, so main while will move to next 
        }

        private static Node? GetStatementStart(Node? node)
        {
            if ((node is NodeTranslate tr)
                && (tr.Block != null)
                && (tr.Block.Count == 1))
            {
                return tr.Block[0].StatementStart;
            }

            return node?.StatementStart;
        }

        private static void BuildStatementGroup(TextBuilder tb, EnumPeek i, Node start)
        {
            var group = new StatementGroup(start);
            while ((i.Current != null)
                && ((GetStatementStart(i.Next) == start) || (i.Next is NodeEndTranslate)))
            {
                group.AddNode(i.Current);
                i.MoveNext();
            }
            if (group.HasNodes)
            {
                tb.AddInfo(group);
            }
            i.Current?.BuildTextView(tb);
            tb.RemoveInfo(group);
        }
    }
}
