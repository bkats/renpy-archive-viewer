﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;

namespace RenPy.Files.Script
{
    public static class TranslationLoader
    {
        public static ITranslateInfo LoadTranslation(IEnumerable<Node> nodes)
        {
            // Build say table and translation table
            var sayTable = new Dictionary<string, Node[]>();
            var translations = new Dictionary<string, string>();

            foreach (var node in nodes)
            {
                LoadNode(node, sayTable, translations);
            }

            return new TranslateInfo(sayTable, translations);
        }

        private static void LoadNode(Node node, Dictionary<string, Node[]> sayTable, Dictionary<string, string> translations)
        {

            if (node is NodeTranslate tr)
            {
                var id = tr.Identifier;
                var says = tr.Block?.ToArray();
                if (id != null && says != null && !sayTable.ContainsKey(id))
                {
                    sayTable.Add(id, says);
                }
            }
            if (node is NodeTranslateSay say)
            {
                var id = say.Identifier;
                if (id != null && !sayTable.ContainsKey(id))
                {
                    sayTable.Add(id, [say]);
                }
            }
            if (node is NodeTranslateString s)
            {
                var old = TextBuilder.EscapeText(s.Old);
                var _new = TextBuilder.EscapeText(s.New);
                translations.Add(old, _new);
            }
        }
    }
}
