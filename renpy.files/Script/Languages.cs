﻿using RenPy.Files.Script.Objects.Ast;

namespace RenPy.Files.Script
{
    public class Languages
    {
        private readonly List<string> languages = [None];
        private readonly Dictionary<string, List<Node>> translations = [];
        public static string None => "None";
        public IReadOnlyList<string> AvailableLanguages => languages;

        public void ClearList()
        {
            languages.Clear();
            languages.Add(None);
            translations.Clear();
        }

        internal string TryAddLanguage(string? language)
        {
            if (language != null)
            {
                language = char.ToUpper(language[0]) + language[1..].ToLower();
                lock (languages)
                {
                    if (!languages.Contains(language))
                    {
                        languages.Add(language);
                    }
                }
                return language;
            }
            return None;
        }

        public string Find(string lang)
        {
            return languages.FirstOrDefault(l => string.Compare(l, lang, StringComparison.InvariantCultureIgnoreCase) == 0) ?? None;
        }

        internal void TryAddTranslation(string? language, Node translation)
        {
            var lang = TryAddLanguage(language);
            if (lang != None)
            {
                lock (translations)
                {
                    if (translations.TryGetValue(lang, out List<Node>? trans))
                    {
                        trans.Add(translation);
                    }
                    else
                    {
                        translations.Add(lang, [translation]);
                    }
                }
            }
        }

        public IEnumerable<Node> GetTranslations(string language)
        {
            var lang = Find(language);
            if (translations.TryGetValue(lang, out var value))
            {
                return value;
            }
            return [];
        }
    }
}

