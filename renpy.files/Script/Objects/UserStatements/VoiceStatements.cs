﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.UserStatements
{
    [UserStatement("voice")]
    public class Voice : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string Transition { get; }

        public Voice(string keyWord, PyExpr transition)
        {
            KeyWord = keyWord;
            Transition = transition;
        }

        public void BuildTextView(TextBuilder output)
        {
            output.AppendLine($"{KeyWord} {Transition}");
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }

    [UserStatement("voice sustain")]
    public class VoiceSustain : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }

        public VoiceSustain(string keyWord)
        {
            KeyWord = keyWord;
        }

        public void BuildTextView(TextBuilder output)
        {
            output.AppendLine(KeyWord);
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }
}
