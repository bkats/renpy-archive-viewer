﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.UserStatements
{
    [UserStatement("pause")]
    public class PauseStatement : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string? Delay { get; }

        public PauseStatement(string keyWord, IDictionary<string, object> values)
        {
            KeyWord = keyWord;
            Delay = RepickData.GetValue<string?>(values, "delay", null);
        }

        public void BuildTextView(TextBuilder output)
        {
            if (Delay == null)
            {
                output.AppendLine("pause");
            }
            else
            {
                output.AppendLine($"pause {Delay}");
            }
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }
}
