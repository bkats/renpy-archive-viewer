﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.UserStatements
{
    [UserStatement("play music")]
    [UserStatement("play sound")]
    [UserStatement("play")]
    public class PlayMusic : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string File { get; }
        public string FadeOut { get; }
        public string FadeIn { get; }
        public string? Channel { get; }
        public bool? Loop { get; }
        public bool IfChanged { get; }
        public string Volume { get; }

        public PlayMusic(string keyWord, IDictionary<string, object> values)
        {
            KeyWord = keyWord;
            File = RepickData.GetValue(values, "file", "");
            FadeOut = RepickData.GetValue(values, "fadeout", "None");
            FadeIn = RepickData.GetValue(values, "fadein", "0");
            Channel = RepickData.GetValue<string?>(values, "channel", null);
            Loop = RepickData.GetValue<bool?>(values, "loop", null);
            IfChanged = RepickData.GetValue(values, "if_changed", false);
            Volume = RepickData.GetValue(values, "volume", "1.0");
        }

        public void BuildTextView(TextBuilder output)
        {
            AudioHelper.PrintKeyWordChannel(output, KeyWord, Channel);
            output.Append(" ", File);
            output.Append(" fadein ", FadeIn);
            if (FadeOut != "None")
            {
                output.Append(" fadeout ", FadeOut);
            }
            if (Loop != null)
            {
                if (Loop == true)
                {
                    output.Append(" loop");
                }
                else
                {
                    output.Append(" noloop");
                }
            }
            if (IfChanged) output.Append(" if_changed");
            output.AppendLine(" volume ", Volume);
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }

    [UserStatement("queue music")]
    [UserStatement("queue sound")]
    [UserStatement("queue")]
    public class QueueMusic : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string File { get; }
        public string FadeIn { get; }
        public string? Channel { get; }
        public bool? Loop { get; }
        public string Volume { get; }

        public QueueMusic(string keyWord, IDictionary<string, object> values)
        {
            KeyWord = keyWord;
            File = RepickData.GetValue(values, "file", "");
            FadeIn = RepickData.GetValue(values, "fadein", "0");
            Channel = RepickData.GetValue<string?>(values, "channel", null);
            Loop = RepickData.GetValue<bool?>(values, "loop", null);
            Volume = RepickData.GetValue(values, "volume", "1.0");
        }

        public void BuildTextView(TextBuilder output)
        {
            AudioHelper.PrintKeyWordChannel(output, KeyWord, Channel);
            output.Append(" ", File);
            output.Append(" fadein ", FadeIn);
            if (Loop != null)
            {
                if (Loop == true)
                {
                    output.Append(" loop");
                }
                else
                {
                    output.Append(" noloop");
                }
            }
            output.AppendLine(" volume ", Volume);
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }

    [UserStatement("stop music")]
    [UserStatement("stop sound")]
    [UserStatement("stop")]
    public class StopMusic : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string FadeOut { get; }
        public string? Channel { get; }

        public StopMusic(string keyWord, IDictionary<string, object> values)
        {
            KeyWord = keyWord;
            FadeOut = RepickData.GetValue(values, "fadeout", "None");
            Channel = RepickData.GetValue<string?>(values, "channel", null);
        }

        public void BuildTextView(TextBuilder output)
        {
            AudioHelper.PrintKeyWordChannel(output, KeyWord, Channel);
            if (FadeOut != "None")
            {
                output.Append(" fadeout ", FadeOut);
            }
            output.AppendLine();
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }

    internal static class AudioHelper
    {
        internal static void PrintKeyWordChannel(TextBuilder output, string keyWord, string? channel)
        {
            output.Append(keyWord);
            if (channel != null)
            {
                if (keyWord.Contains("music") || keyWord.Contains("sound"))
                {
                    output.Append(" channel ", channel);
                }
                else
                {
                    // Match opening quote with the closing quote
                    var strip = channel.IndexOf(channel[^1]) + 1;
                    // Strip quotes and any other stuff around channel name
                    var name = channel[strip..^1];
                    output.Append(" ", name);
                }
            }
        }
    }
}
