﻿namespace RenPy.Files.Script.Objects.UserStatements
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class UserStatementAttribute : Attribute
    {
        public string KeyWord { get; }
        public UserStatementAttribute(string keyWord)
        {
            KeyWord = keyWord;
        }
    }
}
