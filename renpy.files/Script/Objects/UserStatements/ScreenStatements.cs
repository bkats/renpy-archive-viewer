﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Internal;

namespace RenPy.Files.Script.Objects.UserStatements
{
    [UserStatement("show screen")]
    [UserStatement("call screen")]
    public class ScreenShowCall : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string Name { get; }
        public ArgumentInfo? Arguments { get; }
        public bool Predict { get; }
        public string? Transition { get; }

        public ScreenShowCall(string keyWord, IDictionary<string, object> values)
        {
            KeyWord = keyWord;
            Name = RepickData.GetValue(values, "name", "");
            Arguments = RepickData.GetValue<ArgumentInfo?>(values, "arguments", null);
            Predict = RepickData.GetValue(values, "predict", true);
            Transition = RepickData.GetValue<string?>(values, "transition_expr", null);
        }

        public void BuildTextView(TextBuilder output)
        {
            output.Append($"{KeyWord}");
            var match = Regs.MatchName().Match(Name);
            if (match.Success)
            {
                var type = ReferrerType.Show;
                if (KeyWord.StartsWith("call"))
                {
                    type = ReferrerType.Call;
                }
                var screen = match.Groups["name"].Value;
                var name = new ReferrerLocation(type, screen, Parent?.Location ?? LocationInfo.Unknown, DefinitionType.Screen);
                output.Append(" ", name, match.Groups["remaining"].Value);
            }
            else
            {
                output.Append(" ", Name);
            }
            Arguments?.BuildText(output);
            if (Transition != null)
            {
                var trans = new ReferrerLocation(ReferrerType.Show, Transition, Parent?.Location ?? LocationInfo.Unknown, DefinitionType.Transform);
                output.Append($" with ", trans);
            }
            output.AppendLine();
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }

    [UserStatement("hide screen")]
    public class ScreenHide : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string Name { get; }
        public string? Transition { get; }

        public ScreenHide(string keyWord, IDictionary<string, object> values)
        {
            KeyWord = keyWord;
            Name = RepickData.GetValue(values, "name", "");
            Transition = RepickData.GetValue<string?>(values, "transition_expr", null);
        }

        public void BuildTextView(TextBuilder output)
        {
            output.Append($"{KeyWord}");
            var match = Regs.MatchName().Match(Name);
            if (match.Success)
            {
                var screen = match.Groups["name"].Value;
                var name = new ReferrerLocation(ReferrerType.Hide, screen, Parent?.Location ?? LocationInfo.Unknown, DefinitionType.Screen);
                output.Append(" ", name, match.Groups["remaining"].Value);
            }
            else
            {
                output.Append(" ", Name);
            }
            if (Transition != null)
            {
                var trans = new ReferrerLocation(ReferrerType.Show, Transition, Parent?.Location ?? LocationInfo.Unknown, DefinitionType.Transform);
                output.Append($" with ", trans);
            }
            output.AppendLine();
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }
}
