﻿using RenPy.Files.Shared.Internal;
using System.Reflection;

namespace RenPy.Files.Script.Objects.UserStatements
{
    internal static class UserStatementCollection
    {
        private static readonly Dictionary<string, Type> userStatements = new();
        static UserStatementCollection()
        {
            TypeSearch.Search(type =>
            {
                if (typeof(IUserStatement).IsAssignableFrom(type))
                {
                    var attributes = type.GetCustomAttributes<UserStatementAttribute>();
                    foreach (var attribute in attributes)
                    {
                        userStatements.Add(attribute.KeyWord, type);
                    }
                }
            });
        }

        internal static IUserStatement? CreateStatement(string keyWord, object? parseData)
        {
            if (userStatements.TryGetValue(keyWord, out var type))
            {
                object? o;
                if (parseData == null)
                {
                    o = Activator.CreateInstance(type, new object[] { keyWord });
                }
                else
                {
                    o = Activator.CreateInstance(type, new object[] { keyWord, parseData });
                }
                if (o is IUserStatement stm)
                {
                    return stm;
                }
            }
            return null;
        }
    }
}
