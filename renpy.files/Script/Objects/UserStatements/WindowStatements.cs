﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.UserStatements
{
    [UserStatement("window show")]
    [UserStatement("window hide")]
    public class WindowShowHide : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string? Transition { get; }

        public WindowShowHide(string keyWord)
        {
            KeyWord = keyWord;
        }

        public WindowShowHide(string keyWord, PyExpr transition) : this(keyWord)
        {
            Transition = transition;
        }

        public void BuildTextView(TextBuilder output)
        {
            if (Transition != null)
            {
                output.AppendLine($"{KeyWord} {Transition}");
            }
            else
            {
                output.AppendLine($"{KeyWord}");
            }
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }

    [UserStatement("window auto")]
    public class WindowAuto : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string Action { get; }
        public string? Transition { get; }

        public WindowAuto(string keyWord, IDictionary<string, object> values)
        {
            KeyWord = keyWord;
            Action = values.Keys.FirstOrDefault()?.ToString() ?? "?shatter?";

            object? t = values.Values.FirstOrDefault();
            if (t is not bool)
            {
                Transition = t?.ToString();
            }
        }

        public void BuildTextView(TextBuilder output)
        {
            if (Transition != null)
            {
                output.AppendLine($"{KeyWord} {Transition}");
            }
            else
            {
                output.AppendLine($"{KeyWord}");
            }
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }
}
