﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.UserStatements.LayeredImage
{
    [RenPyClass("store.layeredimage", "RawAlways")]
    public class RawAlways : PythonPickleBaseClass, ILayerGroup
    {
        public LayerGroupType LayerGroupType => LayerGroupType.Always;
        public PyExpr Image { get; protected set; } = new();
        public IReadOnlyDictionary<string, object>? Properties { get; protected set; }

        public void BuildTextView(TextBuilder output)
        {
            output.AppendLine("always:");
            var indent = output.Indent;
            if (Properties != null)
            {
                foreach (var item in Properties)
                {
                    indent.AppendLine($"{item.Key} = {item.Value}");
                }
            }
            indent.AppendLine(Image);
        }
    }
}
