﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.UserStatements.LayeredImage
{
    [RenPyClass("store.layeredimage", "RawAttributeGroup")]
    public class RawAttributeGroup : PythonPickleBaseClass, ILayerGroup
    {
        public LayerGroupType LayerGroupType => LayerGroupType.Attribute;
        public string Group { get; protected set; } = string.Empty;

        [RenPyFieldName("image_name")]
        public string ImageName { get; protected set; } = string.Empty;
        public IReadOnlyDictionary<string, object>? Properties { get; protected set; }
        public IReadOnlyList<RawAttribute>? Children { get; protected set; }

        public void BuildTextView(TextBuilder output)
        {
            output.AppendLine($"group {Group}:");
            var indent = output.Indent;
            if (Properties != null)
            {
                foreach (var item in Properties)
                {
                    indent.AppendLine($"{item.Key} = {item.Value}");
                }
            }
            if (Children != null)
            {
                foreach (var item in Children)
                {
                    item.BuildTextView(indent);
                }
            }
        }
    }
}
