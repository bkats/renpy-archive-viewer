﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.UserStatements.LayeredImage
{
    public enum LayerGroupType
    {
        Always,
        Attribute,
        Conditions,
    }

    public interface ILayerGroup
    {
        LayerGroupType LayerGroupType { get; }
        void BuildTextView(TextBuilder output);
    }
}
