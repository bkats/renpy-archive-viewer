﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.UserStatements.LayeredImage
{
    [RenPyClass("store.layeredimage", "RawCondition")]
    public class RawCondition : PythonPickleBaseClass
    {
        public PyExpr Condition { get; protected set; } = new();
        public PyExpr Image { get; protected set; } = new();
        public IReadOnlyDictionary<string, object>? Properties { get; protected set; }

        public void BuildTextView(TextBuilder output)
        {
            output.AppendLine($"if {Condition}:");
            var indent = output.Indent;
            indent.AppendLine(Image);
            if (Properties != null)
            {
                foreach (var item in Properties)
                {
                    indent.AppendLine($"{item.Key} = {item.Value}");
                }
            }
        }
    }
}
