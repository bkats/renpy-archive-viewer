﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.UserStatements.LayeredImage
{
    [RenPyClass("store.layeredimage", "RawConditionGroup")]
    public class RawConditionGroup : PythonPickleBaseClass, ILayerGroup
    {
        public LayerGroupType LayerGroupType => LayerGroupType.Conditions;
        public IReadOnlyList<RawCondition>? Conditions { get; protected set; }

        public void BuildTextView(TextBuilder output)
        {
            if (Conditions != null)
            {
                foreach (var condition in Conditions)
                {
                    condition.BuildTextView(output);
                }
            }
        }
    }
}
