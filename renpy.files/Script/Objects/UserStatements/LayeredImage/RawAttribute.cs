﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.UserStatements.LayeredImage
{
    [RenPyClass("store.layeredimage", "RawAttribute")]
    public class RawAttribute : PythonPickleBaseClass
    {
        public string Name { get; protected set; } = string.Empty;
        public PyExpr? Image { get; protected set; }
        public IReadOnlyDictionary<string, object>? Properties { get; protected set; }

        public void BuildTextView(TextBuilder output)
        {
            output.Append($"attribute {Name}");
            if (Properties != null)
            {
                foreach (var item in Properties)
                {
                    if (item.Key != "default")
                    {
                        output.Append($" {item.Key} {item.Value}");
                    }
                    else
                    {
                        output.Append(" default");
                    }
                }
            }
            if (Image != null)
            {
                output.AppendLine(":");
                var indent = output.Indent;
                indent.AppendLine(Image);
            }
            else
            {
                output.AppendLine();
            }
        }
    }
}