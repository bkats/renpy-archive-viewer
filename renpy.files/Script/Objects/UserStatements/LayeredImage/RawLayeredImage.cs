﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.UserStatements.LayeredImage
{
    [RenPyClass("store.layeredimage", "RawLayeredImage")]
    public class RawLayeredImage : PythonPickleBaseClass, IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord => "layeredimage";
        public string Name { get; protected set; } = string.Empty;
        public IReadOnlyDictionary<string, object>? Properties { get; protected set; }
        public IReadOnlyList<ILayerGroup>? Children { get; protected set; }

        public void BuildTextView(TextBuilder output)
        {
            var def = new DefinitionLocation(DefinitionType.Image, Name, Parent?.Location ?? LocationInfo.Unknown);
            output.AppendLine($"{KeyWord} ", def, ":");
            if (Children != null)
            {
                foreach (var condition in Children)
                {
                    condition.BuildTextView(output.Indent);
                }
            }
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }
}
