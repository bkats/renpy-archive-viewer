﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.UserStatements
{
    [UserStatement("nvl show")]
    [UserStatement("nvl hide")]
    public class NvlShowHide : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }
        public string? Transition { get; }


        public NvlShowHide(string keyWord)
        {
            KeyWord = keyWord;
        }

        public NvlShowHide(string keyWord, string transition) : this(keyWord)
        {
            Transition = transition;
        }

        public NvlShowHide(string keyWord, PyExpr transition) : this(keyWord)
        {
            Transition = transition;
        }

        public void BuildTextView(TextBuilder output)
        {
            if ((Transition == null) || (Transition == "None"))
            {
                output.AppendLine(KeyWord);
            }
            else
            {
                output.AppendLine($"{KeyWord} {Transition}");
            }
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }

    [UserStatement("nvl clear")]
    public class NvlClear : IUserStatement
    {
        public NodeUserStatement? Parent { get; private set; }
        public string KeyWord { get; }

        public NvlClear(string keyWord)
        {
            KeyWord = keyWord;
        }

        public void BuildTextView(TextBuilder output)
        {
            output.AppendLine(KeyWord);
        }

        public void SetParent(NodeUserStatement parent)
        {
            Parent = parent;
        }
    }
}