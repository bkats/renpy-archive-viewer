﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.UserStatements
{
    public interface IUserStatement
    {
        public NodeUserStatement? Parent { get; }
        public string KeyWord { get; }
        public void SetParent(NodeUserStatement parent);
        public void BuildTextView(TextBuilder output);
    }
}
