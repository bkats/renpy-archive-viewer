﻿using RenPy.Files.Shared.Attributes;
using System.Collections;

namespace RenPy.Files.Script.Objects
{
    [RenPyClass("collections", "OrderedDict")]
    public class OrderedDict : Hashtable
    {
        public OrderedDict() { }
        public OrderedDict(ArrayList items)
        {
            foreach (var item in items)
            {
                if (item is ArrayList pair)
                {
                    var key = pair[0]?.ToString() ?? "<unknown>";
                    var value = pair[1];
                    Add(key, value);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        public void SetState(Hashtable state)
        {
            // Note: Items of ArrayList get added by unpickle.
            // So this functions will get no items (they are already added)
            if (state.Count > 0)
            {
                throw new Exception("TODO implement??");
            }
        }

    }
}
