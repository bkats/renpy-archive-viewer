﻿using RenPy.Files.Shared.Attributes;
using System.Collections;

namespace RenPy.Files.Script.Objects
{
    [RenPyClass("renpy.python", "RevertableDict")]
    [RenPyClass("renpy.revertable", "RevertableDict")]
    public class RevertableDict : Hashtable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        public void SetState(Hashtable state)
        {
            // Note: Items of ArrayList get added by unpickle.
            // So this functions will get no items (they are already added)
            if (state.Count > 0)
            {
                throw new Exception("TODO implement??");
            }
        }

    }
}
