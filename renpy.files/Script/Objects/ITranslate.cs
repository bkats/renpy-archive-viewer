﻿namespace RenPy.Files.Script.Objects
{
    internal interface ITranslate
    {
        public string? Language { get; }
    }
}
