﻿using RenPy.Files.Script.Objects.Ast;

namespace RenPy.Files.Script.Objects
{
    public class StatementGroup : IStatementGroup
    {
        private readonly List<Node> nodes = new();

        public StatementGroup(Node start) => StatementStart = start;

        public IReadOnlyList<Node> Nodes => nodes;
        public bool HasNodes => nodes.Count > 0;
        public Node? StatementStart { get; }

        public void AddNode(Node n)
        {
            nodes.Add(n);
        }
    }
}
