﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using RenPy.Files.Shared.Internal;
using System.Text.RegularExpressions;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLBlock")]
    public class SlBlock : SlNode
    {
        public IReadOnlyList<SlNode>? Children { get; protected set; }
        public IReadOnlyDictionary<string, PyExpr>? KeyWord { get; protected set; }

        [RenPyFieldName("atl_transform")]
        public RawBlock? Atl { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (KeyWord != null)
            {
                var info = output.GetInfo<ITranslateInfo>();
                foreach (var k in KeyWord)
                {
                    output.Append(k.Key, " ");
                    var value = k.Value;
                    if (info != null)
                    {
                        var ts = info.TranslateFunctions(value);
                        value = new PyExpr(ts, value.Location);
                    }
                    var r = ProcessKeyWord(k.Key, value);
                    output.AppendLine(r);
                }
            }
            if (Children != null)
            {
                foreach (var node in Children)
                {
                    node.BuildTextView(output);
                }
            }
            if (Atl != null)
            {
                output.AppendLine("at transform:");
                Atl.BuildTextView(output.Indent);
            }
        }

        public bool IsEmpty => (KeyWord == null || KeyWord.Count == 0) && Atl == null && (Children == null || Children.Count == 0);

        private static object[] ProcessKeyWord(string key, PyExpr expr)
        {
            switch (key)
            {
                case "at":
                    var m = Regs.MatchName().Match(expr);
                    var name = m.Groups["name"].Value;
                    var remaining = m.Groups["remaining"].Value;
                    return
                    [
                        new ReferrerLocation(ReferrerType.Assignment, name, expr.Location, DefinitionType.Transform),
                        remaining
                    ];
                case "style":
                    return
                    [
                        "\"",
                        new ReferrerLocation(ReferrerType.Assignment, expr.Expression.Trim('"'), expr.Location, DefinitionType.Style),
                        "\""
                    ];
                case "action":
                case "clicked":
                    return ProcessActions(expr);
                default:
                    return [expr];
            }
        }

        private static object[] ProcessActions(PyExpr expr)
        {
            var originalText = (string)expr;
            var result = new List<object>();
            var ms = Regs.MatchAction().Matches(expr);
            var pos = 0;
            foreach (Match m in ms)
            {
                if (m.Success)
                {
                    if (pos != m.Index)
                    {
                        result.Add(originalText[pos..m.Index]);
                    }
                    ProcessSingleAction(m, result, expr.Location);
                    pos = m.Index + m.Value.Length;
                }
            }
            if (pos != originalText.Length)
            {
                result.Add(originalText[pos..]);
            }
            return [.. result];
        }

        private static void ProcessSingleAction(Match m, List<object> result, LocationInfo location)
        {
            var cmd = m.Groups["cmd"].Value;
            result.Add(cmd);
            result.Add(m.Groups["pre"].Value);

            var name = m.Groups["target"].Value;
            switch (cmd)
            {
                case "Call":
                case "Jump":
                case "Start":
                case "Replay":
                    result.Add(new ReferrerLocation(ReferrerType.Action, name, location, DefinitionType.Label) { MatchHint = m.Value });
                    break;
                case "Show":
                case "ShowMenu":
                case "Hide":
                    result.Add(new ReferrerLocation(ReferrerType.Action, name, location, DefinitionType.Screen) { MatchHint = m.Value });
                    break;
                default:
                    result.Add(name);
                    break;
            }
            result.Add(m.Groups["post"].Value);
        }

    }
}
