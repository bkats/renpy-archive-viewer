﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.Sl2
{
    public class SlNode : PythonPickleBaseClass
    {
        public long Serial { get; protected set; }
        public LocationInfo Location { get; protected set; } = LocationInfo.Unknown;

        public virtual void BuildTextView(TextBuilder output)
        {
            output.AppendLine(ToString());
        }

        public override string ToString()
        {
            return $"Screen node {this.GetType()} at {Location}";
        }
    }
}
