﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLUse")]
    public class SlUse : SlNode
    {
        public string Target { get; protected set; } = "!none!";

        [RenPyFieldName("args")]
        public ArgumentInfo? Arguments { get; protected set; }
        public SlBlock? Block { get; protected set; }
        public PyExpr? Id { get; protected set; }
        public Node? Ast { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            var name = new ReferrerLocation(ReferrerType.Assignment, Target, Location, DefinitionType.Screen);
            if (Arguments != null)
            {
                output.Append($"use ", name);
                Arguments.BuildText(output);
            }
            else
            {
                output.Append($"use ", name, "()");
            }
            if (Id != null)
            {
                output.Append(" id ", Id);
            }
            if (Block != null)
            {
                output.AppendLine(":");
                Block.BuildTextView(output.Indent);
            }
            else
            {
                output.AppendLine();
            }
        }
    }
}
