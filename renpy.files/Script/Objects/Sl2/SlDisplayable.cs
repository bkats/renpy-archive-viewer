﻿using Pickle;
using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLDisplayable")]
    public class SlDisplayable : SlBlock
    {
        public string? Name { get; protected set; }
        public bool Unique { get; protected set; }
        public IPythonType? Displayable { get; protected set; }
        public bool Scope { get; protected set; }

        [RenPyFieldName("child_or_fixed")]
        public bool ChildOrFixed { get; protected set; }
        public string? Style { get; protected set; }

        [RenPyFieldName("pass_context")]
        public bool PassContext { get; protected set; }
        public bool ImageMap { get; protected set; }
        public bool HotSpot { get; protected set; }
        public bool Replaces { get; protected set; }

        [RenPyFieldName("default_keywords")]
        public IReadOnlyDictionary<string, object>? DefaultKeywords { get; protected set; }
        public string? Variable { get; protected set; }
        public IReadOnlyList<PyExpr>? Positional { get; private set; }

        public override void BuildTextView(TextBuilder output)
        {
            var info = output.GetInfo<ITranslateInfo>();

            if (!string.IsNullOrEmpty(Name))
            {
                output.Append($"{Name}");
            }
            else
            {
                if (Displayable != null)
                {
                    output.Append(Displayables.LookUpName(Displayable.Name, Style));
                }
                else
                {
                    if (string.IsNullOrEmpty(Style))
                    {
                        output.Append($"!None!");
                    }
                    else
                    {
                        output.Append($"{Style}");
                    }
                }
            }
            if (Positional != null && Positional.Count > 0)
            {
                foreach (var p in Positional)
                {
                    var value = info?.TranslateFunctions(p) ?? p;
                    output.Append(" ", value);
                }
            }
            if (((KeyWord != null) && (KeyWord.Count > 0))
                || ((Children != null) && (Children.Count > 0))
                || !string.IsNullOrEmpty(Variable))
            {
                output.AppendLine(":");

                var indent = output.Indent;

                if (!string.IsNullOrEmpty(Variable))
                {
                    indent.AppendLine($"variable {Variable}");
                }

                base.BuildTextView(indent);
            }
            else
            {
                output.AppendLine();
            }
        }

        public override string ToString()
        {
            return $"Displayable {Style}{Displayable?.Name}";
        }
    }

    public static class Displayables
    {
        private static readonly List<(string Name, string Displayable, string? Style)> displayables = new() {
            // Name            Displayable        Style
            ( "null",         "Null",            "default" ),
            ( "text",         "Text",            "text" ),
            ( "hbox",         "MultiBox",        "hbox" ),
            ( "vbox",         "MultiBox",        "vbox" ),
            ( "fixed",        "MultiBox",        "fixed" ),
            ( "grid",         "Grid",            "grid" ),
            ( "side",         "Side",            "side" ),
            ( "window",       "Window",          "window" ),
            ( "frame",        "Window",          "frame" ),
            ( "key",          "_key",            null ),
            ( "timer",        "Timer",           "default" ),
            ( "input",        "Input",           "input" ),
            ( "button",       "Button",          "button" ),
            ( "imagebutton",  "_imagebutton",    "image_button" ),
            ( "textbutton",   "_textbutton",     "button" ),
            ( "label",        "_label",          "label" ),
            ( "bar",          "sl2bar",          null ),
            ( "vbar",         "sl2vbar",         null ),
            ( "viewport",     "sl2viewport",     "viewport" ),
            ( "vpgrid",       "sl2vpgrid",       "vpgrid" ),
            ( "imagemap",     "_imagemap",       "imagemap" ),
            ( "hotspot",      "_hotspot",        "hotspot" ),
            ( "hotbar",       "_hotbar",         "hotbar" ),
            ( "transform",    "Transform",       "transform" ),
            ( "add",          "sl2add",          null ),
            ( "drag",         "Drag",            "drag" ),
            ( "draggroup",    "DragGroup",       null ),
            ( "mousearea",    "MouseArea",       null ),
            ( "on",           "OnEvent",         null ),
        };

        public static string LookUpName(string displayable, string? style)
        {
            // Return based on displayable and style, if not found use the displayable name
            return displayables.Where(i => i.Displayable == displayable && i.Style == style)
                .Select(i => i.Name).FirstOrDefault(displayable);
        }
    }
}

/*
        `displayable`
            A function that, when called with the positional and keyword
            arguments, causes the displayable to be displayed.

        `scope`
            If true, the scope is supplied as an argument to the displayable.

        `child_or_fixed`
            If true and the number of children of this displayable is not one,
            the children are added to a Fixed, and the Fixed is added to the
            displayable.

        `style`
            The base name of the main style.

        `pass_context`
            If given, the context is passed in as the first positional argument
            of the displayable.

        `imagemap`
            True if this is an imagemap, and should be handled as one.

        `hotspot`
            True if this is a hotspot that depends on the imagemap it was
            first displayed with.

        `replaces`
            True if the object this displayable replaces should be
            passed to it.

        `default_keywords`
            The default keyword arguments to supply to the displayable.

        `variable`
            A variable that the main displayable is assigned to.
        """

        SLBlock.__init__(self, loc)

        self.displayable = displayable

        self.scope = scope
        self.child_or_fixed = child_or_fixed
        self.style = style
        self.pass_context = pass_context
        self.imagemap = imagemap
        self.hotspot = hotspot
        self.replaces = replaces
        self.default_keywords = default_keywords
        self.variable = variable

        # Positional argument expressions.
        self.positional = [ ]


 
 */
