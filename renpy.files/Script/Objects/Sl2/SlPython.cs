﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLPython")]
    public class SlPython : SlNode
    {
        public PyCode? Code { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if ((Code != null) && Code.IsSingleLine())
            {
                output.Append("$ ");
                Code.BuildTextView(output);
            }
            else
            {
                output.AppendLine("python:");
                Code?.BuildTextView(output.Indent);
            }
        }
    }
}
