﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLScreen")]
    public class SlScreen : SlBlock
    {
        public string? Name { get; protected set; }
        public ISignature? Parameters { get; protected set; }
        public string? Tag { get; protected set; }

        // Following properties are also part in the KeyWord dictionary (so they aren't displayed)
        public PyExpr Modal { get; protected set; } = new PyExpr("False");
        public PyExpr Variant { get; protected set; } = new PyExpr("None");
        public PyExpr ZOrder { get; protected set; } = new PyExpr("0");
        public PyExpr Layer { get; protected set; } = new PyExpr("'screens'");
        public PyExpr Sensitive { get; protected set; } = new PyExpr("True");

        // These are internal house hold items (so they aren't displayed)
        public PyExpr Predict { get; protected set; } = new PyExpr("None");
        public object? Analysis { get; protected set; }
        public bool Prepared { get; protected set; }
        [RenPyFieldName("roll_forward")]
        public PyExpr RollForward { get; protected set; } = new PyExpr("None");

        public override void BuildTextView(TextBuilder output)
        {
            if (string.IsNullOrEmpty(Name))
            {
                output.Append("screen:");
            }
            else
            {
                var name = new DefinitionLocation(DefinitionType.Screen, Name, Location);
                output.Append("screen ", name);
                Parameters?.BuildText(output);
                output.Append(":");
            }
            output.AppendLine();
            var indent = output.Indent;
            if (!string.IsNullOrEmpty(Tag))
            {
                indent.AppendLine("tag ", Tag);
            }
            base.BuildTextView(indent);
        }

        public override string ToString()
        {
            if (Parameters != null)
            {
                return $"screen {Name}({Parameters}):";
            }
            if (!string.IsNullOrEmpty(Name))
            {
                return $"screen {Name}:";
            }
            return "screen:";
        }
    }
}

/*
 *    SLScreen : SLBlock
        # The name of the screen.
        self.name = None

        # Should this screen be declared as modal?
        self.modal = "False"

        # The screen's zorder.
        self.zorder = "0"

        # The screen's tag.
        self.tag = None

        # The variant of screen we're defining.
        self.variant = "None" # expr.

        # Should we predict this screen?
        self.predict = "None" # expr.

        # Should this screen be sensitive.
        self.sensitive = "True"

        # The parameters this screen takes.
        self.parameters = None
*/