﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLFor")]
    public class SlFor : SlBlock
    {
        public string Variable { get; protected set; } = "!none!";
        public PyExpr? Expression { get; protected set; }

        [RenPyFieldName("index_expression")]
        public string? IndexExpression { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.AppendLine(ToString());
            base.BuildTextView(output.Indent);
        }

        public override string ToString()
        {
            return $"for {Variable} in {Expression}:";
        }
    }
}

/*
        self.variable = variable
        self.expression = expression
        self.index_expression = index_expression
*/