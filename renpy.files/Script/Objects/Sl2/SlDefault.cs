﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLDefault")]
    public class SlDefault : SlNode
    {
        public string Variable { get; protected set; } = string.Empty;
        public PyExpr Expression { get; protected set; } = new PyExpr("!none!");

        public override void BuildTextView(TextBuilder output)
        {
            var info = output.GetInfo<ITranslateInfo>();
            var dv = new DefinitionLocation(DefinitionType.Default, Variable, Location);
            output.Append("default ", dv, " = ");
            if (info != null)
            {
                output.AppendLine(info.TranslateFunctions(Expression));
            }
            else
            {
                output.AppendLine(Expression);
            }
        }

        public override string ToString()
        {
            return $"default {Variable} = {Expression}";
        }
    }
}
