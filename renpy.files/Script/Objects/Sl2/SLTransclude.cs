﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLTransclude")]
    public class SlTransclude : SlNode
    {
        public override void BuildTextView(TextBuilder output)
        {
            output.AppendLine("transclude");
        }
    }
}
