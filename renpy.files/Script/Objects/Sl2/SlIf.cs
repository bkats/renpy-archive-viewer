﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Sl2
{
    [RenPyClass("renpy.sl2.slast", "SLIf")]
    public class SlIf : SlNode
    {
        protected bool showIf = false;

        public IReadOnlyList<SlIfEntry>? Entries { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Entries != null)
            {
                string nameIf = showIf ? "showif" : "if";
                foreach (var entry in Entries)
                {
                    entry.BuildTextView(output, nameIf);
                    nameIf = "elif";
                }
            }
        }
    }

    [RenPyClass("renpy.sl2.slast", "SLShowIf")]
    public class SlShowIf : SlIf
    {
        public SlShowIf() { showIf = true; }
    }

    public class SlIfEntry : IPythonSetState
    {
        public PyExpr? Condition { get; private set; }
        public SlBlock Block { get; private set; } = new();

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] conditionBlock)
            {
                if (conditionBlock[1] is SlBlock nodes)
                {
                    Condition = conditionBlock[0] as PyExpr;
                    Block = nodes;
                }
            }
        }

        public void BuildTextView(TextBuilder output, string nameIf)
        {
            if (Condition != null)
            {
                output.AppendLine($"{nameIf} {Condition}:");
            }
            else
            {
                output.AppendLine("else:");
            }
            if (Block.IsEmpty)
            {
                output.Indent.AppendLine("pass");
            }
            else
            {
                Block.BuildTextView(output.Indent);
            }
        }
    }
}
