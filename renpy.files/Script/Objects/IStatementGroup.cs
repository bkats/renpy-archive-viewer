﻿using RenPy.Files.Script.Objects.Ast;

namespace RenPy.Files.Script.Objects
{
    public interface IStatementGroup
    {
        public IReadOnlyList<Node> Nodes { get; }
    }
}
