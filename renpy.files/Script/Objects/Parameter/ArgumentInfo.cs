﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Parameter
{
    [RenPyClass("renpy.ast", "ArgumentInfo")]
    [RenPyClass("renpy.parameter", "ArgumentInfo")]
    public class ArgumentInfo : PythonPickleBaseClass
    {
        public IReadOnlyList<NameValuePair>? Arguments { get; protected set; }

        public PyExpr? ExtraPos { get; protected set; }
        public PyExpr? ExtraKw { get; protected set; }


        [RenPyFieldName("starred_indexes")]
        public IReadOnlyList<int>? StarredIndexes { get; protected set; }

        [RenPyFieldName("doublestarred_indexes")]
        public IReadOnlyList<int>? DoubleStarredIndexes { get; protected set; }

        [RenPyFieldName("__version__")]
        public int Version { get; protected set; }

        public override string ToString()
        {
            return GetCode();
        }

        public void BuildText(TextBuilder output)
        {
            var info = output.GetInfo<ITranslateInfo>();
            output.Append("(");
            var l = new List<string>();
            if (Arguments != null)
            {
                var index = 0;
                foreach (var arg in Arguments)
                {
                    if (arg.Value != null)
                    {
                        var value = info?.TranslateFunctions(arg.Value) ?? arg.Value;
                        if (arg.Name != null)
                        {
                            l.Add($"{arg.Name} = {value}");
                        }
                        else
                        {
                            AddValue(index, l, value);
                        }
                    }
                    index++;
                }
            }
            if (ExtraPos != null)
            {
                l.Add($"*{ExtraPos}");
            }
            if (ExtraKw != null)
            {
                l.Add($"**{ExtraKw}");
            }
            output.Append(string.Join(", ", l));
            output.Append(")");
        }

        public string GetCode()
        {
            var l = new List<string>();
            if (Arguments != null)
            {
                var index = 0;
                foreach (var arg in Arguments)
                {
                    if (arg.Name != null)
                    {
                        l.Add($"{arg.Name}={arg.Value}");
                    }
                    else
                    {
                        AddValue(index, l, arg.Value ?? string.Empty);
                    }
                    index++;
                }
            }
            if (ExtraPos != null)
            {
                l.Add($"*{ExtraPos}");
            }
            if (ExtraKw != null)
            {
                l.Add($"**{ExtraKw}");
            }
            return $"({string.Join(", ", l)})";
        }

        private void AddValue(int index, List<string> list, string v)
        {
            if (StarredIndexes != null && StarredIndexes.Contains(index))
            {
                list.Add($"*{v}");
            }
            else
            {
                if (DoubleStarredIndexes != null
                    && DoubleStarredIndexes.Contains(index))
                {
                    list.Add($"**{v}");
                }
                else
                {
                    list.Add(v);
                }
            }
        }
    }
}
