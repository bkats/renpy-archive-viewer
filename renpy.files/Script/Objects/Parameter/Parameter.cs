﻿using RenPy.Files.Shared.Attributes;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.Parameter
{
    [RenPyClass("renpy.parameter", "Parameter")]
    public class Parameter : PythonPickleBaseClass
    {
        public enum ParaType {
            POSITIONAL_ONLY,
            POSITIONAL_OR_KEYWORD, 
            VAR_POSITIONAL, 
            KEYWORD_ONLY, 
            VAR_KEYWORD
        }
        public string Name { get; protected set; } = string.Empty;
        public ParaType Kind { get; protected set; }
        public string? Default { get; protected set; }
    }
}
