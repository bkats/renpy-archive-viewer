﻿using RenPy.Files.Shared.Attributes;
using RenPy.Files.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RenPy.Files.Script.Objects.Parameter
{
    [RenPyClass("renpy.parameter", "Signature")]
    public class Signature : PythonPickleBaseClass, ISignature
    {
        public IReadOnlyDictionary<string, Parameter>? Parameters { get; set; }

        public void BuildText(TextBuilder output)
        {
            var info = output.GetInfo<ITranslateInfo>();
            var l = new List<string>();
            if (Parameters != null)
            {
                foreach (var arg in Parameters.Values)
                {
                    if (arg.Default != null)
                    {
                        var value = info?.TranslateFunctions(arg.Default) ?? arg.Default;
                        l.Add($"{arg.Name} = {value}");
                    }
                    else
                    {
                        switch (arg.Kind)
                        {
                            case Parameter.ParaType.VAR_POSITIONAL:
                                l.Add($"*{arg.Name}");
                                break;
                            case Parameter.ParaType.VAR_KEYWORD:
                                l.Add($"**{arg.Name}");
                                break;
                            default:
                                l.Add($"{arg.Name}");
                                break;
                        }
                    }
                }
            }
            output.Append("(");
            output.Append(string.Join(", ", l));
            output.Append(")");
        }
    }
}
