﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.Parameter
{
    public interface ISignature
    {
        void BuildText(TextBuilder output);
    }
}
