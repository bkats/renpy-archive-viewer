﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Parameter
{
    [RenPyClass("renpy.ast", "ParameterInfo")]
    public class ParameterInfo : PythonPickleBaseClass, ISignature
    {
        public IReadOnlyList<NameValuePair>? Parameters { get; protected set; }

        public IReadOnlyList<string>? Positional { get; protected set; }

        public PyExpr? ExtraPos { get; protected set; }

        public PyExpr? ExtraKw { get; protected set; }

        [RenPyFieldName("keyword_only")]
        public IReadOnlyList<NameValuePair>? KeywordOnly { get; protected set; }

        [RenPyFieldName("positional_only")]
        public IReadOnlyList<NameValuePair>? PositionalOnly { get; protected set; }

        public override string ToString()
        {
            var l = new List<string>();
            if (Parameters != null)
            {
                foreach (var arg in Parameters)
                {
                    if (arg.Value != null)
                    {
                        l.Add($"{arg.Name}={arg.Value}");
                    }
                    else
                    {
                        l.Add(arg.Name ?? string.Empty);
                    }
                }
            }
            if (ExtraPos != null)
            {
                l.Add("*" + ExtraPos);
            }
            if (ExtraKw != null)
            {
                l.Add("**" + ExtraKw);
            }
            return $"({string.Join(", ", l)})";
        }

        public void BuildText(TextBuilder output)
        {
            var info = output.GetInfo<ITranslateInfo>();
            output.Append("(");
            var l = new List<string>();
            if (Parameters != null)
            {
                foreach (var arg in Parameters)
                {
                    if (arg.Name != null)
                    {
                        if (arg.Value != null)
                        {
                            var value = info?.TranslateFunctions(arg.Value) ?? arg.Value;
                            l.Add($"{arg.Name} = {value}");
                        }
                        else
                        {
                            l.Add(arg.Name);
                        }
                    }
                }
            }
            if (ExtraPos != null)
            {
                l.Add($"*{ExtraPos}");
            }
            if (ExtraKw != null)
            {
                l.Add($"**{ExtraKw}");
            }
            output.Append(string.Join(", ", l));
            output.Append(")");
        }

    }
}
