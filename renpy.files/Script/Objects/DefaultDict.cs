﻿using Pickle;
using RenPy.Files.Shared.Attributes;
using System.Collections;

namespace RenPy.Files.SaveFile.Objects
{
    [RenPyClass("collections", "defaultdict")]
    public class DefaultDict : Hashtable
    {
        public IPythonType DefaultType { get; }
        public DefaultDict(IPythonType defaultType)
        {
            DefaultType = defaultType;
        }
    }
}
