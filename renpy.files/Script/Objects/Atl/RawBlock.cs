﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawBlock")]
    public class RawBlock : RawStatement
    {
        public IReadOnlyList<RawStatement>? Statements { get; protected set; }
        public bool Animation { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Animation)
            {
                output.AppendLine("animation");
            }
            if (Statements != null)
            {
                Type? stType = null;
                foreach (RawStatement statement in Statements)
                {
                    if ((stType != null)
                        && (stType != typeof(RawMultipurpose))
                        && (statement.GetType() == stType))
                    {
                        output.AppendLine("pass");
                    }
                    if (statement is RawBlock block)
                    {
                        output.AppendLine("block:");
                        block.BuildTextView(output.Indent);
                    }
                    else
                    {
                        statement.BuildTextView(output);
                    }
                    stType = statement.GetType();
                }
            }
        }

        public override string ToString()
        {
            return "ATL Raw block";
        }
    }
}
