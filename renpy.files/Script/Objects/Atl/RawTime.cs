﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawTime")]
    public class RawTime : RawStatement
    {
        public PyExpr? Time { get; protected set; }

        public override string ToString()
        {
            return $"time {Time}";
        }
    }
}
