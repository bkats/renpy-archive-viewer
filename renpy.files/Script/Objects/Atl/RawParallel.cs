﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawParallel")]
    public class RawParallel : RawStatement
    {
        public IReadOnlyList<RawStatement>? Blocks { get; protected set; }
        public bool Animation { get; private set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Blocks != null)
            {
                foreach (var bl in Blocks)
                {
                    output.AppendLine("parallel:");
                    bl.BuildTextView(output.Indent);
                }
            }
        }

        public override string ToString()
        {
            return "parallel blocks";
        }
    }
}
