﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawRepeat")]
    public class RawRepeat : RawStatement
    {
        public PyExpr Repeats { get; protected set; } = new PyExpr();

        public override string ToString()
        {
            if (Repeats.Expression != "0")
            {
                return $"repeat {Repeats}";
            }
            else
            {
                return $"repeat";
            }
        }
    }
}
