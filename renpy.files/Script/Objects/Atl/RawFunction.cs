﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawFunction")]
    public class RawFunction : RawStatement
    {
        [RenPyFieldName("expr")]
        public PyExpr? Expression { get; protected set; }

        public override string ToString()
        {
            return $"function {Expression}";
        }
    }
}
