﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawChoice")]
    public class RawChoice : RawStatement
    {
        public IReadOnlyList<Choice>? Choices { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Choices != null)
            {
                foreach (var choice in Choices)
                {
                    output.AppendLine($"choice {choice.Chance}:");
                    choice.Statement?.BuildTextView(output.Indent);
                }
            }
        }

        public override string ToString()
        {
            return "choice";
        }
    }

    public class Choice : IPythonSetState
    {
        public RawStatement? Statement { get; private set; }
        public string? Chance { get; private set; }

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] c)
            {
                if (c[1] is RawStatement block)
                {
                    Chance = c[0].ToString();
                    Statement = block;
                }
            }
        }
    }
}
