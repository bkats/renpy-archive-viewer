﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    public class RawStatement : PythonPickleBaseClass
    {
        [RenPyFieldName("loc")]
        public LocationInfo Location { get; protected set; } = LocationInfo.Unknown;

        public virtual void BuildTextView(TextBuilder output)
        {
            output.AppendLine(ToString());
        }

        public override string ToString()
        {
            return "<Raw Statement>";
        }
    }
}
