﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawContainsExpr")]
    public class RawContainsExpr : RawStatement
    {
        public PyExpr? Expression { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Expression != null)
            {
                var expr = new ReferrerLocation(ReferrerType.Assignment, Expression, Expression.Location, DefinitionType.Transform);
                output.AppendLine("contains ", expr);
            }
            else
            {
                output.AppendLine("contains <missing>");
            }
        }
        public override string ToString()
        {
            return $"contains {Expression}";
        }
    }
}
