﻿using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawEvent")]
    public class RawEvent : RawStatement
    {
        public string Name { get; protected set; } = string.Empty;

        public override string ToString()
        {
            return $"event {Name}";
        }
    }
}
