﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using System.Collections;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawMultipurpose")]
    public class RawMultipurpose : RawStatement
    {
        public string? Warper { get; protected set; }
        [RenPyFieldName("warp_function")]
        public PyExpr? WarpFunction { get; protected set; }
        public PyExpr? Duration { get; protected set; }
        public IReadOnlyList<NameValuePair>? Properties { get; protected set; }
        public IReadOnlyList<ExpressionWith>? Expressions { get; protected set; }
        public IReadOnlyList<NameExpressions>? Splines { get; protected set; }
        public string? Revolution { get; protected set; }
        public PyExpr? Circles { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (!string.IsNullOrEmpty(Warper))
            {
                DislayWarper(output, Warper);
            }
            else
            {
                if (WarpFunction != null)
                {
                    DislayWarper(output, $"warp {WarpFunction}");
                }
                else
                {
                    BuildExpersionAndProperties(output);
                    output.AppendLine();
                }
            }
        }

        private void DislayWarper(TextBuilder output, string warper)
        {
            output.Append(warper);
            if (Duration != null)
            {
                output.Append(" ", Duration);
            }
            BuildExpersionAndProperties(output);
            if (Splines != null && Splines.Count > 0)
            {
                foreach (var sp in Splines)
                {
                    output.Append(" ", sp);
                }
            }
            if (!string.IsNullOrEmpty(Revolution))
            {
                output.Append(" ", Revolution);
            }
            if (Circles != null && Circles != "0")
            {
                output.Append(" circles ", Circles);
            }
            output.AppendLine();
        }

        private void BuildExpersionAndProperties(TextBuilder output)
        {
            if (Expressions != null && Expressions.Count > 0)
            {
                var expr = string.Join(' ', Expressions);
                var info = output.GetInfo<ITranslateInfo>();
                if (info != null)
                {
                    expr = info.TranslateFunctions(expr);
                }
                output.Append(expr);
            }
            if (Properties != null && Properties.Count > 0)
            {
                output.Append(string.Join(' ', Properties));
            }
        }

        public override string ToString()
        {
            return "ATL RawMultipurpose";
        }
    }

    public class ExpressionWith : IPythonSetState
    {
        public string Expression { get; private set; } = string.Empty;
        public string With { get; private set; } = string.Empty;

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] values)
            {
                if (values[0] is PyExpr expr)
                {
                    Expression = expr;
                }
                if (values[1] is PyExpr with)
                {
                    With = with;
                }
            }
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(With))
            {
                return $"{Expression}";
            }
            return $"{Expression} with {With}";
        }
    }

    public class NameExpressions : IPythonSetState
    {
        public string Name { get; private set; } = string.Empty;
        public List<PyExpr>? Expressions { get; private set; }

        void IPythonSetState.SetState(object state)
        {
            if (state is Object[] nameExprs)
            {
                Name = nameExprs[0].ToString() ?? "None?";
                if (nameExprs[1] is ArrayList exprs)
                {
                    Expressions = exprs.Cast<PyExpr>().ToList();
                    // Move last item to front (Ren'Py parser adds first expression at the end of the list)
                    var last = Expressions.Last();
                    Expressions.Remove(last);
                    Expressions.Insert(0, last);
                }
            }
        }

        public override string ToString()
        {
            if (Expressions != null)
            {
                var exprs = string.Join(" knot ", Expressions);
                return $"{Name} {exprs}";
            }
            return Name;
        }
    }
}
