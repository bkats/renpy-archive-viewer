﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawOn")]
    public class RawOn : RawStatement
    {
        public IReadOnlyDictionary<string, RawStatement>? Handlers { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Handlers != null)
            {
                foreach (var handler in Handlers)
                {
                    output.AppendLine($"on {handler.Key}:");
                    handler.Value.BuildTextView(output.Indent);
                }
            }
        }

        public override string ToString()
        {
            return "on events";
        }
    }
}
