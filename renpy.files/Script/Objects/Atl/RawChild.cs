﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Atl
{
    [RenPyClass("renpy.atl", "RawChild")]
    public class RawChild : RawStatement
    {
        public IReadOnlyList<RawStatement>? Children { get; protected set; }
        public bool Animation { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Children != null)
            {
                foreach (var child in Children)
                {
                    output.AppendLine("contains:");
                    child.BuildTextView(output.Indent);
                }
            }
        }

        public override string ToString()
        {
            return "contains: <block>";
        }
    }
}
