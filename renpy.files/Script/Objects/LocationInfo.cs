﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects
{
    public class LocationInfo : IPythonSetState
    {
        public static readonly LocationInfo Unknown = new("<unknown>", 0);

        public string Source { get; private set; }
        public int LineNumber { get; private set; }
        public uint Hash { get; private set; }

        public LocationInfo()
        {
            Source = Unknown.Source;
        }

        public LocationInfo(string src, int line)
        {
            Source = src.Replace('\\', '/');
            LineNumber = line;
        }

        public LocationInfo(object[] state) : this()
        {
            GetValues(state);
        }

        private void GetValues(object[] values)
        {
            if (values.Length >= 2)
            {
                Source = ((string)values[0]).Replace('\\', '/');
                LineNumber = Convert.ToInt32(values[1]);
            }
            else
            {
                Source = Unknown.Source;
            }
            if (values.Length > 2)
            {
                Hash = Convert.ToUInt32(values[2]);
            }
        }

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] values)
            {
                GetValues(values);
            }
        }

        public override string ToString()
        {
            return $"{Source} Line:{LineNumber}";
        }
    }
}
