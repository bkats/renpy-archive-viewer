﻿using RenPy.Files.Script.Objects.Ast;

namespace RenPy.Files.Script.Objects
{
    public class WithPaired
    {
        public NodeWith CurrentWith { get; }
        public bool IsHandled { get; set; }

        public WithPaired(NodeWith currentWith)
        {
            CurrentWith = currentWith;
        }
    }
}