﻿using RenPy.Files.Shared.Attributes;
using System.Collections;

namespace RenPy.Files.Script.Objects
{
    [RenPyClass("renpy.python", "RevertableSet")]
    [RenPyClass("renpy.revertable", "RevertableSet")]
    public class RevertableSet : HashSet<string>
    {
        public void SetState(object[] state)
        {
            if (state[0] is Hashtable d)
            {
                foreach (string key in d.Keys)
                {
                    Add(key);
                }
            }
        }

    }
}
