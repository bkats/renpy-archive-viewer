﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Default")]
    public class NodeDefault : Node
    {
        [RenPyFieldName("varname")]
        public string VariableName { get; protected set; } = "!none!";
        public PyCode? Code { get; protected set; }
        public string Store { get; protected set; } = "store";

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("default ");
            if (Store.Length > 6)
            {
                output.Append($"{Store[6..]}.");
            }
            var def = new DefinitionLocation(DefinitionType.Define, VariableName, Location);
            output.Append(def, " = ");
            if (Code != null)
            {
                Code.BuildTextView(output);
            }
            else
            {
                output.AppendLine("!missing!");
            }
        }

        public override string ToString()
        {
            return $"default {VariableName} = {Code}";
        }
    }
}
