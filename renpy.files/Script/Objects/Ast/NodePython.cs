﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Python")]
    public class NodePython : Node
    {
        public bool Hide { get; protected set; }
        public PyCode? Code { get; protected set; }
        public string Store { get; protected set; } = "store";

        public override void BuildTextView(TextBuilder output)
        {
            if ((Code != null) && Code.IsSingleLine() && !Hide && (Store == "store"))
            {
                output.Append("$ ");
                Code.BuildTextView(output);
            }
            else
            {
                output.Append("python");
                if (Hide) output.Append(" hide");
                if (Store.Length > 6)
                {
                    output.Append(" in ", Store[6..]);
                }
                output.AppendLine(":");

                if (Code != null)
                {
                    Code.BuildTextView(output.Indent);
                }
            }
        }

        public override string ToString()
        {
            return "python:";
        }
    }
}
