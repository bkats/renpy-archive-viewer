﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Transform")]
    public class NodeTransform : Node
    {
        [RenPyFieldName("varname")]
        public string VariableName { get; protected set; } = "!none!";
        public string Store { get; protected set; } = "store";
        public RawBlock? Atl { get; protected set; }
        public ISignature? Parameters { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("transform ");
            if (Store.Length > 6)
            {
                output.Append($"{Store[6..]}.");
            }
            var name = new DefinitionLocation(DefinitionType.Transform, VariableName, Location);
            output.Append(name);
            Parameters?.BuildText(output);
            output.AppendLine(":");
            Atl?.BuildTextView(output.Indent);
        }

        public override string ToString()
        {
            if (Parameters != null)
            {
                return $"transform {VariableName}({Parameters}):";
            }
            return $"transform {VariableName}:";
        }
    }
}

/*
        # The name of the transform.
        'varname',

        # The block of ATL associated with the transform.
        'atl',

        # The parameters associated with the transform, if any.
        'parameters',
 
*/
