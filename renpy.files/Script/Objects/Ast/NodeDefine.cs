﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Define")]
    public class NodeDefine : Node
    {
        [RenPyFieldName("varname")]
        public string VariableName { get; protected set; } = "!none!";
        public string Operator { get; protected set; } = "=";
        public PyCode? Code { get; protected set; }
        public string Store { get; protected set; } = "store";
        public PyCode? Index { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("define ");
            if (Store.Length > 6)
            {
                output.Append($"{Store[6..]}.");
            }
            var def = new DefinitionLocation(DefinitionType.Define, VariableName, Location);
            output.Append(def);
            if (Index != null)
            {
                output.Append($"[{Index}]");
            }
            output.Append($" {Operator} ");
            if (Code != null)
            {
                Code.BuildTextView(output);
            }
            else
            {
                output.AppendLine("!missing!");
            }
        }

        public override string ToString()
        {
            if (Index != null)
            {
                return $"define {VariableName}[{Index}] {Operator} {Code}";
            }
            return $"define {VariableName} {Operator} {Code}";
        }
    }
}
