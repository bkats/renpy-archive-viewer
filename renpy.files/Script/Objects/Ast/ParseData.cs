﻿using RenPy.Files.Script.Objects.UserStatements;
using RenPy.Files.Shared;
using System.Collections;

namespace RenPy.Files.Script.Objects.Ast
{
    public class ParseData : IPythonSetState
    {
        public string KeyWord { get; private set; } = string.Empty;
        public IUserStatement? Statement { get; private set; }

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] s)
            {
                if (s[0] is object[] kw)
                {
                    KeyWord = string.Join(" ", kw);
                    Statement = GetStatement(KeyWord, s[1]);
                }
            }
        }

        private static IUserStatement? GetStatement(string keyWord, object? data)
        {
            if (keyWord == string.Empty)
                return null;

            switch (data)
            {
                case IDictionary rd:
                    var values = RepickData.ToDictionary<string, object>(rd);
                    return UserStatementCollection.CreateStatement(keyWord, values);
                case IUserStatement im:
                    return im;
                default:
                    return UserStatementCollection.CreateStatement(keyWord, data);
            }
        }
    }
}
