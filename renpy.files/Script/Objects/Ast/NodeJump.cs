﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Jump")]
    public class NodeJump : Node
    {
        public PyExpr Target { get; protected set; } = new PyExpr("!none!");
        public bool Expression { get; protected set; }

        [RenPyFieldName("global_label")]
        public string GlobalLabel { get; protected set; } = string.Empty;

        public override void BuildTextView(TextBuilder output)
        {
            var name = Target.Expression;
            var info = output.GetInfo<ILabelInfo>();
            var text = NodeLabel.GetLabelText(info, name);
            var target = new ReferrerLocation(ReferrerType.Jump, name, text, Location, DefinitionType.Label);
            output.Append("jump ");
            if (Expression)
            {
                output.Append("expression ");
            }
            output.AppendLine(target);
        }

        public override string ToString()
        {
            return $"jump {Target}";
        }
    }
}
