﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "TranslateString")]
    public class NodeTranslateString : Node, ITranslate
    {
        public string Old { get; protected set; } = "!none!";
        public string New { get; protected set; } = "!none!";
        public string? Language { get; protected set; }
        public LocationInfo? NewLoc { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            BuildTextView(output, true);
        }

        public void BuildTextView(TextBuilder output, bool header = true)
        {
            var oldText = TextBuilder.EscapeText(Old);
            var newText = TextBuilder.EscapeText(New);
            if (header)
            {
                output.AppendLine($"translate {Language ?? Languages.None} strings:");
            }

            var info = output.GetInfo<ITranslateInfo>();
            if (info != null && (string.IsNullOrEmpty(Language) || Language == Languages.None))
            {
                info.TranslateLiteral(ref newText);
            }

            var indent = output.Indent;
            indent.AppendLine("old \"", oldText, "\"");
            indent.AppendLine("new \"", newText, "\"");
            output.AppendLine();
        }

        public override string ToString()
        {
            return $"\"{Old}\" => \"{New}\"";
        }
    }
}
