﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "TranslateBlock")]
    [RenPyClass("renpy.ast", "TranslateEarlyBlock")]
    public class NodeTranslateBlock : NodeBlock, ITranslate
    {
        public string? Language { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.Append($"translate {Language} ");
            base.BuildTextView(output);
        }
    }
}
