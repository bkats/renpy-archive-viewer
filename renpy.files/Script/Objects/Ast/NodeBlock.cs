﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using System.Collections;

namespace RenPy.Files.Script.Objects.Ast
{
    public class NodeBlock : Node
    {
        [RenPyCustomSetter("SetBlock")]
        public IReadOnlyList<Node>? Block { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Block != null)
            {
                var start = output.CurrentLine;
                StatementBlockBuilder.BuildBlock(Block, output);

                if ((output.CurrentLine - start) > 1)
                {
                    output.EnsureBlankLine();
                }
            }
        }

        public override void RunOnAll(Action<Node> action)
        {
            base.RunOnAll(action);
            if (Block != null)
            {
                foreach (var node in Block)
                {
                    node.RunOnAll(action);
                }
            }
        }

        protected void SetBlock(ArrayList nodes)
        {
            Block = Cast(nodes).ToList();
        }
    }
}
