﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Internal;
using System.Xml.Linq;

namespace RenPy.Files.Script.Objects.Ast
{
    public class ImageSpecifier : IPythonSetState
    {
        public string Name { get; private set; } = string.Empty;
        public PyExpr? Expression { get; private set; }
        public string? Tag { get; private set; }
        public IReadOnlyList<PyExpr> AtList { get; private set; } = new List<PyExpr>();
        public string? Layer { get; private set; }
        public PyExpr? Zorder { get; private set; }
        public IReadOnlyList<PyExpr> Behind { get; private set; } = new List<PyExpr>();

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] data)
            {
                if (data.Length == 7)
                {
                    //    name, expression, tag, at_list, layer, zorder, behind = imspec
                    Name = GetName(data[0]);
                    Expression = (PyExpr)data[1];
                    Tag = data[2]?.ToString();
                    AtList = GetList(data[3]);
                    Layer = data[4]?.ToString();
                    Zorder = (PyExpr?)data[5];
                    Behind = GetList(data[6]);
                }
                if (data.Length == 6)
                {
                    //    name, expression, tag, at_list, layer, zorder = imspec
                    Name = GetName(data[0]);
                    Expression = (PyExpr)data[1];
                    Tag = data[2].ToString();
                    AtList = GetList(data[3]);
                    Layer = data[4]?.ToString();
                    Zorder = (PyExpr?)data[5];
                }
                if (data.Length == 3)
                {
                    //    name, at_list, layer = imspec
                    Name = GetName(data[0]);
                    AtList = GetList(data[1]);
                    Layer = data[2]?.ToString();
                }
            }
        }

        private static string GetName(object v)
        {
            if (v is object[] names)
            {
                return string.Join(" ", names);
            }
            return v.ToString() ?? "!none!";
        }

        private static IReadOnlyList<PyExpr> GetList(object v)
        {
            if (RepickData.TransformValue(v, out IReadOnlyList<PyExpr>? list))
            {
                return list;
            }
            return new List<PyExpr>();
        }

        public void BuildOutput(TextBuilder output, ReferrerType type, LocationInfo location)
        {
            if (Expression != null)
            {
                output.Append(" expression ", Expression);
            }
            else
            {
                var ti = output.GetInfo<ITranslateInfo>();
                if (ti != null && Name.StartsWith("text "))
                {
                    // Check if it is a 'show text "bla bla"'
                    if (Name.StartsWith("text \"") && Name.EndsWith("\""))
                    {
                        var literal = Name[6..^1];
                        if (ti.TranslateLiteral(ref literal))
                        {
                            output.Append(" text \"", literal, "\"");
                        }
                        else
                        {
                            output.Append(" ", Name);
                        }
                    }
                    else
                    {
                        // Try if it is a 'show text _("bla bla")'
                        var name = ti.TranslateFunctions(Name);
                        output.Append(" ", name);
                    }
                }
                else
                {
                    var match = Regs.MatchImageName().Match(Name);
                    if (match.Success)
                    {
                        var image = match.Groups["image"].Value;
                        var name = new ReferrerLocation(type, image, location, DefinitionType.Image);
                        output.Append(" ", name, match.Groups["rem"].Value);
                    }
                    else
                    {
                        output.Append(" ", Name);
                    }
                }
            }
            if (!string.IsNullOrEmpty(Layer))
            {
                output.Append(" onlayer ", Layer);
            }
            if (AtList.Count > 0)
            {
                output.Append(" at");
                foreach (var at in AtList)
                {
                    var atref = new ReferrerLocation(ReferrerType.Assignment, at, at.Location, DefinitionType.Transform);
                    output.Append(" ", atref);
                }
            }
            if (!string.IsNullOrEmpty(Tag))
            {
                output.Append(" as ", Tag);
            }
            if (Zorder != null)
            {
                output.Append($" zorder {Zorder}");
            }
            if (Behind.Count > 0)
            {
                output.Append(" behind ", string.Join(" ", Behind));
            }
        }
    }
}
