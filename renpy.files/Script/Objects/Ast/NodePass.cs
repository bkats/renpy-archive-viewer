﻿using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Pass")]
    public class NodePass : Node
    {
        public override string ToString()
        {
            return "pass";
        }
    }
}
