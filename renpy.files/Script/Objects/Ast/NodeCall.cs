﻿using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Call")]
    public class NodeCall : Node
    {
        public PyExpr Label { get; protected set; } = new PyExpr("!none!");
        public ArgumentInfo? Arguments { get; protected set; }
        public bool Expression { get; protected set; }

        [RenPyFieldName("global_label")]
        public string GlobalLabel { get; protected set; } = string.Empty;

        public override void BuildTextView(TextBuilder output)
        {
            BuildTextView(output, null);
        }

        public void BuildTextView(TextBuilder output, NodeLabel? returnLabel)
        {
            var info = output.GetInfo<ILabelInfo>();
            var text = NodeLabel.GetLabelText(info, Label);
            var label = new ReferrerLocation(ReferrerType.Call, Label, text, Location, DefinitionType.Label);
            output.Append("call ");
            if (Expression)
            {
                output.Append("expression ");
            }
            output.Append(label);
            if (Arguments != null)
            {
                if (Expression)
                {
                    // Is needed to separate arguments from being a part of the expression 
                    output.Append(" pass ");
                }
                Arguments.BuildText(output);
            }
            returnLabel?.BuildReturn(output);
            output.AppendLine();
        }

        public override string ToString()
        {
            if (Arguments == null)
            {
                return $"call {Label}";
            }
            return $"call {Label}{Arguments}";
        }
    }
}
