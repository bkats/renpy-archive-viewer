﻿using RenPy.Files.Script.Objects.Sl2;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Screen")]
    public class NodeScreen : Node
    {
        public SlScreen? Screen { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Screen == null)
            {
                output.AppendLine("screen !missing!");
            }
            else
            {
                Screen?.BuildTextView(output);
            }
        }

        public override string ToString()
        {
            return $"screen {Screen?.Name}";
        }
    }
}
