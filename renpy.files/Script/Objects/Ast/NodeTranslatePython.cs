﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "TranslatePython")]
    public class NodeTranslatePython : Node, ITranslate
    {
        public PyCode? Code { get; protected set; }
        public string Language { get; protected set; } = Languages.None;

        public override void BuildTextView(TextBuilder output)
        {
            output.AppendLine("# Translate Python was in an obsolete AST object,");
            output.AppendLine("# but should be parsed correctly as new (just an internal change?)");
            output.AppendLine($"translate {Language} python:");
            Code?.BuildTextView(output.Indent);
            output.AppendLine("# end of python (old translate style)");
        }
    }
}
