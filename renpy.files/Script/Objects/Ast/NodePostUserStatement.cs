﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "PostUserStatement")]
    public class NodePostUserStatement : Node
    {
        public NodeUserStatement? Parent { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            // Nothing to do, not an actual statement
        }
    }
}
