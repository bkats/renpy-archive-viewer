﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using RenPy.Files.Shared.Internal;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "PyCode")]
    public class PyCode
    {
        public string SourceCode { get; private set; } = string.Empty;
        public LocationInfo Location { get; private set; } = LocationInfo.Unknown;
        public string Mode { get; private set; } = string.Empty;
        public int PythonVersion { get; private set; }

        public void SetState(object[] state)
        {
            if (state.Length < 4)
            {
                throw new ArgumentException("Incorrect number of states provides", nameof(state));
            }

            if (state[1] is string src)
            {
                SourceCode = src;
                Location = new LocationInfo((object[])state[2]);
            }
            if (state[1] is PyExpr expr)
            {
                SourceCode = expr.Expression;
                Location = expr.Location;
            }
            Mode = (string)state[3];
            if (state.Length >= 5)
            {
                PythonVersion = (int)state[4];
            }
        }

        public void BuildTextView(TextBuilder output)
        {
            var info = output.GetInfo<ITranslateInfo>();
            var src = SourceCode;
            if (info != null)
            {
                src = info.TranslateFunctions(SourceCode);
            }
            foreach (var l in LineSplitter.GetLines(src))
            {
                string? trans = null;
                if (info != null)
                {
                    var m = Regs.MatchString().Match(l);
                    if (m.Success)
                    {
                        var literal = m.Groups["literal"].Value;
                        if (info.TranslateLiteral(ref literal))
                        {
                            trans = literal;
                        }
                    }
                }
                if (string.IsNullOrEmpty(trans))
                {
                    output.AppendLine(l);
                }
                else
                {
                    output.Append(l);
                    output.AppendLine(" # Translation: \"", trans, "\"");
                }
            }
        }

        public bool IsSingleLine()
        {
            return !SourceCode.Contains('\n');
        }

        private string GetFirstLine()
        {
            var end = SourceCode.IndexOf('\n');
            if (end >= 0)
            {
                return SourceCode[0..(end - 1)];
            }
            return SourceCode;
        }

        public override string ToString()
        {
            return $"<PyCode> {GetFirstLine()}";
        }
    }
}
