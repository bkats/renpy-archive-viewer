﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "EarlyPython")]
    public class NodeEarlyPython : Node
    {
        public bool Hide { get; protected set; }
        public PyCode? Code { get; protected set; }
        public string Store { get; protected set; } = "store";

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("python early");
            if (Hide) output.Append(" hide");
            if (Store.Length > 6)
            {
                output.Append(" in ", Store[6..]);
            }
            output.AppendLine(":");

            if (Code != null)
            {
                Code.BuildTextView(output.Indent);
            }
        }

        public override string ToString()
        {
            return "python early:";
        }
    }
}
