﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "UserStatement")]
    public class NodeUserStatement : Node
    {
        [RenPyFieldName("init_priority")]
        public int InitPriority { get; protected set; }
        public string Line { get; protected set; } = string.Empty;
        public IReadOnlyList<UserLine>? Block { get; protected set; }
        public ParseData? Parsed { get; protected set; }

        public bool Translatable { get; protected set; }
        [RenPyFieldName("translation_relevant")]
        public bool TranslatableRelevant { get; protected set; }

        public string RollBack { get; protected set; } = "normal";

        public IReadOnlyList<object>? SubParses { get; protected set; }
        [RenPyFieldName("code_block")]
        public IReadOnlyList<object>? CodeBlock { get; protected set; }

        public RawBlock? Atl { get; protected set; }

        protected override void AfterSetState()
        {
            base.AfterSetState();
            Parsed?.Statement?.SetParent(this);
        }

        public override void BuildTextView(TextBuilder output)
        {
            if (Parsed?.Statement != null)
            {
                Parsed.Statement.BuildTextView(output);
            }
            else
            {
                output.AppendLine(Line);
                if (Block != null)
                {
                    var indent = output.Indent;
                    foreach (var b in Block)
                    {
                        b.BuildText(indent);
                    }
                }
            }
        }

        public override string ToString()
        {
            return Line;
        }
    }

    public class UserLine : IPythonSetState
    {
        public LocationInfo? Location { get; private set; }
        public string Line { get; private set; } = string.Empty;
        public IReadOnlyList<UserLine>? SubLines { get; private set; }

        public void BuildText(TextBuilder output)
        {
            output.AppendLine(Line);
            if (SubLines != null)
            {
                foreach (var s in SubLines)
                {
                    s?.BuildText(output.Indent);
                }
            }
        }

        public override string ToString()
        {
            return Line;
        }

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] data)
            {
                var loc = new Object[] { data[0], data[1] };
                Location = new LocationInfo(loc);
                Line = data[2] as string ?? string.Empty;
                if (RepickData.TransformValue(data[3], out IReadOnlyList<UserLine>? sub))
                {
                    SubLines = sub;
                }
            }
        }
    }
}
