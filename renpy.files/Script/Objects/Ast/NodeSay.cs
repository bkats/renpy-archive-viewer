﻿using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using RenPy.Files.Shared.Internal;
using System.Security.Cryptography;
using System.Text;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Say")]
    public class NodeSay : Node
    {
        public string? Who { get; protected set; }
        [RenPyFieldName("who_fast")]
        public bool WhoFast { get; protected set; }
        public string What { get; protected set; } = string.Empty;
        [RenPyFieldName("with_")]
        public PyExpr? With { get; protected set; }
        public bool Interact { get; protected set; }
        public IReadOnlyList<string>? Attributes { get; protected set; }
        [RenPyFieldName("temporary_attributes")]
        public IReadOnlyList<string>? TemporaryAttributes { get; protected set; }
        public ArgumentInfo? Arguments { get; protected set; }
        public string Rollback { get; protected set; } = "normal";
        public string? Identifier { get; protected set; }
        [RenPyFieldName("explicit_identifier")]
        public bool ExplicitIdentifier { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            var who = Who;
            var what = What;
            var info = output.GetInfo<ITranslateInfo>();
            if (info != null)
            {
                info.TranslateLiteral(ref what);
            }
            BuildText(output, who, what);
        }

        public void BuildTextView(TextBuilder output, NodeSay translation)
        {
            var who = translation.Who;
            var what = translation.What;
            BuildText(output, who, what);
        }

        protected void BuildText(TextBuilder output, string? who, string what)
        {
            if (!string.IsNullOrEmpty(who))
            {
                if (Regs.MatchVariable().Match(who).Success)
                {
                    var whoRef = new ReferrerLocation(ReferrerType.Action, who, Location, DefinitionType.Define);
                    output.Append(whoRef, " ");
                }
                else
                {
                    output.Append(who, " ");
                }
            }
            if (Attributes != null && (Attributes.Count > 0))
            {
                output.Append(string.Join(' ', Attributes), " ");
            }
            if (TemporaryAttributes != null && TemporaryAttributes.Count > 0)
            {
                output.Append("@ ", string.Join(' ', TemporaryAttributes), " ");
            }
            what = TextBuilder.EscapeText(what);
            output.Append($"\"{what}\"");
            Arguments?.BuildText(output);
            if (!Interact)
            {
                output.Append(" nointeract");
            }
            if (With != null)
            {
                var w = new ReferrerLocation(ReferrerType.Assignment, With, With.Location, DefinitionType.Transform);
                output.Append(" with ", w);
            }
            if (ExplicitIdentifier && !string.IsNullOrEmpty(Identifier))
            {
                output.Append(" id ", Identifier);
            }
            output.AppendLine();
        }

        public string CalculateIdentifier()
        {
            // The list, string build and MD5 calculation should exactly match that of how Ren'Py calculates this identifier
            List<string> rv = new();
            if (!string.IsNullOrEmpty(Who))
            {
                rv.Add(Who);
            }
            if (Attributes != null)
            {
                rv.AddRange(Attributes);
            }
            if (TemporaryAttributes != null)
            {
                rv.Add("@");
                rv.AddRange(TemporaryAttributes);
            }
            var what = TextBuilder.EscapeText(What, true);
            rv.Add($"\"{what}\"");
            if (!Interact)
            {
                rv.Add("nointeract");
            }
            if (Identifier != null)
            {
                rv.Add("id");
                rv.Add(Identifier);
            }
            if (With != null)
            {
                rv.Add("with");
                rv.Add(With);
            }
            if (Arguments != null)
            {
                rv.Add(Arguments.GetCode());
            }
            var text = string.Join(" ", rv) + "\r\n";
            var data = Encoding.UTF8.GetBytes(text);

            return Convert.ToHexString(MD5.HashData(data))[..8].ToLower();
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Who))
            {
                return $"\"{What}\"";
            }
            return $"{Who} \"{What}\"";
        }
    }
}
