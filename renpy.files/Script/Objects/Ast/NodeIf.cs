﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using System;
using System.Collections;
using System.Linq;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "If")]
    public class NodeIf : Node
    {
        public IReadOnlyList<NodeIfEntry>? Entries { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Entries != null)
            {
                bool first = true;
                foreach (var entry in Entries)
                {
                    entry.BuildTextView(output, first);
                    first = false;
                }
            }
            else
            {
                output.AppendLine("!if, but no ifs!");
            }
        }

        public override string ToString()
        {
            return "# if";
        }

        public override void RunOnAll(Action<Node> action)
        {
            base.RunOnAll(action);
            if (Entries != null)
            {
                foreach (var entry in Entries)
                {
                    foreach (var node in entry.Block)
                    {
                        node.RunOnAll(action);
                    }
                }
            }
        }
    }

    public class NodeIfEntry : IPythonSetState
    {
        public PyExpr Condition { get; private set; } = new();
        public IReadOnlyList<Node> Block { get; private set; } = new List<Node>();

        private void SetEntry(object[] parts)
        {
            if (parts.Length >= 2)
            {
                if (RepickData.TransformValue(parts[0], out PyExpr? c))
                {
                    Condition = c;
                }
                if (parts[1] is IEnumerable block)
                {
                    Block = Node.Cast(block).ToList();
                }
            }
        }

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] entry)
            {
                SetEntry(entry);
            }
        }

        public void BuildTextView(TextBuilder output, bool first)
        {

            if (first)
            {
                output.AppendLine($"if {Condition}:");
            }
            else
            {
                if (String.IsNullOrEmpty(Condition) || (Condition == "True"))
                {
                    output.AppendLine("else:");
                }
                else
                {
                    output.AppendLine($"elif {Condition}:");
                }
            }
            StatementBlockBuilder.BuildBlock(Block, output.Indent);
        }
    }
}
