﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Camera")]
    public class NodeCamera : Node
    {
        public string Layer { get; protected set; } = "master";

        public RawBlock? Atl { get; protected set; }

        [RenPyFieldName("at_list")]
        public IReadOnlyList<PyExpr> AtList { get; private set; } = new List<PyExpr>();

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("camera");
            if (Layer != "master")
            {
                output.Append(" ", Layer);
            }
            if (AtList.Count > 0)
            {
                output.Append(" at");
                foreach (var at in AtList)
                {
                    var atref = new ReferrerLocation(ReferrerType.Assignment, at, at.Location, DefinitionType.Transform);
                    output.Append(" ", atref);
                }
            }
            if (Atl != null)
            {
                output.AppendLine(":");
                Atl.BuildTextView(output.Indent);
            }
            output.AppendLine();
        }

        public override string ToString()
        {
            return $"camera {Layer}";
        }
    }
}
