﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "TranslateSay")]
    public class NodeTranslateSay : NodeSay, ITranslate
    {
        public string? Language { get; protected set; }
        public string? Alternate { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            // Check if original should be replaced with a translation
            var info = output.GetInfo<ITranslateInfo>();
            if ((info != null) && (Identifier != null)
                && info.GetTranslatedNodes(Identifier, out var translatedNodes))
            {
                // The translations can be multiple statements
                foreach (var tn in translatedNodes)
                {
                    if (tn is NodeSay translatedSay)
                    {
                        // Use original say with the translation
                        BuildTextView(output, translatedSay);
                    }
                    else
                    {
                        tn.BuildTextView(output);
                    }
                }
            }
            else
            {
                var who = Who;
                var what = What;
                if (string.IsNullOrEmpty(Language))
                {
                    BuildText(output, who, what);
                }
                else
                {
                    output.AppendLine($"translate {Language} {Identifier}:");
                    BuildText(output.Indent, who, what);
                    output.AppendLine();
                }
            }
        }
    }
}
