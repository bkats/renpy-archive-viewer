﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "With")]
    public class NodeWith : Node
    {
        [RenPyFieldName("expr")]
        public PyExpr? Expression { get; protected set; }
        public PyExpr? Paired { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Expression != null)
            {
                if (Expression != "None")
                {
                    var handled = false;
                    var paired = output.GetInfo<WithPaired>();
                    if (paired != null)
                    {
                        handled = paired.IsHandled;
                        output.RemoveInfo(paired);
                    }
                    if (!handled)
                    {
                        var expr = new ReferrerLocation(ReferrerType.Assignment, Expression, Expression.Location, DefinitionType.Transform);
                        output.AppendLine("with ", expr);
                    }
                }
                else
                {
                    // The with statement also put before a statement when 'with' was parsed on the same line as that statement
                    // scene, show or hide (e.g. show xxx with fade)
                    var paired = new WithPaired(this);
                    output.AddInfo(paired);
                }
            }
        }

        public void BuildPaired(TextBuilder output)
        {
            if (Paired != null)
            {
                var expr = new ReferrerLocation(ReferrerType.Assignment, Paired, Paired.Location, DefinitionType.Transform);
                output.Append(" with ", expr);
            }
        }

        public override string ToString()
        {
            if (Expression == null)
            {
                return $"with {Paired}";
            }
            if (Paired == null)
            {
                return $"with {Expression}";
            }
            return $"with {Paired} {Expression}";
        }
    }
}
