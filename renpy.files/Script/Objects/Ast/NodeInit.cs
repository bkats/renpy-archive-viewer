﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Init")]
    public class NodeInit : NodeBlock
    {
        public int Priority { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            var offset = 0;
            var info = output.GetInfo<IInitOffset>();
            if (info != null)
            {
                offset = info.Offset;
            }
            if (NoInitNeeded(offset))
            {
                base.BuildTextView(output);
            }
            else
            {
                var prio = Priority - offset;
                output.EnsureBlankLine();
                output.Append("init");
                if (prio != 0)
                {
                    output.Append(" ", prio);
                }
                if ((Block != null) && (Block.Count == 1))
                {
                    output.Append(" ");
                    base.BuildTextView(output);
                }
                else
                {
                    output.AppendLine(":");
                    base.BuildTextView(output.Indent);
                }
            }
        }

        private bool NoInitNeeded(int offset)
        {
            if (Block != null && Block.Count > 0)
            {
                if (!InitLevel.DetectInitNeeded(Block[0], Priority - offset))
                {
                    return Block.Count == 1 || Block.All(n => n is NodeTranslateString);
                }
            }
            return false;
        }

        public override string ToString()
        {
            return $"init priority = {Priority}";
        }
    }

    internal static class InitLevel
    {
        private static readonly Dictionary<Type, int> defaultLevels = new()
        {
            { typeof(NodeDefault), 0 },
            { typeof(NodeDefine), 0 },
            { typeof(NodeTransform), 0 },
            { typeof(NodeStyle), 0 },
            { typeof(NodeScreen), -500 },
            { typeof(NodeImage), 500 },
            { typeof(NodeTranslateString), 0 },
            { typeof(NodeUserStatement), 0 },
        };

        public static int GetDefaultPriority(Node node)
        {
            if (defaultLevels.TryGetValue(node.GetType(), out int value))
            {
                return value;
            }
            return 0;
        }

        public static bool DetectInitNeeded(Node node, int priority)
        {
            if (defaultLevels.TryGetValue(node.GetType(), out int value))
            {
                return value != priority;
            }
            return true;
        }
    }
}

/*  Init introduced by statements and their default level
 *  image 500 + offset
 *  define 0 + offset
 *  default 0 + offset
 *  transform 0 + offset
 *  screen -500 + offset
 *  style 0 + offset
 */
