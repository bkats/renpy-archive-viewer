﻿using Pickle.Objects;
using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.Ast
{
    public class NodeUnknownStatement(ClassDict cd) : Node
    {
        public override void BuildTextView(TextBuilder output)
        {
            output.AppendLine($"# Unknown Statement {cd.ClassName} found");
            output.AppendLine($"# Statement has following fields:");
            foreach (var item in cd)
            {
                if (item.Value == null)
                {
                    output.AppendLine($"#  {item.Key} has value 'None'");
                }
                else
                {
                    switch (item.Value){
                        case ValueType v:
                            output.AppendLine($"#  {item.Key} has value '{v}' of type {v.GetType()}");
                            break;
                        case string s:
                            output.AppendLine($"#  {item.Key} has value '{s}' of type string");
                            break;
                        default:
                            output.AppendLine($"#  {item.Key} of type {item.Value.GetType()}");
                            break;
                    }
                }
            }
        }

        public override string ToString()
        {
            return $"Unknown statemen: {cd.ClassName}";
        }
    }
}