﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "EndTranslate")]
    public class NodeEndTranslate : Node
    {
        public override void BuildTextView(TextBuilder output)
        {
            // This node is not part of text view
        }

        public override string ToString()
        {
            return "EndTranslate";
        }
    }
}
