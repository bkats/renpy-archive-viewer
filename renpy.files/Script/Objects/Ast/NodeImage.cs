﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Image")]
    public class NodeImage : Node
    {
        [RenPyFieldName("imgname")]
        [RenPyTagsProperty]
        public string ImageName { get; protected set; } = string.Empty;
        public PyCode? Code { get; protected set; }
        public RawBlock? Atl { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            var img = new DefinitionLocation(DefinitionType.Image, ImageName, Location);
            if (Code != null)
            {
                output.Append("image ", img, " = ");
                Code.BuildTextView(output);
            }
            else
            {
                output.AppendLine("image ", img, ":");
                Atl?.BuildTextView(output.Indent);
            }
        }

        public override string ToString()
        {
            if (Code == null)
            {
                return $"image {ImageName}";
            }
            return $"image {ImageName} {Code}";
        }
    }
}
