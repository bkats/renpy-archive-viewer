﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Return")]
    public class NodeReturn : Node
    {
        public PyExpr? Expression { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("return");
            if (Expression != null) output.Append(" ", Expression);
            output.AppendLine();
        }

        public override string ToString()
        {
            return "return";
        }
    }
}
