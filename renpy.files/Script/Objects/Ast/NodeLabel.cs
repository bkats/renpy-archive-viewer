﻿using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Label")]
    public class NodeLabel : NodeBlock
    {
        private class LabelInfo : ILabelInfo
        {
            public NodeLabel? CurrentLabel { get; set; }
        }

        public ISignature? Parameters { get; protected set; }
        public bool Hide { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            var info = output.GetInfo<ILabelInfo>();
            var text = GetLabelText(info, Name);
            var name = new DefinitionLocation(DefinitionType.Label, Name, text, Location);

            // Just have a blank line before the label to make standout more
            output.EnsureBlankLine();

            output.Append("label ", name);
            Parameters?.BuildText(output);
            if (Hide) output.Append(" hide");
            output.AppendLine(":");

            if (info != null)
            {
                info.CurrentLabel = this;
            }
            else
            {
                info = new LabelInfo() { CurrentLabel = this };
                output.AddInfo(info);
            }

            base.BuildTextView(output.Indent);
        }

        internal void BuildReturn(TextBuilder output)
        {
            // it is part of call xxx from <name>
            output.Append($" from {Name}");
        }

        internal bool IsReturnCandidate
        {
            get => (Block == null) || (Block.Count == 0);
        }

        public string GetGlobalName()
        {
            if (Name.Contains('.'))
            {
                return Name.Split('.')[0];
            }
            else
            {
                return Name;
            }
        }

        public static string GetLabelText(ILabelInfo? info, string name)
        {
            var text = name;
            if (name.Contains('.'))
            {
                if (info?.CurrentLabel != null)
                {
                    var gbl = info.CurrentLabel.GetGlobalName();
                    if (name.StartsWith(gbl))
                    {
                        text = name[gbl.Length..];
                    }
                }
            }
            return text;
        }

        public override string ToString()
        {
            if (Parameters != null)
            {
                return $"label {Name}({Parameters}):";
            }
            return $"label {Name}:";
        }
    }

    public interface ILabelInfo
    {
        public NodeLabel? CurrentLabel { get; set; }
    }
}
