﻿using Pickle.Objects;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using System.Collections;
using System.Runtime.CompilerServices;

namespace RenPy.Files.Script.Objects.Ast
{
    public class Node : PythonPickleBaseClass
    {
        [RenPyMultiField("filename", "linenumber")]
        public LocationInfo Location { get; protected set; } = LocationInfo.Unknown;

        [RenPyTagsProperty('_')]
        public string Name { get; protected set; } = string.Empty;

        [RenPyFieldName("statement_start")]
        public Node? StatementStart { get; protected set; }
        public Node? Next { get; protected set; }

        public virtual void BuildTextView(TextBuilder output)
        {
            output.AppendLine(ToString());
        }

        public override string ToString()
        {
            return $"Node {this.GetType()} at {Location}";
        }

        public static IEnumerable<Node> Cast(IEnumerable nodes)
        {
            foreach (var node in nodes)
            {
                if (node is Node n)
                {
                    yield return n;
                }
                if (node is ClassDict cd)
                {
                    yield return new NodeUnknownStatement(cd);
                }
            }
        }

        public virtual void RunOnAll(Action<Node> action)
        {
            action(this);
        }
    }
}
