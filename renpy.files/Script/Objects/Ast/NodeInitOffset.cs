﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects.Ast
{
    public interface IInitOffset
    {
        int Offset { get; }
    }

    public class NodeInitOffset : Node
    {
        private class InitOffset(int offset) : IInitOffset
        {
            public int Offset { get; set; } = offset;
        }

        public int Offset { get; }

        public NodeInitOffset(int offset) => Offset = offset;

        public override void BuildTextView(TextBuilder output)
        {
            var o = output.GetInfo<InitOffset>();
            if (o != null)
            {
                o.Offset = Offset;
            }
            else
            {
                output.AddInfo(new InitOffset(Offset));
            }
            output.EnsureBlankLine();
            output.AppendLine($"init offset = {Offset}");
            output.AppendLine();
        }

        public override string ToString()
        {
            return $"Init offset {Offset}";
        }
    }
}