﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Scene")]
    public class NodeScene : Node
    {
        [RenPyFieldName("imspec")]
        public ImageSpecifier? ImageSpecifier { get; protected set; }
        public string? Layer { get; protected set; }
        public RawBlock? Atl { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("scene");
            if (ImageSpecifier != null)
            {
                ImageSpecifier.BuildOutput(output, ReferrerType.Scene, Location);
            }
            else
            {
                if (!string.IsNullOrEmpty(Layer))
                {
                    output.Append(" onlayer ", Layer);
                }
            }
            var paired = output.GetInfo<WithPaired>();
            if (paired != null)
            {
                paired.CurrentWith.BuildPaired(output);
                paired.IsHandled = true;
            }
            if (Atl != null)
            {
                output.AppendLine(":");
                Atl.BuildTextView(output.Indent);
            }
            output.AppendLine();
        }

        public override string ToString()
        {
            if (Layer != string.Empty)
            {
                return $"scene {ImageSpecifier?.Name} onlayer {Layer}";
            }
            return $"scene {ImageSpecifier?.Name}";
        }
    }
}
