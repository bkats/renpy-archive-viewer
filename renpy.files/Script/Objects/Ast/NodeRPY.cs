﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "RPY")]
    public class NodeRPY : Node
    {
        [RenPyTagsProperty]
        public string Rest { get; protected set; } = string.Empty;

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("rpy ");
            output.AppendLine(Rest);
        }
    }
}
