﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "ShowLayer")]
    public class NodeShowLayer : Node
    {
        public string Layer { get; protected set; } = "!None!";

        [RenPyFieldName("at_list")]
        public IReadOnlyList<PyExpr>? AtList { get; protected set; }
        public RawBlock? Atl { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("show layer ", Layer);
            if (AtList != null)
            {
                output.Append(" at ", string.Join(" ", AtList));
            }
            if (Atl != null)
            {
                output.AppendLine(":");
                Atl.BuildTextView(output.Indent);
            }
            output.AppendLine();
        }

        public override string ToString()
        {
            return "showlayer <not implemented>  (is this command actually ever used?)";
        }
    }
}
