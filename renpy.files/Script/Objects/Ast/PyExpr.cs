﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "PyExpr")]
    public class PyExpr : PythonPickleBaseClass, IPythonSetState
    {
        private string expression = string.Empty;

        /// <summary>
        /// Unprocessed expression can contain line splits
        /// </summary>
        public string Expression => expression;

        [RenPyMultiField("filename", "linenumber")]
        public LocationInfo Location { get; protected set; } = LocationInfo.Unknown;

        [RenPyFieldName("py")]
        public int PythonVersion { get; protected set; }

        public PyExpr() { }

        public PyExpr(string expr)
        {
            expression = expr;
        }

        public PyExpr(string expr, LocationInfo loc) : this(expr)
        {
            Location = loc;
        }

        public PyExpr(string expr, string fileName, int lineNum) : this(expr)
        {
            // Used from unpickler (RenPy <7.5 class has this constructor)
            Location = new LocationInfo(fileName, lineNum);
        }

        public PyExpr(string expr, string fileName, int lineNum, int pythonVersion) : this(expr, fileName, lineNum)
        {
            // Used from unpickler (RenPy >=7.5/8.0 class has this constructor)
            PythonVersion = pythonVersion;
        }

        void IPythonSetState.SetState(object state)
        {
            expression = state.ToString() ?? string.Empty;
        }

        public override string ToString()
        {
            return expression;
        }

        public static implicit operator string(PyExpr ex) => ex.expression;

        public override bool Equals(object? obj) => (obj is PyExpr expr) && expression.Equals(expr.expression);

        public override int GetHashCode() => expression.GetHashCode();
    }
}
