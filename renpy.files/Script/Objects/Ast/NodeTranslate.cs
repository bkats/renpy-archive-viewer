﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Translate")]
    public class NodeTranslate : NodeBlock, ITranslate
    {
        public string? Language { get; protected set; }
        public string? Identifier { get; protected set; }
        public string? Alternate { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            // Check if this the original or a translation
            if (string.IsNullOrEmpty(Language))
            {
                // Check if original should be replaced with a translation
                var info = output.GetInfo<ITranslateInfo>();
                if ((info != null) && (Identifier != null)
                    && info.GetTranslatedNodes(Identifier, out var translatedNodes))
                {
                    // Translation block contain max 1 say statement, find it
#pragma warning disable IDE0019 // Use pattern matching
                    var say = Block?.FirstOrDefault(n => n is NodeSay) as NodeSay;
#pragma warning restore IDE0019 // Use pattern matching
                    // The translations can be multiple statements
                    foreach (var tn in translatedNodes)
                    {
                        if ((say != null) && tn is NodeSay translationSay)
                        {
                            // Use original say with the translation
                            say.BuildTextView(output, translationSay);
                        }
                        else
                        {
                            tn.BuildTextView(output);
                        }
                    }
                }
                else
                {
                    // Just print original as is
                    base.BuildTextView(output);
                }
            }
            else
            {
                // Print the translation info
                output.AppendLine($"translate {Language} {Identifier}:");
                base.BuildTextView(output.Indent);
                output.AppendLine();
            }
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Language))
            {
                return $"Language: {Language}";
            }
            return "Language: None";
        }
    }
}
