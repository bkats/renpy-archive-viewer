﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Hide")]
    public class NodeHide : Node
    {
        [RenPyFieldName("imspec")]
        public ImageSpecifier? ImageSpecifier { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            output.Append("hide");
            if (ImageSpecifier != null)
            {
                ImageSpecifier.BuildOutput(output, ReferrerType.Hide, Location);
            }
            else
            {
                output.Append(" !something!");
            }
            var paired = output.GetInfo<WithPaired>();
            if (paired != null)
            {
                paired.CurrentWith.BuildPaired(output);
                paired.IsHandled = true;
            }
            output.AppendLine();
        }

        public override string ToString()
        {
            return $"hide {ImageSpecifier?.Name}";
        }
    }
}
