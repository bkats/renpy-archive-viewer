﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "While")]
    public class NodeWhile : NodeBlock
    {
        public PyExpr? Condition { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            if (Condition != null)
            {
                output.AppendLine("while ", Condition, ":");
            }
            else
            {
                output.AppendLine("while <unknown>:");
            }
            base.BuildTextView(output.Indent);
        }

        public override string ToString()
        {
            return $"while {Condition}:";
        }
    }
}
