﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Style")]
    public class NodeStyle : Node
    {
        [RenPyFieldName("style_name")]
        public string StyleName { get; protected set; } = "!none!";
        public string? Parent { get; protected set; }
        public IReadOnlyDictionary<string, PyExpr>? Properties { get; protected set; }
        public bool Clear { get; protected set; }
        public string? Take { get; protected set; }
        [RenPyFieldName("delattr")]
        public IReadOnlyList<string>? DeleteAttributes { get; protected set; }
        public PyExpr? Variant { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            var sn = new DefinitionLocation(DefinitionType.Style, StyleName, Location);
            output.Append("style ", sn);
            if (Parent != null)
            {
                var parent = new ReferrerLocation(ReferrerType.Assignment, Parent, Location, DefinitionType.Style);
                output.Append(" is ", parent);
            }
            if (Clear || Take?.Length > 0 || Variant != null || DeleteAttributes?.Count > 0 || Properties?.Count > 0)
            {
                output.AppendLine(":");
                output = output.Indent;
                if (Clear) output.AppendLine("Clear");
                if (Take != null) output.AppendLine("take ", Take);
                if (Variant != null) output.AppendLine("variant ", Variant);
                if (DeleteAttributes != null)
                {
                    foreach (var del in DeleteAttributes)
                    {
                        output.AppendLine("del ", del);
                    }
                }
                if (Properties != null)
                {
                    foreach (var prop in Properties.Keys)
                    {
                        output.AppendLine($"{prop} {Properties[prop]}");
                    }
                }
            }
            else
            {
                output.AppendLine();
            }
        }

        public override string ToString()
        {
            if (Parent != string.Empty)
            {
                return $"style {StyleName} is {Parent}";
            }
            return $"style {StyleName}";
        }
    }
}
/*
        'style_name',
        'parent',
        'properties',
        'clear',
        'take',
        'delattr',
        'variant',
*/