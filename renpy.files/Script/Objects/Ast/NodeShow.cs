﻿using RenPy.Files.Script.Objects.Atl;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Show")]
    public class NodeShow : Node
    {
        [RenPyFieldName("imspec")]
        public ImageSpecifier? ImageSpecifier { get; protected set; }
        public RawBlock? Atl { get; protected set; }

        /*
                show <name> as <tag> at <at_list> onlayer <layer> zorder <zorder> behind <behind>
                show expression "moon.png" as moon
        */
        public override void BuildTextView(TextBuilder output)
        {
            output.Append("show");
            ImageSpecifier?.BuildOutput(output, ReferrerType.Show, Location);
            var paired = output.GetInfo<WithPaired>();
            if (paired != null)
            {
                paired.CurrentWith.BuildPaired(output);
                paired.IsHandled = true;
            }
            if (Atl != null)
            {
                output.AppendLine(":");
                Atl.BuildTextView(output.Indent);
            }
            output.AppendLine();
        }

        public override string ToString()
        {
            return $"show {ImageSpecifier?.Name}";
        }
    }
}
