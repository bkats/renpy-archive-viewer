﻿using RenPy.Files.Script.Objects.Parameter;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;
using System.Collections;

namespace RenPy.Files.Script.Objects.Ast
{
    [RenPyClass("renpy.ast", "Menu")]
    public class NodeMenu : Node
    {
        public IReadOnlyList<NodeMenuItem>? Items { get; protected set; }

        public string RollBack { get; protected set; } = "force";

        [RenPyFieldName("has_caption")]
        public bool HasCaption { get; protected set; }

        [RenPyFieldName("with_")]
        public PyExpr? With { get; protected set; }

        public ArgumentInfo? Arguments { get; protected set; }

        [RenPyFieldName("item_arguments")]
        [RenPyListContainsNullItems]
        public IReadOnlyList<ArgumentInfo?>? ItemArguments { get; protected set; }

        public PyExpr? Set { get; protected set; }

        public override void BuildTextView(TextBuilder output)
        {
            var group = output.GetInfoAndRemove<IStatementGroup>();
            output.Append("menu");
            // Check if there is a label in the group and add to the menu line
            if ((group != null) && (group.Nodes[0] is NodeLabel label))
            {
                var info = output.GetInfo<ILabelInfo>();
                var text = NodeLabel.GetLabelText(info, label.Name);
                var lbl = new DefinitionLocation(DefinitionType.Label, label.Name, text, label.Location);
                output.Append(" ", lbl);
            }
            Arguments?.BuildText(output);
            output.AppendLine(":");
            var indent = output.Indent;
            if (Set != null)
            {
                indent.AppendLine("set ", Set);
            }
            if (With != null)
            {
                indent.AppendLine("with ", With);
            }
            // Add the items from group, except for label(s). It should be just one optional say.
            if (group != null)
            {
                foreach (var n in group.Nodes)
                {
                    if (n is not NodeLabel)
                    {
                        n.BuildTextView(indent);
                    }
                }
            }
            if (Items != null)
            {
                int i = 0;
                foreach (var item in Items)
                {
                    ArgumentInfo? args = null;
                    if ((ItemArguments != null) && (ItemArguments.Count > i))
                    {
                        args = ItemArguments[i++];
                    }
                    item.BuildTextView(indent, args);
                }
            }
            else
            {
                output.AppendLine("  !missing menu items!");
            }
        }

        public override string ToString()
        {
            return "menu:";
        }

        public override void RunOnAll(Action<Node> action)
        {
            base.RunOnAll(action);
            if (Items != null)
            {
                foreach (var item in Items)
                {
                    foreach (var node in item.Block)
                    {
                        node.RunOnAll(action);
                    }
                }
            }
        }
    }

    public class NodeMenuItem : IPythonSetState
    {
        public string Label { get; private set; } = string.Empty;
        public PyExpr Condition { get; private set; } = new();
        public IReadOnlyList<Node> Block { get; private set; } = [];

        private void SetMenuItem(object[] parts)
        {
            if (parts.Length >= 3)
            {
                if (parts[0] is string label)
                {
                    Label = label;
                }
                ((IPythonSetState)Condition).SetState(parts[1]);
                if (parts[2] is IEnumerable block)
                {
                    Block = Node.Cast(block).ToList();
                }
            }
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Condition) || Condition == "True")
            {
                return $"\"{Label}\":";
            }
            return $"\"{Label}\": if {Condition}";
        }

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] parts)
            {
                SetMenuItem(parts);
            }
        }

        internal void BuildTextView(TextBuilder output, ArgumentInfo? args)
        {
            var info = output.GetInfo<ITranslateInfo>();
            var label = Label;
            info?.TranslateLiteral(ref label);
            label = TextBuilder.EscapeText(label);
            output.Append($"\"{label}\"");
            args?.BuildText(output);
            if (Block.Count > 0)
            {
                if (string.IsNullOrEmpty(Condition) || Condition == "True")
                {
                    output.AppendLine(":");
                }
                else
                {
                    output.AppendLine($" if {Condition}:");
                }
                StatementBlockBuilder.BuildBlock(Block, output.Indent);
            }
            else
            {
                output.AppendLine();
            }
        }
    }
}
