﻿using RenPy.Files.Shared;

namespace RenPy.Files.Script.Objects
{
    public class NameValuePair : IPythonSetState
    {
        public string? Name { get; private set; }
        public string? Value { get; private set; }

        void IPythonSetState.SetState(object state)
        {
            if (state is object[] nv)
            {
                Name = nv[0]?.ToString();
                Value = nv[1]?.ToString();
            }
        }

        public override string ToString()
        {
            return $"{Name} {Value}";
        }
    }
}
