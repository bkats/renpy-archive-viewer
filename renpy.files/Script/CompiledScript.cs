﻿namespace RenPy.Files.Script
{
    public class CompiledScript(string fileName, ScriptRoot root)
    {
        public ScriptRoot Root { get; } = root;
        public string FileName { get; } = fileName;
        public string Language => Root.Language;

        public CompiledScriptTextView GetTextView()
        {
            return Root.BuildTextView();
        }

        public CompiledScriptTextView GetTranslatedTextView(ITranslateInfo translateInfo)
        {
            return Root.BuildTextView(translateInfo);
        }
    }
}