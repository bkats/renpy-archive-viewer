﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared.Internal;
using System.Diagnostics.CodeAnalysis;

namespace RenPy.Files.Script
{
    public class TranslateInfo(IReadOnlyDictionary<string, Node[]> says, IReadOnlyDictionary<string, string> translations) : ITranslateInfo
    {
        public bool GetTranslatedNodes(string id, [NotNullWhen(true)] out Node[]? result)
        {
            return says.TryGetValue(id, out result);
        }

        public bool TranslateLiteral(ref string text)
        {
            if (translations.TryGetValue(text, out var result))
            {
                if (!string.IsNullOrEmpty(result) && text != result)
                {
                    text = result;
                    return true; 
                }
            }
            return false;
        }

        public string TranslateFunctions(string text)
        {
            if (translations.Count == 0)
                return text;

            return Regs.MatchTranslators().Replace(text, m =>
            {
                string pre, match, post;
                pre = m.Groups["start"].Value;
                match = m.Groups["text"].Value;
                post = m.Groups["end"].Value;
                if (translations.TryGetValue(match, out string? t))
                {
                    return pre + t + post;
                }
                else
                {
                    return m.Value;
                }
            });
        }
    }
}
