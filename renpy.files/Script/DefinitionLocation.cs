﻿using RenPy.Files.Script.Objects;

namespace RenPy.Files.Script
{
    public enum DefinitionType
    {
        Unknown,
        Label,
        Screen,
        Style,
        Image,
        Transform,
        Define,
        Default,
    }

    public class DefinitionLocation : CodeLocation
    {
        public DefinitionType Type { get; private set; }

        public DefinitionLocation(DefinitionType type, string name, LocationInfo originalSource) : base(name, originalSource)
        {
            Type = type;
        }

        public DefinitionLocation(DefinitionType type, string name, string text, LocationInfo originalSource) : base(name, text, originalSource)
        {
            Type = type;
        }

        public override string ToString()
        {
            return $"{Type} {Name} ({LineNumber})";
        }
    }
}
