﻿namespace RenPy.Files.Script
{
    [Serializable]
    internal class RenPyScriptException : Exception
    {
        public RenPyScriptException()
        {
        }

        public RenPyScriptException(string? message) : base(message)
        {
        }

        public RenPyScriptException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}