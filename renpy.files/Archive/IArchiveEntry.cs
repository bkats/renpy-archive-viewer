﻿namespace RenPy.Files.Archive
{
    public interface IArchiveEntry
    {
        long Offset { get; }
        long Size { get; }
        byte[] Prefix { get; }
    }
}