﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Internal;
using System.Collections;
using System.Text;

namespace RenPy.Files.Archive
{
    internal static class ArchiveBuilder
    {
        private static readonly string header = "RPA-3.0 {0:X16} {1:X8}\n";

        internal static void Build(Stream archiveStream, Dictionary<string, NewFileEntry> rpaList, Action<double, string> progress, CancellationToken cancel)
        {
            if ((archiveStream.CanSeek == false) || (archiveStream.CanWrite == false))
            {
                throw new RenPyArchiveFileException("Archive builder needs a writable and seekable stream");
            }
            progress(0, "Starting...");

            // Skip header is written later
            archiveStream.Seek(34, SeekOrigin.Begin);

            var total = (double)(rpaList.Count + 1) / 100;
            var done = 0;

            List<FileEntry> files = new();
            foreach (var f in rpaList.Values)
            {
                progress(done / total, $"Writing {f.TargetFile}...");
                cancel.ThrowIfCancellationRequested();
                AddFiller(archiveStream);
                files.Add(WriteFile(archiveStream, f));
                progress(++done / total, $"Finished {f.TargetFile}");
            }

            // Write header
            var offset = archiveStream.Position;
            var key = (uint)(((long)Random.Shared.Next() * 13) + Random.Shared.Next());
            var hdr = string.Format(header, offset, key);
            var buf = Encoding.Default.GetBytes(hdr);
            archiveStream.Seek(0, SeekOrigin.Begin);
            archiveStream.Write(buf);

            cancel.ThrowIfCancellationRequested();
            progress(done / total, $"Writing file index...");

            // Write file index
            archiveStream.Seek(offset, SeekOrigin.Begin);
            var table = BuildTable(files, key);
            var pick = Pickler.Pickle(table);
            File.WriteAllBytes(@"D:\TestOutput\test.pickle", pick);
            var comp = DeflateZlib.CompressZlib(pick);
            archiveStream.Write(comp);

            progress(++done / total, "Finished writing archive");
        }

        private static void AddFiller(Stream archiveStream)
        {
            archiveStream.Write(Fillers.GetFiller());
        }

        internal static FileEntry WriteFile(Stream archive, NewFileEntry org)
        {
            var offset = archive.Position;
            using Stream src = org.GetStream();
            try
            {
                src.CopyTo(archive);
            }
            catch (IOException ex)
            {
                throw new RenPyArchiveFileException($"Failed to read content for: {org.TargetFile}", ex);
            }
            var size = (int)(archive.Position - offset);
            return new(org.TargetFile, size, offset);
        }

        internal static Hashtable BuildTable(List<FileEntry> files, uint key)
        {
            /*
             *  Hashtable:
             *    key = filename
             *    value = Arraylist
             *              object[]
             *                 long offset
             *                 long/int size
             *                 string prefix (just empty)
             */
            Hashtable ifl = new();
            foreach (var f in files)
            {
                ArrayList info = new()
                {
                    new object[] { f.Offset ^ key, f.Size ^ key, string.Empty }
                };
                ifl.Add(f.Filename, info);
            }
            return ifl;
        }
    }

    internal class FileEntry
    {
        public string Filename { get; }
        public int Size { get; }
        public long Offset { get; }

        public FileEntry(string name, int size, long offset)
        {
            Filename = name;
            Size = size;
            Offset = offset;
        }
    }

}