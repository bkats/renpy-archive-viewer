﻿namespace RenPy.Files.Archive
{
    [Serializable]
    internal class RenPyArchiveFileException : Exception
    {
        public RenPyArchiveFileException()
        {
        }

        public RenPyArchiveFileException(string? message) : base(message)
        {
        }

        public RenPyArchiveFileException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}