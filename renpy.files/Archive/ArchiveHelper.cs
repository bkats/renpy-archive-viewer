﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Internal;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace RenPy.Files.Archive
{
    internal partial class ArchiveHelper
    {
        internal static IReadOnlyDictionary<string, ArchiveFileEntry> GetFiles(Stream file, bool forceVersion3 = false)
        {
            int version;
            uint key;
            long offset;

            if (forceVersion3)
            {
                ReadHeaderIgnoreVersion3(file, out version, out key, out offset);
            }
            else
            {
                ReadHeader(file, out version, out key, out offset);
            }
            var buffer = new byte[file.Length - offset];
            try
            {
                file.Seek(offset, SeekOrigin.Begin);
                file.Read(buffer);

                buffer = DeflateZlib.DecompressZlib(buffer);
            }
            catch (Exception ex)
            {
                throw new RenPyArchiveFileException("Failed to load files index", ex);
            }

            // Get files
            try
            {
                return GetArchiveFiles(version, key, buffer);
            }
            catch (Exception ex)
            {
                throw new RenPyArchiveFileException("Unknown parsing fault", ex);
            }
        }

        [GeneratedRegex("RPA-(?<version>\\d)\\.?(?<subversion>\\d)?\\s(?<offset>[0-9A-Fa-f]{16})\\s?(?<key>[0-9A-Fa-f]{0,8})")]
        private static partial Regex MatchWholeHeader();

        private static void ReadHeader(Stream file, out int version, out uint key, out long offset)
        {
            file.Seek(0, SeekOrigin.Begin);
            var buffer = new byte[60];
            file.Read(buffer);
            string header = Encoding.Default.GetString(buffer);
            // RPA-3.0 0000000004e36015 42424242
            if (!header.StartsWith("RPA-"))
            {
                throw new RenPyArchiveFileException("Invalid RenPy Archive");
            }
            var m = MatchWholeHeader().Match(header);
            if (!m.Success)
            {
                throw new RenPyArchiveFileException("Invalid/Unknown Renpy Archive header");
            }

            if (!int.TryParse(m.Groups["version"].Value, out version))
            {
                throw new RenPyArchiveFileException("Failed to get version from archive header");
            }
            key = 0;
            if (version == 3)
            {
                if (!uint.TryParse(m.Groups["key"].Value, System.Globalization.NumberStyles.HexNumber, null, out key))
                {
                    throw new RenPyArchiveFileException("Failed to get key from archive header");
                }
            }
            if (!long.TryParse(m.Groups["offset"].Value, System.Globalization.NumberStyles.HexNumber, null, out offset))
            {
                throw new RenPyArchiveFileException("Failed to get index offset from archive header");
            }
        }

        [GeneratedRegex(@"(?<offset>[0-9A-Fa-f]{16})\s?(?<key>[0-9A-Fa-f]{0,8})")]
        private static partial Regex MatchHeaderData();

        private static void ReadHeaderIgnoreVersion3(Stream file, out int version, out uint key, out long offset)
        {
            // Skip "RPA-3.0 " 8 bytes
            file.Seek(8, SeekOrigin.Begin);
            var buffer = new byte[60];
            file.Read(buffer);
            string header = Encoding.Default.GetString(buffer);
            // 0000000004e36015 42424242
            var m = MatchHeaderData().Match(header);
            if (!m.Success)
            {
                throw new RenPyArchiveFileException("Invalid/Unknown Renpy Archive header");
            }

            version = 3;
            if (!uint.TryParse(m.Groups["key"].Value, System.Globalization.NumberStyles.HexNumber, null, out key))
            {
                throw new RenPyArchiveFileException("Failed to get key from archive header");
            }
            if (!long.TryParse(m.Groups["offset"].Value, System.Globalization.NumberStyles.HexNumber, null, out offset))
            {
                throw new RenPyArchiveFileException("Failed to get index offset from archive header");
            }
        }

        private static IReadOnlyDictionary<string, ArchiveFileEntry> GetArchiveFiles(int version, uint key, byte[] buffer)
        {
            var files = new Dictionary<string, ArchiveFileEntry>();
            var fs = Pickler.Unpickle<Hashtable>(buffer);
            foreach (DictionaryEntry kv in fs)
            {
                object[]? values = null;
                if (kv.Value is ArrayList al)
                {
                    if ((al.Count > 0) && (al[0] is object[] oa))
                    {
                        values = oa;
                    }
                }
                if ((values != null) && (kv.Key is string filename))
                {
                    ArchiveFileEntry entry = ConvertToEntry(version, key, filename, values);
                    files.Add(entry.Filename, entry);
                }
            }
            return files;
        }

        private static ArchiveFileEntry ConvertToEntry(int version, uint key, string filename, object[] values)
        {
            ArchiveFileEntry entry;
            long fileOffset = 0;
            long fileSize = 0;
            byte[] prefix = Array.Empty<byte>();

            if (values.Length >= 2)
            {
                fileOffset = Convert.ToInt64(values[0]);
                fileSize = Convert.ToInt64(values[1]);

                if (version == 3)
                {
                    fileOffset ^= key;
                    fileSize ^= key;
                }
            }
            if (values.Length >= 3)
            {
                if ((values[2] is string prefixString) && !string.IsNullOrEmpty(prefixString))
                {
                    prefix = new byte[prefixString.Length];
                    int i = 0;
                    foreach (char c in prefixString)
                    {
                        prefix[i++] = (byte)c;
                    }
                }
                if (values[2] is byte[] prefixBytes)
                {
                    prefix = prefixBytes;
                }
            }

            entry = new ArchiveFileEntry(filename, fileOffset, fileSize, prefix);
            return entry;
        }
    }
}