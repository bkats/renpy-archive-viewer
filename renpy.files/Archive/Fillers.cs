﻿using System.Text;

namespace RenPy.Files.Archive
{
    internal static class Fillers
    {
        private static readonly string[] fillers = {
            "My eyes are up here\n",
            "Does anybody ever read this\n",
            "Hey where you looking at\n",
            "Is this for real?\n",
            "Scroll further I going up\n",
            "What are you looking for\n",
            "Please don't cum inside\n",
            "Step-brother I'm stuck\n",
            "Are you trying to cheat\n",
            "This file wasn't made by Ren'Py\n",
            "Can we do this another time\n",
            "Do you enjoy reading this VN\n",
            "Maybe you should just run the game\n",
            "Do you like to play some golf\n",
            "Where is Waldo?\n",
            "That achievement is harder than you think\n",
            "Where did you get this game, because this isn't the original RPA file\n",
            "Knock, Knock. Who is there?\n",
            "Please don't shoot the messenger\n",
            "When will we be there?\n",
            "How long will it take?\n",
            "Do we have enough time for this\n",
            "Where are you going?\n",
            "Please don't leave me here all alone\n",
            "I'm here because of your extended car warranty\n",
            "Once upon a time there was a ...\n",
            "This is just a filler\n",
            "Do you want some coffee?\n",

        };

        internal static byte[] GetFiller()
        {
            int i = Random.Shared.Next(fillers.Length);
            return Encoding.ASCII.GetBytes(fillers[i]);
        }
    }
}