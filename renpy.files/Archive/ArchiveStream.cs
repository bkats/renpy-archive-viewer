﻿namespace RenPy.Files.Archive
{
    public class ArchiveStream : Stream
    {
        private readonly IArchiveReader renPyArchiveFile;
        private readonly IArchiveEntry entry;
        private long position = 0;

        public ArchiveStream(IArchiveReader renPyArchiveFile, IArchiveEntry entry)
        {
            this.renPyArchiveFile = renPyArchiveFile;
            this.entry = entry;
        }

        public override bool CanRead => true;

        public override bool CanSeek => true;

        public override bool CanWrite => false;

        public override long Length => entry.Size;

        public override long Position { get => position; set => Seek(value, SeekOrigin.Begin); }

        public override void Flush()
        {
            // No flushing possible (see no need to throw exception)
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int readPrefix = 0;
            // Limit count to end of file entry
            if (position + count > entry.Size)
            {
                count = (int)(entry.Size - position);
            }

            // First load from prefix
            while ((position < entry.Prefix.Length) && (count > 0))
            {
                buffer[offset++] = entry.Prefix[position++];
                count--;
                readPrefix++;
            }

            // Load remaining from actual archive location
            long realPosition = position + entry.Offset - entry.Prefix.Length;
            int read = renPyArchiveFile.ReadInternal(realPosition, buffer, offset, count);
            position += read;
            return readPrefix + read;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            long newpos = position;
            switch (origin)
            {
                case SeekOrigin.Begin:
                    newpos = offset;
                    break;
                case SeekOrigin.Current:
                    newpos += offset;
                    break;
                case SeekOrigin.End:
                    newpos = entry.Size + offset;
                    break;
                default:
                    throw new NotImplementedException();
            }
            // Throw an exception if new position is before the file
            if (newpos < 0)
            {
                throw new ArgumentException("Seek position before file content (negative position)");
            }
            // Limit the position to end of the file
            if (newpos > entry.Size)
            {
                newpos = entry.Size;
            }
            position = newpos;
            return position;
        }

        public override void SetLength(long value)
        {
            //throw new IOException("Read only stream: Can't change file size");
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new IOException("Read only stream: Can't write to file");
        }
    }
}