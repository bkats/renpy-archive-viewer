﻿namespace RenPy.Files.Archive
{
    internal class ArchiveFileEntry : IArchiveEntry
    {

        public ArchiveFileEntry(string filename, long offset, long size)
        {
            Filename = filename;
            Offset = offset;
            Size = size;
            Prefix = Array.Empty<byte>();
        }

        public ArchiveFileEntry(string filename, long offset, long size, byte[] prefix)
        {
            Filename = filename;
            Offset = offset;
            Size = size;
            Prefix = prefix;
        }

        public string Filename { get; }
        public long Offset { get; }
        public long Size { get; }
        public byte[] Prefix { get; }
    }
}
