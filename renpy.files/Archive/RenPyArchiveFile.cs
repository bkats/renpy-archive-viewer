﻿namespace RenPy.Files.Archive
{
    public class RenPyArchiveFile : IDisposable, IArchiveReader
    {
        private readonly Stream file;
        private readonly IReadOnlyDictionary<string, ArchiveFileEntry> files;
        private bool disposedValue;

        public RenPyArchiveFile(string filename, bool forceVersion3 = false)
        {
            Filename = filename;
            file = File.OpenRead(filename);
            files = ArchiveHelper.GetFiles(file, forceVersion3);
        }

        public string Filename { get; }
        public bool IsOpen => !disposedValue;

        public IReadOnlyList<string> Files => files.Keys.ToList();

        public IReadOnlyList<(string filename, long size)> GetFiles()
        {
            List<(string filename, long size)> result = new();
            foreach (var file in files.Keys)
            {
                result.Add((file, files[file].Size));
            }
            return result;
        }

        public Stream GetStream(string filename)
        {
            if (disposedValue)
            {
                throw new RenPyArchiveFileException("Archive is closed");
            }
            if (files.TryGetValue(filename, out ArchiveFileEntry? entry))
            {
                return new ArchiveStream(this, entry);
            }
            throw new RenPyArchiveFileException($"File {filename} does not exist in archive.");
        }

        public void ExtractFile(string filename, string destinationDir)
        {
            ExtractFile(filename, destinationDir, (p) => { }, CancellationToken.None);
        }

        public void ExtractFile(string filename, string destinationDir, Action<double> progressReport, CancellationToken token)
        {
            using var ss = GetStream(filename);
            var sd = Path.GetDirectoryName(filename);
            var dd = string.IsNullOrEmpty(sd) ? destinationDir : Path.Combine(destinationDir, sd);
            var df = Path.Combine(dd, Path.GetFileName(filename));

            Directory.CreateDirectory(dd);

            using var ds = new FileStream(df, FileMode.Create, FileAccess.Write, FileShare.None);
            var copied = 0;
            var bufferSize = (int)Math.Min(ss.Length, 64 * 1024);
            var buffer = System.Buffers.ArrayPool<byte>.Shared.Rent(bufferSize);
            try
            {
                while (copied < ss.Length)
                {
                    var readBytes = ss.Read(buffer);
                    ds.Write(buffer, 0, readBytes);

                    copied += readBytes;
                    progressReport((double)copied / ss.Length * 100);

                    if (token.IsCancellationRequested)
                    {
                        ds.Close();
                        File.Delete(df);
                        token.ThrowIfCancellationRequested();
                    }
                }
            }
            finally
            {
                System.Buffers.ArrayPool<byte>.Shared.Return(buffer);
            }
        }

        int IArchiveReader.ReadInternal(long realPosition, byte[] buffer, int offset, int count)
        {
            if (disposedValue)
                throw new IOException("Archive is closed");
            lock (file)
            {
                file.Seek(realPosition, SeekOrigin.Begin);
                return file.Read(buffer, offset, count);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    file.Close();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public void Close()
        {
            Dispose();
        }
    }

}