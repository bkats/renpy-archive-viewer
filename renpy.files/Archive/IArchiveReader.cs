﻿namespace RenPy.Files.Archive
{
    public interface IArchiveReader
    {
        int ReadInternal(long realPosition, byte[] buffer, int offset, int count);
    }
}
