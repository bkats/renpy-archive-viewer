﻿namespace RenPy.Files.Archive
{
    public class RenPyArchiveBuilder : IDisposable
    {
        private readonly Dictionary<string, NewFileEntry> fileList = new();
        private bool disposedValue;

        public IReadOnlyList<string> FileList => fileList.Keys.ToList();
        public EventHandler<(double progress, string info)>? Progress { get; set; }

        public bool AddFile(string targetName, string orginalFile)
        {
            targetName = targetName.Replace('\\', '/').Trim('/');
            if (fileList.ContainsKey(targetName))
            {
                return false;
            }
            fileList.Add(targetName, new NewFileEntry(targetName, orginalFile));
            return true;
        }

        public bool AddFile(string targetName, Stream orginalFile)
        {
            targetName = targetName.Replace('\\', '/').Trim('/');
            if (fileList.ContainsKey(targetName))
            {
                return false;
            }
            fileList.Add(targetName, new NewFileEntry(targetName, orginalFile));
            return true;
        }

        public bool RemoveFile(string targetName)
        {
            return fileList.Remove(targetName);
        }

        public void BuildArchive(string archiveFile, CancellationToken cancel = default)
        {
            using var s = new FileStream(archiveFile, FileMode.Create, FileAccess.Write, FileShare.None);
            BuildArchive(s, cancel);
        }

        public void BuildArchive(Stream archiveStream, CancellationToken cancel = default)
        {
            if (disposedValue)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
            try
            {
                ArchiveBuilder.Build(archiveStream, fileList, OnProgress, cancel);
            }
            finally
            {
                Dispose();
            }
        }

        public virtual void OnProgress(double progress, string info)
        {
            Progress?.Invoke(this, (progress, info));
        }

        public void BuildArchiveAndReplace(string archiveFile, CancellationToken cancel = default)
        {
            if (File.Exists(archiveFile))
            {
                BuildArchive(archiveFile + ".tmp", cancel);
                File.Replace(archiveFile + ".tmp", archiveFile, archiveFile + ".bak");
            }
            else
            {
                throw new FileNotFoundException($"Archive file {archiveFile} not found");
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (var file in fileList.Values)
                    {
                        file.OriginalContent?.Dispose();
                    }
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    internal class NewFileEntry
    {
        public string TargetFile { get; }
        public string OriginalFile { get; }
        public Stream? OriginalContent { get; }

        internal NewFileEntry(string target, string originalFile)
        {
            TargetFile = target;
            OriginalFile = originalFile;
        }

        internal NewFileEntry(string target, Stream orginalContent)
        {
            TargetFile = target;
            OriginalContent = orginalContent;
            OriginalFile = $"Stream: {TargetFile}";
        }

        internal Stream GetStream()
        {
            return OriginalContent ?? new FileStream(OriginalFile, FileMode.Open, FileAccess.Read, FileShare.Read);
        }
    }
}
