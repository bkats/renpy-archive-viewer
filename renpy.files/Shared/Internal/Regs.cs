﻿using System.Text.RegularExpressions;

namespace RenPy.Files.Shared.Internal
{
    internal static partial class Regs
    {
        [GeneratedRegex(@"\b(?<start>_[_p]?\s*\(\s*(?<type>""{1,3}|'{1,3}))(?<text>(?:.|\n|\r)*?)(?<end>\k<type>\s*\))")]
        internal static partial Regex MatchTranslators();

        [GeneratedRegex(@"^(?<image>[\w\-]+(\s+[\w\-]+)*)(?<rem>.*)$")]
        internal static partial Regex MatchImageName();

        [GeneratedRegex(@"^(?<name>\w+)(?<remaining>.*)$")]
        internal static partial Regex MatchName();

        [GeneratedRegex(@"(?<cmd>\w+)(?<pre>\s*\(\s*(""|'))(?<target>\w+)(?<post>(""|')\s*(,|\)))")]
        internal static partial Regex MatchAction();

        [GeneratedRegex(@"^\w+$")]
        internal static partial Regex MatchVariable();

        [GeneratedRegex(@"(?<type>""|')(?<literal>(.|\\\k<type>)+)\k<type>")]
        internal static partial Regex MatchString();
    }
}
