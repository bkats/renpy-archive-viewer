﻿using System.IO.Compression;

namespace RenPy.Files.Shared.Internal
{
    internal static class DeflateZlib
    {
        internal static byte[] DecompressZlib(byte[] buffer)
        {
            var mem = new MemoryStream(buffer);
            // seek to file index, but skip two byte of Z-lib header which isn't supported by DeflateStream
            mem.Seek(2, SeekOrigin.Begin);
            var output = Decompress(mem);
            // Here the checksum could be checked (But it seems not to work on RPYC blobs)
            //var rawChk = new Span<byte>(buffer, buffer.Length - 4, 4);
            //var orgChk = BitConverter.ToUInt32(new byte[] { rawChk[3], rawChk[2], rawChk[1], rawChk[0] });
            //var chk = Adler32(1, output, 0, output.Length);
            //if (chk != orgChk)
            //{
            //	throw new Exception("Checksum failed");
            //}
            return output;
        }

        private static byte[] Decompress(MemoryStream data)
        {
            using var d = new DeflateStream(data, CompressionMode.Decompress);
            using var t = new MemoryStream();
            d.CopyTo(t);
            return t.ToArray();
        }


        internal static byte[] CompressZlib(byte[] buffer)
        {
            using var o = new MemoryStream();
            // Write header
            o.Write(new byte[] { 0x78, 0x9C });
            using (var d = new DeflateStream(o, CompressionLevel.SmallestSize, true))
            {
                d.Write(buffer);
            }
            // Write adler32 checksum
            var chk = Adler32(1, buffer, 0, buffer.Length);
            var b = BitConverter.GetBytes(chk);
            o.Write(new byte[] { b[3], b[2], b[1], b[0] });

            return o.ToArray();
        }


        // This function is based on zlib-1.1.3, so all credit should go authors
        // Jean-Loup Gailly(jloup@gzip.org) and Mark Adler(madler@alumni.caltech.edu)
        // and contributors of zlib.
        private static ulong Adler32(ulong adler, byte[] buf, int index, int len)
        {
            // largest prime smaller than 65536
            const int BASE = 65521;
            // NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1
            const int NMAX = 5552;

            ulong s1 = adler & 0xffff;
            ulong s2 = adler >> 16 & 0xffff;
            int k;

            while (len > 0)
            {
                k = len < NMAX ? len : NMAX;
                len -= k;
                // Using this block of 16 addition per byte is about 20% faster than just looping over all data per byte
                while (k >= 16)
                {
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    s1 += buf[index++]; s2 += s1;
                    k -= 16;
                }
                if (k != 0)
                {
                    do
                    {
                        s1 += buf[index++]; s2 += s1;
                    }
                    while (--k != 0);
                }
                s1 %= BASE;
                s2 %= BASE;
            }
            return s2 << 16 | s1;
        }

    }
}
