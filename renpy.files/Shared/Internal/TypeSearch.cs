﻿using System.Reflection;

namespace RenPy.Files.Shared.Internal
{
    internal static class TypeSearch
    {
        internal static void Search(Action<Type> action)
        {
            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty;
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.Location.StartsWith(dir))
                {
                    foreach (var type in assembly.GetTypes())
                    {
                        action(type);
                    }
                }
            }
        }
    }
}
