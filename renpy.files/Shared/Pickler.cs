﻿using Pickle;
using RenPy.Files.Shared.Attributes;
using RenPy.Files.Shared.Internal;
using System.Reflection;

namespace RenPy.Files.Shared
{
    internal static class Pickler
    {
        static Pickler()
        {
            TypeSearch.Search(type =>
            {
                var attributes = type.GetCustomAttributes<RenPyClassAttribute>(false);
                foreach (var attribute in attributes)
                {
                    var module = attribute.ModuleName;
                    var className = attribute.ClassName;
                    var constructor = new Pickle.Objects.AnyClassConstructor(type);
                    Unpickler.RegisterConstructor(module, className, constructor);
                }
            });
        }

        internal static T Unpickle<T>(byte[] data)
        {
            var unpickle = new Pickle.Unpickler();
            if (unpickle.Loads(data) is T result)
            {
                return result;
            }
            throw new Exception($"Failed to Unpickle to {typeof(T)}");
        }

        internal static byte[] Pickle<T>(T table)
        {
            if (table == null) throw new ArgumentNullException(nameof(table));

            using var s = new MemoryStream();
            var pick = new Pickle.Pickler();
            pick.Dump(table, s);
            return s.ToArray();
        }

    }
}
