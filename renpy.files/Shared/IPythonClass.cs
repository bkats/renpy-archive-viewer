﻿namespace RenPy.Files.Shared
{
    public interface IPythonClass
    {
        IEnumerable<string> GetFieldNames();
        bool GetFieldValue(string fieldName, out object? state);
    }
}