﻿namespace RenPy.Files.Shared
{
    public interface ICodeLocation
    {
        int LineNumberStatement { get; }
        int LineNumber { get; }
        int ColumnNumber { get; }
        string Name { get; }
        string Text { get; }
        string? MatchHint { get; }

        void SetLineColumn(int lineStatement, int line, int column);
    }
}
