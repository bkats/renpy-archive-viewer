﻿using RenPy.Files.Script.Objects.Ast;
using RenPy.Files.Shared.Attributes;
using System.Collections;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace RenPy.Files.Shared
{
    public static class RepickData
    {
#if DEBUG
        private static readonly HashSet<string> missingClasses = [];
        private static readonly Dictionary<string, Dictionary<string, string>> Reports = [];
#endif

        public static void RepickClass(IPythonClass target)
        {
            foreach (var prop in target.GetType().GetProperties())
            {
                if (prop.CanWrite)
                {
                    // Handle multi fields constructor
                    if (TryLoadMultiField(target, prop)) 
                        continue;

                    // Check for special tags build fields
                    if (TryTagsProperty(target, prop)) 
                        continue;

                    if (TryCustomSetter(target, prop))
                        continue;

                    // Just try to construct field
                    if (target.GetFieldValue(GetFieldName(prop), out object? v) && (v != null))
                    {
                        var type = prop.PropertyType;
                        var canContainNull = GetListContainsNullItems(prop);
                        if (TransformValue(v, type, out object? value, canContainNull))
                        {
                            prop.SetValue(target, value);
                        }
                        else
                        {
#if DEBUG
                            AddReport(target.GetType(), GetFieldName(prop), $"Property {prop.Name} with type {type.Name} could not be loaded with {v.GetType().Name}");
#endif
                        }
                    }
                }
            }
        }

        private static bool TryCustomSetter(IPythonClass target, PropertyInfo prop)
        {
            var at = prop.GetCustomAttributes(typeof(RenPyCustomSetterAttribute), true);
            if (at != null && at.Length == 1 && at[0] is RenPyCustomSetterAttribute setat)
            {
                var set = target.GetType().GetMethod(setat.Setter, BindingFlags.NonPublic | BindingFlags.Instance);
                if (set != null && target.GetFieldValue(GetFieldName(prop), out object? raw))
                {
                    set.Invoke(target, [raw]);
                }
                return true;
            }
            return false;
        }

        private static bool TryTagsProperty(IPythonClass target, PropertyInfo prop)
        {
            var tpa = prop.GetCustomAttributes(typeof(RenPyTagsPropertyAttribute), true);
            if (tpa != null && tpa.Length == 1)
            {
                if (target.GetFieldValue(GetFieldName(prop), out object? raw))
                {
                    if (raw is object[] tags)
                    {
                        var sep = ((RenPyTagsPropertyAttribute)tpa[0]).Separator;
                        prop.SetValue(target, string.Join(sep, tags));
                    }
                    else
                    {
                        prop.SetValue(target, raw?.ToString());
                    }
                }
                // Return true since it was marked as a tag (even if field wasn't found)
                return true;
            }
            return false;
        }

        private static bool TryLoadMultiField(IPythonClass target, PropertyInfo prop)
        {
            var mfa = prop.GetCustomAttributes(typeof(RenPyMultiFieldAttribute), true);
            if (mfa != null && mfa.Length == 1)
            {
                var fieldNames = ((RenPyMultiFieldAttribute)mfa[0]).FieldNames;
                var list = new object?[fieldNames.Length];
                int i = 0;
                bool succes = false;
                foreach (var fn in fieldNames)
                {
                    succes |= target.GetFieldValue(fn, out list[i++]);
                }
                if (succes)
                {
                    var type = prop.PropertyType;
                    var o = Activator.CreateInstance(type, list);
                    if (o != null)
                    {
                        prop.SetValue(target, o);
                    }
                }
                // Return true since it was marked as a multi field (even if fields weren't found)
                return true;
            }
            return false;
        }

        public static bool TransformValue(object v, Type outType, [NotNullWhen(true)] out object? value, bool canContainNull = false)
        {
            value = null;

#if DEBUG
            if (v is Pickle.Objects.ClassDict cd)
            {
                var name = cd.ClassName;
                if (missingClasses.Add(name))
                {
                    System.Diagnostics.Debug.WriteLine($"The Python/RenPy class {name} is not yet implemented");
                }
            }
#endif

            if (v.GetType().IsAssignableTo(outType))
            {
                value = v;
                return true;
            }

            if (outType.IsAssignableTo(typeof(IPythonSetState)))
            {
                value = Activator.CreateInstance(outType);
                if (value != null)
                {
                    ((IPythonSetState)value).SetState(v);
                    return true;
                }
            }

            if (outType.IsEnum)
            {
                if (v is string s)
                {
                    return Enum.TryParse(outType, s, true, out value);
                } 
                else 
                {
                    try
                    {
                        value = Enum.ToObject(outType, v);
                    }
                    catch
                    {
                        return false;
                    }
                    return true;
                }
            }

            if (outType.IsGenericType)
            {
                Type[] typeArguments = outType.GenericTypeArguments;
                var genType = outType.GetGenericTypeDefinition();

                // IReadOnlyList<XXX>
                if (v is IEnumerable al
                    && (typeArguments.Length == 1)
                    && genType.IsAssignableTo(typeof(IReadOnlyList<>)))
                {
                    var con = typeof(RepickData).GetMethod(nameof(ToList))?.MakeGenericMethod(typeArguments);
                    value = con?.Invoke(null, [al, canContainNull]);
                }

                // IDictonary<XXX, YYY>
                if ((typeArguments.Length == 2)
                    && genType.IsAssignableTo(typeof(IReadOnlyDictionary<,>)))
                {
                    switch (v)
                    {
                        case ArrayList ad:
                            var paraType = new Type[] { typeof(ArrayList) };
                            var con = typeof(RepickData).GetMethod(nameof(ToDictionary), paraType)?.MakeGenericMethod(typeArguments);
                            value = con?.Invoke(null, [ad]);
                            break;
                        case IDictionary id:
                            var paraType1 = new Type[] { typeof(IDictionary) };
                            var con1 = typeof(RepickData).GetMethod(nameof(ToDictionary), paraType1)?.MakeGenericMethod(typeArguments);
                            value = con1?.Invoke(null, [id]);
                            break;
                        default:
                            break;
                    }
                }
            }
            return value != null;
        }

        public static bool TransformValue<T>(object? v, [NotNullWhen(true)] out T? value)
        {
            if ((v != null) && TransformValue(v, typeof(T), out object? o) && o is T to)
            {
                value = to;
                return true;
            }
            value = default;
            return false;
        }

        public static T GetValue<T>(IDictionary<string, object> options, string name, T defaultValue)
        {
            T value = defaultValue;
            if (options.TryGetValue(name, out var raw))
            {
                if (raw is T val)
                {
                    value = val;
                }
                else
                {
                    if ((raw is PyExpr expr)
                        && (expr.Expression is T s))
                    {
                        value = s;
                    }
                    else
                    {
                        Trace.TraceInformation($"Attribute {name} with {raw.GetType()} can't be assigned to type {typeof(T)}");
                    }
                }
            }
            return value;
        }

        public static List<T> ToList<T>(IEnumerable al, bool canContainNull = false)
        {
            var list = new List<T>();
            foreach (var item in al)
            {
                if (item != null && TransformValue(item, out T? value))
                {
                    list.Add(value);
                }
                else
                {
                    if (canContainNull)
                    {
#pragma warning disable CS8604 // Possible null reference argument.
                        list.Add(default);
#pragma warning restore CS8604 // Possible null reference argument.
                    }
                }
            }
            return list;
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(ArrayList al)
                where TKey : notnull
        {
            Dictionary<TKey, TValue> dict = [];
            foreach (var item in al)
            {
                if ((item is object[] keyValue)
                    && (keyValue[0] is TKey name)
                    && (keyValue[1] is object raw))
                {
                    if (TransformValue(raw, out TValue? value))
                    {
                        dict.Add(name, value);
                    }
                }
            }
            return dict;
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(IDictionary ht)
                where TKey : notnull
        {
            Dictionary<TKey, TValue> dict = [];
            foreach (var n in ht.Keys)
            {
                if ((n is TKey name)
                    && (ht[n] is object raw))
                {
                    if (TransformValue(raw, out TValue? value))
                    {
                        dict.Add(name, value);
                    }
                }
            }
            return dict;
        }

#if DEBUG
        public static void AddReport(Type type, string item, string v)
        {
            lock (Reports)
            {
                if (Reports.TryGetValue(type.Name, out var fieldMessages))
                {
                    fieldMessages.TryAdd(item, v);
                }
                else
                {
                    var fm = new Dictionary<string, string>
                    {
                        { item, v }
                    };
                    Reports.Add(type.Name, fm);
                }
            }
        }

        public static void PrintMessages()
        {
            foreach (var cl in Reports.Keys)
            {
                System.Diagnostics.Debug.WriteLine($"The Class {cl} has following issue(s):");
                System.Diagnostics.Debug.Indent();
                foreach (var fld in Reports[cl].Keys)
                {
                    System.Diagnostics.Debug.WriteLine($"The field {fld} has following issue: {Reports[cl][fld]}");
                }
                System.Diagnostics.Debug.Unindent();
            }
        }
#endif

        private static string GetFieldName(PropertyInfo prop)
        {
            var fn = prop.GetCustomAttributes(typeof(RenPyFieldNameAttribute), true).FirstOrDefault();
            if (fn != null)
            {
                return ((RenPyFieldNameAttribute)fn).Name;
            }
            return prop.Name.ToLower();
        }

        private static bool GetListContainsNullItems(PropertyInfo prop)
        {
            var fn = prop.GetCustomAttributes(typeof(RenPyListContainsNullItemsAttribute), true).FirstOrDefault();
            return fn != null;
        }
    }
}
