﻿using System.Text;

namespace RenPy.Files.Shared
{
    public class TextBuilder
    {
        private class SharedData
        {
            private readonly StringBuilder sb = new();
            internal readonly List<object> buildInfo = [];

            internal bool LastLineBlank { get; private set; }
            internal int LineNumber { get; private set; } = 1;
            internal string Text => sb.ToString();

            internal void AddLine(string line)
            {
                sb.AppendLine(line);
                LastLineBlank = string.IsNullOrWhiteSpace(line);
                LineNumber++;
            }
        }

        private class LineBuilder(TextBuilder parent, string indentation)
        {
            private readonly TextBuilder parent = parent;
            private readonly StringBuilder line = new();

            private int startLine;
            private bool lineEmpty = true;

            public int StartLine => startLine;
            public int Length => line.Length;
            public string Indentation { get; } = indentation;

            public void EnsureLine(bool sameBlock = false)
            {
                if (lineEmpty)
                {
                    if (!sameBlock)
                        startLine = parent.data.LineNumber;
                    line.Append(Indentation);
                    lineEmpty = false;
                }
            }

            public void AppendLine()
            {
#if DEBUG
                var txt = line.ToString();
                if (txt.Contains('\n')) throw new Exception("Contains new line");
                parent.data.AddLine(txt);
#else
                parent.data.AddLine(line.ToString());
#endif
                lineEmpty = true;
                line.Clear();
            }

            public void Append(string text)
            {
                if (lineEmpty) throw new Exception("New line not created");
                line.Append(text);
            }
        }

        private readonly SharedData data;
        private readonly LineBuilder line;

        public TextBuilder Indent => new(this);
        public bool IsLastLineBlank => data.LastLineBlank;
        public int CurrentLine => data.LineNumber;

        public TextBuilder()
        {
            data = new();
            line = new(this, string.Empty);
        }

        public TextBuilder(IEnumerable<object> info) : this()
        {
            foreach (var item in info)
            {
                data.buildInfo.Add(item);
            }
        }

        private TextBuilder(TextBuilder prev)
        {
            if (prev.line.Length != 0)
            {
                throw new Exception("Current line isn't finished");
            }
            data = prev.data;
            line = new(this, prev.line.Indentation + "    ");
        }

        public void Append(object linePart)
        {
            line.EnsureLine();
            if (linePart is ICodeLocation code)
            {
                code.SetLineColumn(line.StartLine, data.LineNumber, line.Length + 1);
                line.Append(code.Text);
                AddCodeBlock(code);
            }
            else
            {
                var txt = linePart.ToString();
                if (txt != null)
                {
                    if (txt.Contains('\n'))
                    {
                        AddMultipleLines(txt);
                    }
                    else
                    {
                        line.Append(txt);
                    }
                }
            }
        }

        private void AddMultipleLines(string txt)
        {
            var lines = txt.Split('\n');
            foreach (var l in lines[0..^1])
            {
                line.EnsureLine(true);
                line.Append(l);
                line.AppendLine();
            }
            line.EnsureLine(true);
            line.Append(lines[^1]);
        }



        public void Append(params object[] parts)
        {
            foreach (var part in parts)
                Append(part);
        }

        public void AppendLine(params object[] parts)
        {
            Append(parts);
            line.AppendLine();
        }

        /// <summary>
        /// Add a blank line when previous line isn't blank
        /// </summary>
        public void EnsureBlankLine()
        {
            if (!IsLastLineBlank)
            {
                AppendLine();
            }
        }

        private void AddCodeBlock(ICodeLocation code)
        {
            var info = GetInfo<ICodeCollector>();
            info?.AddCodeBlock(code);
        }

        public T? GetInfo<T>()
        {
            return (T?)data.buildInfo?.FirstOrDefault(i => i is T);
        }

        public T? GetInfoAndRemove<T>()
        {
            var info = GetInfo<T>();
            if (info != null)
            {
                RemoveInfo(info);
            }
            return info;
        }

        public void AddInfo(object info)
        {
            data.buildInfo.Add(info);
        }

        public bool RemoveInfo(object info)
        {
            return data.buildInfo.Remove(info);
        }

        public override string ToString()
        {
            return data.Text;
        }

        public static string EscapeText(string text, bool forTranslation = false)
        {
            bool prevSpace = false;
            StringBuilder s = new();
            foreach (char c in text)
            {
                switch (c)
                {
                    case '\\':
                        if (forTranslation)
                        {
                            s.Append("\\\\");
                        }
                        else
                        {
                            s.Append(c);
                        }
                        break;
                    case '\n':
                        s.Append("\\n");
                        break;
                    case '"':
                        s.Append("\\\"");
                        break;
                    case ' ':
                        if (prevSpace && forTranslation)
                        {
                            s.Append("\\ ");
                        }
                        else
                        {
                            s.Append(c);
                        }
                        break;
                    default:
                        s.Append(c);
                        break;
                }
                prevSpace = c == ' ';
            }
            return s.ToString();
        }
    }
}
