﻿namespace RenPy.Files.Shared
{
    internal interface ICodeCollector
    {
        void AddCodeBlock(ICodeLocation code);
    }
}
