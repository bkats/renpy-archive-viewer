﻿namespace RenPy.Files.Shared
{
    public interface IPythonSetState
    {
        void SetState(object state);
    }
}
