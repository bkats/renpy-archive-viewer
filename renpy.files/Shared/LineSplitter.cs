﻿namespace RenPy.Files.Shared
{
    public static class LineSplitter
    {
        public static string[] GetLines(string source)
        {
            bool empty = true;
            var lines = new List<string>();
            var index = 0;
            var end = source.IndexOf("\n", index);
            while (end >= 0)
            {
                var line = GetLine(source, index, end);
                empty = AddLine(lines, line, empty);
                index = end + 1;
                end = source.IndexOf("\n", index);
            }

            if (index < source.Length)
            {
                var line = GetLine(source, index, source.Length);
                AddLine(lines, line, empty);
            }

            return lines.ToArray();
        }

        private static bool AddLine(List<string> lines, string line, bool lastEmpty)
        {
            var empty = line.Length == 0;
            if (!lastEmpty || !empty)
            {
                lines.Add(line);
            }
            return empty;
        }

        private static string GetLine(string source, int offset, int end)
        {
            // strip carriage return characters
            if ((end > offset) && (source[offset] == '\r'))
            {
                offset++;
            }
            if ((end > offset) && (source[end - 1] == '\r'))
            {
                end--;
            }
            var line = source[offset..end].TrimEnd();
            return line;
        }
    }
}