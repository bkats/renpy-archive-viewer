﻿namespace RenPy.Files.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RenPyTagsPropertyAttribute : Attribute
    {
        public char Separator = ' ';

        public RenPyTagsPropertyAttribute() { }

        public RenPyTagsPropertyAttribute(char separator)
        {
            Separator = separator;
        }
    }
}
