﻿namespace RenPy.Files.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RenPyMultiFieldAttribute : Attribute
    {
        public string[] FieldNames { get; }
        public RenPyMultiFieldAttribute(params string[] fieldNames)
        {
            FieldNames = fieldNames;
        }
    }
}
