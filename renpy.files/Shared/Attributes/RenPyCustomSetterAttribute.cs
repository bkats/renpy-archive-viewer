﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RenPy.Files.Shared.Attributes
{
    public class RenPyCustomSetterAttribute(string setter) : Attribute
    {
        public string Setter { get; } = setter;
    }
}
