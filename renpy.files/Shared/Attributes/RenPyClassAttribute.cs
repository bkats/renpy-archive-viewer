﻿namespace RenPy.Files.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class RenPyClassAttribute : Attribute
    {
        public string ModuleName { get; }
        public string ClassName { get; }
        public RenPyClassAttribute(string module, string name)
        {
            ModuleName = module;
            ClassName = name;
        }
    }
}
