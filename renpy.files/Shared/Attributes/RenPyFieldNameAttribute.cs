﻿namespace RenPy.Files.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RenPyFieldNameAttribute : Attribute
    {
        public string Name { get; }
        public RenPyFieldNameAttribute(string name)
        {
            Name = name;
        }
    }
}
