﻿using System.Collections;

namespace RenPy.Files.Shared
{
    public abstract class PythonPickleBaseClass : IPythonClass
    {
        private readonly Dictionary<string, object?> pythonFields = [];
#if DEBUG
        private bool bugCheck = false;
        private readonly HashSet<string> unloadedFields = [];
#endif

        public void SetState(Hashtable state)
        {
            LoadState(state);
        }

        public void SetState(object[] state)
        {
            if ((state.Length == 2) && state[1] is Hashtable fields)
            {
                LoadState(fields);
            }
            else
            {
                throw new ArgumentException("Unexpected state values provided", nameof(state));
            }
        }

        protected virtual void AfterSetState()
        {
#if DEBUG
            bugCheck = true;
#endif
        }

        private void LoadState(Hashtable fields)
        {
            foreach (DictionaryEntry field in fields)
            {
                if (field.Key is string name)
                {
                    pythonFields.Add(name, field.Value);
#if DEBUG
                    unloadedFields.Add(name);
#endif
                }
            }

            RepickData.RepickClass(this);

            AfterSetState();

#if DEBUG
            if (!bugCheck) throw new Exception("missing base call");
            var tn = GetType().Name;
            if (unloadedFields.Count > 0)
            {
                foreach (var item in unloadedFields)
                {
                    RepickData.AddReport(GetType(), item, $"Field isn't loaded");
                }
            }
#endif
        }

        IEnumerable<string> IPythonClass.GetFieldNames()
        {
            if (pythonFields != null)
            {
                return pythonFields.Keys;
            }
            return [];
        }

        bool IPythonClass.GetFieldValue(string fieldName, out object? state)
        {
            state = null;
            if (pythonFields != null)
            {
#if DEBUG
                unloadedFields.Remove(fieldName);
#endif
                return pythonFields.TryGetValue(fieldName, out state);
            }
            return false;
        }
    }
}
