﻿using RenPy.Files.Persistent.Objects;
using RenPy.Files.Shared;
using RenPy.Files.Shared.Internal;

namespace RenPy.Files.Persistent
{
    public class PersistentObject
    {
        private readonly PersistentData value;
        public PersistentObject(string file) : this(File.OpenRead(file))
        {
        }

        public PersistentObject(Stream stream)
        {
            var data = new byte[stream.Length];
            stream.Read(data);
            data = DeflateZlib.DecompressZlib(data);
            value = Pickler.Unpickle<PersistentData>(data);
        }

        public IReadOnlyList<GamePersistent> Data => value.Data;
    }
}
