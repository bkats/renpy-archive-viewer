﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Persistent.Objects
{
    [RenPyClass("renpy.preferences", "Preferences")]
    public class Preferences : PythonPickleBaseClass
    {
        private readonly Dictionary<string, object> preferences = new();
        public IReadOnlyDictionary<string, object> Items => preferences;

        protected override void AfterSetState()
        {
            base.AfterSetState();
            var pc = (IPythonClass)this;
            foreach (string key in pc.GetFieldNames())
            {
                pc.GetFieldValue(key, out object? value);
                preferences.Add(key, value ?? "None");
            }
        }
    }
}
