﻿using RenPy.Files.Shared;
using RenPy.Files.Shared.Attributes;

namespace RenPy.Files.Persistent.Objects
{
    [RenPyClass("renpy.persistent", "Persistent")]
    public class PersistentData : PythonPickleBaseClass
    {
        private readonly Dictionary<string, object> items = new();

        private readonly List<GamePersistent> data = new();

        public IReadOnlyList<GamePersistent> Data => data;
        public IReadOnlyDictionary<string, object> Items => items;

        protected override void AfterSetState()
        {
            base.AfterSetState();
            var pc = (IPythonClass)this;
            foreach (string key in pc.GetFieldNames())
            {
                pc.GetFieldValue(key, out object? value);
                if (!key.StartsWith("_"))
                {
                    if (value != null)
                    {
                        var type = typeof(GamePersistentStrong<>).MakeGenericType(value.GetType());
                        if (Activator.CreateInstance(type, new object[] { key, value }) is GamePersistent x)
                        {
                            data.Add(x);
                        }
                    }
                    else
                    {
                        data.Add(new GamePersistent(key, "None"));
                    }
                }
                else
                {
                    items.Add(key, value ?? "None");
                }
            }
        }
    }

    public class GamePersistent
    {
        public string Name { get; set; }
        public object RawValue { get; set; }

        public GamePersistent(string name, object value)
        {
            Name = name;
            RawValue = value;
        }

        public override string ToString()
        {
            return $"{Name} = {RawValue}";
        }
    }

    public class GamePersistentStrong<T> : GamePersistent where T : notnull
    {
        public T Value { get => (T)RawValue; set => RawValue = value; }

        public GamePersistentStrong(string name, T value) : base(name, value) { }

        public override string ToString()
        {
            return $"{Name} = {Value} ({typeof(T)})";
        }
    }
}
