﻿using Renpy.View.Shared;
using RenPy.Files.Archive;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace RenPy_Archive_Viewer
{
    /// <summary>
    /// Interaction logic for ExtractProgressWindow.xaml
    /// </summary>
    public partial class ExtractProgressWindow : Window
    {
        private readonly ExtractProgressModel data;
        private readonly Task extractTask;
        private readonly CancellationTokenSource cancelExtract;

        public ExtractProgressWindow(RenPyArchiveFile archive, IReadOnlyList<string> files, string destinationDir)
        {
            Owner = App.Current.MainWindow;
            InitializeComponent();
            data = new ExtractProgressModel();
            DataContext = data;
            cancelExtract = new CancellationTokenSource();
            extractTask = new Task(() =>
            {
                var i = 0;
                var token = cancelExtract.Token;
                while (!IsVisible) { Thread.SpinWait(1); }
                while (i < files.Count)
                {
                    data.CurrentFile = files[i];
                    archive.ExtractFile(files[i], destinationDir, (p) => data.FileProgress = p, token);
                    data.TotalProgress = (double)i / files.Count * 100;
                    token.ThrowIfCancellationRequested();
                    i++;
                }
            }, cancelExtract.Token);
            extractTask.ContinueWith(_ => { DialogResult = true; }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void Run()
        {
            extractTask.Start();
            ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            cancelExtract.Cancel();
        }
    }

    public class ExtractProgressModel : ObservableObject
    {
        private string currentFile = string.Empty;
        private double fileProgress;
        private double totalProgress;

        public string CurrentFile
        {
            get { return currentFile; }
            set { SetProperty(ref currentFile, value); }
        }

        public double FileProgress
        {
            get { return fileProgress; }
            set { SetProperty(ref fileProgress, value); }
        }

        public double TotalProgress
        {
            get { return totalProgress; }
            set { SetProperty(ref totalProgress, value); }
        }
    }
}
