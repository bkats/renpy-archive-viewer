﻿using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using RenPy.Files.Script;
using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tools.Font;

namespace RenPy_Archive_Viewer
{
    internal static class ViewHelper
    {
        private static readonly IHighlightingDefinition RenPyHighlighting;
        private static readonly string loremIpsum =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus libero leo, pellentesque ornare, " +
            "adipiscing vitae, rhoncus commodo, nulla. Fusce quis ipsum. Nulla neque massa, feugiat sed, commodo " +
            "in, adipiscing ut, est. In fermentum mattis ligula. Nulla ipsum. Vestibulum condimentum condimentum " +
            "augue. Nunc purus risus, volutpat sagittis, lobortis at, dignissim sed, sapien. Fusce porttitor " +
            "iaculis ante. Curabitur eu arcu. Morbi quam purus, tempor eget, ullamcorper feugiat, commodo " +
            "ullamcorper, neque.";

        static ViewHelper()
        {
            using var s = Application.GetResourceStream(new Uri("pack://application:,,,/renpy.xshd")).Stream;
            using var rd = new System.Xml.XmlTextReader(s);
            RenPyHighlighting = HighlightingLoader.Load(rd, HighlightingManager.Instance);
        }

        public static BitmapSource GetImage(Stream s, bool forceAlpha)
        {
            BitmapImage image = new();
            try
            {
                image.BeginInit();
                image.StreamSource = s;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.EndInit();
                image.Freeze();
            }
            catch
            {
                var text = new FormattedText("Not supported format\n(e.g. SVG)",
                                    new CultureInfo("en-us"), FlowDirection.LeftToRight,
                                    new Typeface("Sans"), 20, new SolidColorBrush(Colors.Black), 1);

                var drawingVisual = new DrawingVisual();
                using (var drawingContext = drawingVisual.RenderOpen())
                {
                    drawingContext.DrawRoundedRectangle(null, new Pen(new SolidColorBrush(Colors.Red), 2), new Rect(0, 0, 320, 160), 5, 5);
                    drawingContext.DrawText(text, new Point(5, 25));
                }
                var bitmap = new RenderTargetBitmap(320, 160, 96, 96, PixelFormats.Pbgra32);
                bitmap.Render(drawingVisual);
                return bitmap;
            }
            return ChangeImageDPI(image, forceAlpha: forceAlpha);
        }

        public static string GetScript(Stream s)
        {
            using var rd = new StreamReader(s);
            return rd.ReadToEnd();
        }

        public static string GetCompiledScript(Stream s)
        {
            var rpyc = CompiledScriptLoader.GetScript("", s);
            return rpyc.GetTextView().Text;
        }

        public static FontContent LoadFont(FileEntry fe)
        {
            var r = new FontContent(fe);
            try
            {
                if (!FontManager.HasFont(fe.Filename))
                {
                    FontManager.LoadFont(fe.Filename, fe.GetStream());
                }
                var s = FontManager.MeasureText(fe.Filename, "Aj", 20);
                int h = (int)s.Height * 8;
                var bitmap = new WriteableBitmap(840, h, 96, 96, PixelFormats.Bgra32, null);
                var rect = new Rect(0, 0, 800, s.Height);
                FontManager.DrawText(bitmap, rect, fe.Filename, "The lazy dog jumps over the quick brown fox?", 20, Colors.Black);
                rect.Y += s.Height + 5;
                FontManager.DrawText(bitmap, rect, fe.Filename, "0123456789 !@#$%^&*()-+= </>[\\]{|}", 20, Colors.Black);
                rect.Y += s.Height + 10;
                FontManager.DrawText(bitmap, rect, fe.Filename, "abcdefghijklmnopqrstuvwxyz", 16, Colors.Black);
                rect.Y += s.Height;
                FontManager.DrawText(bitmap, rect, fe.Filename, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 16, Colors.Black);

                h = (int)(s.Height * 0.8);
                rect.Height = h;
                for (int i = 32; i < 128; i++)
                {
                    string text = $"{(char)i}";
                    s = FontManager.MeasureText(fe.Filename, text, 16);
                    rect.X = 2 + (i % 32) * 26 + (26 - (int)s.Width) / 2;
                    rect.Width = s.Width + 1;
                    rect.Y = (5.5 + i / 32) * h;
                    FontManager.DrawText(bitmap, rect, fe.Filename, text, 16, Colors.Black);
                }

                bitmap.Freeze();

                var info = FontManager.GetFontInfo(fe.Filename);
                r.Preview = bitmap;
                r.FamilyName = info.FamilyName;
                r.StyleName = info.StyleName;
                r.LoremIpsum = FontManager.DrawFormatedText(loremIpsum, fe.Filename, 12, Color.FromRgb(5, 5, 10), 400);
                r.LoremIpsum.Freeze();
            }
            catch (FontRenderException ex)
            {
                r.FamilyName = ex.Message;
            }
            return r;
        }

        public static DefaultContent LoadSelectedFile(FileEntry fe)
        {
            var closeFile = true;
            Stream? s = null;
            try
            {
                s = fe.GetStream();
                switch (fe.FileType)
                {
                    case FileType.Image:
                        return new ImageContent(fe) { Image = GetImage(s, Path.GetExtension(fe.Filename) == ".webp") };
                    case FileType.Script:
                        return new DocumentContent(fe)
                        {
                            Document = new TextDocument(GetScript(s)),
                            HighlightingDefinition = RenPyHighlighting,
                        };
                    case FileType.CompiledScript:
                        return new DocumentContent(fe)
                        {
                            Document = new TextDocument(GetCompiledScript(s)),
                            HighlightingDefinition = RenPyHighlighting,
                        };
                    case FileType.Python:
                        return new DocumentContent(fe)
                        {
                            Document = new TextDocument(GetScript(s)),
                            HighlightingDefinition = HighlightingManager.Instance.GetDefinitionByExtension(".py"),
                        };
                    case FileType.Font:
                        return LoadFont(fe);
                    case FileType.Music:
                    case FileType.Video:
                        closeFile = false;
                        return new MediaContent(fe) { Media = s, };
                    default:
                        break;
                }
            }
            catch (IOException)
            {
                // Just ignore them just display nothing
            }
            finally
            {
                if (closeFile)
                {
                    s?.Close();
                }
            }
            return new DefaultContent(fe);
        }

        private static BitmapSource ChangeImageDPI(BitmapImage source, int dpi = 96, bool forceAlpha = false)
        {
            if ((int)source.DpiX == dpi && !forceAlpha)
                return source;

            WriteableBitmap output;
            if (forceAlpha)
            {
                output = new WriteableBitmap(source.PixelWidth, source.PixelHeight, dpi, dpi, PixelFormats.Bgra32, source.Palette);
            }
            else
            {
                output = new WriteableBitmap(source.PixelWidth, source.PixelHeight, dpi, dpi, source.Format, source.Palette);
            }
            var rect = new Int32Rect(0, 0, source.PixelWidth, source.PixelHeight);
            var stride = output.BackBufferStride;

            output.Lock();
            source.CopyPixels(rect, output.BackBuffer, stride * output.PixelHeight, stride);
            output.Unlock();
            output.Freeze();

            return output;
        }

    }
}
