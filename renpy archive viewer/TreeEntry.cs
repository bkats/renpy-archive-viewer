﻿using Renpy.View.Shared;
using RenPy.Files.Archive;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace RenPy_Archive_Viewer
{
    public enum FileType
    {
        Unknown,
        Directory,
        Image,
        Script,
        CompiledScript,
        Music,
        Video,
        Python,
        Font,
    }

    public abstract class TreeEntry : ObservableObject
    {
        private string name;
        private bool? isChecked = false;
        private readonly DirEntry? parent;

        public TreeEntry(DirEntry? parent, string name)
        {
            this.name = name;
            this.parent = parent;
        }

        public abstract FileType FileType { get; }
        public abstract string FullName { get; }
        public string Name { get => name; set => SetProperty(ref name, value); }
        public virtual bool? IsChecked { get => isChecked; set => SetProperty(ref isChecked, value); }
        public DirEntry? Parent => parent;
        public abstract long Size { get; }

        // TreeView
        private bool isExpanded = false;
        private bool isSelected = false;
        public bool IsExpanded { get => isExpanded; set => SetProperty(ref isExpanded, value); }
        public bool IsSelected
        {
            get => isSelected;
            set
            {
                SetProperty(ref isSelected, value);
                if (value)
                {
                    UpdateSelected(this);
                }
            }
        }

        protected virtual void UpdateSelected(TreeEntry newSelectItem)
        {
            Parent?.UpdateSelected(newSelectItem);
        }
    }

    public class DirEntry : TreeEntry
    {
        private readonly ObservableCollection<TreeEntry> items = new();
        private bool updating = false;

        public DirEntry(DirEntry? parent, string name) : base(parent, name)
        {
            items.CollectionChanged += Items_CollectionChanged;
        }

        public override long Size { get => items.Sum(i => i.Size); }
        public ObservableCollection<TreeEntry> Items => items;
        public override FileType FileType => FileType.Directory;
        public override string FullName => GetFullPath().ToString();
        public override bool? IsChecked
        {
            get => base.IsChecked;
            set => UpdateChildren(value);
        }
        public static bool PauseUpdating { get; set; }

        private void Items_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems != null)
                {
                    foreach (var item in e.NewItems)
                    {
                        if (item is TreeEntry it)
                            it.PropertyChanged += ItemChanged;
                    }
                }
            }
            UpdateChecked();
        }

        private void ItemChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (updating) return;

            if (e.PropertyName == "IsChecked")
            {
                UpdateChecked();
            }
        }

        private void UpdateChecked()
        {
            if (PauseUpdating) return;

            bool? ch = false;
            var en = items.GetEnumerator();
            en.Reset();
            if (en.MoveNext())
            {
                ch = en.Current.IsChecked;
                while (ch != null && en.MoveNext())
                {
                    var item = en.Current;
                    if (item.IsChecked != ch)
                    {
                        ch = null;
                    }
                }
            }
            base.IsChecked = ch;
        }

        private void UpdateChildren(bool? value)
        {
            if (updating)
                return;
            updating = true;

            if (value == null) value = false;
            base.IsChecked = value;

            try
            {
                foreach (var item in items)
                {
                    item.IsChecked = value;
                }
            }
            finally
            {
                updating = false;
            }
        }

        public StringBuilder GetFullPath()
        {
            if (Parent != null)
            {
                var sb = Parent.GetFullPath();
                sb.Append(Name).Append('/');
                return sb;
            }
            return new StringBuilder();
        }
    }

    public class RootEntry : DirEntry
    {
        private readonly MainData data;

        public RootEntry(MainData data) : base(null, "Root")
        {
            this.data = data;
            IsExpanded = true;
        }

        protected override void UpdateSelected(TreeEntry newSelectItem)
        {
            data.SelectedItem = newSelectItem;
        }
    }

    public abstract class FileEntry : TreeEntry
    {
        private readonly long size;
        private bool removed;

        public override FileType FileType => GetFileType();
        public override string FullName => Filename;
        public string Filename => GetFilename();
        public override long Size { get => size; }
        public bool IsRemoved { get => removed; set => SetProperty(ref removed, value); }

        protected FileEntry(DirEntry parent, string name, long size) : base(parent, name)
        {
            this.size = size;
        }

        public abstract Stream GetStream();

        public static readonly Dictionary<string, FileType> SupportedFileTypes = new()
        {
//        (r'\.(png|jpg|jpeg|webp|gif|tif|tiff|bmp|avif|svg)$', renpy.exports.flush_cache_file),
            { ".png", FileType.Image },
            { ".jpg", FileType.Image },
            { ".jpeg", FileType.Image },
            { ".webp", FileType.Image },
            { ".gif", FileType.Image },
            { ".tif", FileType.Image },
            { ".tiff", FileType.Image },
            { ".bmp", FileType.Image },
            { ".avif", FileType.Image },
            { ".svg", FileType.Image },

            { ".rpy", FileType.Script },
            { ".rpym", FileType.Script },
            { ".rpyc", FileType.CompiledScript },
            { ".rpymc", FileType.CompiledScript },
            { ".py", FileType.Python },

//        (r'\.(mp2|mp3|ogg|opus|wav)$', renpy.audio.audio.autoreload),
            { ".mp2", FileType.Music },
            { ".mp3", FileType.Music },
            { ".ogg", FileType.Music },
            { ".opus", FileType.Music },
            { ".wav", FileType.Music },

            { ".avi", FileType.Video },
            { ".webm", FileType.Video },
            { ".mkv", FileType.Video },
            { ".mp4", FileType.Video },
            { ".ogv", FileType.Video },

            { ".otf", FileType.Font },
            { ".ttf", FileType.Font },

        };

        private FileType GetFileType()
        {
            var ext = System.IO.Path.GetExtension(Name).ToLower();
            SupportedFileTypes.TryGetValue(ext, out FileType ft);
            return ft;
        }

        private string GetFilename()
        {
            if (Parent != null)
            {
                var sb = Parent.GetFullPath();
                sb.Append(Name);
                return sb.ToString();
            }
            return Name;
        }
    }

    public class ArchiveFileEntry : FileEntry
    {
        private readonly RenPyArchiveFile archive;

        public ArchiveFileEntry(DirEntry parent, RenPyArchiveFile archive, string filename, long size) : base(parent, Path.GetFileName(filename), size)
        {
            this.archive = archive;
        }

        public override Stream GetStream()
        {
            return archive.GetStream(Filename);
        }
    }

    public class AddedFileEntry : FileEntry
    {
        private readonly string orginalFilename;

        public AddedFileEntry(DirEntry parent, string orginalFilename)
            : base(parent, Path.GetFileName(orginalFilename), new FileInfo(orginalFilename).Length)
        {
            this.orginalFilename = orginalFilename;
        }

        public override Stream GetStream()
        {
            return File.OpenRead(orginalFilename);
        }

        public string OrginalFilename => orginalFilename;
    }
}
