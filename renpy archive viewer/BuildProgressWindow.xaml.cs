﻿using Renpy.View.Shared;
using RenPy.Files.Archive;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace RenPy_Archive_Viewer
{
    /// <summary>
    /// Interaction logic for ExtractProgressWindow.xaml
    /// </summary>
    public partial class BuildProgressWindow : Window
    {
        private readonly BuildProgressModel data;
        private readonly Task buildTask;
        private readonly CancellationTokenSource cancelBuild;

        public BuildProgressWindow(RenPyArchiveBuilder builder, string destinationFile)
        {
            Owner = App.Current.MainWindow;
            InitializeComponent();
            data = new BuildProgressModel();
            DataContext = data;
            cancelBuild = new CancellationTokenSource();
            buildTask = new Task(() =>
            {
                var token = cancelBuild.Token;
                while (!IsVisible) { Thread.SpinWait(1); }
                try
                {
                    builder.Progress += data.BuildProgess;
                    builder.BuildArchive(destinationFile, token);
                }
                catch (System.OperationCanceledException)
                {
                    File.Delete(destinationFile);
                }
                finally
                {
                    builder.Progress -= data.BuildProgess;
                }
            }, cancelBuild.Token);
            buildTask.ContinueWith(_ => { DialogResult = true; }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void Run()
        {
            buildTask.Start();
            ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            cancelBuild.Cancel();
        }
    }

    public class BuildProgressModel : ObservableObject
    {
        private string info = string.Empty;
        private double totalProgress;

        public string Info { get => info; set => SetProperty(ref info, value); }
        public double TotalProgress { get => totalProgress; set => SetProperty(ref totalProgress, value); }

        internal void BuildProgess(object? _1, (double progress, string info) e)
        {
            Info = e.info;
            TotalProgress = e.progress;
        }
    }
}
