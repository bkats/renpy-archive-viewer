﻿using FlyleafLib.MediaPlayer;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;
using Renpy.View.Shared;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace RenPy_Archive_Viewer
{
    public class DefaultContent : ObservableObject, IDisposable
    {
        private TreeEntry orginalItem;

        public TreeEntry OrginalItem { get => orginalItem; set => SetProperty(ref orginalItem, value); }

        public DefaultContent(TreeEntry org) => orginalItem = org;

        protected virtual void Dispose(bool disposing)
        {
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    public class ImageContent : DefaultContent
    {
        private BitmapSource? image;
        public BitmapSource? Image { get => image; set => SetProperty(ref image, value); }

        public ImageContent(FileEntry org) : base(org)
        {
        }
    }

    public class DocumentContent : DefaultContent
    {
        private TextDocument? document;
        private IHighlightingDefinition? highlightingDefinition;
        public TextDocument? Document { get => document; set => SetProperty(ref document, value); }
        public IHighlightingDefinition? HighlightingDefinition { get => highlightingDefinition; set => SetProperty(ref highlightingDefinition, value); }

        public DocumentContent(FileEntry org) : base(org)
        {
        }
    }

    public class DirectoryContent : DefaultContent
    {
        public int FileCount => GetCount<FileEntry>();
        public int DirCount => GetCount<DirEntry>();

        public long TotalSize => OrginalItem?.Size ?? 0;

        public DirectoryContent(DirEntry org) : base(org)
        {
        }

        private int GetCount<T>()
        {
            if (OrginalItem is DirEntry dir)
            {
                return dir.Items.Count(i => i is T);
            }
            return 0;
        }
    }

    public class FontContent : DefaultContent
    {
        public FontContent(TreeEntry org) : base(org)
        {

        }

        public string FamilyName { get; set; } = string.Empty;
        public string StyleName { get; set; } = string.Empty;
        public BitmapSource? Preview { get; set; }
        public BitmapSource? LoremIpsum { get; set; }
    }

    public class MediaContent : DefaultContent
    {
        static private Player? player;
        static private bool looping = true;

        public MediaContent(TreeEntry org) : base(org)
        {
            if (player == null)
            {
                var config = new FlyleafLib.Config();
                player = new Player(config);
            }
            Play = new RelayCommand(PlayExecute);
        }

        public Player? Player => player;

        public bool IsPlaying => player?.Status == Status.Playing || (looping && player?.Status != Status.Paused);
        public bool IsPaused => !IsPlaying;
        public bool Looping { get => looping; set { SetProperty(ref looping, value); } }

        public Stream Media
        {
            set
            {
                if (player != null)
                {
                    player.Open(value);
                    player.PlaybackStopped += Player_PlaybackStopped;
                    player.PropertyChanged += Player_PropertyChanged;
                }
            }
        }

        private bool ignorePause = false;
        private void Player_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(player.Status))
            {
                if (ignorePause && player?.Status == Status.Paused)
                {
                    ignorePause = false;
                }
                else
                {
                    OnPropertyChanged(nameof(IsPaused));
                    OnPropertyChanged(nameof(IsPlaying));
                }
            }
        }

        private void Player_PlaybackStopped(object? sender, PlaybackStoppedArgs e)
        {
            if (player != null)
            {
                Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (player.Status == Status.Ended && looping)
                    {
                        ignorePause = true;
                        player.CurTime = 0;
                    }
                }));
            }
        }

        public ICommand Play { get; }

        private void PlayExecute(object? obj)
        {
            if (player != null)
            {
                if (player.Status == Status.Ended)
                {
                    player.CurTime = 0;
                }
                else
                {
                    player.Play();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (player != null)
                {
                    player.Stop();
                    player.PlaybackStopped -= Player_PlaybackStopped;
                    player.PropertyChanged -= Player_PropertyChanged;
                }
                base.Dispose(disposing);
            }
        }
    }
}