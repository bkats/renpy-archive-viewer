﻿using Renpy.View.Shared;
using RenPy.Files.Archive;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace RenPy_Archive_Viewer
{
    public class MainData : ObservableObject
    {
        private RenPyArchiveFile? archive;
        private readonly ReadOnlyObservableCollection<RootEntry> displayRoot;
        private TreeEntry selectedItem;
        private DefaultContent selectedContent;
        private string newDirName = "New folder";
        private string archiveFilename = string.Empty;
        private bool busy;

        public RenPyArchiveFile? Archive { get => archive; set => SetProperty(ref archive, value); }
        public DirEntry Root { get => displayRoot[0]; }
        public ReadOnlyObservableCollection<RootEntry> DisplayRoot { get => displayRoot; }
        public TreeEntry SelectedItem { get => selectedItem; set { SetProperty(ref selectedItem, value); SetSelectedContent(value); } }
        public DefaultContent SelectedContent { get => selectedContent; private set => SetProperty(ref selectedContent, value); }
        public string NewDirName { get => newDirName; set => SetProperty(ref newDirName, value); }
        public string ArchiveFilename { get => archiveFilename; set => SetProperty(ref archiveFilename, value); }
        public bool Busy { get => busy; set => SetProperty(ref busy, value); }

        public MainData()
        {
            displayRoot = new(new() { new(this) });
            selectedItem = Root;
            selectedContent = new DirectoryContent(Root);
        }

        private void SetSelectedContent(TreeEntry value)
        {
            SelectedContent.Dispose();
            SelectedContent = value switch
            {
                FileEntry fe => ViewHelper.LoadSelectedFile(fe),
                DirEntry dir => new DirectoryContent(dir),
                _ => new DefaultContent(value),
            };
        }

        public void ClearArchive()
        {
            Root.Items.Clear();
            archive = null;
        }

        #region Commands
        public static readonly ICommand NewArchiveCommand = new RelayCommand(NewArchiveExecute);
        public static readonly ICommand OpenArchiveCommand = new RelayCommand(OpenArchiveExecute);
        public static readonly ICommand SaveArchiveCommand = new RelayCommand(SaveArchiveExecute);
        public static readonly ICommand ExtractArchiveCommand = new RelayCommand(ExtractArchiveExecute);
        public static readonly ICommand AddFilesToArchiveCommand = new RelayCommand(AddFilesToArchiveExecute);
        public static readonly ICommand AddFolderToArchiveCommand = new RelayCommand(AddFolderToArchiveExecute);
        public static readonly ICommand MarkForRemovalCommand = new RelayCommand(MarkForRemovalExecute);
        public static readonly ICommand UnMarkForRemovalCommand = new RelayCommand(UnMarkForRemovalExecute);
        public static readonly ICommand RemoveMarkedCommand = new RelayCommand(RemoveMarkedExecute);
        public static readonly ICommand AddDirectoryCommand = new RelayCommand(AddDirectoryExecute);

        public static readonly ICommand About = new RelayCommand(AboutExecute);

        public static void NewArchiveExecute(object? obj)
        {
            if (obj is MainData data)
            {
                data.ClearArchive();
            }
        }

        private static void OpenArchiveExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ArchiveHelper.OpenArchive(data);
            }
        }

        private static void SaveArchiveExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ArchiveHelper.SaveArchive(data);
            }
        }

        private static void ExtractArchiveExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ArchiveHelper.ExtractFromArchive(data);
            }
        }

        private static void AddFilesToArchiveExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ModifyItems.AddNewFile(data);
            }
        }

        private static void AddFolderToArchiveExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ModifyItems.AddNewFolder(data);
            }
        }

        private static void MarkForRemovalExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ModifyItems.MarkDelete(data);
            }
        }

        private static void UnMarkForRemovalExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ModifyItems.UnmarkDelete(data);
            }
        }

        private static void RemoveMarkedExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ModifyItems.CleanUp(data);
            }
        }

        private static void AddDirectoryExecute(object? obj)
        {
            if (obj is MainData data)
            {
                ModifyItems.AddNewDirectory(data).IsSelected = true;
            }
        }

        private static void AboutExecute(object? obj)
        {
            var v = new AboutView()
            {
                Owner = Application.Current.MainWindow,
            };
            v.ShowDialog();
        }
        #endregion Commands
    }
}
