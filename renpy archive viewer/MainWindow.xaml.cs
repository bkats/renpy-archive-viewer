﻿using FlyleafLib;
using System.Windows;
using System.Windows.Input;

namespace RenPy_Archive_Viewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainData data = new();

        public MainWindow()
        {
            InitializeComponent();
            Engine.Start(new EngineConfig()
            {
#if DEBUG
                LogOutput = ":debug",
                LogLevel = LogLevel.Debug,
                FFmpegLogLevel = FFmpegLogLevel.Warning,
#endif

                PluginsPath = ":Plugins",
                FFmpegPath = ":FFmpeg",

                // Use UIRefresh to update Stats/BufferDuration (and CurTime more frequently than a second)
                UIRefresh = true,
                UIRefreshInterval = 100,
                UICurTimePerSecond = false // If set to true it updates when the actual timestamps second change rather than a fixed interval
            });
            DataContext = data;
        }

        private void TreeView_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete:
                    ModifyItems.MarkDelete(data);
                    break;
                case Key.Insert:
                    ModifyItems.UnmarkDelete(data);
                    break;
                case Key.Space:
                    if (data.SelectedItem != null)
                    {
                        data.SelectedItem.IsChecked = data.SelectedItem.IsChecked == false;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
