﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RenPy_Archive_Viewer
{
    internal class ModifyItems
    {
        internal static void MarkDelete(MainData data)
        {
            if (data.Root.IsChecked == false)
            {
                if (data.SelectedItem is DirEntry dir && dir.Items.Count == 0)
                {
                    dir.Parent?.Items.Remove(dir);
                }
                else
                {
                    MarkItem(data.SelectedItem, true);
                }
            }
            else
            {
                MarkSelectedItems(data.Root, true);
            }
        }

        internal static void UnmarkDelete(MainData data)
        {
            if (data.Root.IsChecked == false)
            {
                MarkItem(data.SelectedItem, false);
            }
            else
            {
                MarkSelectedItems(data.Root, false);
            }
        }

        private static void MarkItem(TreeEntry? item, bool delete)
        {
            switch (item)
            {
                case FileEntry file:
                    file.IsRemoved = delete;
                    break;
                case DirEntry dir:
                    foreach (var it in dir.Items)
                    {
                        if (it is FileEntry file)
                        {
                            file.IsRemoved = delete;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private static void MarkSelectedItems(TreeEntry item, bool delete)
        {
            if (item.IsChecked != false)
            {
                if (item is DirEntry dir)
                {
                    foreach (var i in dir.Items)
                    {
                        MarkSelectedItems(i, delete);
                    }
                }
                else
                {
                    MarkItem(item, delete);
                }
            }
        }

        private static string BuildFilter()
        {
            var filter = new StringBuilder("All supported|");
            foreach (var ext in FileEntry.SupportedFileTypes.Keys)
            {
                filter.Append($"*{ext};");
            }
            foreach (var type in Enum.GetValues<FileType>())
            {
                var s = new StringBuilder($"|{type}|");
                var c = false;
                foreach (var ext in FileEntry.SupportedFileTypes.Where(x => x.Value == type).Select(x => x.Key))
                {
                    s.Append($"*{ext};");
                    c = true;
                }
                if (c)
                {
                    filter.Append(s.ToString());
                }
            }
            filter.Append("|All files|*.*");
            return filter.ToString();
        }

        internal static void AddNewFile(MainData data)
        {

            using var ofd = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = BuildFilter(),
                Multiselect = true,
            };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                var dir = GetDirFromSelection(data);
                AddFiles(dir, ofd.FileNames);
            }
        }

        internal static DirEntry AddNewDirectory(MainData data)
        {
            var dir = GetDirFromSelection(data);
            return AddNewDirectory(dir, data.NewDirName);
        }

        private static DirEntry AddNewDirectory(DirEntry dir, string newDirName)
        {
            return AddNewItemToDir(dir, new DirEntry(dir, newDirName));
        }

        internal static async void AddNewFolder(MainData data)
        {
            using var fd = new FolderBrowserDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                var tempRoot = new RootEntry(data);
                DirEntry.PauseUpdating = true;
                data.Busy = true;
                try
                {
                    await System.Threading.Tasks.Task.Run(() => AddFolder(tempRoot, fd.SelectedPath));
                }
                finally
                {
                    DirEntry.PauseUpdating = false;
                }
                var dir = GetDirFromSelection(data);
                IntegrateDirs(tempRoot, dir);
                data.Busy = false;
            }
        }

        private static void IntegrateDirs(DirEntry newDir, DirEntry existingDir)
        {
            foreach (var item in newDir.Items)
            {
                var exist = existingDir.Items.FirstOrDefault(x => x.Name == item.Name);
                // Sub-directory already exist integrate them
                if ((exist is DirEntry ed) && (item is DirEntry dir))
                {
                    IntegrateDirs(dir, ed);
                }
                else
                {
                    AddNewItemToDir(existingDir, item);
                }
            }
        }

        private static DirEntry GetDirFromSelection(MainData data)
        {
            return data.SelectedItem switch
            {
                DirEntry dir => dir,
                FileEntry file => file.Parent ?? data.Root,
                _ => data.Root,
            };
        }

        private static void AddFiles(DirEntry dir, string[] files)
        {
            foreach (var filename in files)
            {
                var ni = new AddedFileEntry(dir, filename);
                AddNewItemToDir(dir, ni);
            }
        }

        private static void AddFolder(DirEntry dir, string path)
        {
            var subFolders = Directory.GetDirectories(path);
            foreach (var subFolder in subFolders)
            {
                var nd = AddNewDirectory(dir, Path.GetFileName(subFolder));
                AddFolder(nd, subFolder);
            }
            var files = Directory.GetFiles(path);
            AddFiles(dir, files);
        }

        private static DirEntry AddNewItemToDir(DirEntry dir, TreeEntry ni)
        {
            // First check for existing item with same name
            var exist = dir.Items.FirstOrDefault(i => i.Name == ni.Name);
            if (exist != null)
            {
                switch (exist)
                {
                    case DirEntry edir:
                        // In case of existing directory added again just do nothing
                        if (ni is DirEntry)
                        {
                            return edir;
                        }
                        // Other cases we allow to have a file and directory with same name
                        break;
                    case ArchiveFileEntry afe:
                        // Just mark existing archive file as deleted (just a replace idea)
                        afe.IsRemoved = true;
                        break;
                    case AddedFileEntry addfe:
                        if (ni is AddedFileEntry)
                        {
                            // Do a replace of the added file by remove existing file
                            dir.Items.Remove(addfe);
                        }
                        // Other cases we allow to have a file and directory with same name
                        break;
                    default:
                        break;
                }
            }

            var ip = dir.Items.FirstOrDefault(x => string.Compare(x.Name, ni.Name) > 0);
            if (ip != null)
            {
                var i = dir.Items.IndexOf(ip);
                dir.Items.Insert(i, ni);
            }
            else
            {
                dir.Items.Add(ni);
            }
            return ni as DirEntry ?? dir;
        }

        internal static void CleanUp(MainData data)
        {
            CleanUpDir(data.Root);
        }

        private static void CleanUpDir(DirEntry dir)
        {
            int i = 0;
            while (i < dir.Items.Count)
            {
                switch (dir.Items[i])
                {
                    case DirEntry subDir:
                        CleanUpDir(subDir);
                        if (subDir.Items.Count == 0)
                        {
                            dir.Items.RemoveAt(i);
                        }
                        else
                        {
                            i++;
                        }
                        break;
                    case FileEntry file:
                        if (file.IsRemoved)
                        {
                            dir.Items.RemoveAt(i);
                        }
                        else
                        {
                            i++;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}