﻿using RenPy.Files.Archive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace RenPy_Archive_Viewer
{
    internal static class ArchiveHelper
    {
        public static void OpenArchive(MainData data)
        {
            using var od = new OpenFileDialog
            {
                DefaultExt = ".rpa",
                Filter = "RenPy Archive (*.rpa)|*.rpa|All Files (*.*)|*.*",
                CheckFileExists = true
            };
            if (od.ShowDialog() == DialogResult.OK)
            {
                data.ClearArchive();
                try
                {
                    data.Archive = LoadArchive(od.FileName);
                    GetFileTree(data.Root, data.Archive);
                    data.ArchiveFilename = od.FileName;
                }
                catch
                {
                    MessageBox.Show($"Failed to open file: {od.FileName}", "RPA Viewer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public static void SaveArchive(MainData data)
        {
            using var od = new SaveFileDialog
            {
                DefaultExt = ".rpa",
                Filter = "RenPy Archive (*.rpa)|*.rpa",
                CheckPathExists = true,
                AddExtension = true
            };
            if (od.ShowDialog() == DialogResult.OK)
            {
                if (od.FileName == data.ArchiveFilename)
                {
                    MessageBox.Show("Overwriting of currently opened archive not supported yet", "RPA viewer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    var newArchive = new RenPyArchiveBuilder();
                    AddItemToArchive(newArchive, data.Root);
                    try
                    {
                        var bld = new BuildProgressWindow(newArchive, od.FileName);
                        bld.Run();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Failed to create archive. Maybe some files could be read or target could not be written:\n{ex}", "RPA Viewer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        public static void ExtractFromArchive(MainData data)
        {
            if (data.Archive == null) return;

            using var db = new FolderBrowserDialog();
            if (db.ShowDialog() == DialogResult.OK)
            {
                var extractList = new List<string>();
                ArchiveHelper.GetFilesToExtract(data.Root, extractList);
                if (extractList.Count > 0)
                {
                    var prg = new ExtractProgressWindow(data.Archive, extractList, db.SelectedPath);
                    prg.Run();
                }
            }
        }

        private static RenPyArchiveFile LoadArchive(string filename)
        {
            RenPyArchiveFile a;
            try
            {
                a = new(filename, true);
            }
            catch
            {
                a = new(filename);
            }
            return a;
        }

        private static DirEntry GetFileTree(DirEntry root, RenPyArchiveFile a)
        {
            root.Items.Clear();
            var files = a.GetFiles();
            foreach (var (filename, size) in files.OrderBy(x => x.filename))
            {
                var p = root;
                var dirs = filename.Split('/');
                int i = 0;
                while (i < dirs.Length - 1)
                {
                    var np = (DirEntry?)p.Items.FirstOrDefault(x => x.Name == dirs[i]);
                    if (np == null)
                    {
                        np = new DirEntry(p, dirs[i]);
                        p.Items.Add(np);
                    }
                    p = np;
                    i++;
                }
                var e = new ArchiveFileEntry(p, a, filename, size);
                p.Items.Add(e);
            }
            return root;
        }

        private static void GetFilesToExtract(DirEntry dir, List<string> extractList)
        {
            foreach (var entry in dir.Items)
            {
                if (entry.IsChecked != false)
                {
                    switch (entry)
                    {
                        case DirEntry subDir:
                            GetFilesToExtract(subDir, extractList);
                            break;
                        case ArchiveFileEntry file:
                            extractList.Add(file.Filename);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private static void AddItemToArchive(RenPyArchiveBuilder archive, TreeEntry item)
        {
            switch (item)
            {
                case DirEntry dir:
                    foreach (var si in dir.Items)
                    {
                        AddItemToArchive(archive, si);
                    }
                    break;
                case ArchiveFileEntry file:
                    if (!file.IsRemoved)
                    {
                        archive.AddFile(file.Filename, file.GetStream());
                    }
                    break;
                case AddedFileEntry file:
                    if (!file.IsRemoved)
                    {
                        archive.AddFile(file.Filename, file.OrginalFilename);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
