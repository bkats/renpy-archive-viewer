﻿using System.Diagnostics;
using System.Reflection;
using System.Windows;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for AboutView.xaml
    /// </summary>
    public partial class AboutView : Window
    {
        public class AboutInfo
        {
            public string Version { get; } = $"V{Assembly.GetExecutingAssembly().GetName().Version}";
        }

        public AboutView()
        {
            InitializeComponent();
            DataContext = new AboutInfo();
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Target) { UseShellExecute = true });
            e.Handled = true;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
