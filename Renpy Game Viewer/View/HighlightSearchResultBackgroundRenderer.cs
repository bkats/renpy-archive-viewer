﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Rendering;
using RenpyGameViewer.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Media;

namespace RenpyGameViewer.View
{

    /// <summary>
    /// AvalonEdit: highlight current line even when not focused
    /// 
    /// Source: http://stackoverflow.com/questions/5072761/avalonedit-highlight-current-line-even-when-not-focused
    /// </summary>
    internal class HighlightSearchResultBackgroundRenderer : IBackgroundRenderer
    {
        private readonly DocumentView editor;
        private ObservableCollection<SearchResult>? searchResults;

        public HighlightSearchResultBackgroundRenderer(DocumentView editor)
        {
            this.editor = editor;
        }

        #region properties
        /// <summary>
        /// Get the <seealso cref="KnownLayer"/> of the <seealso cref="TextEditor"/> control.
        /// </summary>
        public KnownLayer Layer
        {
            get { return KnownLayer.Selection; }
        }

        public ObservableCollection<SearchResult>? SearchResults
        {
            get => searchResults;
            set
            {
                if (searchResults != value)
                {
                    if (searchResults != null)
                    {
                        searchResults.CollectionChanged -= SearchResultsChanged;
                    }
                    searchResults = value;
                    if (searchResults != null)
                    {
                        searchResults.CollectionChanged += SearchResultsChanged;
                    }
                }
            }
        }

        private void SearchResultsChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            editor.TextArea.TextView.Redraw();
        }
        #endregion properties

        #region methods

        private static bool GetVisualArea(TextView view, out int start, out int end)
        {
            view.EnsureVisualLines();

            var visualLines = view.VisualLines;
            if (visualLines.Count > 0)
            {
                start = visualLines.First().FirstDocumentLine.Offset;
                end = visualLines.Last().LastDocumentLine.EndOffset;
                return false;
            }
            start = 0;
            end = 0;
            return true;
        }

        /// <summary>
        /// Draw the background line highlighting of the current line.
        /// </summary>
        /// <param name="textView"></param>
        /// <param name="drawingContext"></param>
        public void Draw(TextView textView, DrawingContext drawingContext)
        {
            if ((editor?.Document == null) || (editor.Document.TextLength == 0))
            {
                return;
            }

            if (GetVisualArea(textView, out int viewStart, out int viewEnd))
            {
                return;
            }

            foreach (var result in GetVisibleResults(viewStart, viewEnd))
            {
                DrawSearchResult(textView, drawingContext, result);
            }
        }

        private IEnumerable<SearchResult> GetVisibleResults(int viewStart, int viewEnd)
        {
            if (SearchResults != null)
            {
                return SearchResults.Where(item => (item.Offset < viewEnd) && (item.EndOffset > viewStart));
            }
            return [];
        }

        private static void DrawSearchResult(TextView textView, DrawingContext drawingContext, SearchResult result)
        {
            BackgroundGeometryBuilder geoBuilder = new()
            {
                AlignToWholePixels = true,
                BorderThickness = 1,
                CornerRadius = 2
            };
            geoBuilder.AddSegment(textView, result);
            var geometry = geoBuilder.CreateGeometry();
            if (geometry != null)
            {
                drawingContext.DrawGeometry(Brushes.Yellow, null, geometry);
            }
        }
        #endregion methods
    }
}
