﻿using RenpyGameViewer.ViewModels;
using System.Windows;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for ExImportBookmarksView.xaml
    /// </summary>
    public partial class ExportBookmarksView : Window
    {
        public ExportBookmarksView(ExportBookmarksViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
            vm.Closing += OnClosing;
        }

        private void OnClosing(object? sender, bool e)
        {
            DialogResult = e;
            Close();
        }
    }
}
