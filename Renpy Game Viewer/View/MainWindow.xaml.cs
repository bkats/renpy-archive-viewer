﻿using RenpyGameViewer.ViewModels;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new GameDataViewModel();
        }

        private void MyEditor_JumpLabelClick(object sender, View.DocumentView.JumpLabelEventArgs e)
        {
            if (DataContext is GameDataViewModel data)
            {
                data.JumpTo(e.JumpLabel, e.ClickedLine);
            }
        }

        private async void SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (sender is ListBox box)
            {
                await Task.Delay(200);
                box.ScrollIntoView(box.SelectedItem);
            }
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            if (DataContext is GameDataViewModel data)
            {
                data.StoreOptions();
            }
        }

        private void Bookmark_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DataContext is GameDataViewModel data && e.OriginalSource != sender)
            {
                data.BookmarkJumpToSelected();
            }
        }

        private void Search_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DataContext is GameDataViewModel data && e.OriginalSource != sender)
            {
                data.TextSearchJumpToSelected();
            }
        }

        private void Bookmarks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is ListBox box)
            {
                box.ScrollIntoView(box.SelectedItem);
            }
        }

        private void Window_Deactivated(object sender, System.EventArgs e)
        {
            // Remove pop-ups when losing focus
            Options.IsChecked = false;
            CheatSheet.IsChecked = false;
        }
    }
}
