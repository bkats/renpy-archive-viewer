﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Rendering;
using System.Windows;
using System.Windows.Media;

namespace RenpyGameViewer.View
{

    /// <summary>
    /// AvalonEdit: highlight current line even when not focused
    /// 
    /// Source: http://stackoverflow.com/questions/5072761/avalonedit-highlight-current-line-even-when-not-focused
    /// </summary>
    internal class HighlightCurrentLineBackgroundRenderer : IBackgroundRenderer
    {
        private readonly DocumentView editor;

        public HighlightCurrentLineBackgroundRenderer(DocumentView editor)
        {
            this.editor = editor;
        }

        #region properties
        /// <summary>
        /// Get the <seealso cref="KnownLayer"/> of the <seealso cref="TextEditor"/> control.
        /// </summary>
        public KnownLayer Layer
        {
            get { return KnownLayer.Background; }
        }
        #endregion properties

        #region methods
        /// <summary>
        /// Draw the background line highlighting of the current line.
        /// </summary>
        /// <param name="textView"></param>
        /// <param name="drawingContext"></param>
        public void Draw(TextView textView, DrawingContext drawingContext)
        {
            if (editor == null)
                return;

            if (editor.Document == null)
                return;

            if (editor.Document.TextLength == 0)
                return;

            textView.EnsureVisualLines();
            var currentLine = editor.Document.GetLineByOffset(editor.CaretOffset);

            var borderPen = new Pen(editor.EditorCurrentLineBorder, editor.EditorCurrentLineBorderThickness);
            if (borderPen.CanFreeze)
                borderPen.Freeze();

            double y1 = int.MaxValue;
            double y2 = int.MinValue;
            foreach (var rect in BackgroundGeometryBuilder.GetRectsForSegment(textView, currentLine, true))
            {
                if (rect.Top < y1) y1 = rect.Top;
                if (rect.Bottom > y2) y2 = rect.Bottom;
            }
            if (y1 > y2) return;
            drawingContext.DrawRectangle(editor.EditorCurrentLineBackground, borderPen,
                                         new Rect(0, y1, textView.ActualWidth, y2 - y1));
        }
        #endregion methods
    }
}
