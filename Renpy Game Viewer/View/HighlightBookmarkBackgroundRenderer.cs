﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Rendering;
using RenpyGameViewer.Logic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace RenpyGameViewer.View
{

    /// <summary>
    /// AvalonEdit: highlight current line even when not focused
    /// 
    /// Source: http://stackoverflow.com/questions/5072761/avalonedit-highlight-current-line-even-when-not-focused
    /// </summary>
    internal class HighlightBookmarkBackgroundRenderer : IBackgroundRenderer
    {
        private readonly DocumentView editor;

        public HighlightBookmarkBackgroundRenderer(DocumentView editor)
        {
            this.editor = editor;
        }

        private IBookmarkManager? manager;
        public IBookmarkManager? Manager
        {
            get => manager;
            set
            {
                if (manager != null)
                {
                    manager.RedrawRequested -= Manager_RedrawRequested;
                }
                manager = value;
                if (manager != null)
                {
                    manager.RedrawRequested += Manager_RedrawRequested;
                }
            }
        }

        private void Manager_RedrawRequested(object? sender, System.EventArgs e) => editor.TextArea.TextView.InvalidateVisual();

        #region properties
        /// <summary>
        /// Get the <seealso cref="KnownLayer"/> of the <seealso cref="TextEditor"/> control.
        /// </summary>
        public KnownLayer Layer
        {
            get { return KnownLayer.Background; }
        }
        #endregion properties

        #region methods

        private static bool GetVisualArea(TextView view, out int start, out int end)
        {
            view.EnsureVisualLines();

            var visualLines = view.VisualLines;
            if (visualLines.Count > 0)
            {
                start = visualLines.First().FirstDocumentLine.LineNumber;
                end = visualLines.Last().LastDocumentLine.LineNumber;
                return false;
            }
            start = 0;
            end = 0;
            return true;
        }

        /// <summary>
        /// Draw the background line highlighting of the current line.
        /// </summary>
        /// <param name="textView"></param>
        /// <param name="drawingContext"></param>
        public void Draw(TextView textView, DrawingContext drawingContext)
        {
            if ((editor?.Document == null)
                || (editor.Document.TextLength == 0)
                || Manager == null)
            {
                return;
            }

            if (GetVisualArea(textView, out int viewStart, out int viewEnd))
            {
                return;
            }

            var brush = new LinearGradientBrush(
                new GradientStopCollection() {
                    new GradientStop(Colors.Khaki, 0),
                    new GradientStop(Colors.Transparent, 0.3),
                    new GradientStop(Colors.Transparent, 0.6),
                    new GradientStop(Colors.Khaki, 1) },
                0);


            for (var i = viewStart; i < viewEnd; i++)
            {
                if (Manager.TryGetValue(i, out var bm))
                {
                    var line = editor.Document.GetLineByNumber(i);
                    double y1 = int.MaxValue;
                    double y2 = int.MinValue;
                    foreach (var rect in BackgroundGeometryBuilder.GetRectsForSegment(textView, line, true))
                    {
                        if (rect.Top < y1) y1 = rect.Top;
                        if (rect.Bottom > y2) y2 = rect.Bottom;
                    }
                    if (y1 > y2) return;
                    drawingContext.DrawRectangle(brush, null,
                                                 new Rect(0, y1, textView.ActualWidth, y2 - y1));
                }
            }
        }

        #endregion methods
    }
}
