﻿using ICSharpCode.AvalonEdit.Rendering;
using RenpyGameViewer.ViewModels;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;

namespace RenpyGameViewer.View
{
    public class DefinitionElementGenerator : ScriptElementGenerator<ScriptDefinition>
    {
        private ScriptDefinition? lastMatch;
        private readonly DocumentView editor;

        public DefinitionElementGenerator(DocumentView editor)
        {
            this.editor = editor;
        }

        public override VisualLineElement? ConstructElement(int offset)
        {
            if (lastMatch == null)
                return null;

            return new DefinitionVisualText(CurrentContext.VisualLine, lastMatch.Text.Length)
            {
                Definition = lastMatch,
                Editor = editor,
            };
        }

        public override int GetFirstInterestedOffset(int startOffset)
        {
            var defs = editor.ScriptDocument?.Definitions;
            if (defs != null)
            {
                lastMatch = GetMatch(defs, startOffset, out int matchOffset);
                return matchOffset;
            }
            return -1;
        }
    }

    internal class DefinitionVisualText : VisualLineText
    {
        public ScriptDefinition? Definition { get; set; }
        public DocumentView? Editor { get; set; }

        public DefinitionVisualText(VisualLine parentVisualLine, int length) : base(parentVisualLine, length)
        {
        }

        public override TextRun CreateTextRun(int startVisualColumn, ITextRunConstructionContext context)
        {
            var tf = TextRunProperties.Typeface;
            TextRunProperties.SetTypeface(new Typeface(tf.FontFamily, FontStyles.Italic, FontWeights.Medium, tf.Stretch));
            return base.CreateTextRun(startVisualColumn, context);
        }
    }
}
