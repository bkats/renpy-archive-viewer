﻿using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace RenpyGameViewer.View
{
    public class PathTextBlock : FrameworkElement
    {
        private FormattedText? formatText;

        #region Properties
        [Bindable(true), Category("Common")]
        [Localizability(LocalizationCategory.Text)]
        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text), typeof(string), typeof(PathTextBlock), new PropertyMetadata(string.Empty, PropertyChanged));

        private static void PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PathTextBlock p)
            {
                p.InvalidateMeasure();
                p.InvalidateVisual();
            }
        }

        /// <summary>
        ///     The DependencyProperty for the Foreground property.
        ///     Flags:              Can be used in style rules
        ///     Default Value:      System Font Color
        /// </summary>
        public static readonly DependencyProperty ForegroundProperty =
        TextElement.ForegroundProperty.AddOwner(
                typeof(PathTextBlock),
                new FrameworkPropertyMetadata(SystemColors.ControlTextBrush,
                    FrameworkPropertyMetadataOptions.Inherits, PropertyChanged));

        /// <summary>
        ///     An brush that describes the foreground color.
        ///     This will only affect controls whose template uses the property
        ///     as a parameter. On other controls, the property will do nothing.
        /// </summary>
        [Bindable(true), Category("Appearance")]
        public Brush Foreground
        {
            get { return (Brush)GetValue(ForegroundProperty); }
            set { SetValue(ForegroundProperty, value); }
        }

        /// <summary>
        ///     The DependencyProperty for the FontFamily property.
        ///     Flags:              Can be used in style rules
        ///     Default Value:      System Dialog Font
        /// </summary>
        public static readonly DependencyProperty FontFamilyProperty =
                TextElement.FontFamilyProperty.AddOwner(
                        typeof(PathTextBlock),
                        new FrameworkPropertyMetadata(SystemFonts.MessageFontFamily,
                            FrameworkPropertyMetadataOptions.Inherits, PropertyChanged));

        /// <summary>
        ///     The font family of the desired font.
        ///     This will only affect controls whose template uses the property
        ///     as a parameter. On other controls, the property will do nothing.
        /// </summary>
        [Bindable(true), Category("Appearance")]
        [Localizability(LocalizationCategory.Font)]
        public FontFamily FontFamily
        {
            get { return (FontFamily)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        /// <summary>
        ///     The DependencyProperty for the FontSize property.
        ///     Flags:              Can be used in style rules
        ///     Default Value:      System Dialog Font Size
        /// </summary>
        public static readonly DependencyProperty FontSizeProperty =
                TextElement.FontSizeProperty.AddOwner(
                        typeof(PathTextBlock),
                        new FrameworkPropertyMetadata(SystemFonts.MessageFontSize,
                            FrameworkPropertyMetadataOptions.Inherits, PropertyChanged));

        /// <summary>
        ///     The size of the desired font.
        ///     This will only affect controls whose template uses the property
        ///     as a parameter. On other controls, the property will do nothing.
        /// </summary>
        [TypeConverter(typeof(FontSizeConverter))]
        [Bindable(true), Category("Appearance")]
        [Localizability(LocalizationCategory.None)]
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        /// <summary>
        ///     The DependencyProperty for the FontStretch property.
        ///     Flags:              Can be used in style rules
        ///     Default Value:      FontStretches.Normal
        /// </summary>
        public static readonly DependencyProperty FontStretchProperty
            = TextElement.FontStretchProperty.AddOwner(typeof(PathTextBlock),
                    new FrameworkPropertyMetadata(TextElement.FontStretchProperty.DefaultMetadata.DefaultValue,
                        FrameworkPropertyMetadataOptions.Inherits, PropertyChanged));

        /// <summary>
        ///     The stretch of the desired font.
        ///     This will only affect controls whose template uses the property
        ///     as a parameter. On other controls, the property will do nothing.
        /// </summary>
        [Bindable(true), Category("Appearance")]
        public FontStretch FontStretch
        {
            get { return (FontStretch)GetValue(FontStretchProperty); }
            set { SetValue(FontStretchProperty, value); }
        }

        /// <summary>
        ///     The DependencyProperty for the FontStyle property.
        ///     Flags:              Can be used in style rules
        ///     Default Value:      System Dialog Font Style
        /// </summary>
        public static readonly DependencyProperty FontStyleProperty =
                TextElement.FontStyleProperty.AddOwner(
                        typeof(PathTextBlock),
                        new FrameworkPropertyMetadata(SystemFonts.MessageFontStyle,
                            FrameworkPropertyMetadataOptions.Inherits, PropertyChanged));

        /// <summary>
        ///     The style of the desired font.
        ///     This will only affect controls whose template uses the property
        ///     as a parameter. On other controls, the property will do nothing.
        /// </summary>
        [Bindable(true), Category("Appearance")]
        public FontStyle FontStyle
        {
            get { return (FontStyle)GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        /// <summary>
        ///     The DependencyProperty for the FontWeight property.
        ///     Flags:              Can be used in style rules
        ///     Default Value:      System Dialog Font Weight
        /// </summary>
        public static readonly DependencyProperty FontWeightProperty =
                TextElement.FontWeightProperty.AddOwner(
                        typeof(PathTextBlock),
                        new FrameworkPropertyMetadata(SystemFonts.MessageFontWeight,
                            FrameworkPropertyMetadataOptions.Inherits, PropertyChanged));

        /// <summary>
        ///     The weight or thickness of the desired font.
        ///     This will only affect controls whose template uses the property
        ///     as a parameter. On other controls, the property will do nothing.
        /// </summary>
        [Bindable(true), Category("Appearance")]
        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }
        #endregion


        private void FormatText(Size availableSize)
        {
            var ppd = VisualTreeHelper.GetDpi(this).PixelsPerDip;
            bool fit = false;
            var text = Text;

            if (string.IsNullOrEmpty(text))
            {
                if (DesignerProperties.GetIsInDesignMode(this))
                {
                    text = "C:\\Design mode\\Sample path\\Work";
                }
                else
                {
                    formatText = null;
                    return;
                }
            }
            var first = text;
            if (first.Length > 3)
            {
                first = text[0..(text.IndexOf('\\', 3) + 1)].Trim() + "\u2026";
            }
            var remaining = text[first.Length..];
            var minRemain = remaining.Length - remaining.LastIndexOf('\\');
            do
            {
                formatText = new FormattedText(text, new CultureInfo("en-us"), FlowDirection.LeftToRight,
                                        new Typeface(FontFamily, FontStyle, FontWeight, FontStretch), FontSize, Foreground, ppd);
                if (formatText.Width > availableSize.Width)
                {
                    if (remaining.Length > minRemain)
                    {
                        remaining = remaining[1..];
                        text = first + remaining;
                    }
                    else
                    {
                        // order is important first limit height (else it will wrap to multiple lines)
                        formatText.MaxTextHeight = formatText.Height;
                        formatText.MaxTextWidth = availableSize.Width;
                        formatText.Trimming = TextTrimming.CharacterEllipsis;
                        fit = true;
                    }
                }
                else
                {
                    fit = true;
                }
            } while (!fit);
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            FormatText(availableSize);
            if (formatText == null)
            {
                return new Size(0, 0);
            }
            return new Size(double.Min(availableSize.Width, formatText.Width),
                double.Min(availableSize.Height, formatText.Height));
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            return arrangeBounds;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (formatText != null)
            {
                drawingContext.DrawText(formatText, new Point());
            }
        }
    }
}
