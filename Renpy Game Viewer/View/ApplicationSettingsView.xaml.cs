﻿using RenpyGameViewer.ViewModels;
using System.Windows;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for ApplicationSettingsView.xaml
    /// </summary>
    public partial class ApplicationSettingsView : Window
    {
        public ApplicationSettingsView(ApplicationSettingsViewModel asvm)
        {
            InitializeComponent();
            DataContext = asvm;
            asvm.Closing += Asvm_Closing;
        }

        private void Asvm_Closing(object? sender, bool e)
        {
            DialogResult = e;
            Close();
        }
    }
}
