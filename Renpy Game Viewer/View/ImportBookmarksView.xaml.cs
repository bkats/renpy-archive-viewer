﻿using RenpyGameViewer.ViewModels;
using System.Windows;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for ImportBookmarksView.xaml
    /// </summary>
    public partial class ImportBookmarksView : Window
    {
        public ImportBookmarksView(ImportBookmarksViewModel ivm)
        {
            InitializeComponent();
            DataContext = ivm;
            ivm.Closing += OnClosing;
        }

        private void OnClosing(object? sender, bool e)
        {
            Close();
        }
    }
}
