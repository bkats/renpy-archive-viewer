﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using ICSharpCode.AvalonEdit.Search;
using RenpyGameViewer.Logic;
using RenpyGameViewer.ViewModels;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for MyEditor.xaml
    /// </summary>
    public partial class DocumentView : TextEditor
    {
        public DocumentView()
        {
            bookmarkMargin = new BookmarkBarMargin();
            bookmarkBackgroundRender = new HighlightBookmarkBackgroundRenderer(this);
            searchResultBackgroundRenderer = new HighlightSearchResultBackgroundRenderer(this);

            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            TextArea.TextView.BackgroundRenderers.Add(bookmarkBackgroundRender);
            TextArea.TextView.BackgroundRenderers.Add(new HighlightCurrentLineBackgroundRenderer(this));
            TextArea.TextView.BackgroundRenderers.Add(searchResultBackgroundRenderer);
            TextArea.TextView.ElementGenerators.Add(new ReferrerElementGenerator(this));
            TextArea.TextView.ElementGenerators.Add(new DefinitionElementGenerator(this));

            ShowLineNumbers = false;

            TextArea.LeftMargins.Add(bookmarkMargin);

            TextArea.LeftMargins.Add(new LineNumberMargin());

            Line line = new()
            {
                X1 = 0,
                Y1 = 0,
                X2 = 0,
                Y2 = 1,
                StrokeDashArray = { 1, 2 },
                Stretch = Stretch.Fill,
                StrokeThickness = 1,
                StrokeDashCap = PenLineCap.Round,
                Stroke = Brushes.PowderBlue,
                Margin = new Thickness(2, 0, 3, 0),
            };
            TextArea.LeftMargins.Add(line);

            TextArea.Caret.PositionChanged += Caret_PositionChanged;
            TextArea.PreviewKeyDown += TextArea_PreviewKeyDown;

            SearchPanel.Install(TextArea);

            using var s = Application.GetResourceStream(new Uri("pack://application:,,,/renpy.xshd")).Stream;
            using var rd = new System.Xml.XmlTextReader(s);
            SyntaxHighlighting = HighlightingLoader.Load(rd, HighlightingManager.Instance);
        }

        private bool defineUpdating;
        private readonly BookmarkBarMargin bookmarkMargin;
        private readonly HighlightBookmarkBackgroundRenderer bookmarkBackgroundRender;
        private readonly HighlightSearchResultBackgroundRenderer searchResultBackgroundRenderer;

        private void Caret_PositionChanged(object? sender, EventArgs e)
        {
            if (TextArea != null)
            {
                defineUpdating = true;
                CaretLine = TextArea.Caret.Line;
                CurrentRelation = ScriptDocument?.Definitions?.Cast<ScriptDefinition>().FirstOrDefault(d => d.LineNumber == CaretLine);
                if (ScriptDocument?.BookmarkManager != null)
                {
                    if (ScriptDocument.BookmarkManager.TryGetValue(TextArea.Caret.Line, out var bm))
                    {
                        SelectedBookmark = bm;
                    }
                    else
                    {
                        SelectedBookmark = null;
                    }
                }
            }
            else
            {
                CaretLine = 0;
            }
            defineUpdating = false;
        }

        public DocumentViewModel? ScriptDocument
        {
            get => (DocumentViewModel)GetValue(ScriptDocumentProperty);
            set => SetValue(ScriptDocumentProperty, value);
        }

        public static readonly DependencyProperty ScriptDocumentProperty =
            DependencyProperty.Register(nameof(ScriptDocument), typeof(DocumentViewModel), typeof(DocumentView), new PropertyMetadata(ScriptDocumentChanged));

        private static void ScriptDocumentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DocumentView editor)
            {
                editor.BeginInit();
                if (e.NewValue is DocumentViewModel doc)
                {
                    editor.Document = doc.Document;
                    editor.bookmarkMargin.Manager = doc.BookmarkManager;
                    editor.bookmarkBackgroundRender.Manager = doc.BookmarkManager;
                    editor.searchResultBackgroundRenderer.SearchResults = doc.SearchResults;
                }
                else
                {
                    editor.Document = null;
                    editor.bookmarkMargin.Manager = null;
                    editor.bookmarkBackgroundRender.Manager = null;
                    editor.searchResultBackgroundRenderer.SearchResults = null;
                }
                editor.EndInit();
            }
        }

        public int CaretLine
        {
            get { return (int)GetValue(CaretLineProperty); }
            set { SetValue(CaretLineProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CaretLocation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CaretLineProperty =
            DependencyProperty.Register(nameof(CaretLine), typeof(int), typeof(DocumentView), new PropertyMetadata(CaretLineChanged));

        private static void CaretLineChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((d is DocumentView edit)
                && (e.NewValue is int loc)
                && (loc != edit.TextArea.Caret.Line))
            {
                edit.TextArea.Caret.Line = loc;
                edit.ScrollToLine(loc);
            }
        }

        private static readonly DependencyProperty EditorCurrentLineBackgroundProperty =
            DependencyProperty.Register("EditorCurrentLineBackground", typeof(Brush),
                typeof(DocumentView), new UIPropertyMetadata(new SolidColorBrush(
                    Color.FromArgb(0x40, SystemColors.HighlightColor.R,
                                         SystemColors.HighlightColor.G,
                                         SystemColors.HighlightColor.B))));

        public static readonly DependencyProperty EditorCurrentLineBorderProperty =
            DependencyProperty.Register("EditorCurrentLineBorder", typeof(Brush),
                typeof(DocumentView), new PropertyMetadata(new SolidColorBrush(
                    Color.FromArgb(0x80, 0xD0, 0xD0, 0xD0))));

        public static readonly DependencyProperty EditorCurrentLineBorderThicknessProperty =
            DependencyProperty.Register("EditorCurrentLineBorderThickness", typeof(double),
                typeof(DocumentView), new PropertyMetadata(1.0d));

        public Brush EditorCurrentLineBackground
        {
            get { return (Brush)GetValue(EditorCurrentLineBackgroundProperty); }
            set { SetValue(EditorCurrentLineBackgroundProperty, value); }
        }

        /// <summary>
        /// Gets/sets the border color of the current editor line.
        /// </summary>
        public Brush EditorCurrentLineBorder
        {
            get { return (Brush)GetValue(EditorCurrentLineBorderProperty); }
            set { SetValue(EditorCurrentLineBorderProperty, value); }
        }

        /// <summary>
        /// Gets/sets the the thickness of the border of the current editor line.
        /// </summary>
        public double EditorCurrentLineBorderThickness
        {
            get { return (double)GetValue(EditorCurrentLineBorderThicknessProperty); }
            set { SetValue(EditorCurrentLineBorderThicknessProperty, value); }
        }

        public ScriptDefinition? CurrentDefinition
        {
            get { return (ScriptDefinition)GetValue(CurrentDefinitionProperty); }
            set { SetValue(CurrentDefinitionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for scriptDefinition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentDefinitionProperty =
            DependencyProperty.Register(nameof(CurrentDefinition), typeof(ScriptDefinition), typeof(DocumentView), new PropertyMetadata(CurrentDefinitionChanged));

        private static void CurrentDefinitionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((d is DocumentView edit) && (e.NewValue is ScriptDefinition define))
            {
                edit.TextArea.Caret.Line = define.LineNumber;
                edit.ScrollToLine(define.LineNumber);
            }
        }

        public IScriptRelation? CurrentRelation
        {
            get { return (IScriptRelation)GetValue(CurrentRelationProperty); }
            set { SetValue(CurrentRelationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for scriptDefinition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentRelationProperty =
            DependencyProperty.Register(nameof(CurrentRelation), typeof(IScriptRelation), typeof(DocumentView), new PropertyMetadata(CurrentRelationChanged));

        private static void CurrentRelationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((d is DocumentView edit)
                && (e.NewValue is IScriptRelation define)
                && (!edit.defineUpdating))
            {
                edit.TextArea.Caret.Line = define.LineNumber;
                edit.ScrollToLine(define.LineNumber);
            }
        }

        public IBookmark? SelectedBookmark
        {
            get => (IBookmark)GetValue(SelectedBookmarkProperty);
            set => SetValue(SelectedBookmarkProperty, value);
        }

        public static readonly DependencyProperty SelectedBookmarkProperty =
            DependencyProperty.Register(nameof(SelectedBookmark), typeof(IBookmark), typeof(DocumentView));

        public class JumpLabelEventArgs(ScriptDefinition jumpLabel, int clickedLine) : EventArgs
        {
            public ScriptDefinition JumpLabel { get; internal set; } = jumpLabel;
            public int ClickedLine { get; internal set; } = clickedLine;
        }

        public event EventHandler<JumpLabelEventArgs>? JumpLabelClick;
        public virtual void OnJumpLabelClick(ScriptDefinition label, int clickedLine)
        {
            JumpLabelClick?.Invoke(this, new JumpLabelEventArgs(label, clickedLine));
        }

        private void TextArea_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                var shiftPressed = (e.KeyboardDevice.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift;

                if (shiftPressed)
                {
                    MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                }
                else
                {
                    TextArea.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                }

                e.Handled = true;
            }
        }
    }
}
