﻿using ICSharpCode.AvalonEdit.Rendering;
using ICSharpCode.AvalonEdit.Utils;
using RenpyGameViewer.ViewModels;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;

namespace RenpyGameViewer.View
{
    public abstract class ScriptElementGenerator<T> : VisualLineElementGenerator where T : IScriptRelation
    {
        protected T? GetMatch(IEnumerable items, int startOffset, out int matchOffset)
        {
            matchOffset = -1;
            T? match = default;

            var lineNr = CurrentContext.VisualLine.FirstDocumentLine.LineNumber;
            var lineOffset = CurrentContext.VisualLine.FirstDocumentLine.Offset;
            var endOffset = CurrentContext.VisualLine.LastDocumentLine.EndOffset;
            var relevantText = CurrentContext.GetText(startOffset, endOffset - startOffset);

            foreach (var d in items.Cast<T>().Where(x => x.LineNumber == lineNr))
            {
                int offset = -1;
                if (d.ColumnNumber > 0)
                {
                    var o = lineOffset + d.ColumnNumber - 1;
                    if (o >= startOffset)
                    {
                        offset = o;
                    }
                }
                else
                {
                    if (d.MatchHint != null)
                    {
                        offset = UseMatchHint(startOffset, lineOffset, relevantText, d);
                    }
                    else
                    {
                        offset = MatchSearch(startOffset, lineOffset, relevantText, d);
                    }
                }
                if (offset > 0)
                {
                    if (matchOffset == -1 || matchOffset > offset)
                    {
                        matchOffset = offset;
                        match = d;
                    }
                }
            }
            return match;
        }

        private static int MatchSearch(int startOffset, int lineOffset, StringSegment relevantText, T d)
        {
            var txt = d.Text;
            if (txt.Contains('.'))
            {
                txt = Regex.Escape(txt);
            }
            else
            {
                txt = $@"\b{txt}\b";
            }
            var reg = new Regex(txt);
            var m = reg.Match(relevantText.Text, relevantText.Offset, relevantText.Count);
            if (m.Success)
            {
                int offset = m.Index + startOffset - relevantText.Offset;
                d.ChangeColumn(offset - lineOffset + 1);
                return offset;
            }
            return -1;
        }

        private static int UseMatchHint(int startOffset, int lineOffset, StringSegment relevantText, T d)
        {
            if (d.MatchHint != null)
            {
                var offset = relevantText.Text.IndexOf(d.MatchHint);
                if (offset >= 0)
                {
                    offset += d.MatchHint.IndexOf(d.Text);
                    offset += startOffset - relevantText.Offset;
                    d.ChangeColumn(offset - lineOffset + 1);
                    return offset;
                }
            }
            return -1;
        }
    }
}
