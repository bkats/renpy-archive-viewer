﻿using RenpyGameViewer.ViewModels;
using System.Windows;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for ExportScripts.xaml
    /// </summary>
    public partial class ExportScriptsView : Window
    {
        public ExportScriptsView(ExportScripts export)
        {
            InitializeComponent();
            DataContext = export;
            export.Finished += Export_Finished;
        }

        private void Export_Finished(object? sender, bool e)
        {
            DialogResult = e;
            Close();
        }

    }
}
