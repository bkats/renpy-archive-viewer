﻿using RenpyGameViewer.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for GameSelectorView.xaml
    /// </summary>
    public partial class GameSelectorView : Window
    {
        public GameSelectorView(GameSelectorViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
            vm.Finished += Vm_Finished;
        }

        private void Vm_Finished(object? sender, bool e)
        {
            DialogResult = e;
            Close();
        }

        private void ListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DataContext is GameSelectorViewModel vm)
            {
                GameSelectorViewModel.Open.Execute(vm);
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ListBox lb)
            {
                lb.ScrollIntoView(lb.SelectedItem);
            }
        }

        private void TreeViewItem_SelectedChanged(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is TreeViewItem tvi)
            {
                tvi.BringIntoView();
            }
        }
    }
}
