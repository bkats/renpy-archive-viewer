﻿using RenpyGameViewer.ViewModels;
using System.Windows;

namespace RenpyGameViewer.View
{
    /// <summary>
    /// Interaction logic for BookmarkView.xaml
    /// </summary>
    public partial class BookmarkView : Window
    {
        public BookmarkView(BookmarkViewModel bmvm)
        {
            InitializeComponent();
            DataContext = bmvm;
            bmvm.Closing += Bmvm_Closing;
        }

        private void Bmvm_Closing(object? sender, bool e)
        {
            DialogResult = e;
            Close();
        }
    }
}
