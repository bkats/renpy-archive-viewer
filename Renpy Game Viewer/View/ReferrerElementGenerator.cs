﻿using ICSharpCode.AvalonEdit.Rendering;
using RenpyGameViewer.ViewModels;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.TextFormatting;

namespace RenpyGameViewer.View
{
    public class ReferrerElementGenerator : ScriptElementGenerator<ScriptReferrer>
    {
        private ScriptReferrer? lastMatch;
        private readonly DocumentView editor;

        public ReferrerElementGenerator(DocumentView editor)
        {
            this.editor = editor;
        }

        public override VisualLineElement? ConstructElement(int offset)
        {
            var matchOffset = GetFirstInterestedOffset(offset);
            if (lastMatch != null && matchOffset == offset)
            {
                return new ReferrerVisualText(CurrentContext.VisualLine, lastMatch.Text.Length)
                {
                    Referrer = lastMatch,
                    Editor = editor,
                };
            }
            return null;
        }

        public override int GetFirstInterestedOffset(int startOffset)
        {
            if (editor.ScriptDocument?.Referrers != null)
            {
                var refs = editor.ScriptDocument.Referrers.Cast<ScriptReferrer>().Where(x => x.Definition != null);
                lastMatch = GetMatch(refs, startOffset, out int matchOffset);
                return matchOffset;
            }
            return -1;
        }
    }

    internal class ReferrerVisualText : VisualLineText
    {
        public ScriptReferrer? Referrer { get; set; }
        public DocumentView? Editor { get; set; }

        public ReferrerVisualText(VisualLine parentVisualLine, int length) : base(parentVisualLine, length)
        {
        }

        public override TextRun CreateTextRun(int startVisualColumn, ITextRunConstructionContext context)
        {
            TextRunProperties.SetTextDecorations(TextDecorations.Underline);
            return base.CreateTextRun(startVisualColumn, context);
        }

        protected override void OnQueryCursor(QueryCursorEventArgs e)
        {
            e.Handled = true;
            e.Cursor = Cursors.Hand;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (Referrer?.Definition != null)
            {
                e.Handled = true;
                var line = ParentVisualLine.FirstDocumentLine.LineNumber;
                Editor?.OnJumpLabelClick(Referrer.Definition, line);
            }
        }
    }
}
