﻿using RenPy.Files.Archive;
using System.IO;

namespace RenpyGameViewer.ViewModels
{
    public class ArchiveFile : IFile
    {
        private readonly RenPyArchiveFile archive;
        private readonly string filename;
        private readonly string name;
        private readonly string folder;
        private readonly long size;

        public string Name => name;

        public string Folder => folder;

        public long Size => size;

        public ArchiveFile(RenPyArchiveFile archive, string filename, long size)
        {
            this.archive = archive;
            this.filename = filename;
            this.size = size;
            name = Path.GetFileName(filename);
            folder = "/" + Path.GetDirectoryName(filename)?.Replace('\\', '/');
        }

        public Stream GetStream()
        {
            return archive.GetStream(filename);
        }
    }
}
