﻿using RenPy.Files.Script;
using RenpyGameViewer.Logic;
using System.Collections.Generic;
using System.IO;

namespace RenpyGameViewer.ViewModels
{
    public class GameInfo(string rootPath, GameUserOptions options)
    {
        private readonly Languages languages = new();
        private readonly List<GameScriptFile> scripts = [];
        private readonly List<GameScriptFile> translationScripts = [];
        public string RootPath { get; } = rootPath;
        public string GamePath { get; } = Path.Combine(rootPath, "game");
        public string GameName { get; } = Path.GetFileName(rootPath);
        public GameUserOptions Options { get; private set; } = options;
        public IReadOnlyList<GameScriptFile> ScriptFiles => scripts;
        public IReadOnlyList<GameScriptFile> TranslationScriptFiles => translationScripts;
        public IReadOnlyList<string> AvailableLanguages => languages.AvailableLanguages;
        public Languages Languages => languages;

        public void AddScriptFile(GameScriptFile file)
        {
            var lang = file.Language;
            if (lang == Languages.None)
            {
                lock (scripts)
                {
                    scripts.Add(file);
                }
            }
            else
            {
                lock (translationScripts)
                {
                    translationScripts.Add(file);
                }
            }
        }
    }
}
