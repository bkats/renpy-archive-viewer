﻿using Renpy.View.Shared;
using RenpyGameViewer.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public class BookmarkViewModel(ScriptBookmark bookmark, IReadOnlyCollection<GameScriptFile>? files) : ObservableObject
    {
        private readonly ScriptBookmark bookmark = bookmark;
        private readonly IReadOnlyCollection<GameScriptFile>? files = files;

        private string file = bookmark.ScriptFile;
        private int lineNumber = bookmark.LineNumber;
        private string text = bookmark.Text;
        private string? comment = bookmark.Comment;
        private bool isProtected = bookmark.IsProtected;

        public string File
        {
            get => file;
            set
            {
                SetProperty(ref file, value);
                UpdateTextLine();
            }
        }

        public int LineNumber
        {
            get => lineNumber;
            set
            {
                SetProperty(ref lineNumber, int.Max(1, value));
                UpdateTextLine();
            }
        }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }

        public string? Comment
        {
            get => comment;
            set
            {
                if (comment == null && value != null)
                {
                    IsProtected = true;
                }
                SetProperty(ref comment, value);
            }
        }

        public bool IsProtected
        {
            get => isProtected;
            set => SetProperty(ref isProtected, value);
        }

        public IEnumerable<string>? Files => files?.Select(f => f.FullName);

        public static readonly ICommand Okay = new RelayCommand(OkayExecute, OkayCanExecute);
        public static readonly ICommand Cancel = new RelayCommand(CancelExecute);

        private static bool OkayCanExecute(object? arg)
        {
            if (arg is BookmarkViewModel b)
            {
                return b.files?.FirstOrDefault(f => f.FullName == b.File) != null;
            }
            return false;
        }

        private static void OkayExecute(object? obj)
        {
            if (obj is BookmarkViewModel b)
            {
                b.bookmark.ScriptFile = b.file;
                b.bookmark.LineNumber = b.lineNumber;
                b.bookmark.Text = b.text;
                b.bookmark.Comment = string.IsNullOrWhiteSpace(b.comment) ? null : b.comment.Trim();
                b.bookmark.IsProtected = b.isProtected;

                b.Closing?.Invoke(b, true);
            }
        }

        private static void CancelExecute(object? obj)
        {
            if (obj is BookmarkViewModel b)
            {
                b.Closing?.Invoke(b, false);
            }
        }

        public event EventHandler<bool>? Closing;

        public async void UpdateTextLine()
        {
            var file = files?.FirstOrDefault(f => f.FullName == File);
            if (file != null)
            {
                int lineNo = LineNumber;
                string? line = null;
                await Task.Run(() =>
                {
                    (lineNo, line) = BookmarksHelper.UpdateText(file, lineNo);
                });
                if (line != null)
                {
                    LineNumber = lineNo;
                    Text = line;
                }
            }
        }
    }
}
