﻿using Renpy.View.Shared;
using RenpyGameViewer.Logic;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public sealed class ActiveBookMark : ObservableObject, IBookmark, IDisposable
    {
        private readonly ScriptBookmark bookmark;

        public ActiveBookMark(IBookmarkManager parent, ScriptBookmark bookmark)
        {
            this.bookmark = bookmark;
            Parent = parent;
            bookmark.PropertyChanged += Bookmark_PropertyChanged;
        }

        private void Bookmark_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(IsProtected)
                || e.PropertyName == nameof(LineNumber))
            {
                OnPropertyChanged(e.PropertyName);
            }
        }

        public IBookmarkManager Parent { get; }

        public int LineNumber => bookmark.LineNumber;

        public bool IsProtected => bookmark.IsProtected;

        public ScriptBookmark ScriptBookmark => bookmark;

        public void Drop(int lineNumber, string text)
        {
            bookmark.Text = text;
            bookmark.LineNumber = lineNumber;
        }

        public void MouseDown(MouseButtonEventArgs e)
        {
        }
        public void MouseUp(MouseButtonEventArgs e)
        {
            if (!IsProtected)
            {
                Dispose();
                Parent.DeleteBookmark(LineNumber);
                Deleted?.Invoke(this, e);
            }
            e.Handled = true;
        }

        public event EventHandler? Deleted;

        public void Dispose()
        {
            bookmark.PropertyChanged -= Bookmark_PropertyChanged;
        }
    }
}
