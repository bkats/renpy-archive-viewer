﻿using Renpy.View.Shared;
using RenpyGameViewer.Logic;
using RenpyGameViewer.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public class ImportBookmarksViewModel : ObservableObject
    {
        private class UndoAction(bool accepted, List<ScriptBookmark> bms, ScriptBookmark? originalBookmark = null)
        {
            public bool Accepted { get; } = accepted;
            public List<ScriptBookmark> Bookmarks { get; } = bms;
            public ScriptBookmark? OriginalBookmark { get; } = originalBookmark;
        }

        private readonly ObservableCollection<ScriptBookmark> rejected = [];
        private readonly ObservableCollection<ScriptBookmark> relocated = [];
        private readonly ObservableCollection<ScriptBookmark> accepted = [];
        private readonly IReadOnlyCollection<GameScriptFile> scriptFiles;
        private ScriptBookmark? selectedBookmark;
        private readonly string filename;

        private readonly Stack<UndoAction> undoActions = [];
        private bool busy;

        public ImportBookmarksViewModel(string filename, IReadOnlyCollection<GameScriptFile> scriptFiles)
        {
            this.scriptFiles = scriptFiles;
            this.filename = filename;
            var bms = BookmarksHelper.Load(filename);
            ProcessBookmarks(bms);
        }

        public ObservableCollection<ScriptBookmark> RejectedBookmarks => rejected;
        public ObservableCollection<ScriptBookmark> RelocatedBookmarks => relocated;
        public ObservableCollection<ScriptBookmark> AcceptedBookmarks => accepted;
        public ScriptBookmark? SelectedBookmark
        {
            get => selectedBookmark;
            set => SetProperty(ref selectedBookmark, value);
        }
        public string Filename => filename;

        public bool Accepted { get; private set; }

        public bool Busy
        {
            get => busy;
            set => SetProperty(ref busy, value);
        }

        public static readonly ICommand RejectAll = new RelayCommand(RejectAllExecute, AllCanExecute);
        public static readonly ICommand AcceptAll = new RelayCommand(AcceptAllExecute, AllCanExecute);
        public static readonly ICommand RejectOne = new RelayCommand(RejectOneExecute, OneCanExecute);
        public static readonly ICommand AcceptOne = new RelayCommand(AcceptOneExecute, OneCanExecute);
        public static readonly ICommand EditAcceptOne = new RelayCommand(EditAcceptOneExecute, OneCanExecute);

        private static bool AllCanExecute(object? arg) => arg is ImportBookmarksViewModel ib && ib.RelocatedBookmarks.Count > 0;
        private static bool OneCanExecute(object? arg) => arg is ImportBookmarksViewModel ib && ib.SelectedBookmark != null;

        private static void AcceptAllExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel ib)
            {
                ib.undoActions.Push(new UndoAction(true, [.. ib.RelocatedBookmarks]));
                foreach (var bm in ib.RelocatedBookmarks)
                {
                    ib.AcceptedBookmarks.Add(bm);
                }
                ib.RelocatedBookmarks.Clear();
            }
        }

        private static void RejectAllExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel ib)
            {
                ib.undoActions.Push(new UndoAction(false, [.. ib.RelocatedBookmarks]));
                foreach (var bm in ib.RelocatedBookmarks)
                {
                    ib.RejectedBookmarks.Add(bm);
                }
                ib.RelocatedBookmarks.Clear();
            }
        }

        private static void AcceptOneExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel ib)
            {
                if (ib.selectedBookmark != null)
                {
                    ib.undoActions.Push(new UndoAction(true, [ib.selectedBookmark]));
                    ib.AcceptedBookmarks.Add(ib.selectedBookmark);
                    ib.RelocatedBookmarks.Remove(ib.selectedBookmark);
                    ib.SelectedBookmark = null;
                }
            }
        }
        private static void EditAcceptOneExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel ib)
            {
                if (ib.selectedBookmark != null)
                {
                    var bm = ib.selectedBookmark.Clone();
                    var vm = new BookmarkViewModel(bm, ib.scriptFiles);
                    var v = new BookmarkView(vm);
                    if (v.ShowDialog() == true)
                    {
                        ib.undoActions.Push(new UndoAction(true, [bm], ib.selectedBookmark));
                        // Add edited
                        ib.AcceptedBookmarks.Add(bm);
                        // Remove original
                        ib.RelocatedBookmarks.Remove(ib.selectedBookmark);
                        ib.SelectedBookmark = null;
                    }
                }
            }
        }

        private static void RejectOneExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel ib)
            {
                if (ib.selectedBookmark != null)
                {
                    ib.undoActions.Push(new UndoAction(false, [ib.selectedBookmark]));
                    ib.RejectedBookmarks.Add(ib.selectedBookmark);
                    ib.RelocatedBookmarks.Remove(ib.selectedBookmark);
                    ib.SelectedBookmark = null;
                }
            }
        }

        public static readonly ICommand Undo = new RelayCommand(UndoExecute, UndoCanExecute);

        private static bool UndoCanExecute(object? arg) => arg is ImportBookmarksViewModel ib && ib.undoActions.Count > 0;
        private static void UndoExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel ib)
            {
                if (ib.undoActions.Count > 0)
                {
                    var action = ib.undoActions.Pop();
                    if (action.OriginalBookmark != null && action.Accepted)
                    {
                        // Remove edited
                        ib.AcceptedBookmarks.Remove(action.Bookmarks[0]);
                        // Add original
                        ib.RelocatedBookmarks.Add(action.OriginalBookmark);
                    }
                    else
                    {
                        foreach (var item in action.Bookmarks)
                        {
                            if (action.Accepted)
                            {
                                ib.AcceptedBookmarks.Remove(item);
                            }
                            else
                            {
                                ib.RejectedBookmarks.Remove(item);
                            }
                            ib.RelocatedBookmarks.Add(item);
                        }
                    }
                }
            }
        }

        public static readonly ICommand ExportRejected = new RelayCommand(ExportExecute, ExportCanExecute);

        private static bool ExportCanExecute(object? arg) => arg is ImportBookmarksViewModel ib && ib.RejectedBookmarks.Count > 0;
        private static void ExportExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel ib)
            {
                var vm = new ExportBookmarksViewModel(Path.GetDirectoryName(ib.filename) ?? Environment.CurrentDirectory)
                {
                    OnlyComments = false,
                    OnlyProtected = false,
                };
                var v = new ExportBookmarksView(vm);
                if (v.ShowDialog() == true)
                {
                    var export = ExportType.XML;
                    if (vm.AsCSV) export = ExportType.CSV;
                    if (vm.AsJSON) export = ExportType.JSON;

                    var bms = ib.RejectedBookmarks.Where(bm => bm.IsProtected || !vm.OnlyProtected).Where(bm => bm.Comment != null || !vm.OnlyComments);
                    BookmarksHelper.Save(vm.Filename, export, ib.rejected);
                }
            }
        }

        public static readonly ICommand Okay = new RelayCommand(OkayExecute, OkayCanExecute);
        public static readonly ICommand Cancel = new RelayCommand(CancelExecute);

        private static bool OkayCanExecute(object? arg)
        {
            if (arg is ImportBookmarksViewModel i)
            {
                return !i.Busy && i.RelocatedBookmarks.Count == 0;
            }
            return false;
        }

        private static void OkayExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel i)
            {
                i.Accepted = true;
                i.Closing?.Invoke(i, true);
            }
        }

        private static void CancelExecute(object? obj)
        {
            if (obj is ImportBookmarksViewModel i)
            {
                i.Accepted = false;
                i.Closing?.Invoke(i, false);
            }
        }

        public event EventHandler<bool>? Closing;

        private void ProcessBookmarks(List<ScriptBookmark> bms)
        {
            Task.Run(() =>
            {
                Busy = true;
                foreach (var bm in bms)
                {
                    // Default rejected pile
                    var pile = RejectedBookmarks;
                    if (scriptFiles.FirstOrDefault(f => f.FullName == bm.ScriptFile) is GameScriptFile script)
                    {
                        var (lineNo, line) = BookmarksHelper.FindTextLine(script, bm.LineNumber, bm.Text);
                        if (line != null)
                        {
                            if (line == bm.Text && lineNo == bm.LineNumber)
                            {
                                pile = AcceptedBookmarks;
                            }
                            else
                            {
                                pile = RelocatedBookmarks;
                                if (bm.LineNumber != lineNo)
                                {
                                    if (bm.Text != line)
                                    {
                                        bm.Comment = $"[Relocated: Old line: {bm.LineNumber} Old text: {bm.Text}]\n" + bm.Comment;
                                    }
                                    else
                                    {
                                        bm.Comment = $"[Relocated: Old line: {bm.LineNumber}]\n" + bm.Comment;
                                    }
                                }
                                else
                                {
                                    bm.Comment = $"[Relocated: Old text: {bm.Text}]\n" + bm.Comment;
                                }
                                bm.LineNumber = lineNo;
                                bm.Text = line;
                            }
                        }
                    }
                    else
                    {
                        bm.Comment = $"[Rejected file not found]\n" + bm.Comment;
                    }
                    // Add to pile
                    Application.Current.Dispatcher.BeginInvoke(() => pile.Add(bm));
                }
                Busy = false;
            });
        }
    }
}
