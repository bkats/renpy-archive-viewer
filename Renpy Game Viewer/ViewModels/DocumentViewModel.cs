﻿using ICSharpCode.AvalonEdit.Document;
using Renpy.View.Shared;
using RenpyGameViewer.Logic;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RenpyGameViewer.ViewModels
{
    public class DocumentViewModel : ObservableObject
    {
        public DocumentViewModel(GameDataViewModel parent, GameScriptFile file)
        {
            Document = new TextDocument(file.Script);
            if (parent.AllDefinitions != null)
            {
                foreach (var def in parent.AllDefinitions.Where(d => d.ScriptFile == file))
                {
                    Definitions.Add(def);
                }
            }
            var refs = parent.GetScriptReferrers(file);
            if (refs != null)
            {
                Referrers = refs;
            }
            parent.TextSearch.UpdateSearchCurrentDoc();
            SearchResults = parent.TextSearch.SearchResultsCurrentDocument;
            parent.BookmarkList.UpdateBookmarks();
            BookmarkManager = parent.BookmarkList.BookmarkManager;
        }

        public TextDocument Document { get; }
        public List<ScriptDefinition> Definitions { get; } = [];
        public IReadOnlyCollection<ScriptReferrer> Referrers { get; } = [];
        public ObservableCollection<SearchResult> SearchResults { get; }
        public BookmarkManager? BookmarkManager { get; }
    }
}
