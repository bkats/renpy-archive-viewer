﻿namespace RenpyGameViewer.ViewModels
{
    public interface IScriptRelation
    {
        public string Name { get; }
        public string Text { get; }
        public string? MatchHint { get; }
        public GameScriptFile ScriptFile { get; }
        public int LineNumber { get; }
        public int ColumnNumber { get; }

        public void ChangeColumn(int column);
    }

    public class ScriptRelation : IScriptRelation
    {
        public string Name { get; protected set; }
        public string Text => Name;
        string? IScriptRelation.MatchHint { get; }
        public GameScriptFile ScriptFile { get; protected set; }
        public int LineNumber { get; protected set; }
        public int ColumnNumber { get; protected set; }

        public ScriptRelation(string name, GameScriptFile file, int line)
        {
            ScriptFile = file;
            Name = name;
            LineNumber = line;
        }

        public void ChangeColumn(int column)
        {
            ColumnNumber = column;
        }
    }
}