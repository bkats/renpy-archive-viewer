﻿using RenPy.Files.Script;
using System.Collections.Generic;

namespace RenpyGameViewer.ViewModels
{
    public class ScriptDefinition(GameScriptFile scriptFile, DefinitionLocation loc) : CodeScriptRelation<DefinitionLocation>(scriptFile, loc)
    {
        private readonly List<ScriptReferrer> references = [];

        public DefinitionType Type => Info.Type;
        public IReadOnlyList<ScriptReferrer> References => references;

        public void AddReference(ScriptReferrer reference)
        {
            if (reference.Definition == null)
            {
                reference.Definition = this;
                lock (references)
                {
                    references.Add(reference);
                }
            }
        }

        public override string ToString()
        {
            return $"{Type} {Name} => {ScriptFile.Name} ({LineNumber})";
        }
    }
}
