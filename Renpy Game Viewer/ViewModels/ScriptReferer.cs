﻿using RenPy.Files.Script;

namespace RenpyGameViewer.ViewModels
{
    public class ScriptReferrer : CodeScriptRelation<ReferrerLocation>
    {
        public ReferrerType Type => Info.Type;
        public ScriptDefinition? Definition { get; internal set; }
        public DefinitionType DefinitionType => Definition?.Type ?? Info.DefinitionType;

        public ScriptReferrer(GameScriptFile scriptFile, ReferrerLocation loc) : base(scriptFile, loc) { }

        public override string ToString()
        {
            return $"{Type} {Name} => {ScriptFile.Name} ({LineNumber})";
        }
    }
}
