﻿using Microsoft.Win32;
using Renpy.View.Shared;
using RenpyGameViewer.Logic;
using RenpyGameViewer.View;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public class BookmarkListViewModel(GameDataViewModel gameData) : ObservableObject
    {
        private readonly GameDataViewModel gameData = gameData;
        private GameInfo? info;
        private ObservableCollection<ScriptBookmark>? bookmarks;

        public ObservableCollection<ScriptBookmark>? Bookmarks
        {
            get => bookmarks;
            set
            {
                SetProperty(ref bookmarks, value);
                OnPropertyChanged(nameof(SortedBookmarks));
            }
        }

        public ICollectionView? SortedBookmarks
        {
            get
            {
                if (Bookmarks == null) return null;
                var view = new CollectionViewSource() { Source = Bookmarks }.View;
                view.SortDescriptions.Add(new SortDescription(nameof(ScriptBookmark.ScriptFile), ListSortDirection.Ascending));
                view.SortDescriptions.Add(new SortDescription(nameof(ScriptBookmark.LineNumber), ListSortDirection.Ascending));
                return view;
            }
        }

        private ScriptBookmark? selectedBookmark;
        private IBookmark? selectedBookmarkEditor;

        public ScriptBookmark? SelectedBookmark
        {
            get => selectedBookmark;
            set => SetProperty(ref selectedBookmark, value);
        }

        public IBookmark? SelectedBookmarkEditor
        {
            get => selectedBookmarkEditor;
            set
            {
                SetProperty(ref selectedBookmarkEditor, value);
                if (value is ActiveBookMark abm)
                {
                    SelectedBookmark = abm.ScriptBookmark;
                }
                OnPropertyChanged(nameof(SelectedBookmarkEditorComment));
            }
        }

        public string? SelectedBookmarkEditorComment => (SelectedBookmarkEditor as ActiveBookMark)?.ScriptBookmark.Comment;

        private BookmarkManager? manager;
        public BookmarkManager? BookmarkManager
        {
            get => manager;
            set => SetProperty(ref manager, value);
        }

        public GameInfo? Info
        {
            get => info;
            set => SetProperty(ref info, value);
        }

        public void UpdateBookmarks()
        {
            if (gameData.SelectedFile == null || info == null)
            {
                manager?.Clear();
            }
            else
            {
                Bookmarks = info.Options.Bookmarks;
                if (BookmarkManager == null)
                {
                    var bk = new BookmarkManager((ln, text) =>
                    {
                        if (manager != null && gameData.SelectedFile != null)
                        {
                            var b = new ScriptBookmark(ln, text, gameData.SelectedFile.FullName);
                            Bookmarks.Add(b);
                            SelectedBookmark = b;
                            return new ActiveBookMark(manager, b);
                        }
                        return null;
                    });

                    bk.BookmarkDeleted += (s, bm) =>
                    {
                        if (bm is ActiveBookMark a)
                        {
                            Bookmarks.Remove(a.ScriptBookmark);
                        }
                    };
                    BookmarkManager = bk;
                }
                else
                {
                    BookmarkManager.Clear();
                }
                var file = gameData.SelectedFile.FullName;
                var activeList = Bookmarks.Where(b => b.ScriptFile == file).Select(bm => new ActiveBookMark(BookmarkManager, bm));
                foreach (var abm in activeList)
                {
                    BookmarkManager.Add(abm);
                    SelectedBookmarkEditor = abm;
                }
            }
        }

        public void BookmarkAddEdit(bool add)
        {
            if (gameData.SelectedFile == null) return;

            var bm = add ? new(1, "", gameData.SelectedFile.FullName) { IsProtected = true } : SelectedBookmark;
            if (bm != null)
            {
                var bmvm = new BookmarkViewModel(bm, gameData.ScriptFiles);
                var bmv = new BookmarkView(bmvm)
                {
                    Owner = Application.Current.MainWindow
                };
                var r = bmv.ShowDialog();
                if (add && r == true)
                {
                    Bookmarks?.Add(bm);
                    if (bm.ScriptFile == gameData.SelectedFile?.FullName)
                    {
                        manager?.Add(new ActiveBookMark(manager, bm));
                    }
                }
                OnPropertyChanged(nameof(SelectedBookmarkEditorComment));
            }
        }

        public void BookmarkRemove()
        {
            if (Bookmarks != null && SelectedBookmark != null)
            {
                var bm = SelectedBookmark;
                if (bm.IsProtected)
                {
                    var r = MessageBox.Show($"Are you sure to remove '{bm.Text}' bookmark", "Bookmarks", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (r != MessageBoxResult.Yes)
                    {
                        return;
                    }
                }

                Bookmarks.Remove(bm);
                SelectedBookmark = null;
                OnPropertyChanged(nameof(SelectedBookmarkEditorComment));
                UpdateBookmarks();
            }
        }

        private void BookmarksImport()
        {
            if (Info != null && gameData.ScriptFiles != null && Bookmarks != null)
            {
                var fd = new OpenFileDialog()
                {
                    Filter = "Supported|*.csv;*.json;*.txt|All files|*.*",
                    InitialDirectory = Info.RootPath,
                };
                if (fd.ShowDialog() == true)
                {
                    var ivm = new ImportBookmarksViewModel(fd.FileName, gameData.ScriptFiles);
                    var iv = new ImportBookmarksView(ivm);
                    iv.Show();
                    iv.Closed += (s, e) =>
                    {
                        if (ivm.Accepted)
                        {
                            foreach (var bm in ivm.AcceptedBookmarks)
                            {
                                Bookmarks.Add(bm);
                            }
                            UpdateBookmarks();
                        }
                    };
                }
            }
        }

        private void BookmarksExport(string filename, ExportType export, bool onlyProtected, bool onlyComments)
        {
            if (Bookmarks != null)
            {
                var bms = Bookmarks.Where(bm => bm.IsProtected || !onlyProtected).Where(bm => bm.Comment != null || !onlyComments);
                BookmarksHelper.Save(filename, export, bms);
            }
        }


        #region Commands
        public static readonly ICommand AddBookmark = new RelayCommand(AddBookmarkExecute, AddBookmarkCanExecute);
        public static readonly ICommand EditBookmark = new RelayCommand(EditBookmarkExecute, EditBookmarkCanExecute);
        public static readonly ICommand RemoveBookmark = new RelayCommand(RemoveBookmarkExecute, EditBookmarkCanExecute);
        public static readonly ICommand ExportBookmark = new RelayCommand(ExportBookmarkExecute, AddBookmarkCanExecute);
        public static readonly ICommand ImportBookmark = new RelayCommand(ImportBookmarkExecute, AddBookmarkCanExecute);
        public static readonly ICommand RemoveAllBookmark = new RelayCommand(RemoveAllBookmarkExecute, AddBookmarkCanExecute);

        private static void AddBookmarkExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml)
            {
                bml.BookmarkAddEdit(true);
            }
        }

        private static bool AddBookmarkCanExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml)
            {
                return bml.info != null && bml.gameData.ScriptFiles != null && bml.gameData.ScriptFiles.Count > 0;
            }
            return false;
        }

        private static void EditBookmarkExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml)
            {
                bml.BookmarkAddEdit(false);
            }
        }

        private static void RemoveBookmarkExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml)
            {
                bml.BookmarkRemove();
            }
        }

        private static bool EditBookmarkCanExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml)
            {
                return bml.info != null && bml.gameData.ScriptFiles != null && bml.gameData.ScriptFiles.Count > 0 && bml.SelectedBookmark != null;
            }
            return false;
        }

        private static void ExportBookmarkExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml && bml.info != null)
            {
                var vm = new ExportBookmarksViewModel(Path.Combine(bml.info.RootPath, "export"));
                var v = new ExportBookmarksView(vm)
                {
                    Owner = Application.Current.MainWindow
                };
                if (v.ShowDialog() == true)
                {
                    var export = ExportType.JSON;
                    if (vm.AsCSV) export = ExportType.CSV;
                    if (vm.AsJSON) export = ExportType.JSON;
                    if (vm.AsXML) export = ExportType.XML;
                    if (vm.AsTXT) export = ExportType.TXT;
                    bml.BookmarksExport(vm.Filename, export, vm.OnlyProtected, vm.OnlyComments);
                }
            }
        }

        private static void ImportBookmarkExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml)
            {
                bml.BookmarksImport();
            }
        }

        private static void RemoveAllBookmarkExecute(object? obj)
        {
            if (obj is BookmarkListViewModel bml && bml.info != null)
            {
                var r = MessageBox.Show("Are you sure to delete all bookmarks", "Bookmarks", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (r == MessageBoxResult.Yes)
                {
                    bml.Bookmarks?.Clear();
                    bml.UpdateBookmarks();
                }
            }
        }
        #endregion
    }
}
