﻿using Renpy.View.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public class ExportScripts : ObservableObject
    {
        private readonly GameInfo info;
        private string outputPath;

        public ExportScripts(GameInfo info)
        {
            this.info = info;
            SelectedLanguage = info.AvailableLanguages.FirstOrDefault(x => x != RenPy.Files.Script.Languages.None);
            outputPath = Path.Combine(info.RootPath, "export");
        }

        public IEnumerable<string> Languages => info.AvailableLanguages.Where(x => x != RenPy.Files.Script.Languages.None);
        public bool TranslationAvailable => info.AvailableLanguages.Count > 1;
        public bool OnlyRPY { get; set; }
        public bool BothRPYandRPYC { get; set; }
        public bool DecodedRPYC { get; set; } = true;
        public bool TranslatedRPYC { get; set; }
        public string? SelectedLanguage { get; set; }
        public string OutputPath { get => outputPath; set => SetProperty(ref outputPath, value); }

        public event EventHandler<bool>? Finished;

        #region Commands
        public static readonly ICommand Export = new RelayCommand(ExportExecute, ExportCanExecute);

        private static bool ExportCanExecute(object? arg)
        {
            var valid = false;
            if (arg is ExportScripts es)
            {
                valid = !es.TranslatedRPYC || !string.IsNullOrEmpty(es.SelectedLanguage);
                foreach (var c in Path.GetInvalidPathChars())
                {
                    valid &= !es.OutputPath.Contains(c);
                }
            }
            return valid;
        }

        private static void ExportExecute(object? obj)
        {
            if (obj is ExportScripts es)
            {
                es.Finished?.Invoke(es, true);
            }
        }

        public static readonly ICommand Browse = new RelayCommand(BrowseExecute);

        private static void BrowseExecute(object? obj)
        {
            if (obj is ExportScripts es)
            {
                using var folderDialog = new FolderBrowserDialog()
                {
                    ShowNewFolderButton = false,
                    InitialDirectory = es.OutputPath,
                    SelectedPath = es.outputPath,
                    Description = "Select folder to output exported files to",
                    UseDescriptionForTitle = true,
                };
                if (folderDialog.ShowDialog() == DialogResult.OK)
                {
                    es.OutputPath = folderDialog.SelectedPath;
                }
            }
        }

        public static readonly ICommand Cancel = new RelayCommand(CancelExecute);

        private static void CancelExecute(object? obj)
        {
            if (obj is ExportScripts es)
            {
                es.Finished?.Invoke(es, false);
            }
        }
        #endregion
    }
}
