﻿using RenPy.Files.Script;
using RenpyGameViewer.Logic;
using System.Collections.Generic;
using System.Linq;

namespace RenpyGameViewer.ViewModels
{
    public class GameContent
    {
        private readonly string language;
        private readonly List<GameScriptFile> scriptFiles = [];
        private readonly DefinitionsCollector definitions = [];
        private readonly Dictionary<GameScriptFile, IReadOnlyCollection<ScriptReferrer>> referrers = [];

        public string Language => language;
        public IReadOnlyCollection<GameScriptFile> ScriptFiles => scriptFiles;
        public DefinitionsCollector Definitions => definitions;
        public IReadOnlyDictionary<GameScriptFile, IReadOnlyCollection<ScriptReferrer>> Referrers => referrers;

        public GameContent(GameInfo info)
        {
            language = info.Languages.Find(info.Options.SelectedLanguage);
            if (language == Languages.None)
            {
                // No specific language so show all files as is 
                scriptFiles.AddRange(info.ScriptFiles);
                // Also add translation files as they are
                scriptFiles.AddRange(info.TranslationScriptFiles);
            }
            else
            {
                // Translation can only done on RPYC files so only add them
                scriptFiles.AddRange(info.ScriptFiles.Where(f => f.Type == FileType.Compiled));
                // Also don't add translation files them self.
            }
        }

        public void AddDefinition(ScriptDefinition def)
        {
            lock (definitions) definitions.Add(def);
        }

        public void AddReferrers(ScriptReferrer referrer)
        {
            lock (referrers)
            {
                var file = referrer.ScriptFile;
                if (referrers.TryGetValue(file, out IReadOnlyCollection<ScriptReferrer>? result))
                {
                    ((List<ScriptReferrer>)result).Add(referrer);
                }
                else
                {
                    var col = new List<ScriptReferrer>();
                    referrers.Add(file, col);
                    col.Add(referrer);
                }
            }
        }
    }
}
