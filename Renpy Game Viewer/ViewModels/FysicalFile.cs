﻿using System.IO;

namespace RenpyGameViewer.ViewModels
{
    public class FysicalFile : IFile
    {
        private readonly string filename;
        private readonly string name;
        private readonly string folder;
        private readonly long size;

        public string Name => name;

        public string Folder => folder;

        public long Size => size;

        public FysicalFile(string gamePath, string filename)
        {
            this.filename = filename;
            name = Path.GetFileName(filename);
            size = new FileInfo(filename).Length;
            int index = gamePath.Length;
            var f = Path.GetDirectoryName(filename)?[index..].Replace('\\', '/');
            folder = string.IsNullOrEmpty(f) ? "/" : f;
        }

        public Stream GetStream()
        {
            return File.OpenRead(filename);
        }
    }
}
