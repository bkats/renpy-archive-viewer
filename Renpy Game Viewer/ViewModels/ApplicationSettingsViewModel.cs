﻿using Renpy.View.Shared;
using RenpyGameViewer.Logic;
using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public class ApplicationSettingsViewModel(ApplicationOptions options) : ObservableObject
    {
        private readonly ApplicationOptions applicationOptions = options;

        private bool sortName = options.SortName;
        private bool sortDir = options.SortAscending;
        private readonly ObservableCollection<string> quickFolders = [.. options.QuickFolders];

        public bool SortName { get => sortName; set { SetProperty(ref sortName, value); } }
        public bool SortTime { get => !sortName; set { SetProperty(ref sortName, !value); } }

        public bool Ascending { get => sortDir; set { SetProperty(ref sortDir, value); } }
        public bool Descending { get => !sortDir; set { SetProperty(ref sortDir, !value); } }

        public ObservableCollection<string> QuickFolders => quickFolders;
        public string? SelectedFolder { get; set; }

        public static readonly ICommand AddFolder = new RelayCommand(AddFolderExecute);
        public static readonly ICommand RemoveFolder = new RelayCommand(RemoveFolderExecute, RemoveFolderCanExecute);
        private static void AddFolderExecute(object? obj)
        {
            if (obj is ApplicationSettingsViewModel asvm)
            {
                var f = new FolderBrowserDialog();
                if (f.ShowDialog() == DialogResult.OK)
                {
                    asvm.SelectedFolder = f.SelectedPath;
                    asvm.quickFolders.Add(asvm.SelectedFolder);
                    asvm.OnPropertyChanged(nameof(SelectedFolder));
                }
            }
        }

        private static void RemoveFolderExecute(object? obj)
        {
            if (obj is ApplicationSettingsViewModel asvm && asvm.SelectedFolder != null)
            {
                asvm.QuickFolders.Remove(asvm.SelectedFolder);
                asvm.SelectedFolder = null;
                asvm.OnPropertyChanged(nameof(SelectedFolder));

            }
        }

        private static bool RemoveFolderCanExecute(object? arg)
        {
            if (arg is ApplicationSettingsViewModel asvm)
            {
                return asvm.quickFolders.Count > 0 && asvm.SelectedFolder != null;
            }
            return false;
        }
        public static readonly ICommand Okay = new RelayCommand(OkayExecute);
        public static readonly ICommand Cancel = new RelayCommand(CancelExecute);

        private static void OkayExecute(object? obj)
        {
            if (obj is ApplicationSettingsViewModel asvm)
            {
                asvm.applicationOptions.QuickFolders = asvm.quickFolders;
                asvm.applicationOptions.SortName = asvm.sortName;
                asvm.applicationOptions.SortAscending = asvm.sortDir;
                asvm.Closing?.Invoke(asvm, true);
            }
        }

        private static void CancelExecute(object? obj)
        {
            if (obj is ApplicationSettingsViewModel asvm)
            {
                asvm.Closing?.Invoke(asvm, false);
            }
        }

        public event EventHandler<bool>? Closing;
    }
}
