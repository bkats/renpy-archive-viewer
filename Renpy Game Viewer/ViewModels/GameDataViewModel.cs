﻿using Renpy.View.Shared;
using RenPy.Files.Script;
using RenpyGameViewer.Logic;
using RenpyGameViewer.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public class GameDataViewModel : ObservableObject
    {
        private static readonly string progname = "Ren'Py Game Viewer";
        private GameInfo? info;
        private GameContent? data;
        private readonly ApplicationOptions applicationOptions = ApplicationOptions.Load();

        public string WindowTitle => info == null ? progname : info.GameName + " - " + progname;
        public IEnumerable<string>? AvailableLanguages => info?.AvailableLanguages;
        public string CurrentLanguage
        {
            get => info?.Options.SelectedLanguage ?? Languages.None;
            set
            {
                if (data != null && value != null && data.Language != value)
                {
                    LoadGameContent(value);
                }
            }
        }

        public GameDataViewModel()
        {
            History.PropertyChanged += History_PropertyChanged;
            TextSearch = new TextSearch(this);
            TextSearch.PropertyChanged += TextSearch_PropertyChanged;
            BookmarkList = new BookmarkListViewModel(this);
        }

        public void SetNewGameInfo(GameInfo info)
        {
            if (this.info != null)
            {
                info.Options.Dirty -= OptionsAutoSave;
            }
            this.info = info;
            applicationOptions.LastOpenedGame = info.RootPath;
            Environment.CurrentDirectory = info.RootPath;
            OnPropertyChanged(nameof(WindowTitle));
            OnPropertyChanged(nameof(AvailableLanguages));
            SetNewGameData(null);
            BookmarkList.Info = info;
            if (info != null)
            {
                info.Options.Dirty += OptionsAutoSave;
            }
        }

        private readonly object timerLock = new();
        private Timer? autoSaveTimer;
        private void OptionsAutoSave(object? sender, EventArgs e)
        {
            if (sender is GameUserOptions options)
            {
                if (options != info?.Options)
                {
                    // This should not happen, but timing of things could maybe make it happen (or bugs)
                    options.Dirty -= OptionsAutoSave;
                }
                else
                {
                    lock (timerLock)
                    {
                        autoSaveTimer ??= new Timer(_ =>
                        {
                            if (info != null && options == info.Options)
                            {
                                Application.Current.Dispatcher.BeginInvoke(() => GameLoader.SaveOptions(info));
                            }
                            lock (timerLock)
                            {
                                autoSaveTimer?.Dispose();
                                autoSaveTimer = null;
                            }
                        });
                        autoSaveTimer.Change(5000, 0);
                    }
                }
            }
        }

        public void SetNewGameData(GameContent? data)
        {
            if (this.data != data)
            {
                this.data = data;
                OnPropertyChanged(nameof(CurrentLanguage));

                OnPropertyChanged(nameof(AllDefinitions));
                OnPropertyChanged(nameof(Labels));
                OnPropertyChanged(nameof(Images));
                OnPropertyChanged(nameof(Transforms));
                OnPropertyChanged(nameof(Declarations));
                OnPropertyChanged(nameof(Screens));
                OnPropertyChanged(nameof(ScriptFiles));

                History.Items.Clear();
                TextSearch.ClearSearchResults();
                if (data != null)
                {
                    UpdateCurrentView(data.Definitions.FirstOrDefault(x => x.Name == "start"));
                }
                else
                {
                    ClearCurrentView();
                }
            }
        }

        public void StoreOptions()
        {
            applicationOptions.Save();
            if (info != null)
            {
                GameLoader.SaveOptions(info);
            }
        }

        #region Current view relations
        private GameScriptFile? selectedFile;
        private ScriptDefinition? lastSelectedDefinition;
        private IScriptRelation? selectedRelation;
        private DocumentViewModel? document;

        public IReadOnlyCollection<GameScriptFile>? ScriptFiles => data?.ScriptFiles;

        public DocumentViewModel? Document { get => document; set => SetProperty(ref document, value); }

        public GameScriptFile? SelectedFile
        {
            get => selectedFile;
            set
            {
                if (value != null)
                {
                    var sr = new ScriptRelation($"file {value.Name}", value, 1);
                    UpdateCurrentView(sr, true, "Open");
                }
            }
        }

        public ScriptReferrer? SelectedReferrer { get => null; set => UpdateCurrentView(value); }

        public ScriptDefinition? LastSelectedDefinition
        {
            get => lastSelectedDefinition;
            private set => SetProperty(ref lastSelectedDefinition, value);
        }

        private bool locationUpdating;
        public ScriptDefinition? SelectedDefinition
        {
            get => locationUpdating ? null : selectedRelation as ScriptDefinition;
            set
            {
                if (locationUpdating) return;
                UpdateCurrentView(value);
            }
        }

        public IScriptRelation? CurrentPosition
        {
            get => locationUpdating ? null : selectedRelation;
            set
            {
                if (locationUpdating) return;
                UpdateCurrentView(value, false);
            }
        }

        private void ClearCurrentView()
        {
            selectedFile = null;
            selectedRelation = null;
            LastSelectedDefinition = null;
            OnPropertyChanged(nameof(ScriptDefinitions));
            OnPropertyChanged(nameof(SelectedFile));
            Document = null;
            OnPropertyChanged(nameof(SelectedDefinition));
            OnPropertyChanged(nameof(CurrentPosition));
            BookmarkList.UpdateBookmarks();
        }

        private void UpdateCurrentView(IScriptRelation? value, bool history = true, string header = "")
        {
            if (selectedRelation != value)
            {
                locationUpdating = true;
                selectedRelation = value;

                if (history)
                {
                    // Update history list
                    switch (selectedRelation)
                    {
                        case ScriptDefinition def:
                            UpdateHistory(def);
                            break;
                        case ScriptReferrer rf:
                            UpdateHistory(rf);
                            break;
                        default:
                            UpdateHistory(header, selectedRelation);
                            break;
                    }
                }

                // Order is important first get the document loaded before reporting the Location change
                if ((selectedRelation != null) && (selectedRelation.ScriptFile != selectedFile))
                {
                    selectedFile = selectedRelation.ScriptFile;
                    if (selectedFile != null)
                    {
                        Document = new DocumentViewModel(this, selectedFile);
                    }
                    OnPropertyChanged(nameof(ScriptDefinitions));
                    OnPropertyChanged(nameof(SelectedFile));
                }
                locationUpdating = false;
                if (selectedRelation is ScriptDefinition define)
                {
                    LastSelectedDefinition = define;
                }
                OnPropertyChanged(nameof(SelectedDefinition));
            }
            OnPropertyChanged(nameof(CurrentPosition));
        }
        #endregion

        #region Information
        public IReadOnlyCollection<ScriptDefinition>? AllDefinitions => data?.Definitions;

        public ICollectionView? ScriptDefinitions
        {
            get
            {
                if (SelectedFile != null)
                {
                    var view = new CollectionViewSource() { Source = data?.Definitions }.View;
                    view.Filter = item => (item is ScriptDefinition lbl) && (lbl.ScriptFile == SelectedFile);
                    view.SortDescriptions.Add(new SortDescription(nameof(ScriptDefinition.Name), ListSortDirection.Ascending));
                    return view;
                }
                return null;
            }
        }

        public IReadOnlyCollection<ScriptReferrer>? GetScriptReferrers(GameScriptFile file)
        {
            if (data != null && data.Referrers.TryGetValue(file, out var result))
            {
                return result;
            }
            return null;
        }

        public ICollectionView? Labels
        {
            get
            {
                var view = new CollectionViewSource() { Source = data?.Definitions.Labels }.View;
                view?.SortDescriptions.Add(new SortDescription(nameof(ScriptDefinition.Name), ListSortDirection.Ascending));
                return view;
            }
        }

        public ICollectionView? Images
        {
            get
            {
                var view = new CollectionViewSource() { Source = data?.Definitions.Images }.View;
                view?.SortDescriptions.Add(new SortDescription(nameof(ScriptDefinition.Name), ListSortDirection.Ascending));
                return view;
            }
        }
        public ICollectionView? Transforms
        {
            get
            {
                var view = new CollectionViewSource() { Source = data?.Definitions.Transforms }.View;
                view?.SortDescriptions.Add(new SortDescription(nameof(ScriptDefinition.Name), ListSortDirection.Ascending));
                return view;
            }
        }

        public ICollectionView? Declarations
        {
            get
            {
                var view = new CollectionViewSource() { Source = data?.Definitions.Other }.View;
                view?.SortDescriptions.Add(new SortDescription(nameof(ScriptDefinition.Name), ListSortDirection.Ascending));
                return view;
            }
        }

        public ICollectionView? Screens
        {
            get
            {
                var view = new CollectionViewSource() { Source = data?.Definitions.Screens }.View;
                view?.SortDescriptions.Add(new SortDescription(nameof(ScriptDefinition.Name), ListSortDirection.Ascending));
                return view;
            }
        }
        #endregion

        #region History
        private bool historyBusy = false;
        public History History { get; set; } = new();

        private void UpdateHistory(ScriptDefinition? newLocation)
        {
            if (newLocation != null && !historyBusy)
            {
                var header = newLocation.Type.ToString();
                History.Add(header, newLocation);
            }
        }

        private void UpdateHistory(ScriptReferrer? newLocation)
        {
            if (newLocation != null && !historyBusy)
            {
                var header = newLocation.Type.ToString();
                History.Add(header, newLocation);
            }
        }

        private void UpdateHistory(string header, IScriptRelation? newLocation)
        {
            if (newLocation != null && !historyBusy)
            {
                History.Add(header, newLocation, true);
            }
        }

        private void History_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(History.Current))
            {
                var o = History.Current?.Original;
                if ((o != null) && (o != selectedRelation))
                {
                    historyBusy = true;
                    UpdateCurrentView(o);
                    historyBusy = false;
                }
            }
        }
        #endregion

        #region Search
        public TextSearch TextSearch { get; }

        private void TextSearch_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(TextSearch.SelectedSearchResult))
            {
                TextSearchJumpToSelected();
            }
        }

        public void TextSearchJumpToSelected()
        {
            if (TextSearch.SelectedSearchResult != null)
            {
                UpdateCurrentView(TextSearch.SelectedSearchResult?.Position, true, "Search");
            }
        }
        #endregion

        #region Bookmarks
        public BookmarkListViewModel BookmarkList { get; }

        public void BookmarkJumpToSelected()
        {
            if (BookmarkList.SelectedBookmark != null)
            {
                var sbm = BookmarkList.SelectedBookmark;
                var script = ScriptFiles?.FirstOrDefault(f => f.FullName == sbm.ScriptFile);
                if (script != null)
                {
                    if (History.Current != null
                        && History.Current.ScriptFile == script
                        && History.Current.LineNumber == sbm.LineNumber
                        )
                    {
                        UpdateCurrentView(History.Current.Original, false, "Bookmark");
                    }
                    else
                    {
                        UpdateCurrentView(new ScriptRelation(sbm.Text, script, sbm.LineNumber), true, "Bookmark");
                    }
                }
            }
        }
        #endregion

        #region Commands
        public static readonly ICommand Open = new RelayCommand(OpenGameExecute, OpenGameCanExecute);
        public static readonly ICommand Export = new RelayCommand(ExportGameExecute, ExportGameCanExecute);
        public static readonly ICommand Back = new RelayCommand(BackExecute, BackCanExecute);
        public static readonly ICommand Forward = new RelayCommand(ForwardExecute, ForwardCanExecute);
        public static readonly ICommand Search = new RelayCommand(SearchExecute);
        public static readonly ICommand ClearSearch = new RelayCommand(ClearSearchExecute);
        public static readonly ICommand ConvertSearch = new RelayCommand(ConvertSearchExecute);
        public static readonly ICommand Settings = new RelayCommand(SettingsExecute);
        public static readonly ICommand About = new RelayCommand(AboutExecute);

        private bool busy;
        private string loadTime = string.Empty;
        public bool Busy { get => busy; set => SetProperty(ref busy, value); }
        public string LoadingTime { get => loadTime; set => SetProperty(ref loadTime, value); }

        private static async void OpenGameExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                game.StoreOptions();
                var gameInfo = GameLoader.OpenGame(game.applicationOptions);
                if (gameInfo != null)
                {
                    game.LoadingTime = string.Empty;
                    var sw = Stopwatch.StartNew();
                    game.Busy = true;
                    try
                    {
                        await GameLoader.LoadFolder(gameInfo);
                        game.SetNewGameInfo(gameInfo);
                        var data = await GameContentBuilder.LoadContent(gameInfo);
                        game.SetNewGameData(data);
                    }
                    finally
                    {
                        game.Busy = false;
                    }
                    sw.Stop();
                    game.LoadingTime = $"Loaded in {sw.Elapsed.TotalSeconds:F2} Seconds";
                }
            }
        }

        private void LoadGameContent(string language)
        {
            if (info != null)
            {
                SetNewGameData(null);
                info.Options.SelectedLanguage = language;
                var newData = GameContentBuilder.LoadContent(info);
                newData.ContinueWith((newData) =>
                        Application.Current.Dispatcher.BeginInvoke(() =>
                            SetNewGameData(newData.Result)
                        )
                    );
            }
        }

        private static bool OpenGameCanExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                return !game.Busy;
            }
            return false;
        }

        private static void ExportGameExecute(object? obj)
        {
            if (obj is GameDataViewModel game && game.info != null)
            {
                var export = new ExportScripts(game.info);
                var view = new ExportScriptsView(export)
                {
                    Owner = Application.Current.MainWindow
                };
                if (view.ShowDialog() == true)
                {
                    var rpy = export.OnlyRPY | export.BothRPYandRPYC;
                    var rpyc = export.BothRPYandRPYC | export.TranslatedRPYC | export.DecodedRPYC;
                    var decode = export.DecodedRPYC | export.TranslatedRPYC;
                    GameExporter.Export(game.info, export.OutputPath, rpy, rpyc, decode, export.SelectedLanguage);
                }
            }
        }

        private static bool ExportGameCanExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                return !game.Busy && game.info != null;
            }
            return false;
        }

        private static void BackExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                game.History.GoBack();
            }
        }

        private static bool BackCanExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                return game.History.CanGoBack();
            }
            return false;
        }

        private static void ForwardExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                game.History.GoForward();
            }
        }

        private static bool ForwardCanExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                return game.History.CanGoForward();
            }
            return false;
        }

        public void JumpTo(ScriptDefinition jumpLabel, int currentLine)
        {
            if (SelectedFile != null && Document != null)
            {
                var line = currentLine - 1;
                var offset = Document.Document.Lines[line].Offset;
                var length = Document.Document.Lines[line].Length;
                var text = Document.Document.GetText(offset, length).Trim();
                UpdateHistory("Line", new ScriptRelation(text, SelectedFile, currentLine));
                UpdateCurrentView(jumpLabel);
            }
        }

        private static void SearchExecute(object? obj)
        {
            if (obj is TextSearch s)
            {
                s.StartSearch();
            }
        }

        private static void ClearSearchExecute(object? obj)
        {
            if (obj is TextSearch s)
            {
                s.ClearSearchResults();
            }
        }


        private static void ConvertSearchExecute(object? obj)
        {
            if (obj is TextSearch s && s.Parent.BookmarkList.Bookmarks != null)
            {
                var bms = s.Parent.BookmarkList;
                foreach (var r in s.SearchResults)
                {
                    var lineNo = r.Position.LineNumber;
                    var script = r.Position.ScriptFile.FullName;
                    var line = r.Before + r.Match + r.After;
                    var bm = new ScriptBookmark(lineNo, line, script)
                    {
                        Comment = $"From search for '{s.SearchText}' matched \"{r.Match}\"",
                    };
                    bms.Bookmarks.Add(bm);
                }
            }
        }

        private static void SettingsExecute(object? obj)
        {
            if (obj is GameDataViewModel game)
            {
                var vm = new ApplicationSettingsViewModel(game.applicationOptions);
                var v = new ApplicationSettingsView(vm)
                {
                    Owner = Application.Current.MainWindow,
                };
                if (v.ShowDialog() == true)
                {
                    game.applicationOptions.Save();
                }
            }
        }

        private static void AboutExecute(object? obj)
        {
            //if (obj is GameDataViewModel game)
            {
                var v = new AboutView()
                {
                    Owner = Application.Current.MainWindow,
                };
                v.ShowDialog();
            }
        }

        #endregion
    }
}
