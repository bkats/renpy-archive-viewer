﻿using System.IO;

namespace RenpyGameViewer.ViewModels
{
    public interface IFile
    {
        string Name { get; }
        string Folder { get; }
        long Size { get; }
        Stream GetStream();
    }
}
