﻿using Renpy.View.Shared;
using System.Collections.ObjectModel;

namespace RenpyGameViewer.ViewModels
{
    public class History : ObservableObject
    {
        private HistoryItem? current;

        public ObservableCollection<HistoryItem> Items { get; } = new();
        public HistoryItem? Current { get => current; set => SetProperty(ref current, value); }

        public void Add(string header, IScriptRelation location, bool temporary = false)
        {
            var item = new HistoryItem(header, location, temporary);

            if (current != null)
            {
                // Throw away current forward items 
                var index = Items.IndexOf(current);
                if (index > 0)
                {
                    while (index-- > 0)
                    {
                        Items.RemoveAt(0);
                    }
                    // If current is a unknown type it was created to go back to a click action or opening a file
                    // These can be removed (no reason to return to that location later on
                    if (Items.Count > 0)
                    {
                        if (Items[0].Temporary)
                        {
                            Items.RemoveAt(0);
                        }
                    }
                }
            }

            Items.Insert(0, item);
            Current = item;
        }

        public void GoBack()
        {
            if (current != null)
            {
                var index = Items.IndexOf(current) + 1;
                if (Items.Count > index)
                {
                    Current = Items[index];
                }
            }
        }

        public bool CanGoBack()
        {
            if (current != null)
            {
                var index = Items.IndexOf(current) + 1;
                return Items.Count > index;
            }
            return false;
        }

        public void GoForward()
        {
            if (current != null)
            {
                var index = Items.IndexOf(current) - 1;
                if (index >= 0)
                {
                    Current = Items[index];
                }
            }
        }

        public bool CanGoForward()
        {
            if (current != null)
            {
                return Items.IndexOf(current) > 0;
            }
            return false;
        }
    }

    public class HistoryItem
    {
        public string Header { get; private set; } = string.Empty;
        public string Name => Original.Name;
        public GameScriptFile ScriptFile => Original.ScriptFile;
        public int LineNumber => Original.LineNumber;
        public bool Temporary { get; }

        public IScriptRelation Original { get; private set; }

        public HistoryItem(string header, IScriptRelation original, bool temporary)
        {
            Original = original;
            Header = header;
            Temporary = temporary;
        }
    }
}
