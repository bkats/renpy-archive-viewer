﻿using RenPy.Files.Script;
using System;

namespace RenpyGameViewer.ViewModels
{
    public class CodeScriptRelation<T> : IScriptRelation where T : CodeLocation
    {
        private bool remapped = false;
        private int newLineNumber;
        private int newColumnNumber;

        public GameScriptFile ScriptFile { get; private set; }
        public T Info { get; }
        public bool Remapped => remapped;
        public string Name => Info.Name;
        public string Text => Info.Text;
        public string? MatchHint => Info.MatchHint;
        public int LineNumber => remapped ? newLineNumber : Info.LineNumber;
        public int ColumnNumber => remapped ? newColumnNumber : Info.ColumnNumber;

        public CodeScriptRelation(GameScriptFile scriptFile, T code)
        {
            ScriptFile = scriptFile;
            Info = code;
        }

        public void ReplaceScript(GameScriptFile newScript, int newLocation)
        {
            if (remapped)
                throw new Exception("Already remapped to different source");

            remapped = true;
            ScriptFile = newScript;
            if (Info.LineNumber != Info.LineNumberStatement)
            {
                // In case of an expression is split over multiple lines correct for the offset
                newLineNumber = newLocation + Info.LineNumber - Info.LineNumberStatement;
            }
            else
            {
                newLineNumber = newLocation;
            }
            newColumnNumber = 0;
        }
        public void ChangeColumn(int column)
        {
            if (remapped)
            {
                newColumnNumber = column;
            }
        }
    }
}
