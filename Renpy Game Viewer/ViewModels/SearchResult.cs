﻿using ICSharpCode.AvalonEdit.Document;

namespace RenpyGameViewer.ViewModels
{
    public class SearchResult : ISegment
    {
        public IScriptRelation Position { get; }
        public string Before { get; }
        public string Match { get; }
        public string After { get; }
        public bool Continues { get; }

        // Position of match in document
        public int Offset { get; }
        public int Length { get; }
        public int EndOffset => Offset + Length;

        public SearchResult(IScriptRelation position, string line, int offset, int len, int lineOffset)
        {
            Position = position;

            // Split line in three piece: before, match and after
            int afterOffset = offset + len;
            if (afterOffset > line.Length)
            {
                afterOffset = line.Length;
                Continues = true;
            }
            Before = line[0..offset];
            Match = line[offset..afterOffset];
            if (Continues)
            {
                After = "…";
            }
            else
            {
                After = line[afterOffset..];
            }

            // Set match offset position absolute from beginning of document
            Offset = lineOffset + offset;
            Length = len;
        }
    }
}