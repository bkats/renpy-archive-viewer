﻿using Renpy.View.Shared;
using RenPy.Files.Script;
using RenpyGameViewer.Logic;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RenpyGameViewer.ViewModels
{
    public class TextSearch(GameDataViewModel parent) : ObservableObject
    {
        private readonly GameDataViewModel parent = parent;

        private string searchText = string.Empty;
        private bool ignoreCase;
        private bool wholeWord;
        private bool useRegEx;
        private bool searchNoResults;
        private SearchResult? selectedResult;

        public GameDataViewModel Parent => parent;
        public string SearchText { get => searchText; set => SetProperty(ref searchText, value); }
        public bool IgnoreCase { get => ignoreCase; set => SetProperty(ref ignoreCase, value); }
        public bool WholeWord { get => wholeWord; set => SetProperty(ref wholeWord, value); }
        public bool UseRegEx { get => useRegEx; set => SetProperty(ref useRegEx, value); }
        public ObservableCollection<SearchResult> SearchResults { get; } = new();
        public ObservableCollection<SearchResult> SearchResultsCurrentDocument { get; } = new();
        public SearchResult? SelectedSearchResult { get => selectedResult; set => SetProperty(ref selectedResult, value); }
        public bool SearchNoResults { get => searchNoResults; set => SetProperty(ref searchNoResults, value); }
        public ObservableCollection<string> SearchHistory { get; } = new();

        public void StartSearch()
        {
            SearchNoResults = false;
            if (parent.ScriptFiles != null && searchText.Length > 0)
            {
                if (SearchResults.Count > 0)
                {
                    var m = SearchResults[0].Match;
                    SearchHistory.Remove(m);
                    SearchHistory.Insert(0, m);
                    while (SearchHistory.Count > 10) SearchHistory.RemoveAt(SearchHistory.Count - 1);
                }
                var s = new SearchEngine(searchText, ignoreCase, wholeWord, useRegEx);
                IEnumerable<GameScriptFile> files;
                if (parent.CurrentLanguage != Languages.None)
                {
                    // In case of language isn't 'none' there are no RPY files so search all files
                    files = parent.ScriptFiles.OrderBy(f => f);
                }
                else
                {
                    // Only search in RPY or only in RPYC when there is no RPY version
                    files = parent.ScriptFiles.Where(f => (f.Type == FileType.Script) || (f.Partner == null)).OrderBy(f => f);
                }
                var r = s.Search(files);
                SearchResults.Clear();
                foreach (var item in r)
                {
                    SearchResults.Add(item);
                }
                UpdateSearchCurrentDoc();
            }
            SearchNoResults = SearchResults.Count == 0;
        }

        public void UpdateSearchCurrentDoc()
        {
            SearchResultsCurrentDocument.Clear();
            foreach (var item in SearchResults.Where(x => x.Position.ScriptFile == parent.SelectedFile))
            {
                SearchResultsCurrentDocument.Add(item);
            }
        }

        public void ClearSearchResults()
        {
            SearchResults.Clear();
            SearchResultsCurrentDocument.Clear();
        }

    }
}
