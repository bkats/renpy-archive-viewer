﻿using Renpy.View.Shared;
using RenpyGameViewer.Logic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace RenpyGameViewer.ViewModels
{

    public class GameSelectorViewModel : ObservableObject
    {
        private readonly ObservableCollection<Root> roots = [];
        private IEnumerable<Game>? games;
        private PathObject selected;
        private Game? selectedGame;

        private bool busy;

        public ObservableCollection<Root> Roots => roots;
        public IEnumerable<Game>? Games
        {
            get
            {
                if (games == null) return null;
                if (!sortType)
                {
                    return sortDirection ? games.OrderBy(i => i.Name) : games.OrderByDescending(i => i.Name);
                }
                else
                {
                    return sortDirection ? games.OrderBy(i => i.TimeStamp) : games.OrderByDescending(i => i.TimeStamp);
                }
            }
            set
            {
                SetProperty(ref games, value);
            }
        }
        public PathObject Selected
        {
            get => selected;
            set
            {
                SetProperty(ref selected, value);
                Games = value?.Games;
            }
        }

        public Game? SelectedGame { get => selectedGame; set { SetProperty(ref selectedGame, value); } }

        private bool sortType;
        private bool sortDirection;
        public bool? SortName
        {
            get => !sortType ? sortDirection : null;
            set
            {
                sortType = false;
                sortDirection = value ?? false;
                OnPropertyChanged(nameof(SortName));
                OnPropertyChanged(nameof(SortTimeStamp));
                OnPropertyChanged(nameof(Games));
            }
        }

        public bool? SortTimeStamp
        {
            get => sortType ? sortDirection : null;
            set
            {
                sortType = true;
                sortDirection = value ?? false;
                OnPropertyChanged(nameof(SortName));
                OnPropertyChanged(nameof(SortTimeStamp));
                OnPropertyChanged(nameof(Games));
            }
        }

        public bool Busy
        {
            get => busy;
            set => SetProperty(ref busy, value);
        }

        public GameSelectorViewModel(IEnumerable<string> quickFolders)
        {
            foreach (var folder in quickFolders)
            {
                roots.Add(new(this, folder, Path.GetFileName(folder)));
            }
            foreach (var d in DriveInfo.GetDrives())
            {
                roots.Add(new(this, d.Name));
            }
            selected = roots[0];
        }

        public string InitialGame
        {
            set => Task.Run(() =>
            {
                Busy = true;
                // Find root
                PathObject? po = roots.FirstOrDefault(r => value.StartsWith(r.FullName, StringComparison.OrdinalIgnoreCase));
                if (po != null)
                {
                    var remaining = value[po.FullName.Length..];
                    var game = Path.GetFileName(remaining);
                    var path = Path.GetDirectoryName(remaining)?.Trim('\\');
                    po.WaitForPopulated();
                    if (!string.IsNullOrEmpty(path))
                    {
                        var pathParts = path.Split(Path.DirectorySeparatorChar);
                        po.IsExpanded = true;
                        foreach (var p in pathParts)
                        {
                            var f = po.Folders.FirstOrDefault(f => f.Name == p);
                            if (f != null)
                            {
                                po.IsExpanded = true;
                                po = f;
                                po.WaitForPopulated();
                            }
                        }
                    }
                    po.IsSelected = true;
                    SelectedGame = po.Games.FirstOrDefault(g => g.AlternateName == game);
                }
                Busy = false;
            });
        }

        public event EventHandler<bool>? Finished;

        #region Commands
        public static readonly ICommand Open = new RelayCommand(OpenExecute, OpenCanExecute);

        private static bool OpenCanExecute(object? arg)
        {
            var valid = false;
            if (arg is GameSelectorViewModel gs)
            {
                valid = gs.SelectedGame != null;
            }
            return valid;
        }

        private static void OpenExecute(object? obj)
        {
            if (obj is GameSelectorViewModel gs)
            {
                gs.Finished?.Invoke(gs, true);
            }
        }

        public static readonly ICommand Cancel = new RelayCommand(CancelExecute);

        private static void CancelExecute(object? obj)
        {
            if (obj is GameSelectorViewModel gs)
            {
                gs.Finished?.Invoke(gs, false);
            }
        }
        #endregion

    }

    public abstract class PathObject : ObservableObject
    {
        private bool isExpanded;
        private bool isSelected;

        public PathObject(string fullName)
        {
            FullName = fullName;
            // Magic code, prevents IOException on getting icon
            try
            {
                Icon = ExtractIcon.GetIconOfPathLarge(FullName, false);
            }
            catch
            {
                // It's magic following line gets never executed, but if I remove try-catch then
                // there will be IOExeception on 'drive is not ready'
                Trace.WriteLine($"Error getting icon for {FullName}");
            }
        }

        public BitmapSource? Icon { get; }

        public ObservableCollection<Folder> Folders { get; } = [];

        public ObservableCollection<Game> Games { get; } = [];

        public bool IsExpanded
        {
            get => isExpanded;
            set
            {
                SetProperty(ref isExpanded, value);
                if (value)
                {
                    foreach (var f in Folders)
                    {
                        if (!f.IsPopulated)
                        {
                            Application.Current.Dispatcher.BeginInvoke(f.Populate);
                        }
                    }
                }
            }
        }

        public bool IsSelected
        {
            get => isSelected;
            set
            {
                SetProperty(ref isSelected, value);
                UpdateSelected(this);
            }
        }

        public string FullName { get; }
        public abstract string Name { get; }
        public bool IsPopulated { get; protected set; }

        #region Async/Waiting handling flags
        private bool popbusy;
        private readonly object lockPop = new();
        private readonly Semaphore populated = new Semaphore(0, 1);
        #endregion

        protected async void Populate()
        {
            lock (lockPop)
            {
                if (popbusy) return;
                popbusy = true;
            }
            try
            {
                if (!IsPopulated)
                {
                    await foreach (var (path, options) in DirTreeBuilder.ProcessFolder(FullName))
                    {
                        if (options != null)
                        {
                            Games.Add(new Game(this, path, options));
                        }
                        else
                        {
                            Folders.Add(new Folder(this, path));
                        }
                    }
                    populated.Release();
                }
                IsPopulated = true;
            }
            catch (IOException e)
            {
                Debug.WriteLine(e);
            }
            finally { popbusy = false; }
        }

        public void WaitForPopulated()
        {
            bool wait = !IsPopulated;
            while (wait)
            {
                if (populated.WaitOne(500))
                {
                    wait = false;
                    // Keep populated released for possible other threads waiting
                    populated.Release();
                }
                // If didn't time-out, populated should be true
                if (!IsPopulated)
                {
                    bool busy = false;
                    lock (lockPop)
                    {
                        busy = popbusy;
                    }
                    // Check busy else we lock-up forever
                    if (!busy)
                    {
                        Debug.WriteLineIf(!IsPopulated, "Not busy and not populated!!!!");
                        wait = false;
                    }
                }
            }
        }

        public abstract void UpdateSelected(PathObject newSelectItem);
    }

    public class Root : PathObject
    {
        private readonly GameSelectorViewModel parent;
        private readonly string? name;

        public override string Name => name ?? FullName;

        public Root(GameSelectorViewModel parent, string fullName, string? name = null) : base(fullName)
        {
            this.parent = parent;
            this.name = name;
            Populate();
        }

        public override void UpdateSelected(PathObject newSelectItem)
        {
            parent.Selected = newSelectItem;
        }
    }

    public class Folder(PathObject parent, string name) : PathObject(name)
    {
        public override string Name => Path.GetFileName(FullName);
        public PathObject Parent { get; } = parent;

        public override void UpdateSelected(PathObject newSelectItem)
        {
            Parent?.UpdateSelected(newSelectItem);
        }
    }

    public class Game(PathObject parent, string path, GameUserOptions options) : ObservableObject
    {
        private readonly object iconLock = new();
        private BitmapSource? displayIcon;
        public string RootPath { get; } = path;
        public string AlternateName => Path.GetFileName(RootPath);
        public string Name => Options.Name;
        public GameUserOptions Options { get; } = options;
        public PathObject Parent { get; } = parent;
        public DateTime TimeStamp => new DirectoryInfo(Path.Combine(RootPath, "game")).LastWriteTime;
        public BitmapSource? Icon
        {
            get
            {
                // Not sure if lock is needed (should be called from UI thread), but to be safe
                lock (iconLock)
                {
                    if (displayIcon == null)
                    {
                        // Set temporary to default (prevent starting async retreival multiple times)
                        displayIcon = DirTreeBuilder.DefaultIcon.GetDefaultIcon();
                        // Get an icon from the game async
                        DirTreeBuilder.GetIconFromGameAsync(RootPath, Options, icon => Icon = icon);
                    }
                }
                // Return what we have
                return displayIcon;
            }

            private set
            {
                OnPropertyChanged(nameof(Name));
                SetProperty(ref displayIcon, value);
            }
        }
    }
}
