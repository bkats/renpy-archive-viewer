﻿using RenPy.Files.Script;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace RenpyGameViewer.ViewModels
{
    public enum FileType
    {
        Unknown,
        Script,
        Compiled,
    }

    // Note: This class shouldn't keep open the original file after creation
    public abstract class GameScriptFile(IFile file) : IComparable<GameScriptFile>
    {
        public string Folder { get; } = file.Folder;
        public string Name { get; } = file.Name;
        public long Size { get; } = file.Size;
        public abstract FileType Type { get; }
        public string Script { get; protected set; } = string.Empty;
        public GameScriptFile? Partner { get; set; }
        public string? OriginalRPAFile { get; set; }
        public string? OriginalRPAFileShort => Path.GetFileName(OriginalRPAFile);
        public bool IsInsideRPA => !string.IsNullOrEmpty(OriginalRPAFile);
        public string Language { get; protected set; } = Languages.None;

        public abstract void BuildTextView(ITranslateInfo? translateInfo);

        public string FullName
        {
            get
            {
                if (Folder == "/")
                    return '/' + Name;
                return $"{Folder}/{Name}";
            }
        }


        public override string ToString()
        {
            return FullName;
        }

        int IComparable<GameScriptFile>.CompareTo(GameScriptFile? other)
        {
            return ToString().CompareTo(other?.ToString());
        }
    }

    internal static partial class Regs
    {
        [GeneratedRegex(@"$\s*(init\s+)?translate\s+(?<language>\w+)", RegexOptions.Multiline)]
        internal static partial Regex FindLanguage();
    }

    public class GameTextScript : GameScriptFile
    {
        public GameTextScript(IFile file, Languages languages) : base(file)
        {
            Script = new StreamReader(file.GetStream()).ReadToEnd();
            var lang = Regs.FindLanguage().Match(Script, 0, int.Min(Script.Length, 5000));
            if (lang.Success)
            {
                Language = languages.Find(lang.Groups["language"].Value);
            }
        }

        public override FileType Type => FileType.Script;

        public override void BuildTextView(ITranslateInfo? translateInfo)
        {
        }
    }

    public class GameCompiledScript : GameScriptFile
    {
        private readonly CompiledScript? compiledScript;

        public override FileType Type => FileType.Compiled;
        public IReadOnlyList<DefinitionLocation>? Definitions { get; protected set; }
        public IReadOnlyList<ReferrerLocation>? Referrers { get; protected set; }
        public CompiledScript? CompiledScript => compiledScript;

        public GameCompiledScript(IFile file, Languages languages) : base(file)
        {
            using var s = file.GetStream();
            try
            {
                compiledScript = CompiledScriptLoader.GetScript(file.Name, s, languages);
                Language = compiledScript.Language;
            }
            catch (Exception ex)
            {
                Script = "Failed to load script\r\n\r\n" + ex.ToString();
            }
        }

        public override void BuildTextView(ITranslateInfo? translateInfo)
        {
            if (compiledScript == null)
            {
                return;
            }
            CompiledScriptTextView? cs = null;
            if (translateInfo != null)
            {
                try
                {
                    cs = compiledScript.GetTranslatedTextView(translateInfo);
                }
                catch (Exception ex)
                {
                    Script = ex.ToString();
                    Definitions = null;
                    Referrers = null;
                    return;
                }
            }
            if (cs == null)
            {
                try
                {
                    cs = compiledScript.GetTextView();
                }
                catch (Exception ex)
                {
                    Script = ex.ToString();
                    Definitions = null;
                    Referrers = null;
                    return;
                }
            }
            Script = cs.Text;
            Definitions = cs.Definitions;
            Referrers = cs.Referrers;
        }
    }
}
