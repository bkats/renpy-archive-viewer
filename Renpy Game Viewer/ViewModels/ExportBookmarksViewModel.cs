﻿using Microsoft.Win32;
using Renpy.View.Shared;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace RenpyGameViewer.ViewModels
{
    public class ExportBookmarksViewModel : ObservableObject
    {
        private string filename;
        private bool onlyComments = true;
        private bool onlyProtected;
        private bool asCSV;
        private bool asJSON = true;
        private bool asXML;
        private bool asTXT;

        public ExportBookmarksViewModel(string exportPath)
        {
            filename = Path.Combine(exportPath, @"bookmarks.json");
            int i = 1;
            while (File.Exists(filename))
            {
                filename = Path.Combine(exportPath, $@"bookmarks ({i}).json");
                i++;
            }
        }

        public bool FileExists => File.Exists(filename);
        public string Filename { get => filename; set { SetProperty(ref filename, value); OnPropertyChanged(nameof(FileExists)); } }
        public bool OnlyComments { get => onlyComments; set => SetProperty(ref onlyComments, value); }
        public bool OnlyProtected { get => onlyProtected; set => SetProperty(ref onlyProtected, value); }
        public bool AsCSV
        {
            get => asCSV;
            set
            {
                SetProperty(ref asCSV, value);
                if (value) UpdateExt("csv");
            }
        }

        public bool AsJSON
        {
            get => asJSON;
            set
            {
                SetProperty(ref asJSON, value);
                if (value) UpdateExt("json");
            }
        }

        public bool AsXML
        {
            get => asXML;
            set
            {
                SetProperty(ref asXML, value);
                if (value) UpdateExt("xml");
            }
        }

        public bool AsTXT
        {
            get => asTXT;
            set
            {
                SetProperty(ref asTXT, value);
                if (value) UpdateExt("txt");
            }
        }

        private void UpdateExt(string newExt)
        {
            string[] defaultExts = [".xml", ".json", ".csv", ".txt"];
            var currentExt = Path.GetExtension(filename);
            if (defaultExts.Any(e => string.Compare(e, currentExt, true) == 0))
            {
                Filename = Path.ChangeExtension(filename, newExt);
            }
        }

        public static readonly ICommand Browse = new RelayCommand(BrowseExecute);
        public static readonly ICommand Okay = new RelayCommand(OkayExecute);
        public static readonly ICommand Cancel = new RelayCommand(CancelExecute);

        private static void BrowseExecute(object? obj)
        {
            if (obj is ExportBookmarksViewModel ei)
            {
                var fd = new SaveFileDialog()
                {
                    OverwritePrompt = false,
                    FileName = ei.filename,
                    InitialDirectory = Path.GetDirectoryName(ei.filename),
                };
                if (fd.ShowDialog() == true)
                {
                    ei.Filename = fd.FileName;
                }
            }
        }

        private static void OkayExecute(object? obj)
        {
            if (obj is ExportBookmarksViewModel ei)
            {
                if (File.Exists(ei.Filename))
                {
                    var result = MessageBox.Show($"File {Path.GetFileName(ei.Filename)} already exist.\nDo you want to overwrite the file?",
                        "Export bookmarks", MessageBoxButton.YesNo);
                    if (result != MessageBoxResult.Yes)
                    {
                        return;
                    }
                }
                ei.Closing?.Invoke(ei, true);
            }
        }

        private static void CancelExecute(object? obj)
        {
            if (obj is ExportBookmarksViewModel ei)
            {
                ei.Closing?.Invoke(ei, false);
            }
        }

        public event EventHandler<bool>? Closing;
    }
}
