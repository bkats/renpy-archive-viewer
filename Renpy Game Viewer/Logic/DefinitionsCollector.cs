using RenPy.Files.Script;
using RenpyGameViewer.ViewModels;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RenpyGameViewer.Logic 
{
    public class DefinitionsCollector : IReadOnlyList<ScriptDefinition>
    {
        private readonly List<ScriptDefinition> screens = [];
        private readonly List<ScriptDefinition> images = [];
        private readonly List<ScriptDefinition> transforms = [];
        private readonly List<ScriptDefinition> labels = [];
        private readonly List<ScriptDefinition> other = [];
        private readonly List<ScriptDefinition> all = [];

        public IReadOnlyList<ScriptDefinition> Screens => screens;
        public IReadOnlyList<ScriptDefinition> Images => images;
        public IReadOnlyList<ScriptDefinition> Transforms => transforms;
        public IReadOnlyList<ScriptDefinition> Labels => labels;
        public IReadOnlyList<ScriptDefinition> Other => other;

        public void Add(ScriptDefinition def)
        {
            all.Add(def);
            switch (def.Type)
            {
                case DefinitionType.Screen:
                    screens.Add(def);
                    break;
                case DefinitionType.Image:
                    images.Add(def);
                    break;
                case DefinitionType.Label:
                    labels.Add(def);
                    break;
                case DefinitionType.Transform:
                    transforms.Add(def);
                    break;
                default:
                    other.Add(def);
                    break;
            }
        }

        public ScriptDefinition? Match(ScriptReferrer referrer)
        {
            var list = other;
            switch (referrer.DefinitionType)
            {
                case DefinitionType.Screen:
                    list = screens;
                    break;
                case DefinitionType.Image:
                    var image = images.FirstOrDefault(m => m.Name == referrer.Name);
                    if (image != null)
                    {
                        return image;
                    }
                    break;
                case DefinitionType.Transform:
                    var transform = transforms.FirstOrDefault(m => m.Name == referrer.Name);
                    if (transform != null)
                    {
                        return transform;
                    }
                    break;
                case DefinitionType.Label:
                    list = labels;
                    break;
                default:
                    break;
            }
            return list.FirstOrDefault(m => m.Name == referrer.Name);
        }

        public int Count => all.Count;

        public ScriptDefinition this[int index] => all[index];

        public IEnumerator<ScriptDefinition> GetEnumerator() => all.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}