﻿using RenPy.Files.Archive;
using RenPy.Files.Script;
using RenpyGameViewer.ViewModels;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RenpyGameViewer.Logic
{
    public static class GameExporter
    {
        private static readonly Dictionary<string, RenPyArchiveFile> cache = [];

        public static void Export(GameInfo info, string outputPath, bool rpy, bool rpyc, bool decode, string? language)
        {
            ITranslateInfo? translateInfo = null;
            if (language != null)
            {
                var nodes = info.Languages.GetTranslations(language);
                translateInfo = TranslationLoader.LoadTranslation(nodes);
            }
            try
            {
                Parallel.ForEach(info.ScriptFiles, s =>
                {
                    var target = Path.Combine(outputPath, s.FullName[1..]);
                    if (s.Type == FileType.Script && rpy)
                    {
                        SaveTextView(target, s.Script);
                    }
                    if (s.Type == FileType.Compiled && rpyc && s is GameCompiledScript cs)
                    {
                        if (decode)
                        {
                            target = Path.ChangeExtension(target, rpy ? ".txt" : ".rpy");
                            ExportFileCompiled(cs, target, translateInfo);
                        }
                        else
                        {
                            var path = Path.Combine(info.GamePath, cs.FullName);
                            if (File.Exists(path))
                            {
                                MakeDirectory(path);
                                File.Copy(path, target);
                            }
                            else
                            {
                                ExportFromRPA(cs, outputPath);
                            }
                        }
                    }
                });
            }
            finally
            {
                foreach (var file in cache.Keys)
                {
                    cache[file].Close();
                }
                cache.Clear();
            }
        }

        private static void ExportFromRPA(GameCompiledScript cs, string outputPath)
        {
            if (cs.OriginalRPAFile != null)
            {
                RenPyArchiveFile? rpa;
                lock (cache)
                {
                    if (!cache.TryGetValue(cs.OriginalRPAFile, out rpa))
                    {
                        rpa = new RenPyArchiveFile(cs.OriginalRPAFile);
                        cache.Add(cs.OriginalRPAFile, rpa);
                    }
                }
                rpa.ExtractFile(cs.FullName[1..], outputPath);
            }
        }

        private static void ExportFileCompiled(GameCompiledScript s, string target, ITranslateInfo? translate)
        {
            if (s.CompiledScript != null)
            {
                CompiledScriptTextView txtView;
                if (translate != null)
                {
                    txtView = s.CompiledScript.GetTranslatedTextView(translate);
                }
                else
                {
                    txtView = s.CompiledScript.GetTextView();
                }
                SaveTextView(target, txtView.Text);
            }
        }

        private static void SaveTextView(string fileName, string text)
        {
            MakeDirectory(fileName);
            File.WriteAllText(fileName, text);
        }

        private static void MakeDirectory(string fileName)
        {
            var dir = Path.GetDirectoryName(fileName);
            if (!string.IsNullOrEmpty(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }
    }
}
