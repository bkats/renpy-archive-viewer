﻿using System.Windows.Input;

namespace RenpyGameViewer.Logic
{
    /// <summary>
    /// Represents a bookmark in the bookmark margin.
    /// </summary>
    public interface IBookmark
    {
        IBookmarkManager Parent { get; }

        /// <summary>
        /// Gets the line number of the bookmark.
        /// </summary>
        int LineNumber { get; }

        bool IsProtected { get; }

        /// <summary>
        /// Handles the mouse down event.
        /// </summary>
        void MouseDown(MouseButtonEventArgs e);

        /// <summary>
        /// Handles the mouse up event.
        /// </summary>
        void MouseUp(MouseButtonEventArgs e);

        /// <summary>
        /// Notifies the bookmark that it was dropped on the specified line.
        /// </summary>
        void Drop(int lineNumber, string text);
    }
}
