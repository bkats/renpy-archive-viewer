﻿using System.IO;
using System.Text.Json;

namespace RenpyGameViewer.Logic
{
    internal static class JsonHelper
    {
        public static readonly JsonSerializerOptions Options = new JsonSerializerOptions() { WriteIndented = true };

        public static void Serialize<T>(Stream stream, T obj)
        {
            JsonSerializer.Serialize(stream, obj, Options);
        }
    }
}
