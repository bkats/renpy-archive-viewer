﻿using Renpy.View.Shared;
using System;
using System.Linq;
using System.Text.Json.Serialization;

namespace RenpyGameViewer.Logic
{
    public sealed class ScriptBookmark : ObservableObject, IEquatable<ScriptBookmark>
    {
        private int lineNumber;
        private string text;
        private string? comment;
        private bool isProtected;

        /// <summary>
        /// Only for JSON deserialazition
        /// </summary>
        public ScriptBookmark()
        {
            text = string.Empty;
            ScriptFile = string.Empty;
        }

        public ScriptBookmark(int line, string text, string file)
        {
            lineNumber = line;
            this.text = text.Trim();
            ScriptFile = file;
        }

        public string ScriptFile { get; set; }

        public int LineNumber
        {
            get => lineNumber;
            set => SetProperty(ref lineNumber, value);
        }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value.Trim());
        }

        [JsonIgnore]
        public string CommentShort
        {
            get
            {
                if (comment != null)
                {
                    var line = comment.Split('\n').FirstOrDefault(l => !string.IsNullOrWhiteSpace(l));
                    return (line ?? comment).Trim();
                }
                return string.Empty;
            }
        }

        public string? Comment
        {
            get => comment;
            set
            {
                SetProperty(ref comment, value);
                OnPropertyChanged(nameof(CommentShort));
            }
        }

        public bool IsProtected
        {
            get => isProtected;
            set => SetProperty(ref isProtected, value);
        }

        public ScriptBookmark Clone()
        {
            return new ScriptBookmark(lineNumber, text, ScriptFile)
            {
                Comment = comment,
                IsProtected = isProtected
            };
        }

        public bool Equals(ScriptBookmark? other)
        {
            return other != null
                && other.ScriptFile == ScriptFile
                && other.lineNumber == lineNumber
                && other.text == text
                && other.comment == comment
                && other.isProtected == isProtected;
        }

        public override bool Equals(object? obj) => obj is ScriptBookmark bm && Equals(bm);
        public override int GetHashCode()
        {
            return ScriptFile.GetHashCode()
                ^ 333667 * lineNumber
                ^ 49 * text.GetHashCode()
                ^ 89 * comment?.GetHashCode() ?? 0
                ^ 991 * isProtected.GetHashCode() * 1345672137;
        }

        public override string ToString() => $"{ScriptFile} ({LineNumber}) \"{Text}\"";
    }
}
