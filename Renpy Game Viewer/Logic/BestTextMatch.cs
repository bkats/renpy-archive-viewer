﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace RenpyGameViewer.Logic
{
    public static partial class BestTextMatch
    {
        public static (int lineNo, string? line) MatchLines(int orgLineNo, string orgLine, IEnumerable<string> Lines, bool debug = false)
        {
            var options = new List<(double distance, int lineNo, string line)>();

            // Trim spaces from line
            orgLine = orgLine.Trim();

            int index = 0;
            string? lastLine = null;
            string? currentTextOnLine = null;
            foreach (var line in Lines)
            {
                // For comparing use trimmed version with out leading or trailing spaces
                var trimLine = line.Trim();
                index++;
                if (orgLineNo == index)
                {
                    currentTextOnLine = line;
                }
                double dist = Math.Abs(orgLineNo - index);
                if (dist < 500)
                {
                    if (orgLine == trimLine)
                    {
                        // Distance on exact match is 0 - 0.5
                        var distance = dist / 1000;
                        if (debug)
                        {
                            Console.WriteLine($"{index}) Exact match: {distance}");
                        }
                        options.Add((distance, index, line));
                    }
                    else
                    {
                        // Limit max distance no need process low probabilities
                        if (dist < 200)
                        {
                            var coeff = MatchLine(orgLine, trimLine);
                            var distance = 1 - coeff * (1 - dist / 1000);
                            if (debug)
                            {
                                Console.WriteLine($"{index}) Matches for: {coeff} => {distance}");
                            }
                            options.Add((distance, index, line));
                        }
                    }
                }
                lastLine = line;
            }

            if (options.Count > 0)
            {
                // Find lowest distance
                var line = options.OrderBy(o => o.distance).FirstOrDefault();
                // Only accept low enough distances
                if (line.distance < 0.6)
                {
                    return (line.lineNo, line.line);
                }
            }

            if (currentTextOnLine != null)
            {
                return (orgLineNo, currentTextOnLine);
            }

            return (index - 1, lastLine);
        }

        public static double MatchLine(string orgLine, string candidateLine, bool debug = false)
        {
            if (orgLine == candidateLine)
            {
                return 1;
            }
            var splitter = WordSplitter();
            var orgWords = splitter.Matches(orgLine).Select(m => m.Value).ToList();
            var canWords = splitter.Matches(candidateLine).Select(m => m.Value).ToList();

            List<string> a;
            List<string> b;
            if (orgWords.Count > canWords.Count)
            {
                a = orgWords;
                b = canWords;
            }
            else
            {
                b = orgWords;
                a = canWords;
            }
            // Correct coeff for different line length
            double cor = (double)orgLine.Length / candidateLine.Length;
            if (cor > 1) cor = 1 / cor;
            // Take difference of length only for ~65%, there is already a correction number of matched words
            cor = 0.35 + cor * 0.65;
            // Limit coeff, because there must be difference between the line (most likely in non-words)
            return Math.Min(MatchLine(a, b, debug) * cor, 0.97);
        }

        private static double MatchLine(List<string> a, List<string> b, bool debug)
        {
            var wordList = new List<(double coeff, string bWord, int aIndex)>();

            double coeff;

            int bIndex = 0;
            int divider = a.Count + b.Count;
            // Match each word in b (short list) with all words in a
            foreach (var bWord in b)
            {
                var os = MatchWords(bWord, a);
                int aIndex = 0;
                foreach (var o in os)
                {
                    // Correct found word coefficient with distance of word with matched word
                    int distFront = Math.Abs(aIndex - bIndex);
                    int distBack = Math.Abs((a.Count - aIndex) - (b.Count - bIndex));
                    double minDist = Math.Min(distBack, distFront);
                    coeff = o.coeff * (1 - minDist / divider);
                    wordList.Add((coeff, bWord, o.index));

                    aIndex++;
                }
                bIndex++;
            }

            // Start with 100% match
            coeff = 1;
            int foundWords = 0;
            foreach (var bWord in b)
            {
                // Get best match for word in b (short list)
                var m = wordList.OrderBy(w => w.coeff).LastOrDefault(m => m.bWord == bWord);

                // Take only words that have reasonable match
                if (m.coeff > 0.25)
                {
                    foundWords++;
                    coeff *= m.coeff;

                    // Remove used possible candidate from list for other matches
                    int i = 0;
                    while (i < wordList.Count)
                    {
                        if (wordList[i].aIndex == m.aIndex)
                        {
                            wordList.RemoveAt(i);
                        }
                        else
                        {
                            i++;
                        }
                    }
                }

                if (debug)
                {
                    Console.WriteLine($"Word: {bWord} ~ {a[m.aIndex]} => {m.coeff} => {coeff}");
                }
            }

            // Correct with the difference in word counts
            return coeff * ((double)foundWords / a.Count);
        }

        public static List<(double coeff, int index)> MatchWords(string word, List<string> candidateWords)
        {
            var options = new List<(double, int)>();
            int i = 0;
            foreach (var c in candidateWords)
            {
                options.Add((MatchWord(word, c), i++));
            }
            return options;
        }

        public static double MatchWord(string word, string candidateWord)
        {
            if (word == candidateWord)
            {
                return 1;
            }
            var a = word.ToLower();
            var b = candidateWord.ToLower();
            if (a == b)
            {
                return 0.97;
            }

            // Very simple algo
            double slen = Math.Min(word.Length, candidateWord.Length);
            double llen = Math.Max(word.Length, candidateWord.Length);
            return (double)word.Intersect(candidateWord).Count() / word.Union(candidateWord).Count() * slen / llen;
        }

        [GeneratedRegex(@"\w+")]
        private static partial Regex WordSplitter();
    }
}
