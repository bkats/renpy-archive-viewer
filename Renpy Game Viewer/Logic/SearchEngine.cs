﻿using ICSharpCode.AvalonEdit.Document;
using RenpyGameViewer.ViewModels;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RenpyGameViewer.Logic
{

    public class SearchEngine
    {
        private readonly Regex search;

        public SearchEngine(string searchText, bool ignoreCase, bool wholeWord, bool useRegEx)
        {
            if (!useRegEx) searchText = Regex.Escape(searchText);

            if (wholeWord) searchText = $@"\b{searchText}\b";

            var options = RegexOptions.Multiline | RegexOptions.Compiled;
            if (ignoreCase) options |= RegexOptions.IgnoreCase;

            search = new Regex(searchText, options);
        }

        public IEnumerable<SearchResult> Search(GameScriptFile file)
        {
            var result = new List<SearchResult>();
            var doc = new TextDocument(file.Script);
            var ms = search.Matches(file.Script);
            foreach (Match m in ms)
            {
                try
                {
                    var line = doc.GetLineByOffset(m.Index);
                    var text = doc.GetText(line);
                    var sr = new ScriptRelation($"{m.Value} → {text.Trim()}", file, line.LineNumber);
                    int offset = m.Index - line.Offset;
                    result.Add(new SearchResult(sr, text, offset, m.Length, line.Offset));
                }
                catch
                {
                }
            }

            return result;
        }

        public IEnumerable<SearchResult> Search(IEnumerable<GameScriptFile> files)
        {
            var result = new List<SearchResult>();

            foreach (var file in files)
            {
                result.AddRange(Search(file));
            }

            return result;
        }
    }
}
