﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace RenpyGameViewer.Logic
{
    public class ApplicationOptions
    {
        #region Options
        private string? lastOpenedGame;
        private bool sortName = true;
        private bool sortAscending = true;
        private List<string>? quickFolders = [];

        public string? LastOpenedGame { get => lastOpenedGame; set => Update(ref lastOpenedGame, value); }
        public bool SortName { get => sortName; set => Update(ref sortName, value); }
        public bool SortAscending { get => sortAscending; set => Update(ref sortAscending, value); }

        public IReadOnlyCollection<string> QuickFolders
        {
            get
            {
                if (quickFolders == null)
                {
                    quickFolders = [];
                    quickFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
                    quickFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.Personal));
                }
                return quickFolders;
            }

            set
            {
                List<string> l = [.. value];
                Update(ref quickFolders, l);
            }
        }

        #endregion

        #region Administration
        private bool dirty = false;

        private void Update<T>(ref T field, T newValue)
        {
            if (!EqualityComparer<T>.Default.Equals(field, newValue))
            {
                dirty = true;
                field = newValue;
            }
        }

        [JsonIgnore]
        public bool IsDirty => dirty;

        public static string OptionsDirectory
        {
            get
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                return Path.Combine(path, "RPGameViewer");
            }
        }

        public static string OptionsFile => Path.Combine(OptionsDirectory, "options.json.txt");

        public static ApplicationOptions Load()
        {
            if (File.Exists(OptionsFile))
            {
                using var s = File.OpenRead(OptionsFile);
                var options = JsonSerializer.Deserialize<ApplicationOptions>(s);
                if (options != null)
                {
                    return options;
                }
            }
            return new();
        }

        public void Save()
        {
            Directory.CreateDirectory(OptionsDirectory);
            using var s = File.Create(OptionsFile);
            JsonHelper.Serialize(s, this);
            dirty = false;
        }
        #endregion

    }
}
