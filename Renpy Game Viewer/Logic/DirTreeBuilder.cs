﻿using RenPy.Files.Archive;
using RenPy.Files.Script;
using RenPy.Files.Script.Objects.Ast;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace RenpyGameViewer.Logic
{
    public static partial class DirTreeBuilder
    {
        internal static class DefaultIcon
        {
            private static readonly object imageLock = new();
            private static BitmapImage? defaultIcon;

            public static BitmapImage? GetDefaultIcon()
            {
                if (defaultIcon == null)
                {
                    lock (imageLock)
                    {
                        if (defaultIcon == null)
                        {
                            var ic = new BitmapImage();
                            ic.BeginInit();
                            ic.StreamSource = Application.GetResourceStream(new System.Uri("pack://application:,,,/Resources/logo32.png")).Stream;
                            ic.CacheOption = BitmapCacheOption.OnLoad;
                            ic.EndInit();
                            ic.Freeze();
                            defaultIcon = ic;
                        }
                    }
                }
                return defaultIcon;
            }
        }

        private static (string, GameUserOptions?) DetectGameFolder(string folder)
        {
            string name = Path.GetFileNameWithoutExtension(folder);
            GameUserOptions? options = null;
            try
            {
                // Check if .\game and .\renpy folder exist
                if (!Directory.Exists(Path.Combine(folder, "game"))
                    || !Directory.Exists(Path.Combine(folder, "renpy")))
                {
                    //if (options != null)
                    //{
                    //    old files???
                    //    Remove options file??
                    //}
                    return (folder, null);
                }

                options = GameLoader.LoadOptions(folder);

                if (options != null && !string.IsNullOrWhiteSpace(options.Name))
                {
                    return (folder, options);
                }

                var files = Directory.GetFiles(folder);

                if (files.FirstOrDefault(w => w.EndsWith(".exe")) != null
                    || files.FirstOrDefault(l => l.EndsWith(".sh")) != null)
                {
                    if (files.FirstOrDefault(p => p.EndsWith(".py")) is string s)
                    {
                        options ??= new();
                        options.Name = Path.GetFileNameWithoutExtension(s);
                        GameLoader.SaveOptions(folder, options);
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
            return (folder, options);
        }

        public static async IAsyncEnumerable<(string path, GameUserOptions? options)> ProcessFolder(string path)
        {
            IEnumerable<string> subs = [];
            try
            {
                var e = new EnumerationOptions
                {
                    IgnoreInaccessible = true,
                    AttributesToSkip = FileAttributes.Hidden
                };
                subs = Directory.EnumerateDirectories(path, "*", e).Order();
            }
            catch (UnauthorizedAccessException)
            {
            }
            foreach (var sub in subs)
            {
                yield return await Task.Run(() => DetectGameFolder(sub));
            }
        }

        internal static void GetIconFromGameAsync(string path, GameUserOptions options, Action<BitmapSource?> iconSetter)
        {
            BitmapImage? icon = null;
            Task.Run(() =>
            {
                try
                {
                    var gameFolder = Path.Combine(path, "Game");

                    icon = TryLoadingFromPreviousLocation(gameFolder, options, iconSetter);

                    string name;
                    if (icon == null)
                    {
                        Debug.WriteLine($"Looking for icon {path}");
                        string iconPath = string.Empty;

                        // First try file system directly
                        (icon, name) = TryGetIconFromFromFilesystem(ref iconPath, gameFolder);

                        if (string.IsNullOrEmpty(iconPath) || icon == null)
                        {
                            // Try archive files for options.rpyc and icon (icon can still be loaded from file system)
                            (icon, name) = TryGetIconFromArchives(gameFolder, ref iconPath, out string? archiveFile);

                            if (icon == null)
                            {
                                // As last ditch try the executable (this has to be done UI thread, COM interface)
                                var exeFile = Directory.EnumerateFiles(path, "*.exe").FirstOrDefault();
                                if (!string.IsNullOrEmpty(exeFile))
                                {
                                    LoadFromExecutableOnMainThread(exeFile, iconSetter);
                                    // Store location of icon
                                    iconPath = Path.GetRelativePath(path, exeFile);
                                }
                                // Nothing found or loading from executable, fallback to default Ren'Py logo
                                icon = DefaultIcon.GetDefaultIcon();
                            }
                            else
                            {
                                if (archiveFile == null)
                                {
                                    options.IconArchiveFile = string.Empty;
                                }
                                else
                                {
                                    options.IconArchiveFile = Path.GetRelativePath(gameFolder, archiveFile);
                                }
                            }
                        }
                        // Store the found icon location
                        if (Path.IsPathFullyQualified(iconPath))
                        {
                            iconPath = Path.GetRelativePath(gameFolder, iconPath);
                        }
                        options.IconLocation = iconPath;
                        if (!string.IsNullOrWhiteSpace(name))
                        {
                            options.Name = name;
                        }
                        GameLoader.SaveOptions(path, options);
                    }
                    // IMPORTANT: Leave default icon check it used as indication that loading is done on main thread
                    // As last action set the icon (if not default icon)
                    if (icon != DefaultIcon.GetDefaultIcon())
                    {
                        iconSetter(icon);
                    }
                }
                catch (Exception e)
                {
                    // Something went horrible wrong
                    Debug.WriteLine($"For {path} exception: {e}");
                }
            });
        }

        private static BitmapImage? TryLoadingFromPreviousLocation(string gameFolder, GameUserOptions options, Action<BitmapSource?> iconSetter)
        {
            BitmapImage? icon = null;
            if (!string.IsNullOrEmpty(options.IconLocation))
            {
                // Try to load icon from last known location
                if (!string.IsNullOrEmpty(options.IconArchiveFile))
                {
                    var fullPath = Path.Combine(gameFolder, options.IconArchiveFile);
                    if (File.Exists(fullPath))
                    {
                        using var a = new RenPyArchiveFile(fullPath);
                        icon = GetFromArchive(a, options.IconLocation);
                    }
                }
                else
                {
                    if (Path.GetExtension(options.IconLocation) == ".exe")
                    {
                        // Exe files are in the root of the game, not in game folder
                        var root = Path.GetDirectoryName(gameFolder) ?? throw new NullReferenceException($"Something went wrong with a game folder");
                        var fullPath = Path.Combine(root, options.IconLocation);
                        if (File.Exists(fullPath))
                        {
                            LoadFromExecutableOnMainThread(fullPath, iconSetter);
                            return DefaultIcon.GetDefaultIcon();
                        }
                    }
                    else
                    {
                        icon = GetIconFromFileSystem(gameFolder, options.IconLocation);
                    }
                }
            }
            return icon;
        }

        private static void LoadFromExecutableOnMainThread(string exeFile, Action<BitmapSource?> iconSetter)
        {
            // Load and set icon on main thread
            Application.Current.Dispatcher.BeginInvoke(() =>
                iconSetter(ExtractIcon.GetIconOfPathLarge(exeFile, false)
            ));
        }

        private static (BitmapImage?, string) TryGetIconFromArchives(string gameFolder, ref string iconPath, out string? archiveFile)
        {
            string name = string.Empty;
            BitmapImage? icon = null;
            archiveFile = null;
            // Now we try by looking in all RPA files
            var RPAs = OpenAllRPA(gameFolder);
            try
            {
                // Still looking for icon location or only icon
                if (string.IsNullOrEmpty(iconPath))
                {
                    using var configFile = FindFileStream(RPAs, "options.rpyc");
                    if (configFile != null)
                    {
                        var script = CompiledScriptLoader.GetScript("options,rpyc", configFile);
                        (iconPath, name) = GetIconPath(script.Root);
                    }
                }
                if (!string.IsNullOrEmpty(iconPath))
                {
                    // Try get icon from file system then archives
                    icon = GetIconFromFileSystem(gameFolder, iconPath) ?? GetFromArchive(RPAs, iconPath, out archiveFile);
                }
            }
            finally
            {
                foreach (var rpa in RPAs) rpa.Close();
            }
            return (icon, name);
        }

        private static (BitmapImage?, string) TryGetIconFromFromFilesystem(ref string iconPath, string gameFolder)
        {
            string name = string.Empty;
            BitmapImage? icon = null;

            var optionFiles = Directory.GetFiles(gameFolder, "options.rpyc", SearchOption.AllDirectories);
            // Search for options.rpyc that isn't a translation file
            var scriptFile = optionFiles.FirstOrDefault(f => !f.Contains(@"\tl\"));
            if (!string.IsNullOrEmpty(scriptFile))
            {
                using var fs = File.OpenRead(scriptFile);
                var script = CompiledScriptLoader.GetScript(scriptFile, fs);
                (iconPath, name) = GetIconPath(script.Root);
                if (!string.IsNullOrEmpty(iconPath))
                {
                    icon = GetIconFromFileSystem(gameFolder, iconPath);
                }
            }

            return (icon, name);
        }

        private static (string icon, string name) GetIconPath(ScriptRoot scriptRoot)
        {
            var icon = string.Empty;
            var name = string.Empty;
            scriptRoot.RunOnAll(node =>
            {
                if (node is NodeDefine def && def.Store == "store.config")
                {
                    if (def.VariableName == "window_icon")
                    {
                        icon = def.Code?.SourceCode.Trim('"', '\'', ' ', '\t');
                    }
                    if (def.VariableName == "name")
                    {
                        name = def.Code?.SourceCode.Trim();
                    }
                }
            });
            if (name.Contains('"'))
            {
                var m = MatchString().Match(name);
                if (m.Success)
                {
                    name = m.Groups["literal"].Value;
                }
            }
            return (icon, name);
        }

        [GeneratedRegex(@"(?<type>""|')(?<literal>(.|\\\k<type>)+?)\k<type>")]
        private static partial Regex MatchString();

        private static List<RenPyArchiveFile> OpenAllRPA(string gameFolder)
        {
            var result = new List<RenPyArchiveFile>();
            var rpaFiles = Directory.GetFiles(gameFolder, "*.rpa");
            foreach (var rpaFile in rpaFiles)
            {
                var rpa = new RenPyArchiveFile(rpaFile);
                result.Add(rpa);
            }
            return result;
        }

        private static Stream? FindFileStream(List<RenPyArchiveFile> rpas, string filename)
        {
            RenPyArchiveFile? rpa = null;
            string? fullPath = null;
            foreach (var a in rpas)
            {
                // Find the file that isn't a translation file
                fullPath = a.Files.FirstOrDefault(f => !f.StartsWith("tl/") && f.EndsWith(filename));
                if (!string.IsNullOrEmpty(fullPath))
                {
                    rpa = a;
                    break;
                }
            }
            if (rpa != null && !string.IsNullOrEmpty(fullPath))
            {
                return rpa.GetStream(fullPath);
            }
            return null;
        }

        private static BitmapImage? GetIconFromFileSystem(string gameFolder, string iconPath)
        {
            var path = Path.Combine(gameFolder, iconPath);
            if (File.Exists(path))
            {
                var bitmap = new BitmapImage();
                using var s = File.OpenRead(path);
                bitmap.BeginInit();
                bitmap.StreamSource = s;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
                return bitmap;
            }
            return null;
        }

        private static BitmapImage? GetFromArchive(List<RenPyArchiveFile> rpas, string filename, out string? archiveFile)
        {
            var rpa = rpas.FirstOrDefault(rpa => rpa.Files.Any(f => f == filename));
            if (rpa != null)
            {
                archiveFile = rpa.Filename;
                return GetFromArchive(rpa, filename);
            }
            archiveFile = null;
            return null;
        }
        private static BitmapImage? GetFromArchive(RenPyArchiveFile archive, string filename)
        {
            using var s = archive.GetStream(filename);
            if (s != null)
            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = s;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
                return bitmap;
            }
            return null;
        }
    }
}
