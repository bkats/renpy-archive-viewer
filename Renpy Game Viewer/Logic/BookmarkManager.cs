﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Media.Imaging;

namespace RenpyGameViewer.Logic
{
    /// <summary>
    /// Stores the entries in the icon bar margin. Multiple icon bar margins
    /// can use the same manager if split view is used.
    /// </summary>
    public class BookmarkManager(Func<int, string, IBookmark?> createFunction) : IBookmarkManager
    {
        private readonly List<IBookmark> bookmarks = [];
        private BitmapSource? bookmarkImage;
        private BitmapSource? bookmarkProtectedImage;
        private readonly Func<int, string, IBookmark?> createBookmarkAction = createFunction;

        public IReadOnlyList<IBookmark> Bookmarks => bookmarks;

        public BitmapSource? BookmarkImage
        {
            get => bookmarkImage;
            set { bookmarkImage = value; Redraw(); }
        }

        public BitmapSource? BookmarkProtectedImage
        {
            get => bookmarkProtectedImage;
            set { bookmarkProtectedImage = value; Redraw(); }
        }

        public IEnumerable<int> Keys => bookmarks.Select(bm => bm.LineNumber);
        public IEnumerable<IBookmark> Values => bookmarks;
        public int Count => bookmarks.Count;

        public IBookmark this[int key]
        {
            get
            {
                TryGetValue(key, out var value);
                return value ?? throw new KeyNotFoundException($"No bookmark on line number {key} found");
            }
        }

        public void CreateNewBookMark(int lineNumber, string lineText)
        {
            var nbm = createBookmarkAction(lineNumber, lineText);
            if (nbm != null && nbm.Parent == this)
            {
                bookmarks.Add(nbm);
                if (nbm is INotifyPropertyChanged o)
                {
                    o.PropertyChanged += BookMark_PropertyChanged;
                }
            }
            Redraw();
        }

        private void BookMark_PropertyChanged(object? sender, PropertyChangedEventArgs e) => Redraw();

        public void Add(IBookmark bm)
        {
            bookmarks.Add(bm);
            BookmarkAdded?.Invoke(this, bm);
            if (bm is INotifyPropertyChanged o)
            {
                o.PropertyChanged += BookMark_PropertyChanged;
            }
            Redraw();
        }

        public void DeleteBookmark(int lineNumber)
        {
            if (TryGetValue(lineNumber, out var del))
            {
                bookmarks.Remove(del);
                Redraw();
                BookmarkDeleted?.Invoke(this, del);
                if (del is INotifyPropertyChanged o)
                {
                    o.PropertyChanged -= BookMark_PropertyChanged;
                }
                if (del is IDisposable dis)
                {
                    dis.Dispose();
                }
            }
        }

        public void Clear()
        {
            foreach (var bm in bookmarks)
            {
                if (bm is INotifyPropertyChanged o)
                {
                    o.PropertyChanged -= BookMark_PropertyChanged;
                }
                if (bm is IDisposable dis)
                {
                    dis.Dispose();
                }
            }
            bookmarks.Clear();
            Redraw();
        }

        public void Redraw()
        {
            RedrawRequested?.Invoke(this, EventArgs.Empty);
        }

        public bool ContainsKey(int key) => bookmarks.Any(bm => bm.LineNumber == key);
        public bool TryGetValue(int key, [MaybeNullWhen(false)] out IBookmark value)
        {
            value = bookmarks.FirstOrDefault(bm => bm.LineNumber == key);
            return value != null;
        }

        public IEnumerator<KeyValuePair<int, IBookmark>> GetEnumerator() =>
            bookmarks.Select(bm => new KeyValuePair<int, IBookmark>(bm.LineNumber, bm)).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => bookmarks.GetEnumerator();

        public event EventHandler? RedrawRequested;

        public event EventHandler<IBookmark>? BookmarkAdded;

        public event EventHandler<IBookmark>? BookmarkDeleted;
    }
}
