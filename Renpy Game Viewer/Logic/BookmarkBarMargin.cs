﻿// File is based on IconBarMargin from AlphaSierraPapa for the SharpDevelop Team
// See for more info https://github.com/icsharpcode/SharpDevelop/blob/master/src/AddIns/DisplayBindings/AvalonEdit.AddIn/Src/IconBarMargin.cs

using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;
using ICSharpCode.AvalonEdit.Utils;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace RenpyGameViewer.Logic
{
    /// <summary>
    /// Icon bar: contains breakpoints and other icons.
    /// </summary>
    public class BookmarkBarMargin : AbstractMargin, IDisposable
    {
        private IBookmarkManager? manager;

        public IBookmarkManager? Manager
        {
            get => manager;
            set
            {
                if (manager != value)
                {
                    if (manager != null)
                    {
                        manager.RedrawRequested -= OnRedrawRequested;
                    }
                    manager = value;
                    if (manager != null)
                    {
                        manager.RedrawRequested += OnRedrawRequested;
                    }
                }
            }
        }

        #region OnTextViewChanged
        /// <inheritdoc/>
        protected override void OnTextViewChanged(TextView oldTextView, TextView newTextView)
        {
            if (oldTextView != null)
            {
                oldTextView.VisualLinesChanged -= OnRedrawRequested;
                if (manager != null)
                    manager.RedrawRequested -= OnRedrawRequested;
            }
            base.OnTextViewChanged(oldTextView, newTextView);
            if (newTextView != null)
            {
                newTextView.VisualLinesChanged += OnRedrawRequested;
                if (manager != null)
                    manager.RedrawRequested += OnRedrawRequested;
            }
            InvalidateVisual();
        }

        void OnRedrawRequested(object? sender, EventArgs e)
        {
            // Don't invalidate the IconBarMargin if it'll be invalidated again once the
            // visual lines become valid.
            if (TextView != null && TextView.VisualLinesValid)
            {
                InvalidateVisual();
            }
        }

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
            TextView = null; // detach from TextView (will also detach from manager)
        }
        #endregion

        /// <inheritdoc/>
        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters)
        {
            // accept clicks even when clicking on the background
            return new PointHitTestResult(this, hitTestParameters.HitPoint);
        }

        /// <inheritdoc/>
        protected override Size MeasureOverride(Size availableSize)
        {
            return new Size(20, 0);
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            Size renderSize = RenderSize;
            var back = new LinearGradientBrush(Colors.AliceBlue, Color.FromArgb(255, 230, 230, 235), 0);
            drawingContext.DrawRectangle(back, null, new Rect(0, 0, renderSize.Width - 2, renderSize.Height));
            drawingContext.DrawLine(new Pen(Brushes.PowderBlue, 1),
                                    new Point(renderSize.Width - 2.5, 0),
                                    new Point(renderSize.Width - 2.5, renderSize.Height));

            TextView textView = TextView;
            if (textView != null && textView.VisualLinesValid && manager != null)
            {
                Size pixelSize = PixelSnapHelpers.GetPixelSize(this);
                foreach (VisualLine line in textView.VisualLines)
                {
                    int lineNumber = line.FirstDocumentLine.LineNumber;
                    if (manager.TryGetValue(lineNumber, out var bm))
                    {
                        double lineMiddle = line.GetTextLineVisualYPosition(line.TextLines[0], VisualYPosition.TextMiddle) - textView.VerticalOffset;
                        if (dragDropBookmark == bm && dragStarted)
                            drawingContext.PushOpacity(0.25);

                        var rect = new Rect(0, PixelSnapHelpers.Round(lineMiddle - 8, pixelSize.Height), 16, 16);
                        if (bm.IsProtected)
                        {
                            if (manager.BookmarkProtectedImage != null)
                            {
                                drawingContext.DrawImage(manager.BookmarkProtectedImage, rect);
                            }
                            else
                            {
                                drawingContext.DrawEllipse(Brushes.LightCoral, new Pen(Brushes.Gold, 2), new(8, lineMiddle), 6, 6);
                            }
                        }
                        else
                        {
                            if (manager.BookmarkImage != null)
                            {
                                drawingContext.DrawImage(manager.BookmarkImage, rect);
                            }
                            else
                            {
                                drawingContext.DrawEllipse(Brushes.Yellow, new Pen(Brushes.Gold, 2), new(8, lineMiddle), 6, 6);
                            }
                        }
                        if (dragDropBookmark == bm && dragStarted)
                            drawingContext.Pop();
                    }
                }
                if (dragDropBookmark != null && dragStarted)
                {
                    if (dragDropBookmark.IsProtected && !protectionOverruled)
                    {
                        drawingContext.DrawEllipse(null, new Pen(Brushes.Red, 3), new(8, dragDropCurrentPoint), 6, 6);
                    }
                    else
                    {
                        if (manager.BookmarkImage != null)
                        {
                            var rect = new Rect(0, PixelSnapHelpers.Round(dragDropCurrentPoint - 8, pixelSize.Height), 16, 16);
                            drawingContext.DrawImage(manager.BookmarkImage, rect);
                        }
                        else
                        {
                            drawingContext.DrawEllipse(Brushes.Yellow, new Pen(Brushes.IndianRed, 2), new(8, dragDropCurrentPoint), 6, 6);
                        }
                    }
                }
            }
        }

        IBookmark? dragDropBookmark; // bookmark being dragged (!=null if drag'n'drop is active)
        double dragDropStartPoint;
        double dragDropCurrentPoint;
        bool dragStarted; // whether drag'n'drop operation has started (mouse was moved minimum distance)
        bool protectionOverruled;

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            CancelDragDrop();
            base.OnMouseDown(e);
            int line = GetLineFromMousePosition(e);
            if (!e.Handled && line > 0 && manager != null)
            {
                if (manager.TryGetValue(line, out var bm))
                {
                    bm.MouseDown(e);
                    if (!e.Handled)
                    {
                        if (e.ChangedButton == MouseButton.Left && CaptureMouse())
                        {
                            StartDragDrop(bm, e);
                            e.Handled = true;
                        }
                    }
                }
            }
            // don't allow selecting text through the IconBarMargin
            if (e.ChangedButton == MouseButton.Left)
                e.Handled = true;
        }

        protected override void OnLostMouseCapture(MouseEventArgs e)
        {
            CancelDragDrop();
            base.OnLostMouseCapture(e);
        }

        void StartDragDrop(IBookmark bm, MouseEventArgs e)
        {
            dragDropBookmark = bm;
            dragDropStartPoint = dragDropCurrentPoint = e.GetPosition(this).Y;
            if (TextView != null)
            {
                if (TextView.Services.GetService(typeof(TextArea)) is TextArea area)
                    area.PreviewKeyDown += TextArea_PreviewKeyDown;
            }
        }

        void CancelDragDrop()
        {
            if (dragDropBookmark != null)
            {
                dragDropBookmark = null;
                dragStarted = false;
                if (TextView != null)
                {
                    if (TextView.Services.GetService(typeof(TextArea)) is TextArea area)
                        area.PreviewKeyDown -= TextArea_PreviewKeyDown;
                }
                ReleaseMouseCapture();
                InvalidateVisual();
            }
        }

        void TextArea_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            // any key press cancels drag'n'drop
            CancelDragDrop();
            if (e.Key == Key.Escape)
                e.Handled = true;
        }

        int GetLineFromMousePosition(MouseEventArgs e)
        {
            TextView textView = TextView;
            if (textView == null)
                return 0;
            VisualLine vl = textView.GetVisualLineFromVisualTop(e.GetPosition(textView).Y + textView.ScrollOffset.Y);
            if (vl == null)
                return 0;
            return vl.FirstDocumentLine.LineNumber;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (dragDropBookmark != null)
            {
                protectionOverruled = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
                dragDropCurrentPoint = e.GetPosition(this).Y;
                if (Math.Abs(dragDropCurrentPoint - dragDropStartPoint) > SystemParameters.MinimumVerticalDragDistance)
                    dragStarted = true;
                InvalidateVisual();
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            int line = GetLineFromMousePosition(e);
            if (!e.Handled && dragDropBookmark != null)
            {
                if (dragStarted)
                {
                    protectionOverruled = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
                    if (line != 0 && (!dragDropBookmark.IsProtected || protectionOverruled))
                    {
                        var text = GetTextForLine(line);
                        dragDropBookmark.Drop(line, text);
                    }
                    e.Handled = true;
                }
                CancelDragDrop();
            }
            if (!e.Handled && line != 0 && manager != null)
            {
                if (manager.TryGetValue(line, out var bm))
                {
                    bm.MouseUp(e);
                    if (e.Handled)
                        return;
                }
                if (e.ChangedButton == MouseButton.Left && TextView != null)
                {
                    var text = GetTextForLine(line);
                    manager.CreateNewBookMark(line, text);
                }
            }
        }

        private string GetTextForLine(int line)
        {
            var vl = TextView.GetVisualLine(line);
            var lineOffset = vl.FirstDocumentLine.Offset;
            var endOffset = vl.LastDocumentLine.EndOffset;
            return TextView.Document.GetText(lineOffset, endOffset - lineOffset) ?? string.Empty;
        }
    }
}
