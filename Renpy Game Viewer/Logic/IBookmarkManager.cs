﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace RenpyGameViewer.Logic
{
    /// <summary>
    /// The bookmark margin.
    /// </summary>
    public interface IBookmarkManager : IReadOnlyDictionary<int, IBookmark>
    {
        /// <summary>
        /// Gets the list of bookmarks.
        /// </summary>
        IReadOnlyList<IBookmark> Bookmarks { get; }

        BitmapSource? BookmarkImage { get; }
        BitmapSource? BookmarkProtectedImage { get; }

        void CreateNewBookMark(int lineNumber, string relevantText);

        void DeleteBookmark(int lineNumber);

        /// <summary>
        /// Redraws the bookmark margin. Bookmarks need to call this method when the Image changes.
        /// </summary>
        void Redraw();

        event EventHandler RedrawRequested;
    }
}
