﻿using RenPy.Files.Script;
using RenpyGameViewer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RenpyGameViewer.Logic
{
    internal static class GameContentBuilder
    {
        static public async Task<GameContent> LoadContent(GameInfo info)
        {
            var data = new GameContent(info);
            await Task.Run(() =>
            {
                var lang = info.Languages.Find(info.Options.SelectedLanguage);
                if (lang != Languages.None)
                {
                    var nodes = info.Languages.GetTranslations(lang);
                    var translateInfo = TranslationLoader.LoadTranslation(nodes);
                    BuildScripts(data, translateInfo);
                }
                else
                {
                    BuildScripts(data, null);
                }
                GetAllDefinitions(data);
                GetAllReferrers(data);
            });
            return data;
        }

        private static void BuildScripts(GameContent data, ITranslateInfo? translateInfo)
        {
            Parallel.ForEach(data.ScriptFiles, file =>
            {
                file.BuildTextView(translateInfo);
            });
#if DEBUG
            RenPy.Files.Shared.RepickData.PrintMessages();
#endif
        }

        private static void GetAllDefinitions(GameContent data)
        {
            Parallel.ForEach(data.ScriptFiles.Where(s => s.Type == FileType.Compiled), script =>
            {
                if (script is GameCompiledScript compiled)
                {
                    var locs = compiled.Definitions;
                    if (locs != null)
                    {
                        ProcessAllDefinitions(data, compiled, locs);
                    }
                }
            });
        }

        private static void GetAllReferrers(GameContent data)
        {
            Parallel.ForEach(data.ScriptFiles.Where(s => s.Type == FileType.Compiled), script =>
            {
                if (script is GameCompiledScript compiled)
                {
                    var locs = compiled.Referrers;
                    if (locs != null && locs.Count > 0)
                    {
                        ProcessAllReferrers(data, compiled, locs);
                    }
                }
            });
        }

        private static GameScriptFile? HookUpOriginalSource(GameScriptFile script, string sourceName, IReadOnlyCollection<GameScriptFile> scripts)
        {
            var rpy = scripts.FirstOrDefault(x => sourceName.EndsWith($"game{x}", StringComparison.InvariantCultureIgnoreCase));
            if (rpy != null)
            {
                if (rpy.Partner != null && rpy.Partner != script) throw new Exception("Already assigned to a RPYC file");
                script.Partner = rpy;
                rpy.Partner = script;
            }

            return rpy;
        }

        private static void ProcessAllDefinitions(GameContent data, GameCompiledScript script, IReadOnlyList<DefinitionLocation> locs)
        {
            bool first = true;
            foreach (var lbl in locs)
            {
                var jump = new ScriptDefinition(script, lbl);
                // Associate location with RPY file if available using lbl.Source field.
                var rpy = script.Partner;
                if (rpy == null && first)
                {
                    first = false;
                    rpy = HookUpOriginalSource(script, lbl.OriginalSource.Source, data.ScriptFiles);
                }
                if ((rpy != null) && (data.Language == Languages.None))
                {
                    jump.ReplaceScript(rpy, lbl.OriginalSource.LineNumber);
                }
                data.AddDefinition(jump);
            }
        }

        private static void ProcessAllReferrers(GameContent data, GameCompiledScript script, IReadOnlyList<ReferrerLocation> locs)
        {
            bool first = true;

            Parallel.ForEach(locs, lbl =>
            {
                var jump = new ScriptReferrer(script, lbl);

                // Try to hook-up with definition
                var def = data.Definitions.Match(jump);
                if (def != null)
                {
                    def.AddReference(jump);

                    // Associate location with RPY file if available.
                    var rpy = script.Partner;
                    if (rpy == null && first)
                    {
                        rpy = HookUpOriginalSource(script, lbl.OriginalSource.Source, data.ScriptFiles);
                    }
                    first = false;

                    if ((rpy != null) && (data.Language == Languages.None))
                    {
                        jump.ReplaceScript(rpy, lbl.OriginalSource.LineNumber);
                    }

                    data.AddReferrers(jump);
                }
            });
        }
    }
}