﻿using RenpyGameViewer.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Xml.Serialization;

namespace RenpyGameViewer.Logic
{
    public enum ExportType
    {
        CSV,
        JSON,
        XML,
        TXT,
    }

    public static class BookmarksHelper
    {
        public static List<ScriptBookmark> Load(string filename)
        {
            List<ScriptBookmark>? bms = null;

            using var f = File.OpenRead(filename);

            try
            {
                bms = JsonSerializer.Deserialize<List<ScriptBookmark>>(f);
            }
            catch { }

            if (bms == null || bms.Count == 0)
            {
                // Not a json file, so try CSV
                bms = [];
                f.Seek(0, SeekOrigin.Begin);
                using var tf = new StreamReader(f);

                var line = tf.ReadLine();
                while (line != null)
                {
                    var items = ParseItems(line);
                    // Skip invalid line or the header like: "File","Line","Text","Comment","Protected"
                    if (items.Count == 5
                        && string.Compare(items[0], "file", true) != 0
                        && string.Compare(items[1], "line", true) != 0)
                    {
                        // Also skip lines with invalid line numbers
                        if (int.TryParse(items[1], out var lineNo))
                        {
                            // Get the other fields
                            var file = items[0];
                            var text = items[2];
                            var comment = string.IsNullOrWhiteSpace(items[3]) ? null : items[3];
                            // Is protected when text matches with 'yes' or text is not 'no' and there are comments
                            var isProt = string.Compare(items[4], "yes", true) == 0
                                || (string.Compare(items[4], "no", true) != 0 && comment != null);

                            var bm = new ScriptBookmark(lineNo, text, file)
                            {
                                Text = UnEscape(text),
                                Comment = UnEscape(comment),
                                IsProtected = isProt
                            };
                            bms.Add(bm);
                        }
                    }
                    line = tf.ReadLine();
                }
            }
            return bms;
        }

        private static List<string> ParseItems(string originalline)
        {
            var line = originalline.AsSpan().Trim();
            int pos = 0;
            var items = new List<string>();
            while (pos < line.Length)
            {
                var start = pos;
                int endpos;
                if (line[pos] == '"')
                {
                    var escaped = false;
                    pos++;
                    start++; // skip '"'
                    while (pos < line.Length && (escaped || line[pos] != '"'))
                    {
                        escaped = !escaped && line[pos] == '\\';
                        pos++;
                    }
                    endpos = pos; // without '"'
                    pos++;
                }
                else
                {
                    while (pos < line.Length && line[pos] != ',') pos++;
                    endpos = pos;
                }
                // pos should point to ',' or end of line
                if (pos == line.Length || line[pos] == ',')
                {
                    items.Add(line[start..endpos].ToString());
                }
                else
                {
                    // Incorrect formated line
                    return [];
                }
                pos++;
            }
            return items;
        }

        public static void Save(string filename, ExportType type, IEnumerable<ScriptBookmark> bms)
        {
            var dir = Path.GetDirectoryName(filename);
            if (dir != null)
            {
                Directory.CreateDirectory(dir);
            }
            switch (type)
            {
                case ExportType.JSON:
                    ExportJson(filename, bms);
                    break;
                case ExportType.CSV:
                    ExportCSV(filename, bms);
                    break;
                case ExportType.XML:
                    ExportXML(filename, bms);
                    break;
                case ExportType.TXT:
                    ExportTxt(filename, bms);
                    break;
                default:
                    break;
            }
        }

        private static void ExportXML(string filename, IEnumerable<ScriptBookmark> bms)
        {
            using var f = File.Create(filename);
            var list = bms.ToList();
            var s = new XmlSerializer(list.GetType());
            s.Serialize(f, list);
        }

        private static void ExportCSV(string filename, IEnumerable<ScriptBookmark> bms)
        {
            using StreamWriter tf = File.CreateText(filename);
            tf.WriteLine("\"File\",\"Line\",\"Text\",\"Comment\",\"Protected\"");
            foreach (var bm in bms)
            {
                string[] items = [
                    bm.ScriptFile,
                    bm.LineNumber.ToString(),
                    EscapeText(bm.Text),
                    EscapeText(bm.Comment),
                    bm.IsProtected ? "Yes" : "No"
                ];
                tf.WriteLine(string.Join(",", items.Select(i => $"\"{i}\"")));
            }
        }

        private static void ExportJson(string filename, IEnumerable<ScriptBookmark> bms)
        {
            using var f = File.Create(filename);
            JsonHelper.Serialize(f, bms);
        }

        private static void ExportTxt(string filename, IEnumerable<ScriptBookmark> bms)
        {
            using StreamWriter tf = File.CreateText(filename);
            var first = true;
            foreach (var bm in bms)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    tf.WriteLine("======");
                    tf.WriteLine();
                }
                tf.WriteLine($"{bm.ScriptFile} ({bm.LineNumber}): {bm.Text}");
                tf.WriteLine();
                tf.WriteLine(bm.Comment);
                tf.WriteLine();
            }
        }

        private static string EscapeText(string? v)
        {
            if (string.IsNullOrWhiteSpace(v))
            {
                return string.Empty;
            }
            StringBuilder et = new();
            foreach (var c in v)
            {
                switch (c)
                {
                    case '\n':
                        et.Append("\\n");
                        break;
                    case '\r':
                        et.Append("\\r");
                        break;
                    case '\t':
                        et.Append("\\t");
                        break;
                    case '\\':
                        et.Append("\\\\");
                        break;
                    case '"':
                        et.Append("\\\"");
                        break;
                    default:
                        // Skip any other control character
                        if (c >= ' ')
                        {
                            et.Append(c);
                        }
                        break;
                }
            }
            return et.ToString();
        }

        [return: NotNullIfNotNull(nameof(comment))]
        private static string? UnEscape(string? comment)
        {
            if (string.IsNullOrWhiteSpace(comment))
            {
                return comment;
            }
            var et = new StringBuilder();
            var txt = comment.AsSpan();
            int i = 0;
            while (i < txt.Length)
            {
                if (txt[i] == '\\')
                {
                    i++;
                    switch (txt[i])
                    {
                        case 'n':
                            et.Append('\n');
                            break;
                        case 'r':
                            et.Append('\r');
                            break;
                        case 't':
                            et.Append('\t');
                            break;
                        case '\\':
                            et.Append('\\');
                            break;
                        case '"':
                            et.Append('"');
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    et.Append(txt[i]);
                }
                i++;
            }
            return et.ToString();
        }

        public static (int, string?) UpdateText(GameScriptFile file, int lineNumber)
        {
            var lines = new StringReader(file.Script);
            string? line = lines.ReadLine();
            var ln = 1;
            while (ln < lineNumber && line != null)
            {
                var temp = lines.ReadLine();
                if (temp != null)
                {
                    ln++;
                    line = temp;
                }
                else
                {
                    lineNumber = ln;
                }
            }
            return (lineNumber, line);
        }

        public static (int, string?) FindTextLine(GameScriptFile file, int lineNumber, string line)
        {
            var text = file.Script;
            var lines = new LineEnum(text);
            var (lineNo, match) = BestTextMatch.MatchLines(lineNumber, line, lines);
            return (lineNo, match?.Trim());
        }

        private class LineEnum(string text) : IEnumerable<string>
        {
            public IEnumerator<string> GetEnumerator()
            {
                using var sr = new StringReader(text);
                var line = sr.ReadLine();
                while (line != null)
                {
                    yield return line;
                    line = sr.ReadLine();
                }
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}
