﻿using RenPy.Files.Script;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text.Json.Serialization;

namespace RenpyGameViewer.Logic
{
    public class GameUserOptions
    {
        private bool dirty = false;
        private string selectedLanguage = Languages.None;
        private string? iconLocation;
        private string? iconArchiveFile;
        private string name = string.Empty;
        private readonly ObservableCollection<ScriptBookmark> bookmarks = [];

        public GameUserOptions()
        {
            bookmarks.CollectionChanged += (s, e) =>
            {
                IsDirty = true;
                if (e.NewItems != null)
                {
                    foreach (var i in e.NewItems.Cast<ScriptBookmark>())
                        i.PropertyChanged += ScriptBookmark_PropertyChanged;
                }
                if (e.OldItems != null)
                {
                    foreach (var i in e.OldItems.Cast<ScriptBookmark>())
                        i.PropertyChanged += ScriptBookmark_PropertyChanged;
                }
            };
        }

        private void ScriptBookmark_PropertyChanged(object? sender, PropertyChangedEventArgs e) => IsDirty = true;

        public string SelectedLanguage
        {
            get => selectedLanguage;
            set => Update(ref selectedLanguage, value);
        }

        public string? IconLocation
        {
            get => iconLocation;
            set => Update(ref iconLocation, value);
        }

        public string? IconArchiveFile
        {
            get => iconArchiveFile;
            set => Update(ref iconArchiveFile, value);
        }

        public string Name
        {
            get => name;
            set => Update(ref name, value);
        }

        public ObservableCollection<ScriptBookmark> Bookmarks
        {
            get => bookmarks;
            set
            {
                bookmarks.Clear();
                foreach (var bm in value)
                {
                    bookmarks.Add(bm);
                }
            }
        }

        private void Update<T>(ref T field, T newValue)
        {
            if (!EqualityComparer<T>.Default.Equals(field, newValue))
            {
                field = newValue;
                IsDirty = true;
            }
        }

        public void Saved() => IsDirty = false;

        [JsonIgnore]
        public bool IsDirty
        {
            get => dirty;
            private set
            {
                dirty = value;
                if (dirty)
                {
                    Dirty?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler? Dirty;
    }
}
