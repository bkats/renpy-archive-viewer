﻿using RenPy.Files.Archive;
using RenpyGameViewer.View;
using RenpyGameViewer.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;

namespace RenpyGameViewer.Logic
{
    internal static class GameLoader
    {
        private static readonly string optionsFilename = ".rgvoptions.txt";

        public static GameInfo? OpenGame(ApplicationOptions options)
        {
            var gsvm = new GameSelectorViewModel(options.QuickFolders);
            if (options.SortName)
            {
                gsvm.SortName = options.SortAscending;
            }
            else
            {
                gsvm.SortTimeStamp = options.SortAscending;
            }
            var gsv = new GameSelectorView(gsvm)
            {
                Owner = Application.Current.MainWindow,
            };
            if (!string.IsNullOrEmpty(options.LastOpenedGame))
            {
                gsvm.InitialGame = options.LastOpenedGame;
            }
            if (gsv.ShowDialog() == true && gsvm.SelectedGame != null)
            {
                return new GameInfo(gsvm.SelectedGame.RootPath, gsvm.SelectedGame.Options);
            }
            return null;
        }

        public static GameUserOptions? LoadOptions(string rootDir)
        {
            GameUserOptions? userOptions = null;
            var optionFile = Path.Combine(rootDir, optionsFilename);
            if (File.Exists(optionFile))
            {
                using var rd = File.OpenRead(optionFile);
                userOptions = JsonSerializer.Deserialize<GameUserOptions>(rd);
                userOptions?.Saved();
            }
            return userOptions;
        }

        public static void SaveOptions(GameInfo info)
        {
            SaveOptions(info.RootPath, info.Options);
        }

        public static void SaveOptions(string path, GameUserOptions options)
        {
            if (options.IsDirty)
            {
                var optionFile = Path.Combine(path, optionsFilename);
                try
                {
                    using var wr = new FileStream(optionFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
                    wr.SetLength(0);
                    JsonHelper.Serialize(wr, options);
                    File.SetAttributes(optionFile, FileAttributes.Hidden);
                }
                catch
                {
                }
                options.Saved();
            }
        }

        static public async Task LoadFolder(GameInfo gameInfo)
        {
            if (gameInfo == null)
                throw new ArgumentException("Game object isn't created correctly", nameof(gameInfo));

            await Task.Run(() =>
            {
                GetFolderContent(gameInfo, gameInfo.GamePath);
                HookupSourceWithCompiled(gameInfo.ScriptFiles);
            });
        }

        private static void HookupSourceWithCompiled(IEnumerable<GameScriptFile> scriptFiles)
        {
            foreach (var source in scriptFiles.Where(x => x.Type == FileType.Script))
            {
                var cname = source.FullName + "c";
                var com = scriptFiles.FirstOrDefault(x => x.Type == FileType.Compiled
                    && x.FullName.Equals(cname, StringComparison.InvariantCultureIgnoreCase));
                if (com != null)
                {
                    if (com.Partner != null && com.Partner != source) throw new Exception("Already assigned to a RPYC file");
                    source.Partner = com;
                    com.Partner = source;
                }
            }
        }

        private static void GetFolderContent(GameInfo data, string path)
        {
            var subs = Directory.GetDirectories(path);
            Parallel.ForEach(subs, sub => GetFolderContent(data, Path.Combine(path, sub)));

            var files = Directory.GetFiles(path, "*.rp*");
            Parallel.ForEach(files, file =>
            {
                var ext = Path.GetExtension(file).ToLower();
                switch (ext)
                {
                    case ".rpa":
                        LoadFilesFromRPA(data, file);
                        break;
                    case ".rpy":
                    case ".rpym":
                        var textFile = new GameTextScript(new FysicalFile(data.GamePath, file), data.Languages);
                        data.AddScriptFile(textFile);
                        break;
                    case ".rpyc":
                    case ".rpymc":
                        var compiledFile = new GameCompiledScript(new FysicalFile(data.GamePath, file), data.Languages);
                        data.AddScriptFile(compiledFile);
                        break;
                    default:
                        break;
                }
            });
        }

        private static void LoadFilesFromRPA(GameInfo data, string rpaFile)
        {
            // Archive is only used until all files are loaded by GameScriptFile creation
            using var a = new RenPyArchiveFile(rpaFile);
            var files = a.GetFiles();
            Parallel.ForEach(files, file =>
            {
                var ext = Path.GetExtension(file.filename).ToLower();
                switch (ext)
                {
                    case ".rpy":
                    case ".rpym":
                        var textFile = new GameTextScript(new ArchiveFile(a, file.filename, file.size), data.Languages)
                        {
                            OriginalRPAFile = rpaFile
                        };
                        data.AddScriptFile(textFile);
                        break;
                    case ".rpyc":
                    case ".rpymc":
                        var compiledFile = new GameCompiledScript(new ArchiveFile(a, file.filename, file.size), data.Languages)
                        {
                            OriginalRPAFile = rpaFile
                        };
                        data.AddScriptFile(compiledFile);
                        break;
                    default:
                        break;
                }
            });
        }
    }
}
