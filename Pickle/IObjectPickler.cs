/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

// ReSharper disable InconsistentNaming

namespace Pickle
{

    /// <summary>
    /// Interface for object Picklers used by the pickler, to pickle custom classes. 
    /// </summary>
    public interface IObjectPickler
    {
        /**
         * Pickle an object.
         */
        void Pickle(object o, Stream outs, Pickler currentPickler);
    }

}
