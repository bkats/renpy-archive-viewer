/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using System.Collections;
using System.Text;
// ReSharper disable UnusedParameter.Local
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Pickle
{
    /// <summary>
    /// Exception thrown that represents a certain Python exception.
    /// </summary>
    public class PythonException : Exception
    {
        public string PyroTraceback { get; set; } = string.Empty;
        public string PythonExceptionType { get; set; } = string.Empty;

        public PythonException(string message) : base(message)
        {
        }

        // special constructor for UnicodeDecodeError
        // ReSharper disable once UnusedMember.Global
#pragma warning disable IDE0060 // Remove unused parameter
        public PythonException(string encoding, byte[] data, int i1, int i2, string message)
#pragma warning restore IDE0060 // Remove unused parameter
            : base("UnicodeDecodeError: " + encoding + ": " + message)
        {
        }

        /// <summary>
        /// for the unpickler to restore state
        /// </summary>
        // ReSharper disable once UnusedMember.Global
#pragma warning disable IDE1006 // Naming Styles
        public void SetState(Hashtable values)
#pragma warning restore IDE1006 // Naming Styles
        {
            if (!values.ContainsKey("_pyroTraceback"))
                return;
            object? tb = values["_pyroTraceback"];
            // if the traceback is a list of strings, create one string from it
            if (tb is ICollection tbcoll)
            {
                var sb = new StringBuilder();
                foreach (object line in tbcoll)
                {
                    sb.Append(line);
                }
                PyroTraceback = sb.ToString();
            }
            else
            {
                PyroTraceback = tb?.ToString() ?? string.Empty;
            }
            //Console.WriteLine("pythonexception state set to:{0}",_pyroTraceback);
        }

    }
}

