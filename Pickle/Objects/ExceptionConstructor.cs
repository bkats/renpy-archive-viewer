/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using System.Reflection;

namespace Pickle.Objects
{

    /// <summary>
    /// This creates Python Exception instances. 
    /// It keeps track of the original Python exception type name as well.
    /// </summary>
    public class ExceptionConstructor : IObjectConstructor
    {

        private readonly string _pythonExceptionType;
        private readonly Type _type;

        public ExceptionConstructor(Type type, string module, string name)
        {
            if (!string.IsNullOrEmpty(module))
                _pythonExceptionType = module + "." + name;
            else
                _pythonExceptionType = name;
            _type = type;
        }

        public object Construct(object?[]? args)
        {
            try
            {
                if (!string.IsNullOrEmpty(_pythonExceptionType))
                {
                    // put the python exception type somewhere in the message
                    if (args == null || args.Length == 0)
                    {
                        args = new object[] { "[" + _pythonExceptionType + "]" };
                    }
                    else
                    {
                        var msg = $"[{_pythonExceptionType}] {args[0]}";
                        args = new object[] { msg };
                    }
                }
                object? ex = Activator.CreateInstance(_type, args);
                if (ex != null)
                {
                    PropertyInfo? prop = ex.GetType().GetProperty("PythonExceptionType");
                    prop?.SetValue(ex, _pythonExceptionType, null);
                    return ex;
                }
            }
            catch (Exception x)
            {
                throw new PickleException("problem constructing object", x);
            }
            throw new PickleException("problem constructing object");
        }
    }

}
