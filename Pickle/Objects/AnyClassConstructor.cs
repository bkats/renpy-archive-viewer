/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

namespace Pickle.Objects
{

    /// <summary>
    /// This object constructor uses reflection to create instances of any given class. 
    /// </summary>
    public class AnyClassConstructor : IObjectConstructor
    {

        private readonly Type _type;

        public AnyClassConstructor(Type type)
        {
            _type = type;
        }

        public object Construct(object?[]? args)
        {
            object? result;
            try
            {
                result = Activator.CreateInstance(_type, args);
                if (result == null)
                {
                    throw new PickleException("problem constructing object");
                }
            }
            catch (Exception x)
            {
                throw new PickleException("problem constructing object", x);
            }
            return result;
        }
    }

}
