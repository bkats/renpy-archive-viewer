/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using System.Collections;
using System.Text;

namespace Pickle.Objects
{

    /// <summary>
    /// Creates byte arrays (byte[]). 
    /// </summary>
    public class ByteArrayConstructor : IObjectConstructor
    {

        public object Construct(object?[]? args)
        {
            // args for bytearray constructor: [ String string, String encoding ]
            // args for bytearray constructor (from python3 bytes): [ ArrayList ] or just [byte[]] (when it uses BINBYTES opcode)
            // or, zero arguments: empty bytearray.
            if (args == null || args.Length > 2)
                throw new PickleException($"Invalid pickle data for bytearray; expected 0, 1 or 2 args, got {args?.Length}");

            switch (args.Length)
            {
                case 0:
                    break;
                case 1 when args[0] is byte[] r:
                    return r;
                case 1:
                    if (args[0] is ArrayList values)
                    {
                        var data = new byte[values.Count];
                        for (int i = 0; i < data.Length; ++i)
                        {
                            data[i] = Convert.ToByte(values[i]);
                        }
                        return data;
                    }
                    break;
                default:
                    if (args[0] is string str && args[1] is string encoding)
                    {
                        // This thing is fooling around with byte<>string mappings using an encoding.
                        // I think that is fishy... but for now it seems what Python itself is also doing...
                        if (encoding.StartsWith("latin-"))
                            encoding = string.Concat("ISO-8859-", encoding.AsSpan(6));
                        return Encoding.GetEncoding(encoding).GetBytes(str);
                    }
                    break;
            }
            return Array.Empty<byte>();
        }
    }

}
