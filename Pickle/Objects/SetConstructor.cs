/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using System.Collections;

namespace Pickle.Objects
{

    /// <summary>
    /// This object constructor creates sets. (HashSet&lt;object&gt;)
    /// </summary>
    public class SetConstructor : IObjectConstructor
    {

        public object Construct(object?[]? args)
        {
            // create a HashSet, args=arraylist of stuff to put in it
            if (args != null && args.Length > 0 && args[0] is ArrayList elements)
            {
                IEnumerable<object?> array = elements.ToArray();
                return new HashSet<object?>(array);
            }
            return new HashSet<object?>();
        }
    }

}
