/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */


// ReSharper disable InconsistentNaming

namespace Pickle
{

    /// <summary>
    /// Interface for deconstructed objects used by the pickler, to pickle custom classes. 
    /// </summary>
    public interface IObjectDeconstructor
    {
        /// <summary>
        /// Get the module of the class being pickled
        /// </summary>
        string GetModule();
        /// <summary>
        /// Get the name of the class being pickled
        /// </summary>
        string GetName();
        /// <summary>
        /// Get the deconstructed values, which will be used as arguments for reconstructing
        /// </summary>
        object[] Deconstruct(object obj);
    }

}
