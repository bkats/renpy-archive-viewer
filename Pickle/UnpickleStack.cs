/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using System.Collections;

namespace Pickle
{

    /// <summary>
    /// Helper type that represents the unpickler working stack. 
    /// </summary>
    public class UnpickleStack
    {
        public readonly object MARKER;
        internal const int DefaultCapacity = 4;
        private object?[] _stack;
        private int _count;

        public UnpickleStack(int capacity = DefaultCapacity)
        {
            _stack = new object[capacity];
            MARKER = new object(); // any new unique object
        }

        public void Add(object? o)
        {
            if (_count == _stack.Length)
            {
                Array.Resize(ref _stack, _count * 2);
            }

            _stack[_count++] = o;
        }

        public void AddMark()
        {
            Add(MARKER);
        }

        public object? Pop()
        {
            if (_count == 0)
            {
                throw new ArgumentOutOfRangeException("index"); // match exception type/parameter name thrown from ArrayList used previously
            }

            return _stack[--_count];
        }

        public ArrayList PopAllSinceMarker()
        {
            var result = new ArrayList();
            var o = Pop();
            while (o != MARKER)
            {
                result.Add(o);
                o = Pop();
            }
            result.TrimToSize();
            result.Reverse();
            return result;
        }

        internal object?[] PopAllSinceMarkerAsArray()
        {
            int i = _count - 1;
            while (_stack[i] != MARKER)
                i--;

            var result = new object[_count - 1 - i];
            Array.Copy(_stack, i + 1, result, 0, result.Length);

            _count = i;
            return result;
        }

        public object? Peek()
        {
            return _stack[_count - 1];
        }

        public void Trim()
        {
            if (_count < _stack.Length && _stack.Length > DefaultCapacity)
            {
                var newArr = new object[Math.Max(_count, DefaultCapacity)];
                Array.Copy(_stack, 0, newArr, 0, _count);
                _stack = newArr;
            }
        }

        public int Size()
        {
            return _count;
        }

        public void Clear()
        {
            _count = 0;
            Trim();
        }
    }

}
