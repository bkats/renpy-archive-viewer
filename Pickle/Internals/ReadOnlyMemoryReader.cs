﻿/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

namespace Pickle.Internals
{
    internal struct ReadOnlyMemoryReader : IInputReader
    {
        private readonly ReadOnlyMemory<byte> input;
        private int position;

        public ReadOnlyMemoryReader(ReadOnlyMemory<byte> bytes)
        {
            input = bytes;
            position = 0;
        }

        public byte ReadByte() => input.Span[position++];

        public ReadOnlySpan<byte> ReadBytes(int bytesCount)
        {
            var result = input.Span.Slice(position, bytesCount);
            position += bytesCount;
            return result;
        }

        public string ReadLine(bool includeLF = false) => PickleUtils.RawStringFromBytes(ReadLineBytes(includeLF));

        public ReadOnlySpan<byte> ReadLineBytes(bool includeLF = false)
        {
            var result = ReadBytes(GetLineEndIndex(includeLF));
            if (!includeLF)
                Skip(1);
            return result;
        }

        public void Skip(int bytesCount) => position += bytesCount;

        private readonly int GetLineEndIndex(bool includeLF = false)
        {
            var bytes = input.Span[position..];
            int index = bytes.IndexOf((byte)'\n');
            if (includeLF)
                index++;
            return index;
        }
    }
}