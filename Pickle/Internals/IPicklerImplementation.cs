﻿/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

namespace Pickle.Internals
{
    internal interface IPicklerImplementation : IDisposable
    {
        int BytesWritten { get; }
        byte[] Buffer { get; }

        void Dump(object o);
        void Save(object o);
    }
}