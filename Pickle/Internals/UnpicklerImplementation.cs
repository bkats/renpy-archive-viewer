﻿/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

using Pickle.Objects;
using System.Buffers.Binary;
using System.Buffers.Text;
using System.Collections;
using System.Diagnostics;
using System.Numerics;

namespace Pickle.Internals
{
    // the following type is generic in order to allow for 
    // IInputReader interface method devirtualizaiton and inlining
    // please see https://adamsitnik.com/Value-Types-vs-Reference-Types/#how-to-avoid-boxing-with-value-types-that-implement-interfaces for more
    internal class UnpicklerImplementation<T> where T : struct, IInputReader
    {
        private static readonly string[] quoteStrings = new[] { "\"", "'" };
        private static readonly object boxedFalse = false;
        private static readonly object boxedTrue = true;

        private readonly UnpickleStack stack;
        private readonly Unpickler unpickler;
        private readonly IDictionary<int, object?> memo;

#pragma warning disable IDE0044 // Add readonly modifier
        private T input; // must NOT be readonly, it's a mutable struct
#pragma warning restore IDE0044 // Add readonly modifier
        private Dictionary<PythonType, string>? concatenatedModuleNames;

        public UnpicklerImplementation(T input, IDictionary<int, object?> memo, UnpickleStack stack, Unpickler unpickler)
        {
            this.input = input;
            this.memo = memo;
            this.stack = stack;
            this.unpickler = unpickler;
        }

        public object? Load()
        {
            byte key;
            while ((key = input.ReadByte()) != Opcodes.STOP)
            {
                Dispatch(key);
            }

            var value = stack.Pop();
            stack.Clear();
            unpickler.memo.Clear();
            return value; // final result value
        }

        private void Dispatch(byte key)
        {
            switch (key)
            {
                case Opcodes.MARK:
                    LoadMark();
                    return;
                case Opcodes.POP:
                    LoadPop();
                    return;
                case Opcodes.POP_MARK:
                    LoadPopMark();
                    return;
                case Opcodes.DUP:
                    LoadDup();
                    return;
                case Opcodes.FLOAT:
                    LoadFloat();
                    return;
                case Opcodes.INT:
                    LoadInt();
                    return;
                case Opcodes.BININT:
                    LoadBinInt();
                    return;
                case Opcodes.BININT1:
                    LoadBinInt1();
                    return;
                case Opcodes.LONG:
                    LoadLong();
                    return;
                case Opcodes.BININT2:
                    LoadBinInt2();
                    return;
                case Opcodes.NONE:
                    LoadNone();
                    return;
                case Opcodes.PERSID:
                    LoadPersId();
                    return;
                case Opcodes.BINPERSID:
                    LoadBinPersId();
                    return;
                case Opcodes.REDUCE:
                    LoadReduce();
                    return;
                case Opcodes.STRING:
                    LoadString();
                    return;
                case Opcodes.BINSTRING:
                    LoadBinString();
                    return;
                case Opcodes.SHORT_BINSTRING:
                    LoadShortBinString();
                    return;
                case Opcodes.UNICODE:
                    LoadUnicode();
                    return;
                case Opcodes.BINUNICODE:
                    LoadBinUnicode();
                    return;
                case Opcodes.APPEND:
                    LoadAppend();
                    return;
                case Opcodes.BUILD:
                    LoadBuild();
                    return;
                case Opcodes.GLOBAL:
                    LoadGlobal();
                    return;
                case Opcodes.DICT:
                    LoadDict();
                    return;
                case Opcodes.EMPTY_DICT:
                    LoadEmptyDictionary();
                    return;
                case Opcodes.APPENDS:
                    LoadAppends();
                    return;
                case Opcodes.GET:
                    LoadGet();
                    return;
                case Opcodes.BINGET:
                    LoadBinGet();
                    return;
                case Opcodes.INST:
                    LoadInst();
                    return;
                case Opcodes.LONG_BINGET:
                    LoadLongBinGet();
                    return;
                case Opcodes.LIST:
                    LoadList();
                    return;
                case Opcodes.EMPTY_LIST:
                    LoadEmptyList();
                    return;
                case Opcodes.OBJ:
                    LoadObj();
                    return;
                case Opcodes.PUT:
                    LoadPut();
                    return;
                case Opcodes.BINPUT:
                    LoadBinput();
                    return;
                case Opcodes.LONG_BINPUT:
                    LoadLongBinput();
                    return;
                case Opcodes.SETITEM:
                    LoadSetItem();
                    return;
                case Opcodes.TUPLE:
                    LoadTuple();
                    return;
                case Opcodes.EMPTY_TUPLE:
                    LoadEmptyTuple();
                    return;
                case Opcodes.SETITEMS:
                    LoadSetItems();
                    return;
                case Opcodes.BINFLOAT:
                    LoadBinFloat();
                    return;

                // protocol 2
                case Opcodes.PROTO:
                    LoadProto();
                    return;
                case Opcodes.NEWOBJ:
                    LoadNewObj();
                    return;
                case Opcodes.EXT1:
                case Opcodes.EXT2:
                case Opcodes.EXT4:
                    throw new PickleException("Unimplemented opcode EXT1/EXT2/EXT4 encountered. Don't use extension codes when pickling via copyreg.add_extension() to avoid this error.");
                case Opcodes.TUPLE1:
                    LoadTuple1();
                    return;
                case Opcodes.TUPLE2:
                    LoadTuple2();
                    return;
                case Opcodes.TUPLE3:
                    LoadTuple3();
                    return;
                case Opcodes.NEWTRUE:
                    LoadTrue();
                    return;
                case Opcodes.NEWFALSE:
                    LoadFalse();
                    return;
                case Opcodes.LONG1:
                    LoadLong1();
                    return;
                case Opcodes.LONG4:
                    LoadLong4();
                    return;

                // Protocol 3 (Python 3.x)
                case Opcodes.BINBYTES:
                    LoadBinBytes();
                    return;
                case Opcodes.SHORT_BINBYTES:
                    LoadShortBinBytes();
                    return;

                // Protocol 4 (Python 3.4-3.7)
                case Opcodes.BINUNICODE8:
                    LoadBinUnicode8();
                    return;
                case Opcodes.SHORT_BINUNICODE:
                    LoadShortBinUnicode();
                    return;
                case Opcodes.BINBYTES8:
                    LoadBinBytes8();
                    return;
                case Opcodes.EMPTY_SET:
                    LoadEmptySet();
                    return;
                case Opcodes.ADDITEMS:
                    LoadAddItems();
                    return;
                case Opcodes.FROZENSET:
                    LoadFrozenSet();
                    return;
                case Opcodes.MEMOIZE:
                    LoadMemoize();
                    return;
                case Opcodes.FRAME:
                    LoadFrame();
                    return;
                case Opcodes.NEWOBJ_EX:
                    LoadNewObjEx();
                    return;
                case Opcodes.STACK_GLOBAL:
                    LoadStackGlobal();
                    return;

                // protocol 5 (Python 3.8+)
                case Opcodes.BYTEARRAY8:
                    LoadByteArray8();
                    break;
                case Opcodes.READONLY_BUFFER:
                    // Loadreadonly_buffer();
                    break;
                case Opcodes.NEXT_BUFFER:
                    LoadNextBuffer();
                    break;

                default:
                    throw new InvalidOpcodeException("invalid pickle opcode: " + key);
            }
        }


        private void LoadByteArray8()
        {
            // this is the same as LoadBinbytes8 because we make no distinction
            // here between the bytes and bytearray python types
            long len = BinaryPrimitives.ReadInt64LittleEndian(input.ReadBytes(sizeof(long)));
            stack.Add(input.ReadBytes(PickleUtils.CheckedCast(len)).ToArray());
        }

        //private void Loadreadonly_buffer()
        //{
        //    // this opcode is ignored, we don't distinguish between readonly and read/write buffers
        //}

        private void LoadNextBuffer()
        {
            stack.Add(unpickler.NextBuffer());
        }


        private void LoadBuild()
        {
            var args = stack.Pop();
            var target = stack.Peek();

            if (args == null || target == null)
            {
                throw new PickleException("Build: Unexpected null values in pickle for target or args");
            }

            object?[] arguments = { args };
            Type[] argumentTypes = { args.GetType() };

            // call the SetState method with the given arguments
            try
            {
                var setStateMethod = target.GetType().GetMethod("SetState", argumentTypes);
                if (setStateMethod == null)
                {
                    if (target is ClassDict cd)
                    {
                        // For robust loading just store the info 
                        cd.Add("__state__", args);
                    }
                    else
                    {
                        throw new PickleException($"no SetState() found in type {target.GetType()} with argument type {args.GetType()}");
                    }
                }
                else
                {
                    setStateMethod.Invoke(target, arguments);
                }
            }
            catch (Exception e)
            {
                throw new PickleException("failed to SetState()", e);
            }
        }

        private void LoadProto()
        {
            byte proto = input.ReadByte();
            if (proto > Unpickler.HIGHEST_PROTOCOL)
                throw new PickleException("unsupported pickle protocol: " + proto);
        }

        private void LoadNone()
        {
            stack.Add(null);
        }

        private void LoadFalse()
        {
            stack.Add(boxedFalse);
        }

        private void LoadTrue()
        {
            stack.Add(boxedTrue);
        }

        private void LoadInt()
        {
            ReadOnlySpan<byte> bytes = input.ReadLineBytes(includeLF: true);
            if (bytes.Length == 3 && bytes[2] == (byte)'\n' && bytes[0] == (byte)'0')
            {
                switch (bytes[1])
                {
                    case (byte)'0':
                        LoadFalse();
                        return;
                    case (byte)'1':
                        LoadTrue();
                        return;
                }
            }

            bytes = bytes[..^1];
            if (bytes.Length > 0 && Utf8Parser.TryParse(bytes, out int intNumber, out int bytesConsumed) && bytesConsumed == bytes.Length)
            {
                stack.Add(intNumber);
            }
            else if (bytes.Length > 0 && Utf8Parser.TryParse(bytes, out long longNumber, out bytesConsumed) && bytesConsumed == bytes.Length)
            {
                stack.Add(longNumber);
            }
            else
            {
                stack.Add(long.Parse(PickleUtils.RawStringFromBytes(bytes)));
                Debug.Fail("long.Parse should have thrown.");
            }
        }

        private void LoadBinInt()
        {
            int integer = BinaryPrimitives.ReadInt32LittleEndian(input.ReadBytes(sizeof(int)));
            stack.Add(integer);
        }

        private void LoadBinInt1()
        {
            stack.Add((int)input.ReadByte());
        }

        private void LoadBinInt2()
        {
            int integer = BinaryPrimitives.ReadUInt16LittleEndian(input.ReadBytes(sizeof(short)));
            stack.Add(integer);
        }

        private void LoadLong()
        {
            string val = input.ReadLine();
            if (val.EndsWith("L"))
            {
                val = val[..^1];
            }
            if (long.TryParse(val, out long longvalue))
            {
                stack.Add(longvalue);
            }
            else if (BigInteger.TryParse(val, out BigInteger bigInteger))
            {
                stack.Add(bigInteger);
            }
            else
            {
                throw new PickleException("long too large in Loadlong");
            }
        }

        private void LoadLong1()
        {
            byte n = input.ReadByte();
            stack.Add(PickleUtils.DecodeLong(input.ReadBytes(n)));
        }

        private void LoadLong4()
        {
            int n = BinaryPrimitives.ReadInt32LittleEndian(input.ReadBytes(sizeof(int)));
            stack.Add(PickleUtils.DecodeLong(input.ReadBytes(n)));
        }

        private void LoadFloat()
        {
            ReadOnlySpan<byte> bytes = input.ReadLineBytes(includeLF: true);
            if (!Utf8Parser.TryParse(bytes, out double d, out int bytesConsumed) || !PickleUtils.IsWhitespace(bytes[bytesConsumed..]))
            {
                throw new FormatException();
            }
            stack.Add(d);
        }

        private void LoadBinFloat()
        {
            double val = BitConverter.Int64BitsToDouble(BinaryPrimitives.ReadInt64BigEndian(input.ReadBytes(sizeof(long))));
            stack.Add(val);
        }

        private void LoadString()
        {
            string rep = input.ReadLine();
            bool quotesOk = false;
            foreach (string q in quoteStrings) // double or single quote
            {
                if (rep.StartsWith(q))
                {
                    if (!rep.EndsWith(q))
                    {
                        throw new PickleException("insecure string pickle");
                    }
                    rep = rep[1..^1]; // strip quotes
                    quotesOk = true;
                    break;
                }
            }

            if (!quotesOk)
                throw new PickleException("insecure string pickle");

            stack.Add(PickleUtils.DecodeEscaped(rep));
        }

        private void LoadBinString()
        {
            int len = BinaryPrimitives.ReadInt32LittleEndian(input.ReadBytes(sizeof(int)));
            stack.Add(PickleUtils.RawStringFromBytes(input.ReadBytes(len)));
        }

        private void LoadBinBytes()
        {
            int len = BinaryPrimitives.ReadInt32LittleEndian(input.ReadBytes(sizeof(int)));
            stack.Add(input.ReadBytes(len).ToArray());
        }

        private void LoadBinBytes8()
        {
            long len = BinaryPrimitives.ReadInt64LittleEndian(input.ReadBytes(sizeof(long)));
            stack.Add(input.ReadBytes(PickleUtils.CheckedCast(len)).ToArray());
        }

        private void LoadUnicode()
        {
            string str = PickleUtils.DecodeUnicodeEscaped(input.ReadLine());
            stack.Add(str);
        }

        private void LoadBinUnicode()
        {
            int len = BinaryPrimitives.ReadInt32LittleEndian(input.ReadBytes(sizeof(int)));
            var data = input.ReadBytes(len);
            stack.Add(PickleUtils.GetStringFromUtf8(data));
        }

        private unsafe void LoadBinUnicode8()
        {
            long len = BinaryPrimitives.ReadInt64LittleEndian(input.ReadBytes(sizeof(long)));
            stack.Add(PickleUtils.GetStringFromUtf8(input.ReadBytes(PickleUtils.CheckedCast(len))));
        }

        private void LoadShortBinUnicode()
        {
            int len = input.ReadByte();
            stack.Add(PickleUtils.GetStringFromUtf8(input.ReadBytes(len)));
        }

        private void LoadShortBinString()
        {
            byte len = input.ReadByte();
            stack.Add(PickleUtils.RawStringFromBytes(input.ReadBytes(len)));
        }

        private void LoadShortBinBytes()
        {
            byte len = input.ReadByte();
            stack.Add(input.ReadBytes(len).ToArray());
        }

        private void LoadTuple()
        {
            stack.Add(stack.PopAllSinceMarkerAsArray());
        }

        private void LoadEmptyTuple()
        {
            stack.Add(Array.Empty<object>());
        }

        private void LoadTuple1()
        {
            stack.Add(new[] { stack.Pop() });
        }

        private void LoadTuple2()
        {
            object? o2 = stack.Pop();
            object? o1 = stack.Pop();
            stack.Add(new[] { o1, o2 });
        }

        private void LoadTuple3()
        {
            object? o3 = stack.Pop();
            object? o2 = stack.Pop();
            object? o1 = stack.Pop();
            stack.Add(new[] { o1, o2, o3 });
        }

        private void LoadEmptyList()
        {
            stack.Add(new ArrayList(5));
        }

        private void LoadEmptyDictionary()
        {
            stack.Add(new Hashtable(5));
        }

        private void LoadEmptySet()
        {
            stack.Add(new HashSet<object>());
        }

        private void LoadList()
        {
            ArrayList top = stack.PopAllSinceMarker();
            stack.Add(top); // simply add the top items as a list to the stack again
        }

        private void LoadDict()
        {
            object?[] top = stack.PopAllSinceMarkerAsArray();
            var map = new Hashtable(top.Length);
            for (int i = 0; i < top.Length; i += 2)
            {
                object? key = top[i] ?? throw new PickleException("Dictonary Key can't be null");
                object? value = top[i + 1];
                map[key] = value;
            }
            stack.Add(map);
        }

        private void LoadFrozenSet()
        {
            var top = stack.PopAllSinceMarkerAsArray();
            var set = new HashSet<object?>();
            foreach (var element in top)
                set.Add(element);
            stack.Add(set);
        }

        private void LoadAddItems()
        {
            var top = stack.PopAllSinceMarkerAsArray();
            var set = (HashSet<object?>?)stack.Pop();
            if (top == null || set == null) throw new PickleException("unexpected null");
            foreach (var item in top)
                set.Add(item);
            stack.Add(set);
        }

        private void LoadGlobal()
        {
            string module = PickleUtils.GetStringFromUtf8(input.ReadLineBytes());
            string name = PickleUtils.GetStringFromUtf8(input.ReadLineBytes());

            LoadGlobalSub(module, name);
        }

        private void LoadStackGlobal()
        {
            string name = (string?)stack.Pop() ?? string.Empty;
            string module = (string?)stack.Pop() ?? string.Empty;
            LoadGlobalSub(module, name);
        }

        private void LoadGlobalSub(string module, string name)
        {
            stack.Add(new PythonType(module, name));
        }

        private void LoadPop()
        {
            stack.Pop();
        }

        private void LoadPopMark()
        {
            object? o;
            do
            {
                o = stack.Pop();
            } while (o != stack.MARKER);
            stack.Trim();
        }

        private void LoadDup()
        {
            stack.Add(stack.Peek());
        }

        private void LoadGet()
        {
            int i = int.Parse(input.ReadLine());
            if (!memo.TryGetValue(i, out var value)) throw new PickleException("invalid memo key");
            stack.Add(value);
        }

        private void LoadBinGet()
        {
            byte i = input.ReadByte();
            if (!memo.TryGetValue(i, out var value)) throw new PickleException("invalid memo key");
            stack.Add(value);
        }

        private void LoadLongBinGet()
        {
            int i = BinaryPrimitives.ReadInt32LittleEndian(input.ReadBytes(sizeof(int)));
            if (!memo.TryGetValue(i, out var value)) throw new PickleException("invalid memo key");
            stack.Add(value);
        }

        private void LoadPut()
        {
            int i = int.Parse(input.ReadLine());
            memo[i] = stack.Peek();
        }

        private void LoadBinput()
        {
            byte i = input.ReadByte();
            memo[i] = stack.Peek();
        }

        private void LoadMemoize()
        {
            memo[memo.Count] = stack.Peek();
        }

        private void LoadLongBinput()
        {
            int i = BinaryPrimitives.ReadInt32LittleEndian(input.ReadBytes(sizeof(int)));
            memo[i] = stack.Peek();
        }

        private void LoadAppend()
        {
            var value = stack.Pop();
            var list = (ArrayList?)stack.Peek();
            list?.Add(value);
        }

        private void LoadAppends()
        {
            object?[] top = stack.PopAllSinceMarkerAsArray();
            var list = (ArrayList?)stack.Peek();
            if (list != null)
            {
                foreach (var t in top)
                {
                    list.Add(t);
                }
            }
        }

        private void LoadSetItem()
        {
            var value = stack.Pop();
            var key = stack.Pop();
            var dict = (Hashtable?)stack.Peek();
            if (dict != null && key != null)
            {
                dict[key] = value;
            }
        }

        private void LoadSetItems()
        {
            var newitems = new List<KeyValuePair<object, object?>>();
            var value = stack.Pop();
            while (value != stack.MARKER)
            {
                var key = stack.Pop() ?? throw new PickleException("Unexpected null");
                newitems.Add(new KeyValuePair<object, object?>(key, value));
                value = stack.Pop();
            }

            var dict = (Hashtable?)stack.Peek();
            if (dict != null)
            {
                foreach (var item in newitems)
                {
                    dict[item.Key] = item.Value;
                }
            }
        }

        private void LoadMark()
        {
            stack.AddMark();
        }

        private void LoadReduce()
        {
            var args = (object?[]?)stack.Pop();
            if (stack.Pop() is PythonType pt)
            {
                var constructor = GetObjectConstructor(pt);
                stack.Add(constructor.Construct(args));
            }
        }

        private void LoadNewObj()
        {
            LoadReduce(); // we just do the same as class(*args) instead of class.__new__(class,*args)
        }

        private void LoadNewObjEx()
        {
            Hashtable? kwargs = (Hashtable?)stack.Pop();
            var args = (object?[]?)stack.Pop();
            if (stack.Pop() is PythonType pt)
            {
                var constructor = GetObjectConstructor(pt);
                if (kwargs == null || kwargs.Count == 0)
                    stack.Add(constructor.Construct(args));
                else
                    throw new PickleException("newobj_ex with keyword arguments not supported");
            }
        }

        private void LoadFrame()
        {
            // for now we simply skip the frame opcode and its length
            input.Skip(sizeof(long));
        }

        private void LoadPersId()
        {
            // the persistent id is taken from the argument
            string pid = input.ReadLine();
            stack.Add(unpickler.PersistentLoad(pid));
        }

        private void LoadBinPersId()
        {
            // the persistent id is taken from the stack
            var pid = stack.Pop();
            stack.Add(unpickler.PersistentLoad(pid));
        }

        private void LoadObj()
        {
            var popped = stack.PopAllSinceMarkerAsArray();

            object[] args;
            if (popped.Length > 1)
            {
                args = new object[popped.Length - 1];
                Array.Copy(popped, 1, args, 0, args.Length);
            }
            else
            {
                args = Array.Empty<object>();
            }

            if (popped[0] is PythonType pt)
                stack.Add(GetObjectConstructor(pt).Construct(args));
        }

        private void LoadInst()
        {
            string module = PickleUtils.GetStringFromUtf8(input.ReadLineBytes());
            string classname = PickleUtils.GetStringFromUtf8(input.ReadLineBytes());

            var args = stack.PopAllSinceMarkerAsArray();
            var pt = new PythonType(module, classname);
            if (!Unpickler.objectConstructors.TryGetValue(GetModuleNameKey(pt), out IObjectConstructor? constructor))
            {
                constructor = new ClassDictConstructor(module, classname);
            }
            stack.Add(constructor.Construct(args));
        }

        private string GetModuleNameKey(PythonType sp)
        {
            concatenatedModuleNames ??= new Dictionary<PythonType, string>();

            if (!concatenatedModuleNames.TryGetValue(sp, out string? key))
            {
                key = sp.Module + "." + sp.Name;
                concatenatedModuleNames.Add(sp, key);
            }

            return key;
        }

        private IObjectConstructor GetObjectConstructor(PythonType sp)
        {
            if (Unpickler.objectConstructors.TryGetValue(GetModuleNameKey(sp), out IObjectConstructor? constructor))
            {
                return constructor;
            }
            var module = sp.Module;
            var name = sp.Name;
            switch (module)
            {
                // check if it is an exception
                case "exceptions":
                    // python 2.x
                    return new ExceptionConstructor(typeof(PythonException), module, name);
                case "builtins":
                case "__builtin__":
                    if (name.EndsWith("Error") || name.EndsWith("Warning") || name.EndsWith("Exception")
                    || name == "GeneratorExit" || name == "KeyboardInterrupt"
                        || name == "StopIteration" || name == "SystemExit")
                    {
                        // it's a python 3.x exception
                        return new ExceptionConstructor(typeof(PythonException), module, name);
                    }
                    else
                    {
                        // return a dictionary with the class's properties
                        return new ClassDictConstructor(module, name);
                    }
                default:
                    return new ClassDictConstructor(module, name);
            }
        }

        private readonly struct PythonType : IEquatable<PythonType>, IPythonType
        {
            public string Module { get; }
            public string Name { get; }
            public PythonType(string module, string name) { Module = module; Name = name; }
            public bool Equals(PythonType other) => Module == other.Module && Name == other.Name;
            public override bool Equals(object? obj) => obj is PythonType sp && Equals(sp);
            public override int GetHashCode() => Module.GetHashCode() ^ Name.GetHashCode();
        }
    }
}