﻿/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

namespace Pickle
{
    public interface IPythonType
    {
        public string Module { get; }
        public string Name { get; }
        public string FullName => $"{Module}.{Name}";
    }
}