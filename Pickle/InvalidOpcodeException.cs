/* part of Pickle, by Irmen de Jong (irmen@razorvine.net) */

namespace Pickle
{
	/// <summary>
	/// Exception thrown when the unpickler encountered an unknown or unimplemented opcode.
	/// </summary>
	public class InvalidOpcodeException : PickleException
	{
		public InvalidOpcodeException(string message) : base(message)
		{
		}
	}
}
